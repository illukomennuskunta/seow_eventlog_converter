'Scorched Earth Dynamic Campaign System, Copyright 2002-2017
'Contributors:
'       Wallace L. "RAF74_Wall-dog" Garneau
'       IV/JG7_4Shades



'This module contains all the functions required to import, analyze and store 
'data and events from a hosted mission. The module supports two modes: the first
'is an "initialization" mode used for loading a campaign template, the second
'is an "analysis" mode used for updating events from a mission as part of
'an existing campaign mission sequence.

Imports SEDCS
Imports System.IO
Imports Microsoft.Win32
Imports Microsoft.VisualBasic
Imports SEDCS.modUtilities

Module modMissionAnalyzer

    'Private frmSEMB As New frmSEMB
    'Private frmSECalendar As New frmSECalendar

    'Here is a simple data structure to represent parsed military units in Scorched Earth.
    Public Structure SE_Unit
        Dim Fuel As Integer              'the fuel load of the unit, in litres.
        Dim Fuel_Capacity As Integer     'the total amount of fuel able to be carried by the unit, in litres.
        Dim Fuel_Freight As Integer      'the total amount of fuel transported as freight by the unit, in litres.
        Dim Morale As Integer            'morale, ranging from 0 to 4 inclusive.
        Dim Nationality As String        'nationality, e.g. "r" or "g".
        Dim Object_Group_Name As String  'a more cryptic (internal) name.
        Dim Object_Type As String        'type of unit - see Object_Specifications table.
        Dim Verbose_Type As String       'verbose type of unit - see Object_Specifications table.
        Dim Object_Class As String       'class/role of unit - see Object_Specifications table.
        Dim Formation As String          'entrenchment formation.
        Dim OnlyAI As Integer            'flag to determine whether only AI can fly this unit.
        Dim Parent As String             'a literal name at one step up the naming hierarchy.
        Dim Loadout As String            'internal loadout directive.
        Dim Transporting_Group As String 'a reference to the Object_Group_Name of a unit carrying this unit.
        Dim Quantity As Integer          'number of members in unit.
        Dim Recon As Integer             'the recon level of this unit, 0 <= Recon <= 100.
        Dim Refueling As Integer         'the number of hours before the unit is refueled again.
        Dim RefuelingTime As Integer     'the number of hours the unit requires to refuel.
        Dim Penalty As Integer           'the number of missions before the unit is reassembled.
        Dim Penalty0 As Integer          'the penalty incurred by airframe 0 in this flight.
        Dim Penalty0Log As String        'the penalty string for airframe 0 in this flight.
        Dim Penalty1 As Integer          'the penalty incurred by airframe 1 in this flight.
        Dim Penalty1Log As String        'the penalty string for airframe 1 in this flight.
        Dim Penalty2 As Integer          'the penalty incurred by airframe 2 in this flight.
        Dim Penalty2Log As String        'the penalty string for airframe 2 in this flight.
        Dim Penalty3 As Integer          'the penalty incurred by airframe 3 in this flight.
        Dim Penalty3Log As String        'the penalty string for airframe 3 in this flight.
        Dim Skill As Integer             'skill of the entire unit.
        Dim Skill0 As Integer            'skill of unit member of rank 0.
        Dim Skill1 As Integer            'skill of unit member of rank 1.
        Dim Skill2 As Integer            'skill of unit member of rank 2.
        Dim Skill3 As Integer            'skill of unit member of rank 3.
        Dim CC As Integer                'command/control value for this object type.
        Dim Crew As Integer              'default number of crew personnel for this object type.
        Dim Skin As String               'paintscheme of unit member.
        Dim Unit_Name As String          'a literal (human readable) name.
        Dim X As Double                  'the x coordinate of the location of the unit.
        Dim Y As Double                  'the y coordinate of the location of the unit.
        Dim Z As Double                  'the z coordinate (orientation) of the location of the unit.
        Dim TargetX As Double            'the x coordinate of the barrage target location of the unit.
        Dim TargetY As Double            'the y coordinate of the barrage target location of the unit.
        Dim ShipDamage As Double         'the current damage state of the ship: 0 means undamaged, 1 means destroyed.
        Dim Hull1Damage As Double        'the current damage state of the Hull1 chunk.
        Dim Hull2Damage As Double        'the current damage state of the Hull2 chunk.
        Dim Hull3Damage As Double        'the current damage state of the Hull3 chunk.
        Dim GunDamage As Double          'the current estimated damage state of the ship gun complement.
        Dim isDestroyed As Boolean       'has this ship received a "destroyed" event in this mission?
        Dim Speed As Double              'Speed value for this object in Object_Specifications (m/hr).
        Dim Range As Double              'Range of this unit in Object_Specifications (km).
        Dim BarrageDelay As Double       'Time delay for this unit to commence barraging at start of mission (minutes).
        Dim BarrageSalvoes As Integer    'Number of salvoes this unit will fire in the current barrage.
        Dim BarragePeriod As Double      'Time between successive barrage salvoes from this unit (minutes).
        Dim BarrageDispersion As Integer 'Dispersion radius for fall of shot from this unit (metres).
        Dim BarrageHitsMorale As Integer 'Number of hits to cause morale check for fall of shot from this unit.
        Dim RallyX As ArrayList          'Array of x coordinates of marked rally points.
        Dim RallyY As ArrayList          'Array of y coordinates of marked rally points.
        Dim RallyT As ArrayList          'Array of time coordinates of marked rally points.
        Dim Scrambled_X As Double        'X landing coordinate for scrambled flight.
        Dim Scrambled_Y As Double        'Y landing coordinate for scrambled flight.
        Dim Scrambled_T As Integer       'Elapsed time (seconds) that flight was scrambled.
        Dim Signals As String            'Encoded mask for signalling.
        Dim ActivatedBy As String        'Group name of signaller unit this unit will be activated by.
        Dim ActivationTime As Integer    'Time of activation in mission (in seconds).
        Dim FlightStatus As String       'Flight status, ok/aborted/cancelled/overrun.
    End Structure

    Public Structure SE_Transporter
        Dim Name As String               'the group name of the transporter
        Dim X As Double                  'the x coordinate of the location of the transporter
        Dim Y As Double                  'the y coordinate of the location of the transporter
        Dim Z As Double                  'the z coordinate of the location of the transporter
        Dim Capacity As Integer          'the number of objects able to be carried by the transporter
    End Structure


    Public Structure SE_CombinedUnit
        Dim Group As String             'the group name of the combined unit
        Dim Type As String              'the object type of the combined unit
        Dim CUClass As String           'either "Vehicle" or "Train"
        Dim Nation As String            'the nation alignment of the combined unit
        Dim FuelCapacity As Integer     'the total capacity in litres of the internal fuel store
        Dim CurrentFuel As Integer      'the current holding of the internal fuel store
        Dim CurrentFuelFreight As Integer      'the current holding of the freight fuel store
        Dim MaxStrength As Integer      'the total number of member vehicles/wagons in this combined unit type
        Dim CurrentStrength As Integer  'the current number of undestroyed members in this combined unit
        Dim Member1 As String           'type or status of this member of the combined unit
        Dim Member2 As String           'type or status of this member of the combined unit
        Dim Member3 As String           'type or status of this member of the combined unit
        Dim Member4 As String           'type or status of this member of the combined unit
        Dim Member5 As String           'type or status of this member of the combined unit
        Dim Member6 As String           'type or status of this member of the combined unit
        Dim Member7 As String           'type or status of this member of the combined unit
        Dim Member8 As String           'type or status of this member of the combined unit
        Dim Member9 As String           'type or status of this member of the combined unit
        Dim Member10 As String          'type or status of this member of the combined unit
        Dim Member11 As String          'type or status of this member of the combined unit
        Dim Member12 As String          'type or status of this member of the combined unit
        Dim Member13 As String          'type or status of this member of the combined unit
        Dim Member14 As String          'type or status of this member of the combined unit
        Dim Member15 As String          'type or status of this member of the combined unit
        Dim Member16 As String          'type or status of this member of the combined unit
        Dim Member17 As String          'type or status of this member of the combined unit
        Dim Member18 As String          'type or status of this member of the combined unit
        Dim Member19 As String          'type or status of this member of the combined unit
        Dim Member20 As String          'type or status of this member of the combined unit
        Dim Member21 As String          'type or status of this member of the combined unit
        Dim Member22 As String          'type or status of this member of the combined unit
        Dim Member23 As String          'type or status of this member of the combined unit
        Dim Member24 As String          'type or status of this member of the combined unit
        Dim Member25 As String          'type or status of this member of the combined unit
        Dim Member26 As String          'type or status of this member of the combined unit
        Dim Member27 As String          'type or status of this member of the combined unit
        Dim Member28 As String          'type or status of this member of the combined unit
        Dim Member29 As String          'type or status of this member of the combined unit
        Dim Member30 As String          'type or status of this member of the combined unit
        Dim Member31 As String          'type or status of this member of the combined unit
        Dim Member32 As String          'type or status of this member of the combined unit
        Dim UNM1 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM2 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM3 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM4 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM5 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM6 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM7 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM8 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM9 As String              'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM10 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM11 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM12 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM13 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM14 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM15 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM16 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM17 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM18 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM19 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM20 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM21 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM22 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM23 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM24 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM25 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM26 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM27 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM28 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM29 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM30 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM31 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
        Dim UNM32 As String             'In-Game descriptor for this member in Unit_Name_Mapping table.
    End Structure


    Public Structure SE_MetaUnit
        Dim Group As String                 'the parent group name of the meta unit
        Dim Member As Integer               'the member number of the meta unit within the parent group
        Dim IL2_Type As String              'the IL2 object type of the meta unit (each IL2 object expands to a meta-game group)
        Dim Meta_Type As String             'the meta-game group type of the meta unit
        Dim Meta_Verbose As String          'the meta-game verbose name of the meta unit
        Dim Nation As String                'the nation alignment of the combined unit
        Dim FuelCapacity As Integer         'the total capacity in litres of the internal fuel store
        Dim CurrentFuel As Integer          'the current holding of the internal fuel store
        Dim CurrentFuelFreight As Integer   'the current holding of the freight fuel store
        Dim MaxStrength As Integer          'the total number of crew in this meta unit type
        Dim CurrentStrength As Integer      'the current number of undestroyed crew in this meta unit
        Dim Crew1 As String                 'type of this crew of the meta unit
        Dim Crew2 As String                 'type of this crew of the meta unit
        Dim Crew3 As String                 'type of this crew of the meta unit
        Dim Crew4 As String                 'type of this crew of the meta unit
        Dim Crew5 As String                 'type of this crew of the meta unit
        Dim Crew6 As String                 'type of this crew of the meta unit
        Dim Crew7 As String                 'type of this crew of the meta unit
        Dim Crew8 As String                 'type of this crew of the meta unit
        Dim Crew9 As String                 'type of this crew of the meta unit
        Dim Crew10 As String                'type of this crew of the meta unit
        Dim Crew11 As String                'type of this crew of the meta unit
        Dim Crew12 As String                'type of this crew of the meta unit
        Dim Crew13 As String                'type of this crew of the meta unit
        Dim Crew14 As String                'type of this crew of the meta unit
        Dim Crew15 As String                'type of this crew of the meta unit
        Dim Crew16 As String                'type of this crew of the meta unit
        Dim Crew17 As String                'type of this crew of the meta unit
        Dim Crew18 As String                'type of this crew of the meta unit
        Dim Crew19 As String                'type of this crew of the meta unit
        Dim Crew20 As String                'type of this crew of the meta unit
        Dim Crew21 As String                'type of this crew of the meta unit
        Dim Crew22 As String                'type of this crew of the meta unit
        Dim Crew23 As String                'type of this crew of the meta unit
        Dim Crew24 As String                'type of this crew of the meta unit
        Dim Crew25 As String                'type of this crew of the meta unit
        Dim Crew26 As String                'type of this crew of the meta unit
        Dim Crew27 As String                'type of this crew of the meta unit
        Dim Crew28 As String                'type of this crew of the meta unit
        Dim Crew29 As String                'type of this crew of the meta unit
        Dim Crew30 As String                'type of this crew of the meta unit
        Dim Crew31 As String                'type of this crew of the meta unit
        Dim Crew32 As String                'type of this crew of the meta unit
        Dim Role1 As String                 'role of this crew of the meta unit
        Dim Role2 As String                 'role of this crew of the meta unit
        Dim Role3 As String                 'role of this crew of the meta unit
        Dim Role4 As String                 'role of this crew of the meta unit
        Dim Role5 As String                 'role of this crew of the meta unit
        Dim Role6 As String                 'role of this crew of the meta unit
        Dim Role7 As String                 'role of this crew of the meta unit
        Dim Role8 As String                 'role of this crew of the meta unit
        Dim Role9 As String                 'role of this crew of the meta unit
        Dim Role10 As String                'role of this crew of the meta unit
        Dim Role11 As String                'role of this crew of the meta unit
        Dim Role12 As String                'role of this crew of the meta unit
        Dim Role13 As String                'role of this crew of the meta unit
        Dim Role14 As String                'role of this crew of the meta unit
        Dim Role15 As String                'role of this crew of the meta unit
        Dim Role16 As String                'role of this crew of the meta unit
        Dim Role17 As String                'role of this crew of the meta unit
        Dim Role18 As String                'role of this crew of the meta unit
        Dim Role19 As String                'role of this crew of the meta unit
        Dim Role20 As String                'role of this crew of the meta unit
        Dim Role21 As String                'role of this crew of the meta unit
        Dim Role22 As String                'role of this crew of the meta unit
        Dim Role23 As String                'role of this crew of the meta unit
        Dim Role24 As String                'role of this crew of the meta unit
        Dim Role25 As String                'role of this crew of the meta unit
        Dim Role26 As String                'role of this crew of the meta unit
        Dim Role27 As String                'role of this crew of the meta unit
        Dim Role28 As String                'role of this crew of the meta unit
        Dim Role29 As String                'role of this crew of the meta unit
        Dim Role30 As String                'role of this crew of the meta unit
        Dim Role31 As String                'role of this crew of the meta unit
        Dim Role32 As String                'role of this crew of the meta unit
        Dim Rank1 As String                 'rank of this crew of the meta unit
        Dim Rank2 As String                 'rank of this crew of the meta unit
        Dim Rank3 As String                 'rank of this crew of the meta unit
        Dim Rank4 As String                 'rank of this crew of the meta unit
        Dim Rank5 As String                 'rank of this crew of the meta unit
        Dim Rank6 As String                 'rank of this crew of the meta unit
        Dim Rank7 As String                 'rank of this crew of the meta unit
        Dim Rank8 As String                 'rank of this crew of the meta unit
        Dim Rank9 As String                 'rank of this crew of the meta unit
        Dim Rank10 As String                'rank of this crew of the meta unit
        Dim Rank11 As String                'rank of this crew of the meta unit
        Dim Rank12 As String                'rank of this crew of the meta unit
        Dim Rank13 As String                'rank of this crew of the meta unit
        Dim Rank14 As String                'rank of this crew of the meta unit
        Dim Rank15 As String                'rank of this crew of the meta unit
        Dim Rank16 As String                'rank of this crew of the meta unit
        Dim Rank17 As String                'rank of this crew of the meta unit
        Dim Rank18 As String                'rank of this crew of the meta unit
        Dim Rank19 As String                'rank of this crew of the meta unit
        Dim Rank20 As String                'rank of this crew of the meta unit
        Dim Rank21 As String                'rank of this crew of the meta unit
        Dim Rank22 As String                'rank of this crew of the meta unit
        Dim Rank23 As String                'rank of this crew of the meta unit
        Dim Rank24 As String                'rank of this crew of the meta unit
        Dim Rank25 As String                'rank of this crew of the meta unit
        Dim Rank26 As String                'rank of this crew of the meta unit
        Dim Rank27 As String                'rank of this crew of the meta unit
        Dim Rank28 As String                'rank of this crew of the meta unit
        Dim Rank29 As String                'rank of this crew of the meta unit
        Dim Rank30 As String                'rank of this crew of the meta unit
        Dim Rank31 As String                'rank of this crew of the meta unit
        Dim Rank32 As String                'rank of this crew of the meta unit
        Dim UNM1 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM2 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM3 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM4 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM5 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM6 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM7 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM8 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM9 As String                  'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM10 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM11 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM12 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM13 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM14 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM15 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM16 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM17 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM18 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM19 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM20 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM21 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM22 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM23 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM24 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM25 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM26 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM27 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM28 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM29 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM30 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM31 As String                 'In-Game descriptor for this crew in Meta_UNM table.
        Dim UNM32 As String                 'In-Game descriptor for this crew in Meta_UNM table.
    End Structure

    Public Structure SE_MetaMap
        Dim Sector As String                'Sector in which this metamap is defined.
        Dim Title As String                 'Title of metamap.
        Dim Scale As Integer                'Scale of metamap (in metres).
        Dim Centroid_X As Integer           'X location of metamap centroid (in IL2 coords, meters)
        Dim Centroid_Y As Integer           'Y location of metamap centroid (in IL2 coords, meters)
        Dim Mode As String                  'static (houses) or dynamic (no houses)
        Dim Processed As Boolean            'Has the metamap been processed (terrain compiled) already.
        Dim Active As Boolean               'Is the metamap active in the campaign?
        Dim RequestedBy As String           'Name of the commander that requested this metamap.
        Dim Alignment As String             'Alignment of the commander that requested this metamap.
        Dim Sequence As String              'Campaign sequence when this metamap was requested.
        Dim Target As String                'Target game for this metamap.
    End Structure

    Public Structure SE_Ship_Damage_Events
        Dim Group_Name As String                'Group name of this ship unit.
        Dim Ship_Type As String                 'Type of ship, e.g. "USSCasablancaCVE55".
        Dim Ship_Class As String                'Class descriptor of ship, e.g. "SBA".
        Dim ChunkEvents As String               'Delimited string of chunk event records (ordered chronologically).
        Dim LastAntagonist As String            'Name of the last unit to score a hit on this ship.
        Dim LastAntagonistGroup As String       'Group name of the last unit to score a hit on this ship.
        Dim LastHitX As Integer                 'X location of last hit on this active ship.
        Dim LastHitY As Integer                 'Y location of last hit on this active ship.
        Dim isSpotted As Boolean                'Whether an enemy unit has damaged this ship in the latest mission.
    End Structure

    Public Structure SE_Installation
        Dim Name As String                      'Name of industrial installation.
        Dim Type As String                      'Type of installation, e.g. fuel tank, factory etc.
        Dim Damage_Old As Double                'Prior damage level of installation; 0 = undamaged, 100 = completely destroyed.
        Dim Damage_New As Double                'Damage level of installation; 0 = undamaged, 100 = completely destroyed.
        Dim Holding As String                   'Fuel holding or factory production type.
        Dim Production_Points As Integer        'Bank of points in this installation.
        Dim Antagonist As String                'Object that caused the latest damage event.
        Dim AntagonistGroup As String           'Group of object that caused the latest damage event.
        Dim X As Integer                        'X location of last substantive hit.
        Dim Y As Integer                        'Y location of last substantive hit.
        Dim Clock As String                     'Event time of last substantive hit.
        Dim isDestroyed As Boolean              'Has this installation had a recorded destruction event during this mission?
    End Structure

    Public Structure SE_MaxSpeedProfile
        Dim Name As String                      'Name of mobile unit.
        Dim TypeCode As String                  'Type code of mobile unit object.
        Dim strElapsedTime As String            'Delimited string of elapsed times at which maximum speed was changed.
        Dim strMaxSpeed As String               'Delimited string of maximum speeds imposed on the unit by damage events.
        Dim strX As String                      'Delimited string of X coordinates of the damage events.
        Dim strY As String                      'Delimited string of Y coordinates of the damage events.
    End Structure


    Public Structure Army_Division
        Dim Name As String                      'Name of Army "Division"
        Dim Type As String                      'Type of Division, artillery, infantry, engineer, armour
        Dim Country As String                   'Country
        Dim Alignment As String                 'Allied or Axis
        Dim CountryID As String                 'IL-2 code for country
        Dim Max_Platoons As Integer             'Maximum number of 3rd level units
        Dim Icon_Set As String                  'name of icon set for MP display
        Dim Organizational_Level As String      'Head level of this "Division", e.g. Front, Army, Corps, Division, Brigade, Regiment, Battalion, Company
        Dim Sector As String                    'Sector this division is mapped to
    End Structure


    Public Structure Army_Organizational_Structure
        Dim Country As String                   'Country
        Dim Front_Long As String                'Long name for "Front"
        Dim Front_Short As String               'Short name for "Front"
        Dim Army_Long As String                 'Long name for "Army"
        Dim Army_Short As String                'Short name for "Army"
        Dim Corps_Long As String                'Long name for "Corps"
        Dim Corps_Short As String               'Short name for "Corps"
        Dim Division_Long As String             'Long name for "Division"
        Dim Division_Short As String            'Short name for "Division"
        Dim Brigade_Long As String              'Long name for "Brigade"
        Dim Brigade_Short As String             'Short name for "Brigade"
        Dim Regiment_Long As String             'Long name for "Regiment"
        Dim Regiment_Short As String            'Short name for "Regiment"
        Dim Battalion_Long As String            'Long name for "Battalion"
        Dim Battalion_Short As String           'Short name for "Battalion"
        Dim Company_Long As String              'Long name for "Company"
        Dim Company_Short As String             'Short name for "Company"
        Dim Platoon_Long As String              'Long name for "Platoon"
        Dim Platoon_Short As String             'Short name for "Platoon"
        Dim Squad_Long As String                'Long name for "Squad"
        Dim Squad_Short As String               'Short name for "Squad"
        Dim Team_Long As String                 'Long name for "Team"
        Dim Team_Short As String                'Short name for "Team"
    End Structure



    Public Structure Air_Regiment
        Dim Name As String              'Internal IL2 code for this air regiment
        Dim Verbose As String           'Verbose name, may have special characters and quotes etc
        Dim Alignment As String         'Either Allied or Axis
        Dim Country As String           'Country of origin
        Dim Country_ID As String        'Country id for placement as Stationary
        Dim Role As String              'Unit class, e.g. PFI or PDB
    End Structure

    Public Structure Air_Regiment_Structure
        Dim Country As String           'Country of origin
        Dim Country_ID As String        'Country id for placement as Stationary
        Dim Squadron_Short As String    'Short form of squadron designator, e.g. 'Esk.'
        Dim Flight_Short As String      'Short form of flight designator, e.g. 'Zv.'
    End Structure

    Public Structure Pilot_Record
        Dim Pilot_Name As String        'Pilot name
        Dim Pilot_Aircraft As String    'Flight name and aircraft number for the aircraft
        Dim inControl As Boolean        'Is this pilot actually in control of the aircraft at spawn (seat 0)?
        Dim Pilot_Seat As String        'Seat number in the aircraft
        Dim Raw_Seat As String          'Raw In_Game seat, for use in Driveable Vehicle analysis
        Dim Pilot_Status As String      'The pilot's status at mission end
        Dim EventTime As String         'The mission time stamp for this disposition status
        Dim FBScore As Integer          'Score reported by FB+PF
        Dim E_Air As Integer
        Dim E_StaticAir As Integer
        Dim E_Tank As Integer
        Dim E_Car As Integer
        Dim E_Artillery As Integer
        Dim E_AAA As Integer
        Dim E_Wagon As Integer
        Dim E_Ship As Integer
        Dim F_Air As Integer
        Dim F_StaticAir As Integer
        Dim F_Tank As Integer
        Dim F_Car As Integer
        Dim F_Artillery As Integer
        Dim F_AAA As Integer
        Dim F_Wagon As Integer
        Dim F_Ship As Integer
        Dim Bullets As Integer
        Dim BulletsHit As Integer
        Dim BulletsHitAir As Integer
        Dim Rockets As Integer
        Dim RocketsHit As Integer
        Dim Bombs As Integer
        Dim BombsHit As Integer
    End Structure

    Public Structure SE_AirframeCapture
        Dim Airframe_Name As String
        Dim isDriveable As Boolean
        Dim X As Double
        Dim Y As Double
        Dim Time As String
        Dim PilotSeat As String
    End Structure

    Public Structure AICrewCapture
        Dim CrewMember As String
        Dim Flight As String
        Dim EventTime As String
        Dim X As String
        Dim Y As String
    End Structure

    Public Structure AICrewDisposition
        Dim Air_Regiment As String
        Dim Airframe_Index As String
        Dim Airframe_Type As String
        Dim Airframe_Class As String
        Dim Crew_Index As Integer
        Dim Skill As Integer
        Dim Alignment As String
        Dim Disposition As String
        Dim EventTime As String
        Dim X As Integer
        Dim Y As Integer
    End Structure

    Public Structure SE_ACStarter
        Dim Name As String
        Dim Alignment As String
        Dim Fuel As Integer
        Dim Type As String
        Dim X As Integer
        Dim Y As Integer
    End Structure

    'We will load the SE_Unit structures into a hashtable for easy addressing.
    Public hashUnit As New Hashtable

    'We will keep track of metamaps.
    Public hashMetaMaps As New Hashtable

    'We will keep track of active barrages.
    Public hashBarrageUnit As New Hashtable
    Public hashRocketPar As New Hashtable

    'We will keep track of damage events for each ship via a hashtable.
    Dim hashShipEvents As New Hashtable
    Dim hashShipPriorState As New Hashtable
    Public hashShipSpeedProfile As New Hashtable

    'We will keep track of damage events recorded against industrial installations.
    Public hashInstallation As New Hashtable

    'Keep track of driveable units in action.
    Dim hashDriveable As New Hashtable

    'Keep track of all AC_Starter units on the map.
    Dim hashACStarter As New Hashtable

    'We will keep track of where each pilot is sitting and what happens to them
    'using the following hashtable.
    Public hashPilotRecord As New Hashtable
    Public hashAICrewDisposition As New Hashtable

    'We will keep track of all transporting units.
    Dim hashTransporter As New Hashtable

    'We will load and reference active air regiments and national naming structures.
    Public hashAirRegiment As New Hashtable
    Public hashAirRegimentStruct As New Hashtable

    'We will load and reference national naming structures for army units.
    Public hashArmyUnitStruct As New Hashtable

    'We will also keep track of all sea/ground units by a default naming convention.
    Public intNext_SE_Unit_Number As Integer = 0

    'Keep track of all Control Points encoded in the template
    Public intControlPoints As Integer = 0

    'Keep track of all recon flights actually piloted by humans.
    Public strAllReconFlights As String = ""
    Public strConfirmedHumanReconFlights As String = ""
    Public strVanquishedUnits As String = ""

    'Keep track of all units actually triggered in the mission.
    Public strMissionActuallyScrambled As String = ""
    Public strMissionDesignatedScramblers As String = ""

    'Keep track of all supply flights actually piloted by humans.
    Public strAllSupplyDropFlights As String = ""
    Public strConfirmedHumanSupplyDropFlights As String = ""
    Public hashSupplyDropTime As New Hashtable

    'Keep track of all paratrooper assault flights actually piloted by humans.
    Public strAllParatrooperAssaultFlights As String = ""
    Public strConfirmedHumanParatrooperAssaultFlights As String = ""
    Public hashParatroooperAssaultTime As New Hashtable

    'Keep track of all airframes destroyed or captured.
    Public hashCaptureEvents As New Hashtable
    Public strAirframesLost As String = ""

    Public dtMissionStartDate As Date
    Public intMissionDuration As Integer
    Public strMissionCommencement As String
    Public strPreviousMissionFileName As String
    Public strThisMap As String
    Public strPreviousMap As String
    'Public frmSEMB As New frmSEMB
    'Public frmStatusMA As New frmStatusBar
    Public srLogMA As StreamReader
    Public srMissionMA As StreamReader
    Public srLogCE As StreamReader
    Dim strHostPilot As String
    Dim BeginDate As String
    Public BeginTime As String
    Dim EndTime As String
    Public strEndOfMission As String
    Public strLastMissionSequenceNumber As String
    Public strDestroyedBridges As String
    Public strDiscoKills As String
    Public strDestroyedUnlandedAircraft As String

    'Define variables for the details of each side's army, navy and rail system.
    Public strDominantAllied As String
    Public strDominantAxis As String
    Public strRollingStopTrains As String

    Public intMaxDivisions As Integer = 250
    Public strAlliedNavyName As String
    Public strAlliedRailwayName As String
    'Public AlliedArmy() As Army_Division
    Public AlliedArmy As Array = Array.CreateInstance(GetType(Army_Division), intMaxDivisions)
    Public strAlliedFlotillas As String
    Public strAlliedPlatoons As String
    Public strAlliedTrains As String

    Public strAxisNavyName As String
    Public strAxisRailwayName As String
    'Public AxisArmy() As Army_Division
    Public AxisArmy As Array = Array.CreateInstance(GetType(Army_Division), intMaxDivisions)
    Public strAxisFlotillas As String
    Public strAxisPlatoons As String
    Public strAxisTrains As String


    Public Sub GetAnalyzedMission(ByVal item As Integer, ByRef strAnalyzedMissionName As String)
        'This subroutine determines the name of the previous Analyzed/Initialized mission.

        Dim rsLastMission As New ADODB.Recordset
        Dim strSql As String
        Dim dblTime As Double
        Dim dtMissionDate As Date
        Try
            strSql = "SELECT * FROM MissionData ORDER BY Mis_Nbr DESC"
            Call modDB.openDBRecordset(item, rsLastMission, strSql)
            If rsLastMission.EOF = False Then
                strAnalyzedMissionName = rsLastMission("Mission_Name").Value

                'Convert the date and time into a sequence number for use in subUpdateCampaignStatus
                'and/or in basMissionBuilder.subResolveLastMission()
                dblTime = rsLastMission("Mis_Time").Value
                dtMissionDate = CDate(rsLastMission("Mis_Date").Value)
                strLastMissionSequenceNumber = DT2SN(DateAdd(DateInterval.Hour, dblTime, dtMissionDate))

            Else
                strAnalyzedMissionName = "no mission"
                strLastMissionSequenceNumber = ""
            End If
            rsLastMission.Close()

        Catch ex As Exception
            Call basMess.MessageBox("frmAdvanceClock.subGetAnalyzedMission: " _
                & Err.Number & " " & Err.Description, vbCritical)

        End Try
    End Sub


    'This is the first routine called in this module - called by frmParser.vb
    'This routine opens the mission file and branches to either initialization or
    'analysis mode, according to the presence of a unit<->name mapping for the theatre.
    Public Sub subAnalyzeMission(ByRef frm As Form, ByVal item As Integer, ByRef isSuccessful As Boolean)
        Dim rsCheckDB As New ADODB.Recordset
        Dim strSql As String
        Dim start_time As DateTime
        Dim stop_time As DateTime
        Dim elapsed_time As TimeSpan
        Dim strLogExtension As String
        Dim strLogFile As String
        Dim isValid As Boolean

        Try
            '-------------------
            'Set the completion status.
            isSuccessful = False
            start_time = Now
            basMain.intProgressBarCount = 0
            modDB.intSQLCounter = 0

            '-------------------
            'Open the database connection
            Call modDB.OpenDBConnection(item)

            '-------------------
            'Tell the user what is about to happen.
            Call basMess.ProgressBarShow()
            If CS_Fog_Of_War = 1 Then
                Call basMess.ProgressBarInit(1, 25)
            Else
                Call basMess.ProgressBarInit(1, 32)
            End If
            Call basMess.ProgressBarTitle("SE DCS Mission Analysis")
            Call basMess.ProgressBarLabel("Opening Files ...")

            '-------------------
            If CS_MetaMode = META_IL2_ONLY Or CS_MetaMode = META_IL2_IF44 Then

                'Open the log file as named in the user's conf.ini file and set the last mission file name.
                If (basFiles.OpenLogFile(item) > 0) Then
                    Call basMess.StatusLabelTxt("Log file " & basFiles.strFileName & " not found.", True)
                    Call basMess.MessageBox("No valid mission has been recorded in the log file '" _
                                & basFiles.strFileName & "'. You must run a coop mission " _
                                & "in FB/AEP before you can analyze its log and mission data here in the DCS.", vbInformation)
                    'Exit Sub
                    GoTo modMissionAnalyzerEnd
                End If

            ElseIf CS_MetaMode = META_IF44_ONLY Then

                'No log file used - set default file name, commencement and duration.
                strSql = "SELECT * FROM Sector_Status WHERE Campaign_Sector='" & modCfg.User(item).strLoadedSector & "' ORDER BY Campaign_Counter DESC;"
                Call modDB.openDBRecordset(item, rsCheckDB, strSql)
                If rsCheckDB.EOF = False Then
                    basFiles.StrMissionName = rsCheckDB("Filename").Value
                    basFiles.StrMissionFileName = modCfg.User(item).txtMissionFolder & "/" & rsCheckDB("Filename").Value
                    strMissionCommencement = rsCheckDB("Campaign_Sequence").Value
                    strMissionCommencement = Right(strMissionCommencement, 4)
                    strMissionCommencement = Left(strMissionCommencement, 2)
                End If
                rsCheckDB.Close()

            End If

            '-------------------
            'Open the mission file.
            If (basFiles.OpenMissionFile(item) > 0) Then
                Call basMess.StatusLabelTxt("Mission """ & basFiles.StrMissionFileName & """ not found.", True)
                Call basMess.MessageBox("Mission file """ & basFiles.StrMissionFileName & """ not found - check " _
                            & "the Mission Folder location under the File : Options menu.", vbInformation)
                Call basFiles.CloseLogFile(item)
                'Exit Sub
                GoTo modMissionAnalyzerEnd
            End If

            '-------------------
            'Check that the mission filename is the same as the name stored in Sector_Status.
            If (basFiles.CheckMissionFilename(item) > 0) Then
                Call basMess.StatusLabelTxt("Mission filename """ & basFiles.StrMissionName & """ does not match!", True)
                Call basMess.MessageBox("Mission filename """ & basFiles.StrMissionName & """ found in the logfile does not match " _
                            & "the filename generated during the last Build.", vbInformation)
                Call basFiles.CloseLogFile(item)
                'Exit Sub
                GoTo modMissionAnalyzerEnd
            End If

            '-------------------
            'Now we are ready to get to work on analyzing the mission. Set the unit number counter
            'to zero, initialize the PilotDisposition and Platoon buffers and commence the analysis.
            intNext_SE_Unit_Number = 0
            hashPilotRecord.Clear()
            hashMetaMaps.Clear()
            strAlliedPlatoons = ""
            strAxisPlatoons = ""
            strRollingStopTrains = ""
            strEndpointMessages = ""
            strMissionActuallyScrambled = "|"
            strMissionDesignatedScramblers = strLoadDesignatedScramblers(item)


            '-------------------
            Call subDoMissionAnalysis(frm, item, isSuccessful)

            '-------------------
            'We can also calculate immediate groundwar updates too... Only do this here if the campaign 
            'does not have Fog of War enabled. Otherwise, perform this function at mission build time.
            If isAddForce Then
                Call modUtilities.subStoreCombinedUnits(item, basMain.strAddForceMission)
                Call modUtilities.subStoreMetaUnits(item, strLastMissionSequenceNumber, basFiles.StrMissionName)
            End If
            If (CS_Fog_Of_War = 0) And (isSuccessful = True) And Not isAddForce Then
                strLastMissionName = basFiles.StrMissionName
                strThisMissionMap = strThisMap
                Call basMess.StatusLabelTxt("Debriefing mission performance ...", False)
                Call basMess.ProgressBarLabel("Gathering force reports ...")
                Call basMess.ProgressBarPerformStep()
                frmMain.Refresh()
                Call basMissionBuilder.subResolveLastMission(item)
                Call modUtilities.subWriteStoredCampaignNotices(item, isValid)

            ElseIf (CS_Fog_Of_War = 1) And _
                   (isSuccessful = True) And _
                   modCfg.User(item).IsCampaignInitialized = False And Not isAddForce Then
                'Set airbase control flags on template load with Fog Of War on.
                strLastMissionName = basFiles.StrMissionName
                strThisMissionMap = strThisMap
                Call basMess.ProgressBarLabel("Controlling airbases ...")
                Call basMess.ProgressBarPerformStep()
                Call basMissionBuilder.subUpdateAirbaseControl(item)

            End If


            If modCfg.User(item).IsCampaignInitialized = False And isSuccessful = True And Not isAddForce Then
                '-------------------
                'Initialize fuel supplies. 
                Call basMess.StatusLabelTxt("Initializing fuel supplies ...", False)
                Call basMess.ProgressBarLabel("Initializing fuel supplies ...")
                Call basMess.ProgressBarPerformStep()
                Call modSupply.isInitializeOffMapReserves(item)
                Call modSupply.isInitializeFuelDumps(item)
                Call modSupply.isInitializeAirbaseFuelReserves(item)
            End If

            'We need to clear the hashBarrageUnit and hashRocketPar hashtables, ready for the next Analyze stage.
            hashBarrageUnit.Clear()
            hashRocketPar.Clear()

            'A final bit of housekeeping - but still very important!
            'This is added for DCS v3.3.7 based on findings of the Operation Detachment testing.
            Call modUtilities.packFreight(item)

            '-------------------
            'Now everything is complete we can simply close down forms and exit the module.
            'But it really isn't that simple - we need to update the DCS campaign status
            'strings, upload and archive files etc.
            If isAddForce Then
                frmSEMB.Text = "SEOW DCS Add Forces Complete!"
            Else
                frmSEMB.Text = "SEOW DCS Mission Analysis Complete!"
            End If
            stop_time = Now
            elapsed_time = stop_time.Subtract(start_time)
            intCycleExecutionDuration = CInt(elapsed_time.TotalSeconds)
            If modCfg.User(item).IsCampaignInitialized And (isSuccessful = True) And Not isAddForce Then
                strLogExtension = Path.GetExtension(modCfg.User(item).txtLogFileLocation)
                strLogFile = Path.GetFileName(modCfg.User(item).txtLogFileLocation)
                strLogFile = Replace(strLogFile, strLogExtension, "")
                strLogFile += "_SE-" & modCfg.User(item).TxtTheatre _
                            & "-" & strLastMissionSequenceNumber _
                            & strLogExtension
                subUpdateCampaignStatus(item, strThisMap, "Analyzed", _
                                        strLastMissionSequenceNumber, _
                                        strLogFile, _
                                        CInt(elapsed_time.TotalSeconds))
                Call basFiles.CloseLogFile(item)
                Call basFiles.CloseMissionFile()
                modCfg.User(item).strLoadedSector = strThisMap
                Call modUtilities.subUploadFile(item, modCfg.User(item).txtLogFileLocation, _
                                                strThisMap, strLastMissionSequenceNumber)
                modCfg.User(item).IsCampaignInitialized = True
                Call basMess.StatusLabelTxt("Mission analysis complete (" _
                                        & elapsed_time.TotalSeconds.ToString("#####.0") & " sec and " & modDB.intSQLCounter & " queries).", True)
                Call basMess.MessageBox("The latest campaign mission for Sector " & strThisMap _
                            & ", Theatre '" & modCfg.User(item).TxtTheatre & "' has " _
                            & "been analyzed. There are " & intInProgress(item) & " missions in progress. " _
                            & "Check the situation map in the MP.", vbInformation)
            ElseIf (isSuccessful = True) And Not isAddForce Then
                Call modUtilities.subUpdateCampaignStatus(item, strThisMap, basMain.CYCLE_INIT, _
                                        strLastMissionSequenceNumber, _
                                        Path.GetFileName(basFiles.StrAbsoluteMissionFileName), _
                                        CInt(elapsed_time.TotalSeconds))
                Call modUtilities.subUploadFile(item, basFiles.StrAbsoluteMissionFileName, _
                                                strThisMap, strLastMissionSequenceNumber)
                Call modUtilities.subUploadFile(item, Replace(basFiles.StrAbsoluteMissionFileName, ".mis", ".properties"), _
                                                strThisMap, strLastMissionSequenceNumber)
                modCfg.User(item).strLoadedSector = strThisMap
                Call basFiles.CloseLogFile(item)
                Call basFiles.CloseMissionFile()
                modCfg.User(item).IsCampaignInitialized = True
                Call basMess.MessageBox("A new campaign has been created for Sector " & strThisMap _
                           & ", Theatre '" & modCfg.User(item).TxtTheatre & "' (with " & intInProgress(item) & " missions in progress)." _
                           & " Check the situation map in the MP.", vbInformation)
                Call basMess.StatusLabelTxt("Campaign initialization complete (" _
                                        & elapsed_time.TotalSeconds.ToString("#####.0") & " sec and " & modDB.intSQLCounter & " queries).", True)
                'MainForm.Refresh()

            ElseIf (isSuccessful = True) And isAddForce Then
                modCfg.User(item).strLoadedSector = strThisMap
                Call basFiles.CloseLogFile(item)
                Call basFiles.CloseMissionFile()
                modCfg.User(item).IsCampaignInitialized = True
                Call basMess.MessageBox("New forces have been added to Sector " & strThisMap _
                           & ", Theatre '" & modCfg.User(item).TxtTheatre & "' (now with " & intInProgress(item) & " missions in progress)." _
                           & " Check the situation map in the MP.", vbInformation)
                Call basMess.StatusLabelTxt("Add Forces complete (" _
                                        & elapsed_time.TotalSeconds.ToString("#####.0") & " sec and " & modDB.intSQLCounter & " queries).", True)
                'MainForm.Refresh()

            ElseIf (isSuccessful = False) Then
                '-------------------
                'Check to see if the DB has been flushed.
                Call modDB.openDBRecordset(item, rsCheckDB, "SELECT * FROM MissionData")
                If rsCheckDB.EOF = True Then
                    If modCfg.User(item).IsCampaignInitialized Then
                        Call basMess.StatusLabelTxt("Campaign Aborted!", True)
                    Else
                        Call basMess.StatusLabelTxt("Campaign Initialization Aborted - Try Again!", True)
                    End If
                    rsCheckDB.Close()
                Else
                    rsCheckDB.Close()
                    If modCfg.User(item).IsCampaignInitialized Then
                        Call basMess.StatusLabelTxt("Mission Analysis Failed - Proceed at Own Risk!", True)
                    Else
                        Call basMess.StatusLabelTxt("Campaign Initialization Failed - Flush the Database!", True)
                        Call modUtilities.subFlushDB(item)
                    End If
                End If
                modCfg.User(item).strLoadedSector = "no sector"
                basFiles.CloseLogFile(item)
                basFiles.CloseMissionFile()
            End If
            frmSEMB.Text = "SEOW DCS Alert"
            'KATANA : frmStatusMA.Hide()

            'Clear hashUnit after all is said and done
            'Previous to v7.3 this was done in subStoreUnitHashtable
            hashUnit.Clear()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAnalyzeMission: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

        ' Normal End subroutine
modMissionAnalyzerEnd:
        Call basMess.ProgressBarValue(0)
        Call basMess.ProgressBarHide()

    End Sub

    'This routine calculates the number of "In Progress" missions.
    Private Function intInProgress(ByVal item As Integer) As Integer
        Dim strSql As String
        Dim rsIP As New ADODB.Recordset
        Dim strMission As String
        Try
            strSql = "SELECT DISTINCT Mission_Name,Unit_Group FROM " _
                    & "Waypoints WHERE Mission_Name='" & basFiles.StrMissionName & "' AND " _
                    & "Reached=0;"
            modDB.openDBRecordset(item, rsIP, strSql)
            intInProgress = rsIP.RecordCount
            rsIP.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.intInProgress: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function


    'This routine treats all the data in the log and mission files as brand new campaign information for the theatre.
    'The log and mission files are open and positioned at the start of the relevant data.
    Private Sub subDoMissionAnalysis(ByRef frm As Form, ByVal item As Integer, ByRef isValid As Boolean)
        Dim strLine, strMapLoader As String

        Try

            '-------------------
            'Now we step through the mission file and process as we go.
            Call basMess.StatusLabelTxt("Debriefing mission plans ...", False)
            strLine = Nothing
            strLine = srMissionMA.ReadLine()

            '-------------------
            'Process the environmental data from the [MAIN] section.
            Call basMess.ProgressBarLabel("Processing sector environment ...")
            Call basMess.ProgressBarPerformStep()
            If (subLoadEnvironment(frm, item, strLine) = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            'If supply tracking is on, determine the sector movement costs and store in public variables.
            If CS_Track_Fuel_Consumption = 1 Then
                strMapLoader = basMissionBuilder.getMapLoader(item, strThisMap, strLastMissionSequenceNumber)
                Call modSupply.getMovementCosts(item)
            End If

            'Load the map coordinate data.
            Call modUtilities.loadMapCoordinates(item)


            Call basMess.FormRefresh(frm)

            '-------------------
            'Carry existing Task Forces and Combined Units forward.
            Call basMess.ProgressBarLabel("Maintaining task forces and combined units ...")
            Call basMess.ProgressBarPerformStep()
            Call subMaintainTaskForces(item)
            Call modUtilities.loadCombinedUnitDefinitions(item)
            Call modUtilities.loadMetaUnitDefinitions(item)

            '-------------------
            'Load any existing units, e.g. from a previous mission, into the unit hashtable.
            Call basMess.ProgressBarLabel("Gathering forces ...")
            Call basMess.ProgressBarPerformStep()
            Call modUtilities.subLoadRocketParameters(item)
            Call basMissionBuilder.subUpdateTransitDelays(item)
            Call subLoadUnitHashtable(item, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Process any data for active aircraft units. 
            Call basMess.ProgressBarLabel("Processing active air units ...")
            Call basMess.ProgressBarPerformStep()
            Call subGatherACStarters(isValid)
            Call subProcessWings(item, strLine, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Process any data for active ground/sea units. 
            Call basMess.ProgressBarLabel("Processing active ground/sea units ... mobile details")
            Call basMess.ProgressBarPerformStep()

            'Gather the group names of all rolling stop trains in the last mission.
            Call basMissionBuilder.subLoadRollingStopTrains(item, strThisMap, basFiles.StrMissionName)

            Call subProcessChiefs(item, strLine, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Process any data for stationary ground/sea units. 
            Call basMess.ProgressBarLabel("Processing stationary ground/sea units ... parked")
            Call basMess.ProgressBarPerformStep()
            Call modScenery.subPurgeScenery(item)
            Call subProcessStationaries(item, strLine, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If
            Call subProcessRockets(item, strLine, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Process any data for parked off map units. 
            Call basMess.ProgressBarLabel("Processing off-map units ... parked")
            Call basMess.ProgressBarPerformStep()
            Call subProcessOffMapParked(item, isValid)
            If (isValid = False) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Process any data for building infrastructure.
            Call basMess.ProgressBarLabel("Processing scenery ...")
            Call basMess.ProgressBarPerformStep()
            Call modScenery.subGetBuildings(item, strLine)

            '-------------------
            'Process any data for targets.
            Call basMess.ProgressBarLabel("Processing targets ...")
            Call basMess.ProgressBarPerformStep()
            'subLoadTargets(strLine)

            '-------------------
            'Process any data for cameras.
            Call basMess.ProgressBarLabel("Processing cameras ...")
            Call basMess.ProgressBarPerformStep()
            Call modCamera.subGetCameras(item, strLine)

            '-------------------
            'Process any data for bridges.
            Call basMess.ProgressBarLabel("Processing bridges ...")
            Call basMess.ProgressBarPerformStep()
            'subLoadBridges(strLine)

            '-------------------
            'Process any data for houses.
            If modScenery.LOAD_TILE_DAMAGE_FROM_TEMPLATE Then
                Call basMess.ProgressBarLabel("Processing urban areas ...")
                Call basMess.ProgressBarPerformStep()
                modScenery.subGetDamageTiles(item, strLine)
            End If

            '-------------------
            'Process any data for front markers.
            Call basMess.ProgressBarLabel("Processing front line ...")
            Call basMess.ProgressBarPerformStep()
            'subLoadFrontMarkers(strLine)

            '-------------------
            'Process any data for Coop Mod Slot.
            If CS_Triggers >= 1 Then
                Call basMess.ProgressBarLabel("Processing Catsy Trigger directives ...")
                Call basMess.ProgressBarPerformStep()
                Call subLoadCatsyTriggers(item, strLine)
            End If

            '-------------------
            'Process any data for Coop Mod Slot.
            If CS_Coop_Mod_Slot = 1 Then
                Call basMess.ProgressBarLabel("Processing Coop Mod Slot directives ...")
                Call basMess.ProgressBarPerformStep()
                Call subLoadCoopModSlot(item, strLine)
            End If

            '-------------------
            'Process naming conventions for Transporting Units.
            Call basMess.ProgressBarLabel("Processing transportation logistics ...")
            Call basMess.ProgressBarPerformStep()
            Call subUpdateTransporters()

            '-------------------
            'Here is a branch point. For an existing campaign, go through and check out action events.
            'For a new campaign, load the campaign objectives, if any, from the template .properties file.
            If modCfg.User(item).IsCampaignInitialized And Not isAddForce Then
                '-------------------
                'Load any actions and events from the log file.
                Call basMess.StatusLabelTxt("Debriefing mission performance ...", False)
                Call basMess.ProgressBarLabel("Processing action reports ...")
                Call basMess.ProgressBarPerformStep()
                Call subLoadEventReports(item)
                If (CS_Enforce_Landings = 1) Then
                    Call subPenalizeDisconnections(item)
                End If

                '-------------------
                Call subMissionCommencement(item)

            ElseIf Not isAddForce Then
                '-------------------
                'Load campaign objectives from the template .properties file.
                Call basMess.StatusLabelTxt("Loading campaign objectives ...", False)
                Call basMess.ProgressBarLabel("Processing campaign objectives ...")
                Call basMess.ProgressBarPerformStep()
                Call subLoadCampaignObjectives(item)

                '-------------------
                'Initialize control of industrial facilities. 
                Call basMess.StatusLabelTxt("Initializing industrial facilities ...", False)
                Call basMess.ProgressBarLabel("Initializing infrastructure resources ...")
                Call basMess.ProgressBarPerformStep()
                Call subInitializeInstallations(item)
                Call subInitializeRailwayStations(item)

            End If

            '-------------------
            'Write the contents of the hashUnit hashtable, now containing all units, into the relevant database tables.
            Call basMess.StatusLabelTxt("Consolidating reports ...", False)
            Call basMess.ProgressBarLabel("Consolidating reports ...")
            Call subUpdateShipDamageStatus(item, isValid)
            Call subStoreUnitHashtable(item, isValid)
            If (isValid = False Or isAddForce) Then
                GoTo subDoMissionAnalysisEnd
            End If

            '-------------------
            'Finally, process the losses of all air units. The rationale here is that air losses
            'become known to the respective HQs immediately after a mission, rather than taking 1 hour
            'or more like ground/sea losses. Why draw the distinction? Well, I just think it will
            'be more reasonable for commanders to see their flights rebased/updated on the situation 
            'map immediately after each mission, especially if we think that airbases have reasonably
            'fixed and reliable communication infrastructure. If it is subsequently deemed that air
            'losses should be processed at the same time as other losses, then we simply need to comment
            'out the following line, and ensure that aircraft units are included in the query in the
            'strSql statement for rsUnits in basMissionBuilder.subResolveLastMission(). That is all it takes
            'to move the loss adjustment from here to there (in principle).
            'Change for DCS 3.2.4: Ship damage tracking
            'We also need to add any ship damage sustained in the current mission to the previous 
            'ship damage state, and register as sunk any ships that exceed their viable damage levels.
            'This must be done before air losses are allocated in order to handle carrier/transporter losses
            'appropriately. However in order to correctly ignore flights already destroyed during the mission,
            'we need to constract a list of all aircraft currently listed as destroyed, crashed or shot down.
            Call basMess.StatusLabelTxt("Debriefing mission performance ...", False)
            Call basMess.ProgressBarPerformStep()
            Call basMess.ProgressBarLabel("Updating order of battle (naval units) ...")
            Call subGetVanquished(item)
            Call subAllocateShipDamageToFreight(item, isValid)
            Call subScanPartialIndustrialDamage(item, isValid)
            Call basMess.ProgressBarLabel("Updating order of battle (air units) ...")
            Call subAllocateAirLosses(item, isValid)

            '-------------------
            'Update the pilot performance records based on mission actions.
            Call basMess.StatusLabelTxt("Debriefing mission performance ...", False)
            Call basMess.ProgressBarLabel("Updating pilot/crew performance ...")
            Call basMess.ProgressBarPerformStep()
            frmMain.Refresh()
            Call subStorePilotRecords(item, isValid)
            Call subStoreAICrewDisposition(item, isValid)

            '-------------------
            'Update the Mission_Recon records based on confirmed human recon flights.
            'Ditto for Supply Drops and Paratrooper Assaults.
            Call basMess.StatusLabelTxt("Debriefing mission performance ...", False)
            Call basMess.ProgressBarLabel("Updating recon, supply drop, leaflet drop and assault records ...")
            Call basMess.ProgressBarPerformStep()
            frmMain.Refresh()
            Call subConfirmReconPhotos(item)
            Call subConfirmSupplyDrops(item)
            Call subConfirmParatrooperAssaults(item)
            Call subAdjustAssaultEvents(item)

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subDoMissionAnalysis: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

subDoMissionAnalysisEnd:
        'Call basMess.ProgressBarHide()

    End Sub


    'This routine initializes control settings of all industrial installations. 
    Private Sub subInitializeInstallations(ByVal item As Integer)
        Dim strSql As String
        Dim intNationControl As Integer
        Dim rsInfs As New ADODB.Recordset
        Dim thisIndustrialSector As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim dblUnitDistance As Double
        Dim intAllied, intAxis As Integer
        Dim strInitialFuel As String
        Dim strHoldingFormat As String = "########.#"
        Dim intRecon As Integer
        Try

            'Establish the enumerator of the unit hashtable.
            thisEnumerator = hashUnit.GetEnumerator()

            'Query the Industrial_Installations table for installations.
            strSql = "SELECT * FROM Industrial_Installations WHERE " _
                    & "Map = '" & strThisMap & "'"
            Call modDB.openDBRecordset(item, rsInfs, strSql)

            Do Until rsInfs.EOF = True

                thisIndustrialSector = rsInfs("City").Value & " " & rsInfs("Sector").Value

                'Step through the entries in the unit hashtable, testing for proximity to the installation.
                intAllied = 0
                intAxis = 0
                While thisEnumerator.MoveNext()

                    'Get the unit details.
                    thisUnit = thisEnumerator.Value

                    'Calculate the distance between the current unit and the control point.
                    dblUnitDistance = dblGetDistance(CStr(thisUnit.X), CStr(thisUnit.Y), _
                                                     CStr(rsInfs("IA_X").Value), CStr(rsInfs("IA_Y").Value))

                    'If the distance is less than the supplied control radius, then increment the 
                    'appropriate national control number by one.
                    If dblUnitDistance <= CDbl(CS_Control_Radius) Then
                        Select Case UCase(thisUnit.Nationality)
                            Case "R"
                                intAllied += 1
                            Case "G"
                                intAxis += 1
                        End Select
                    End If

                End While

                'Rewind the hashtable enumerator.
                thisEnumerator.Reset()

                'We have compiled numbers of units within the control radius of the supplied control point. 
                'Now all we have to do is write out the front marker directive.
                If intAllied > 0 And intAxis = 0 Then
                    'Only Allied units nearby.
                    intNationControl = 1
                ElseIf intAllied = 0 And intAxis > 0 Then
                    'Only Axis units nearby.
                    intNationControl = 2
                Else
                    'We should only get here if a control point is in dispute, or there is no-one 
                    'nearby at all. Either way, do not put a marker here.
                    intNationControl = 0
                End If

                'Establish a random seed.
                Randomize()

                'Upon initializing a campaign, we assign random recons to the set percentage of 
                'installations. This allows commanders to get an idea of the opposing industrial strength.
                If CS_Dynamic_Reconnaissance = 1 Then
                    'Dynamic recon model - set random recons for the percentage of units set in Options form.
                    If intNationControl = 1 And Rnd() < CDbl(CS_Initial_Campaign_Intelligence_Axis) / 100.0 Then
                        intRecon = CInt(100 * Rnd())
                    ElseIf intNationControl = 2 And Rnd() < CDbl(CS_Initial_Campaign_Intelligence_Allied) / 100.0 Then
                        intRecon = CInt(100 * Rnd())
                    Else
                        intRecon = 0
                    End If
                Else
                    'Static recon model - all units are visible.
                    intRecon = 100
                End If

                'Initialize control settings for this installation.
                If strNationality(CStr(intNationControl)) <> rsInfs("Controlled").Value And _
                   strNationality(CStr(intNationControl)) <> "n" Then

                    If Left(rsInfs("Installation_Type").Value, 7) = "Factory" Then
                        strSql = "UPDATE Industrial_Installations SET " _
                               & "Controlled = '" & strNationality(CStr(intNationControl)) & "', " _
                               & "Recon = " & intRecon & ", " _
                               & "Holding = '' WHERE " _
                               & "Map = '" & strThisMap & "' AND " _
                               & "Location = '" & rsInfs("Location").Value & "'"
                    Else
                        'Placeholder for supply tracking...
                        If CS_Track_Fuel_Consumption = 1 Then
                            Select Case UCase(strNationality(CStr(intNationControl)))
                                Case "R"
                                    strInitialFuel = Format(CDbl(rsInfs("Capacity").Value) * CDbl(CS_Initial_Allied_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                                Case "G"
                                    strInitialFuel = Format(CDbl(rsInfs("Capacity").Value) * CDbl(CS_Initial_Axis_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                                Case Else
                                    strInitialFuel = Format(CDbl(rsInfs("Capacity").Value) * CDbl(CS_Initial_Neutral_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                            End Select
                            strSql = "UPDATE Industrial_Installations SET " _
                                   & "Controlled = '" & strNationality(CStr(intNationControl)) & "', " _
                                   & "Recon = " & intRecon & ", " _
                                   & "Holding = '" & strInitialFuel & "' WHERE " _
                                   & "Map = '" & strThisMap & "' AND " _
                                   & "Location = '" & modDB.strSQLquote(rsInfs("Location").Value) & "';"
                        Else
                            strSql = "UPDATE Industrial_Installations SET " _
                                   & "Controlled = '" & strNationality(CStr(intNationControl)) & "', " _
                                   & "Recon = " & intRecon & ", " _
                                   & "Holding = '" & rsInfs("Capacity").Value & "' WHERE " _
                                   & "Map = '" & strThisMap & "' AND " _
                                   & "Location = '" & modDB.strSQLquote(rsInfs("Location").Value) & "';"
                        End If
                    End If

                    'Perform the SQL update.
                    Call modDB.ExecuteDBCommand(item, strSql)

                End If

                'Move to next industrial facility.
                rsInfs.MoveNext()
            Loop

            'Close the recordset.
            rsInfs.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subInitializeInstallations: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    'This routine initializes control settings of all industrial installations. 
    Private Sub subInitializeRailwayStations(ByVal item As Integer)
        Dim strSql As String
        Dim intNationControl As Integer
        Dim rsRS As New ADODB.Recordset
        Dim thisIndustrialSector As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim dblUnitDistance As Double
        Dim intAllied, intAxis As Integer
        Dim strInitialFuel As String
        Dim strHoldingFormat As String = "#######0.#"
        Dim intRecon As Integer
        Try

            'Establish the enumerator of the unit hashtable.
            thisEnumerator = hashUnit.GetEnumerator()

            'Query the Industrial_Installations table for installations.
            strSql = "SELECT * FROM Railway_Stations WHERE " _
                    & "Map = '" & strThisMap & "'"
            Call modDB.openDBRecordset(item, rsRS, strSql)

            Do Until rsRS.EOF = True

                'Step through the entries in the unit hashtable, 
                'testing for proximity to the railway station.
                intAllied = 0
                intAxis = 0
                While thisEnumerator.MoveNext()

                    'Get the unit details.
                    thisUnit = thisEnumerator.Value

                    'Calculate the distance between the current unit and the railway station.
                    dblUnitDistance = dblGetDistance(CStr(thisUnit.X), _
                                                     CStr(thisUnit.Y), _
                                                     CStr(rsRS("Target_X").Value), _
                                                     CStr(rsRS("Target_Y").Value))

                    'If the distance is less than the supplied control radius, then increment the 
                    'appropriate national control number by one.
                    If dblUnitDistance <= CDbl(CS_Control_Radius) Then
                        Select Case UCase(thisUnit.Nationality)
                            Case "R"
                                intAllied += 1
                            Case "G"
                                intAxis += 1
                        End Select
                    End If

                End While

                'Rewind the hashtable enumerator.
                thisEnumerator.Reset()

                'We have compiled numbers of units within the control radius of the supplied railway station. 
                'Now all we have to do is write out the front marker directive.
                If intAllied > 0 And intAxis = 0 Then
                    'Only Allied units nearby.
                    intNationControl = 1
                ElseIf intAllied = 0 And intAxis > 0 Then
                    'Only Axis units nearby.
                    intNationControl = 2
                Else
                    'We should only get here if a railway station is in dispute, or there is no-one 
                    'nearby at all. Either way, do not put a marker here.
                    intNationControl = 0
                End If

                'Initialize control settings for this installation.
                'Take the supply setting into account.
                If CS_Track_Fuel_Consumption = 1 Then
                    Select Case UCase(strNationality(CStr(intNationControl)))
                        Case "R"
                            strInitialFuel = Format(CDbl(rsRS("Supply_Capacity").Value) * CDbl(CS_Initial_Allied_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                        Case "G"
                            strInitialFuel = Format(CDbl(rsRS("Supply_Capacity").Value) * CDbl(CS_Initial_Axis_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                        Case Else
                            strInitialFuel = Format(CDbl(rsRS("Supply_Capacity").Value) * CDbl(CS_Initial_Neutral_Infrastructure_Fuel_Load) / 1000.0, strHoldingFormat)
                    End Select
                    strSql = "UPDATE Railway_Stations SET " _
                           & "Control = '" & strNationality(CStr(intNationControl)) & "', " _
                           & "Supply_Holding = '" & strInitialFuel & "' WHERE " _
                           & "Map = '" & strThisMap & "' AND " _
                           & "Station_Name = '" & modDB.strSQLquote(rsRS("Station_Name").Value) & "';"
                Else
                    strSql = "UPDATE Railway_Stations SET " _
                           & "Control = '" & strNationality(CStr(intNationControl)) & "', " _
                           & "Supply_Holding = " & rsRS("Supply_Capacity").Value & " WHERE " _
                           & "Map = '" & strThisMap & "' AND " _
                           & "Station_Name = '" & modDB.strSQLquote(rsRS("Station_Name").Value) & "';"
                End If

                'Perform the SQL update.
                Call modDB.ExecuteDBCommand(item, strSql)


                'Move to next railway station.
                rsRS.MoveNext()
            Loop

            'Close the recordset.
            rsRS.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subInitializeRailwayStations: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub





    'This subroutine records a "Crashed" Disposition event for all aircraft listed in the
    'strDiscoKills string, as constructed by subAddDisconnectionEvent().
    Private Sub subPenalizeDisconnections(ByVal item As Integer)
        Dim strDiscoList() As String
        Dim strLineArray() As String
        Dim intDisco As Integer
        Dim strDiscoDate As String
        Dim strDiscoAircraft As String
        Dim strSql As String
        Dim strX, strY As String
        Dim rsDisco As New ADODB.Recordset
        Dim strLine As String

        Try
            'Parse strDiscoList into separate disconnection events.
            strDiscoList = Split(strDiscoKills, "^")

            'Loop through the disconnection events.
            intDisco = 1
            While intDisco <= strDiscoList.GetUpperBound(0)

                'Split up each disconnection event into aircraft name and event time.
                strLineArray = Split(strDiscoList(intDisco), "|")
                strDiscoAircraft = strLineArray(0)
                strDiscoDate = strLineArray(1)

                'Create query to locate base coords of this aircraft.
                strSql = "SELECT Start_X_Axis, Start_Y_Axis FROM ObjMissionData, Unit_Name_Mapping " _
                        & "WHERE Unit_Name_Mapping.Mission = '" & basFiles.StrMissionName & "' AND " _
                        & "ObjMissionData.Mission_Name='" & strPreviousMissionFileName & "' AND " _
                        & "Unit_Name_Mapping.InGame_Name = '" & Left(strDiscoAircraft, Len(strDiscoAircraft) - 1) & "' AND " _
                        & "Unit_Name_Mapping.Unit_Name = ObjMissionData.Obj_Group_Name;"
                Call modDB.openDBRecordset(item, rsDisco, strSql)

                'Find the base coords of the aircraft's parent flight.
                If rsDisco.EOF = True Then
                    'This clause should never be entered.
                    strX = "0.00"
                    strY = "0.00"
                Else
                    strX = CStr(rsDisco("Start_X_Axis").Value)
                    strY = CStr(rsDisco("Start_Y_Axis").Value)
                End If

                'Close the recordset.
                rsDisco.Close()

                'Build a dummy logfile entry for this event and pass it to subDispositionEvent().
                strLine = strDiscoDate & " " & strDiscoAircraft & " crashed at " & strX & " " & strY & vbCrLf
                Call subDispositionEvent(item, strLine)

                'Next Disconnection event.
                intDisco += 1

            End While



        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subPenalizeDisconnections: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub

    'This subroutine goes through all units in the hashUnit structure and updates any Transporting_Group 
    'references. Transporting units are typically ships and trains, while transported units are 
    'typically aircraft, tanks, guns and vehicles. This function is called BEFORE the action events are
    'processed.
    Private Sub subUpdateTransporters()
        Dim strUnitName As String
        Dim strTransporter As String
        Dim thisUnit As SE_Unit
        Dim thisTransporter As SE_Transporter
        Try
            'Bail out if we are adding forces.
            If isAddForce Then
                Exit Sub
            End If

            'Capture the hashUnit keys into an array.
            Dim UnitKeys As ArrayList = New ArrayList(hashUnit.Keys)

            'Step through the keys in the hashTemp hashtable.
            For Each key As String In UnitKeys

                strUnitName = key

                'Grab the corresponding hashUnit value.
                thisUnit = hashUnit(key)
                strTransporter = thisUnit.Transporting_Group

                Select Case strTransporter
                    Case "Independent", "Disembark"
                        'This unit is independent - no action required.

                    Case Else
                        If hashTransporter.ContainsKey(strTransporter) = True Then
                            'Update the raw transporter name with the proper name.
                            thisTransporter = hashTransporter(strTransporter)
                            thisUnit.Transporting_Group = thisTransporter.Name
                        Else
                            'Transporter is temporary airbase - remove the transporter reference.
                            thisUnit.Transporting_Group = "Independent"
                        End If

                        'Update the current unit transporter data into the hashUnit hashtable.
                        hashUnit(strUnitName) = thisUnit

                End Select

            Next

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subUpdateTransporters: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This subroutine applies the latest damage sustained by each ship to its previous damage state,
    'thereby determining the current data state and also determining whether each ship is counted
    'as sunk or not.
    Private Sub subUpdateShipDamageStatus(ByVal item As Integer, ByRef isValid As Boolean)
        Dim strSql, strShipName As String
        Dim strShip As String
        Dim thisShip As SE_Unit
        Dim thisShipEvents As SE_Ship_Damage_Events
        Dim dblTotalDamageOld, dblHull1DamageOld, dblHull2DamageOld, dblHull3DamageOld, dblGunDamageOld As Double
        Dim dblTotalDamageNew, dblHull1DamageNew, dblHull2DamageNew, dblHull3DamageNew, dblGunDamageNew As Double
        Dim thisChunkEventTime, thisChunkEventVictim, thisChunkEventVictimGroup As String
        Dim thisChunk, thisChunkCumulativeDamage, thisChunkEventAggressor As String
        Dim thisChunkEventAggressorGroup, thisChunkEventX, thisChunkEventY As String
        Dim strChunkEvents, strNewSubMode As String
        Dim dblDestroyed As Double

        Try
            'Bail out if we are adding forces.
            If isAddForce Then
                Exit Sub
            End If

            'Capture the hashUnit keys into an array.
            Dim ShipKeys As ArrayList = New ArrayList(hashUnit.Keys)

            'Step through the keys in the hashTemp hashtable.
            For Each key As String In ShipKeys

                strShipName = key

                'Grab the corresponding hashUnit value.
                thisShip = hashUnit(key)

                'Only proceed with this ship if it is undestroyed.
                If thisShip.Quantity > 0 Then

                    'Check for ship class.
                    Select Case Left(thisShip.Object_Class, 1)
                        Case "S"
                            'We have found a ship. Extract the vital chunk damage levels.
                            dblTotalDamageOld = thisShip.ShipDamage
                            dblHull1DamageOld = thisShip.Hull1Damage
                            dblHull2DamageOld = thisShip.Hull2Damage
                            dblHull3DamageOld = thisShip.Hull3Damage
                            dblGunDamageOld = thisShip.GunDamage

                            'Store these in a simple hashtable for later use by subAllocateShipDamageToFreight().
                            If hashShipPriorState.ContainsKey(key) = False Then
                                hashShipPriorState.Add(key, thisShip)
                            Else
                                hashShipPriorState(key) = thisShip
                            End If

                            'Now we need to find its damage events from the last mission.
                            If hashShipEvents.ContainsKey(strShipName) Then

                                'Get the ship status structure and the list of chunk damage events.
                                thisShipEvents = hashShipEvents(strShipName)
                                strChunkEvents = thisShipEvents.ChunkEvents

                                'If the ship now exceeds its viable damage level, place a "sunk" ActionData record.
                                'Note that the hashUnit table which will be written to the DB immediately after this routine.
                                If modUtilities.isSunk(thisShip, strChunkEvents) = True And _
                                   thisShip.isDestroyed = False Then

                                    'Generate an ActionData "sunk" event.
                                    'Create SQL Statement and execute.
                                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, " _
                                            & "Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                                            & modDB.strSQLquote(thisShip.Object_Group_Name) & "', '" _
                                            & modDB.strSQLquote(thisShip.Object_Group_Name) & "', 'sunk', '" _
                                            & modDB.strSQLquote(thisShipEvents.LastAntagonist) & "', '" _
                                            & modDB.strSQLquote(thisShipEvents.LastAntagonistGroup) & "', '" _
                                            & basFiles.StrMissionName & "', '" _
                                            & CInt(thisShipEvents.LastHitY) & "', '" _
                                            & CInt(thisShipEvents.LastHitX) & "', '" _
                                            & strEndOfMission & "')"
                                    modDB.ExecuteDBCommand(item, strSql)

                                    'Update all the ship damage parameters to the state at the end of the mission.
                                    subCalculateShipDamageFromEvents(dblTotalDamageOld, dblHull1DamageOld, dblHull2DamageOld, dblHull3DamageOld, dblGunDamageOld, _
                                                                     strChunkEvents, _
                                                                     dblTotalDamageNew, dblHull1DamageNew, dblHull2DamageNew, dblHull3DamageNew, dblGunDamageNew)

                                    'Update the ship parameters into the hashUnit table which will be 
                                    'written to the DB immediately after this routine.
                                    thisShip.Hull1Damage = dblHull1DamageNew
                                    thisShip.Hull2Damage = dblHull2DamageNew
                                    thisShip.Hull3Damage = dblHull3DamageNew
                                    thisShip.GunDamage = dblGunDamageNew
                                    thisShip.ShipDamage = dblTotalDamageNew

                                    'If the ship has been damaged by an enemy unit in this mission, then 
                                    'boost the recon value of the ship by 50%.
                                    If thisShipEvents.isSpotted = True Then
                                        thisShip.Recon = Math.Min(thisShip.Recon + 50, 100)
                                    End If

                                    'Destroy all freight on this ship.
                                    dblDestroyed = 1.0
                                    Call modUtilities.subDestroyFreight(item, strShipName, _
                                                                        CStr(thisShipEvents.LastHitX), _
                                                                        CStr(thisShipEvents.LastHitY), _
                                                                        "sunk", _
                                                                        thisShipEvents.LastAntagonist, _
                                                                        thisShipEvents.LastAntagonistGroup, _
                                                                        strEndOfMission, _
                                                                        dblDestroyed, 1)

                                Else

                                    'Update all the ship damage parameters to the state at the end of the mission.
                                    subCalculateShipDamageFromEvents(dblTotalDamageOld, dblHull1DamageOld, dblHull2DamageOld, dblHull3DamageOld, dblGunDamageOld, _
                                                                     strChunkEvents, _
                                                                     dblTotalDamageNew, dblHull1DamageNew, dblHull2DamageNew, dblHull3DamageNew, dblGunDamageNew)

                                    'Update the ship parameters into the hashUnit table which will be 
                                    'written to the DB immediately after this routine.
                                    thisShip.Hull1Damage = Math.Min(1.0, dblHull1DamageNew)
                                    thisShip.Hull2Damage = Math.Min(1.0, dblHull2DamageNew)
                                    thisShip.Hull3Damage = Math.Min(1.0, dblHull3DamageNew)
                                    thisShip.GunDamage = dblGunDamageNew
                                    thisShip.ShipDamage = Math.Min(1.0, dblTotalDamageNew)

                                    'If the ship is a submarine, and is damaged, then force it to surface.
                                    If thisShip.Object_Class = "SSU" And thisShip.ShipDamage > 0 Then
                                        thisShip.Object_Type = strSurfacedSub(thisShip.Object_Type)
                                    End If

                                    'Reduce the ship fuel in proportion to the amount of strength left.
                                    'Disable this for now - may reintroduce after play testing.
                                    'Introduce for DCS v3.3.0
                                    If CS_Track_Fuel_Consumption = 1 Then
                                        thisShip.Fuel = CInt(Math.Min(thisShip.Fuel_Capacity * (1.0 - thisShip.ShipDamage), thisShip.Fuel))
                                        thisShip.Fuel = Math.Max(0, thisShip.Fuel)
                                    End If

                                    'If the ship has been damaged by an enemy unit in this mission, then 
                                    'boost the recon value of the ship by 50%.
                                    If thisShipEvents.isSpotted = True Then
                                        thisShip.Recon = Math.Min(thisShip.Recon + 50, 100)
                                    End If

                                End If

                                'Load the updated ship status back into the unit hashtable.
                                hashUnit(key) = thisShip

                            Else
                                'This ship had no damage events - no action required.

                            End If

                        Case Else
                            'This unit is not a ship - no action required.

                    End Select

                End If

            Next

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subUpdateShipDamageStatus: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    'This subroutine performs incremental allocation of ship damage events. Although all ship damage
    'events have, by this stage, been recorded in the ActionData table, the destruction of any freight
    'carried by the ship (including air units on carriers) has not yet been registered in the ActionData
    'table. That is the purpose of this routine.
    Private Sub subAllocateShipDamageToFreight(ByVal item As Integer, ByRef isValid As Boolean)
        Dim thisShip As SE_Unit
        Dim thisShipDamageEvents As SE_Ship_Damage_Events
        Dim strDamageEvents, strPartialDamageEventList As String
        Dim AllShips, AllEvents As New ArrayList
        Dim ShipKey, EventKey, strEventRecord, strEventParameter() As String
        Dim dblShipDamageOld, dblHull1DamageOld, dblHull2DamageOld, dblHull3DamageOld, dblGunDamageOld As Double
        Dim dblShipDamageNew, dblHull1DamageNew, dblHull2DamageNew, dblHull3DamageNew, dblGunDamageNew As Double
        Dim strEventTime, strEventProtagonist, strEventProtagonistGroup, strEventChunk As String
        Dim strEventChunkCDamage, strEventAntagonist, strEventAntagonistGroup, strEventX, strEventY As String
        Dim dblShipDamagePrevious, dblDamageEventEffect As Double
        Dim index As Integer
        Dim strEvent() As String
        Dim thisEnumerator As IDictionaryEnumerator

        Try

            'Set the completion status.
            isValid = False

            If intMissionDuration > basMain.MINIMUMDURATION And modCfg.User(item).IsCampaignInitialized And Not isAddForce Then

                'We are in the blessed position of having a hashtable of all ship damage events.
                'Loop through this data structure for each ship, reducing freight loads by the effect
                'of each damage event, recording the details in ActionData as End Of Mission  "destroyed" events.
                thisEnumerator = hashShipPriorState.GetEnumerator
                While thisEnumerator.MoveNext

                    ShipKey = thisEnumerator.Key
                    thisShip = thisEnumerator.Value

                    'Parse the ship status structure.
                    dblShipDamageOld = thisShip.ShipDamage
                    dblHull1DamageOld = thisShip.Hull1Damage
                    dblHull2DamageOld = thisShip.Hull2Damage
                    dblHull3DamageOld = thisShip.Hull3Damage
                    dblGunDamageOld = thisShip.GunDamage

                    'Find the list of ship damage events for this ship.
                    If Left(thisShip.Object_Class, 1) = "S" And _
                       thisShip.Quantity > 0 And _
                       hashShipEvents.ContainsKey(ShipKey) = True Then

                        'Gather the ship damage events.
                        thisShipDamageEvents = hashShipEvents(ShipKey)
                        strDamageEvents = thisShipDamageEvents.ChunkEvents

                        'For each event, calculate the effective ship status after the event has happened.
                        'The change in ship damage state from the previous event is the net damage incurred
                        'by the current event. The net damage is applied to the freight carried by the ship.
                        dblShipDamagePrevious = dblShipDamageOld
                        strPartialDamageEventList = ""
                        strEvent = Split(strDamageEvents, "^")
                        index = 0
                        While index <= strEvent.GetUpperBound(0) - 1

                            'Extract the details of the current ship damage event.
                            strEventRecord = strEvent(index)
                            strEventParameter = Split(strEventRecord, "|")
                            strEventTime = strEventParameter(0)
                            strEventProtagonist = strEventParameter(1)
                            strEventProtagonistGroup = strEventParameter(2)
                            strEventChunk = strEventParameter(3)
                            strEventChunkCDamage = strEventParameter(4)
                            strEventAntagonist = strEventParameter(5)
                            strEventAntagonistGroup = strEventParameter(6)
                            strEventX = strEventParameter(7)
                            strEventY = strEventParameter(8)

                            'Add the current ship damage event to strPartialDamageEventList.
                            strPartialDamageEventList &= strEventRecord & "^"

                            'Determine the ship damage state after this event occurred.
                            Call modUtilities.subCalculateShipDamageFromEvents(dblShipDamageOld, dblHull1DamageOld, dblHull2DamageOld, dblHull3DamageOld, dblGunDamageOld, _
                                                                               strPartialDamageEventList, _
                                                                               dblShipDamageNew, dblHull1DamageNew, dblHull2DamageNew, dblHull3DamageNew, dblGunDamageNew)

                            'Calculate the net damage effect of this event.
                            dblDamageEventEffect = dblShipDamageNew - dblShipDamagePrevious

                            'If a palpable hit, destroy this percentage of any freight carried by the ship.
                            If dblDamageEventEffect > basMain.PARTIALDAMAGETHRESHOLD Then
                                Call modUtilities.subDestroyFreight(item, ShipKey, strEventX, strEventY, _
                                                                    "destroyed by", strEventAntagonist, _
                                                                    basFiles.StrMissionName, strEndOfMission, _
                                                                    dblDamageEventEffect, 1)
                            End If

                            'Save the current damage state for the next event.
                            dblShipDamagePrevious = dblShipDamageNew

                            'Next event.
                            index += 1

                        End While

                    Else
                        Call basMess.MessageBox("modMissionAnalyzer.subAllocateShipDamageToFreight: " _
                            & "no ship events structure for ship unit " & ShipKey, vbCritical)
                    End If

                End While

            End If

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAllocateShipDamageToFreight: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This subroutine is a virtual copy of the basMissionBuilder.subAllocateLosses() routine. The difference
    'is that the rsUnits recordset is opened here explicitly only for air units, where as in 
    'basMissionBuilder the routine is called after the recordset is opened, and then only for 
    'ground and sea units. Maybe someone will get around to using the same routine in both places one day.
    'NB: I discovered a logic error in this routine shortly after DCS2.0 release.
    'The routine correctly allocated losses for the primary unit, but failed to take into account losses
    'of a reinforcing top-up flight. To handle this I essentially double the number of loops below. To map
    'the top-up flight into the primary flight needs an extra JOIN on Unit_Name_Mapping table.
    Private Sub subAllocateAirLosses(ByVal item As Integer, ByRef isValid As Boolean)
        Dim strSql As String
        Dim rsAircraftUnits As New ADODB.Recordset
        Dim rsAction As New ADODB.Recordset
        Dim rsDisposition As New ADODB.Recordset
        Dim rsWithdrawnLoss As New ADODB.Recordset
        Dim rsDestroyedCarriers As New ADODB.Recordset
        Dim rsDestroyedFreight As New ADODB.Recordset
        Dim rsPool As New ADODB.Recordset
        Dim intUnitStrength As Integer
        Dim dblLastX As Double
        Dim dblLastY As Double
        Dim intPool As Integer
        Dim strResupplyPoint As String
        Dim intQuantity As Integer
        Dim dtEndOfMission As Date
        Dim strFreightEvent As String
        Dim strFreightAntagonist As String
        Dim strFreightTime As String
        Dim intRefuelingTime As Integer
        Dim rsDemolitionOrders As New ADODB.Recordset

        Try

            'Set the completion status.
            isValid = False

            If intMissionDuration > 60 And modCfg.User(item).IsCampaignInitialized And Not isAddForce Then

                'A new section to cater for cases where aircraft are forced down in enemy territory and the crew bail
                'and are captured, but IL-2 fails to log a destruction event for the aircraft. Such instances need
                'to be logged in ActionData for later processing.
                Call subCapturedAirframes(item)

                'Open the rsUnits recordset for all aircraft units.
                strSql = "SELECT * FROM ObjMissionData, Object_Specifications, MissionData WHERE " _
                         & "ObjMissionData.Mission_Name = MissionData.Mission_Name AND " _
                         & "ObjMissionData.Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                         & "ObjMissionData.Obj_Type = Object_Specifications.Object_Type AND " _
                         & "MissionData.Map = '" & strThisMap & "' AND " _
                         & "Object_Specifications.Object_Class LIKE 'P%'"
                Call modDB.openDBRecordset(item, rsAircraftUnits, strSql)

                'Loop through the air units and allocate losses.
                Do Until rsAircraftUnits.EOF = True

                    intUnitStrength = rsAircraftUnits("Quantity").Value
                    dblLastX = rsAircraftUnits("Start_X_Axis").Value
                    dblLastY = rsAircraftUnits("Start_Y_Axis").Value

                    'Loop through the list of Action events, looking for "destroyed by" or "shot down by" records. 
                    strSql = "SELECT ActionData.Obj1Group AS Reduced_Flight, " _
                            & "Event, Obj2, Obj2Group, X_Axis, Y_Axis, EventTime FROM ActionData WHERE " _
                            & "ActionData.Mission = '" & basFiles.StrMissionName & "' AND " _
                            & "ActionData.Obj1Group = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND " _
                            & "(Event = 'destroyed by' OR Event = 'shot down by' OR Event = 'captured by')"
                    Call modDB.openDBRecordset(item, rsAction, strSql)

                    Do Until rsAction.EOF = True
                        'For every "destroyed by" line encountered, decrement 1 from the original unit strength
                        'and capture the event location coords. The coords are needed in case the unit is completely
                        'annihilated, so we can place the gravestone at the right spot (see below).
                        intUnitStrength -= 1
                        strFreightEvent = "destroyed by"
                        strFreightTime = rsAction("EventTime").Value
                        strFreightAntagonist = rsAction("Obj2").Value
                        dblLastX = rsAction("X_Axis").Value
                        dblLastY = rsAction("Y_Axis").Value
                        rsAction.MoveNext()
                    Loop

                    'Close the Action recordset.
                    rsAction.Close()

                    'Loop through the Resupply_Reinforcements table and look for incoming
                    'top-ups for this unit that were shot down.
                    strSql = "SELECT Resupply_Replacements.Unit_Name AS Reduced_Flight, " _
                            & "Event, Obj2, Obj2Group, X_Axis, Y_Axis, EventTime FROM " _
                            & "ActionData, Resupply_Replacements, Unit_Name_Mapping WHERE " _
                            & "ActionData.Mission = '" & basFiles.StrMissionName & "' AND " _
                            & "ActionData.Mission = Unit_Name_Mapping.Mission AND " _
                            & "Unit_Name_Mapping.InGame_Name = ActionData.Obj1Group AND " _
                            & "Unit_Name_Mapping.Unit_Name = Resupply_Replacements.Unit_Name AND " _
                            & "Unit_Name_Mapping.Unit_Name = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND " _
                            & "Resupply_Replacements.Used = 1 AND " _
                            & "(Event = 'destroyed by' OR Event = 'shot down by')"
                    Call modDB.openDBRecordset(item, rsAction, strSql)

                    Do Until rsAction.EOF = True
                        'For every "destroyed by" line encountered, decrement 1 from the original unit strength
                        'and capture the event location coords. The coords are needed in case the unit is completely
                        'annihilated, so we can place the gravestone at the right spot (see below).
                        intUnitStrength -= 1
                        strFreightEvent = "destroyed by"
                        strFreightTime = rsAction("EventTime").Value
                        strFreightAntagonist = rsAction("Obj2").Value
                        dblLastX = rsAction("X_Axis").Value
                        dblLastY = rsAction("Y_Axis").Value
                        rsAction.MoveNext()
                    Loop

                    'Close the Action recordset.
                    rsAction.Close()


                    'Loop through the list of Disposition events, looking for "crashed" records. 
                    strSql = "SELECT * FROM ObjMissionDisposition WHERE " _
                            & "Mission = '" & basFiles.StrMissionName & "' AND " _
                            & "ObjGroup = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND "
                    If rsAircraftUnits("Object_Class").Value = "PGL" Then
                        strSql &= "(Disposition = 'Crashed' OR Disposition = 'Landed') ORDER BY EventTime;"
                    Else
                        strSql &= "Disposition = 'Crashed' ORDER BY EventTime;"
                    End If

                    Call modDB.openDBRecordset(item, rsDisposition, strSql)

                    Do Until rsDisposition.EOF = True
                        'For every "crashed" line encountered, decrement 1 from the original unit strength.
                        If rsDisposition("Disposition").Value = "Crashed" Then
                            strFreightEvent = "Crashed"
                            strFreightTime = rsDisposition("EventTime").Value
                            strFreightAntagonist = ""
                            intUnitStrength -= 1
                        End If

                        'Capture the event location coords. The coords are needed in case the unit is 
                        'completely annihilated, so we can place the gravestone at the right spot.
                        'Alternativbely, for gliders, we need to capture the landing location for
                        'subsequent unloading of paratroop units.
                        dblLastX = rsDisposition("X_Axis").Value
                        dblLastY = rsDisposition("Y_Axis").Value
                        rsDisposition.MoveNext()
                    Loop

                    'Close the Disposition recordset.
                    rsDisposition.Close()


                    'Loop through the list of Disposition events for TopUps, looking for "crashed" records. 
                    strSql = "SELECT * FROM ObjMissionDisposition, Resupply_Replacements, Unit_Name_Mapping " _
                            & "WHERE " _
                            & "Unit_Name_Mapping.Unit_Name = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND " _
                            & "ObjMissionDisposition.Mission = '" & basFiles.StrMissionName & "' AND " _
                            & "ObjMissionDisposition.Mission = Unit_Name_Mapping.Mission AND " _
                            & "Unit_Name_Mapping.InGame_Name = ObjMissionDisposition.ObjGroup AND " _
                            & "Unit_Name_Mapping.Unit_Name = Resupply_Replacements.Unit_Name AND " _
                            & "Resupply_Replacements.Used = 1 AND " _
                            & "Disposition = 'Crashed'"
                    Call modDB.openDBRecordset(item, rsDisposition, strSql)

                    Do Until rsDisposition.EOF = True
                        'For every "crashed" line encountered, decrement 1 from the original unit strength
                        'and capture the event location coords. The coords are needed in case the unit is completely
                        'annihilated, so we can place the gravestone at the right spot (see below).
                        intUnitStrength -= 1
                        strFreightEvent = "Crashed"
                        strFreightTime = rsDisposition("EventTime").Value
                        strFreightAntagonist = ""
                        dblLastX = rsDisposition("X_Axis").Value
                        dblLastY = rsDisposition("Y_Axis").Value
                        rsDisposition.MoveNext()
                    Loop

                    'Close the Disposition recordset.
                    rsDisposition.Close()


                    'We have gathered all the destruction events, so we can update the Quantity field for the 
                    'current unit in ObjMissionData table. I am getting paranoid about SQL statements not
                    'being refreshed quickly enough - hence the transaction methods!
                    intUnitStrength = Math.Max(0, intUnitStrength)
                    If intUnitStrength = 0 Then
                        'Move the unit to the location of the last destroyed member, and set the strength to zero, i.e
                        'put a gravestone there! Convert to 'Independent' status to ensure
                        'that carrier-based aircraft get visible gravestones.
                        strSql = "UPDATE ObjMissionData SET Start_X_Axis = " & dblLastX & ", " _
                                & "Start_Y_Axis = " & dblLastY & ", " _
                                & "Quantity = 0, Fuel = 0, Fuel_Freight = 0, Transporting_Group = 'Independent' " _
                                & "WHERE Obj_Group_Name = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND " _
                                & "Mission_Name = '" & basFiles.StrMissionName & "'"
                        Call modDB.ExecuteDBCommand(item, strSql)

                        'Apply the same fate to any units transported by this unit.
                        modUtilities.subDestroyFreight(item, rsAircraftUnits("Obj_Group_Name").Value, _
                                                        dblLastX, dblLastY, strFreightEvent, _
                                                        strFreightAntagonist, _
                                                        basFiles.StrMissionName, strFreightTime, _
                                                        1.0, intUnitStrength)

                    ElseIf intUnitStrength < rsAircraftUnits("Quantity").Value Then
                        'Decrement the unit strength by the number of losses found. For non-zero strength units,
                        'the new location will be calculated in basMissionBuilder.subAdvanceUnit().
                        strSql = "UPDATE ObjMissionData SET Quantity = " & intUnitStrength & " " _
                                & "WHERE Obj_Group_Name = '" & rsAircraftUnits("Obj_Group_Name").Value & "' AND " _
                                & "Mission_Name = '" & basFiles.StrMissionName & "'"
                        Call modDB.ExecuteDBCommand(item, strSql)

                    End If


                    rsAircraftUnits.MoveNext()
                Loop

                'Close the air units recordset.
                rsAircraftUnits.Close()



                'Now we need to do something similar for all units that have been withdrawn in the previous mission.
                'So make a query to select all withdrawn units that sustained losses in the last mission.
                strSql = "SELECT * FROM Withdrawals, ActionData, Object_Specifications WHERE " _
                        & "Withdrawals.Map = '" & strThisMap & "' AND " _
                        & "ActionData.Mission = '" & basFiles.StrMissionName & "' AND " _
                        & "ActionData.Mission = Withdrawals.Mission AND " _
                        & "Withdrawals.Withdraw_Group = ActionData.Obj1Group AND " _
                        & "Withdrawals.Withdraw_Type = Object_Specifications.Object_Type;"
                Call modDB.openDBRecordset(item, rsWithdrawnLoss, strSql)

                While rsWithdrawnLoss.EOF = False

                    'We are in a recordset where each line corresponds to the destruction of a member
                    'of a withdrawn flight. We therefore need to decrement the number of this type of aircraft
                    'that is available at the resupply point. Since this routine is called after the unit
                    'hashtable is stored, we have to work directly in the database.
                    strResupplyPoint = getAirSupplyName(item, rsWithdrawnLoss("Withdraw_X").Value, rsWithdrawnLoss("Withdraw_Y").Value)

                    'The strResupplyPoint name is something like "Ardennes_NNW_Withdraw", but we will
                    'need to subtract losses off the supply pool at the associated supply point
                    'in this case that would be "Ardennes_NNW_Supply"
                    strResupplyPoint = Replace(strResupplyPoint, "Withdraw", "Supply")

                    'Gather the Refueling_Time value for this object type.
                    intRefuelingTime = rsWithdrawnLoss("Refueling_Time").Value

                    strSql = "SELECT * FROM Resupply_Replacements WHERE " _
                            & "Map = '" & strThisMap & "' AND " _
                            & "Supply_Point = '" & modDB.strSQLquote(strResupplyPoint) & "' AND " _
                            & "Committed = 0 AND " _
                            & "ObjType = '" & rsWithdrawnLoss("Withdraw_Type").Value & "' AND " _
                            & "Transit = " & intRefuelingTime & ";"
                    Call modDB.openDBRecordset(item, rsPool, strSql)

                    If rsPool.EOF = False Then

                        'We have found the pool of reinforcements that the withdrawn unit was added to.
                        'Just find out how many aircraft are there, and reduce it by one.
                        intPool = rsPool("Number_Objects").Value
                        strSql = "UPDATE Resupply_Replacements SET Number_Objects = " & Math.Max(0, intPool - 1) & " " _
                                & "WHERE Map = '" & strThisMap & "' AND " _
                                & "Supply_Point = '" & modDB.strSQLquote(strResupplyPoint) & "' AND " _
                                & "Committed = 0 AND " _
                                & "ObjType = '" & rsWithdrawnLoss("Withdraw_Type").Value & "'"
                        Call modDB.ExecuteDBCommand(item, strSql)

                        'Apply the same fate to any units transported by this unit.
                        Call modUtilities.subDestroyFreight(item, rsWithdrawnLoss("Obj1Group").Value, _
                                                    rsWithdrawnLoss("X_Axis").Value, _
                                                    rsWithdrawnLoss("Y_Axis").Value, _
                                                    "destroyed by", _
                                                    Trim(rsWithdrawnLoss("Obj2").Value), _
                                                    basFiles.StrMissionName, _
                                                    strEndOfMission, _
                                                    1.0, intUnitStrength)


                    Else

                        'Hmm, we didn't find the reinforcement pool - something must be wrong.
                        Call basMess.MessageBox("modMissionAnalyzer.subAllocateAirLosses: No pool of " _
                                    & rsWithdrawnLoss("Withdraw_Type").Value _
                                    & " objects at location " & strResupplyPoint & ". Cannot allocate losses!", vbExclamation)
                    End If

                    'Close the reinforcement pool recordset.
                    rsPool.Close()

                    'Move to the next action report involving a withdrawn flight.
                    rsWithdrawnLoss.MoveNext()

                End While

                'Close the recordset of action reports for withdrawn units.
                rsWithdrawnLoss.Close()



                'We have now scanned all air losses and allocated them into ObjMissionData. All
                'Reinforcement and Withdrawals have been performed, so let us wipe the 
                'Resupply_Replacements table of old TopUps data.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Resupply_Replacements WHERE " _
                        & "Committed = 1 AND Used = 1;"
                Call modDB.ExecuteDBCommand(item, strSql)


                'Now, a new section for DCS2.0beta(G), involving losses to aircraft carriers. We need
                'to track losses immediately, so that commanders do not have to wait for a mission
                'to realize that their naval airforce is sunk!

                'All shipping losses are reported in ActionData table, either as "destroyed by"
                'from enemy action or as "sunk" from collisions.
                strSql = "SELECT * FROM ObjMissionData, Object_Specifications, MissionData, ActionData WHERE " _
                        & "ObjMissionData.Mission_Name = MissionData.Mission_Name AND " _
                        & "ObjMissionData.Mission_Name = ActionData.Mission AND " _
                        & "ObjMissionData.Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "ObjMissionData.Obj_Type = Object_Specifications.Object_Type AND " _
                        & "MissionData.Map = '" & strThisMap & "' AND " _
                        & "(Object_Specifications.Object_Class = 'SAC' OR " _
                        & "Object_Specifications.Object_Class = 'SACE') AND " _
                        & "(ActionData.Event = 'sunk' OR ActionData.Event = 'sank' OR " _
                        & "ActionData.Event = 'crashed' OR ActionData.Event = 'destroyed by') AND " _
                        & "ObjMissionData.Obj_Group_Name = ActionData.Obj1Group;"
                Call modDB.openDBRecordset(item, rsDestroyedCarriers, strSql)

                While rsDestroyedCarriers.EOF = False

                    'Set the sunk carrier quantity to zero.
                    strSql = "UPDATE ObjMissionData SET Quantity = 0, Fuel = 0, Fuel_Freight = 0, " _
                            & "Start_X_Axis= " & CDbl(rsDestroyedCarriers("X_Axis").Value) & ", " _
                            & "Start_Y_Axis= " & CDbl(rsDestroyedCarriers("Y_Axis").Value) & " " _
                            & "WHERE Obj_Group_Name = '" & rsDestroyedCarriers("Obj_Group_Name").Value & "' AND " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "'"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    'Apply the same fate to any units transported by this unit.
                    Call modUtilities.subDestroyFreight(item, rsDestroyedCarriers("Obj_Group_Name").Value, _
                                                    rsDestroyedCarriers("X_Axis").Value, _
                                                    rsDestroyedCarriers("Y_Axis").Value, _
                                                    "sunk", _
                                                    Trim(rsDestroyedCarriers("Obj2").Value), _
                                                    basFiles.StrMissionName, strEndOfMission, _
                                                    1.0, intUnitStrength)


                    'Go to the next destroyed carrier.
                    rsDestroyedCarriers.MoveNext()

                End While

                'Close the recordset.
                rsDestroyedCarriers.Close()

                'Now, a new section for DCS3.1.0, involving engineer demolition of airbases. We need
                'to track any aircraft destruction events immediately, so that commanders do not have 
                'to wait for an hour to realize that their airbase is destroyed!
                Call subDemolishAirbases(item)

            End If


            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAllocateAirLosses: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub

    'This routine polls all capture reports and determines whether the airframe has been destroyed or not.
    'If not, the airframe is either captured by the enemy or destroyed, according to CS_Aircraft_Capture.
    Private Sub subCapturedAirframes(ByVal item As Integer)
        Dim strDestroyed(), strCaptured() As String
        Dim strSql As String
        Dim strCapturedAirframe As String
        Dim rsMission, rsCA As New ADODB.Recordset
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisCapturedUnit As SE_AirframeCapture
        Dim strFlightName As String
        Dim strEvent As String
        Dim strAntagonist As String
        Dim strAntagonistGroup As String
        Dim dtEndOfMission As Date
        Dim strUnitNation As String
        Dim strPilotSeat As String

        Try

            'Loop through all captured airframes.
            thisEnumerator = hashCaptureEvents.GetEnumerator()
            While thisEnumerator.MoveNext()

                'Here is the captured airframe name.
                thisCapturedUnit = thisEnumerator.Value
                strPilotSeat = thisCapturedUnit.PilotSeat
                If thisCapturedUnit.isDriveable = True Then
                    strFlightName = thisCapturedUnit.Airframe_Name
                    strUnitNation = modUtilities.strPlatoonAlignment(item, strFlightName)
                Else
                    strFlightName = Left(thisCapturedUnit.Airframe_Name, Len(thisCapturedUnit.Airframe_Name) - 1)
                    strUnitNation = modUtilities.strFlightAlignment(item, strFlightName)
                End If

                'Does that name exist in the list of destroyed units?
                If InStr(strAirframesLost, thisCapturedUnit.Airframe_Name & "|") < 1 Then

                    'It does not, so we have trapped an occasion where aircrew have bailed and been captured,
                    'but the aircraft is not captured. Let's capture or destroy it!
                    If CS_Aircraft_Capture = 1 Then
                        strEvent = "captured by"
                    Else
                        strEvent = "destroyed by"
                    End If

                    'Determine the identity of the victorious forces.
                    If strUnitNation = "r" Then
                        strAntagonist = "Axis Forces"
                        strAntagonistGroup = "Axis Forces"
                    Else
                        strAntagonist = "Allied Forces"
                        strAntagonistGroup = "Allied Forces"
                    End If


                    'Create SQL Statement
                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, " _
                    & "Obj2, Obj2Group, Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                    & thisCapturedUnit.Airframe_Name & "', '" _
                    & strFlightName & "', '" _
                    & strEvent & "', '" _
                    & modDB.strSQLquote(strAntagonist) & "', '" _
                    & modDB.strSQLquote(strAntagonistGroup) & "', '" _
                    & basFiles.StrMissionName & "', '" _
                    & CInt(thisCapturedUnit.Y) & "', '" & CInt(thisCapturedUnit.X) & "', '" _
                    & strEndOfMission & "')"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    'We might need to have the unit change hands!
                    If CS_Aircraft_Capture = 1 And thisCapturedUnit.isDriveable = True Then
                        Call subCapturePlatoon(item, strFlightName, strUnitNation, strPilotSeat)
                    End If

                    'Remove any corresponding penalty for this aircraft's flight.
                    strSql = "UPDATE ObjMissionData SET Penalty = Penalty-1 WHERE " _
                            & "Obj_Group_Name = '" & strFlightName & "' AND " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    strSql = "UPDATE ObjMissionData SET Penalty = 0 WHERE " _
                            & "Penalty < 0 AND Obj_Group_Name = '" & strFlightName & "' AND " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    'Changed for DCS v3.2.2, following bug report by Brandle.
                    'We have promoted this subroutine to the top of subAllocateAirLosses() so that
                    'all these captured/destroyed events are simply logged here, and are later 
                    'processed for loss allocations by subAllocateAirLosses() itself.
                    '
                    'Set the flight strength to zero, in ObjMissionData.
                    'strSql = "UPDATE ObjMissionData SET Quantity = 0, " _
                    '        & "Fuel = 0, Fuel_Freight = 0 WHERE " _
                    '        & "Obj_Group_Name = '" & strFlightName & "' AND " _
                    '        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    'Call modDB.ExecuteDBCommand(item, strSql)


                    'Unload every unit carried as freight by the flight.
                    'strSql = "UPDATE ObjMissionData SET Transporting_Group = 'Independent' WHERE " _
                    '        & "Transporting_Group = '" & strFlightName & "' AND " _
                    '        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    'Call modDB.ExecuteDBCommand(item, strSql)

                    strAirframesLost &= thisCapturedUnit.Airframe_Name & "|"

                End If

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subCapturedAirframes: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This subroutine scans the ActionData table to gather any capture messages for members
    'of the current unit in the rsUnits recordset. The calling context should be for non-aircraft units.
    Private Sub subCapturePlatoon(ByVal item As Integer, ByVal strOldGroup As String, _
                                  ByVal strOldNation As String, ByVal strPilotSeat As String)
        Dim rsCaptureUnit As New ADODB.Recordset
        Dim strSql As String
        Dim strNation, strClass, strNewNation, strNewUnitName, strNewGroupName, strNewParent As String
        Dim strCapturedPilot, strCaptureNotice As String

        Try

            'Assume that the platoon to be captured is VMI class.
            strClass = "VMI"

            'Determine the new nation for the captured unit.
            If strOldNation = "r" Then
                strNewNation = "g"
            Else
                strNewNation = "r"
            End If

            'Find a new platoon name of the appropriate type and alignment.
            If isNewPlatoonName(item, strClass, strNewNation, strNewUnitName, strNewGroupName, strNewParent) = True Then
                'Update ObjMissionData settings.
                strSql = "SELECT * FROM ObjMissionData " _
                        & "WHERE Obj_Group_Name = '" & strOldGroup & "' AND " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                Call modDB.openDBRecordset(item, rsCaptureUnit, strSql)

                If rsCaptureUnit.EOF = False Then
                    strSql = "INSERT INTO ObjMissionData " _
                            & "(Mission_Name, Unit_Name, Obj_Group_Name, Obj_Type, Obj_Parent, Obj_Nationality, Transporting_Group, " _
                            & "Dissolved, Start_X_Axis, Start_Y_Axis, Start_Orientation, GAttack_X_Axis, " _
                            & "GAttack_Y_Axis, Fuel, Fuel_Freight, Skill, Skill0, Skill1, Skill2, Skill3, Quantity, Morale, OnlyAI, " _
                            & "Refueling, Penalty, Recon_Percent, Formation, Skin, Theatre) VALUES ('" _
                            & basFiles.StrMissionName & "', '" _
                            & strNewUnitName & "', '" _
                            & strNewGroupName & "', '" _
                            & rsCaptureUnit("Obj_Type").Value & "', '" _
                            & strNewParent & "', '" _
                            & strNewNation & "', " _
                            & "'Independent', '" _
                            & rsCaptureUnit("Dissolved").Value & "', '" _
                            & rsCaptureUnit("Start_X_Axis").Value & "', '" _
                            & rsCaptureUnit("Start_Y_Axis").Value & "', '" _
                            & rsCaptureUnit("Start_Orientation").Value & "', '" _
                            & 0 & "', '" _
                            & 0 & "', '" _
                            & rsCaptureUnit("Fuel").Value & "', '" _
                            & rsCaptureUnit("Fuel_Freight").Value & "', '" _
                            & rsCaptureUnit("Skill").Value & "', '" _
                            & rsCaptureUnit("Skill0").Value & "', '" _
                            & rsCaptureUnit("Skill1").Value & "', '" _
                            & rsCaptureUnit("Skill2").Value & "', '" _
                            & rsCaptureUnit("Skill3").Value & "', '" _
                            & rsCaptureUnit("Quantity").Value & "', '" _
                            & 4 & "', '" _
                            & rsCaptureUnit("OnlyAI").Value & "', '" _
                            & rsCaptureUnit("Refueling").Value & "', '" _
                            & 0 & "', '" _
                            & 100 & "', '" _
                            & rsCaptureUnit("Formation").Value & "', '" _
                            & rsCaptureUnit("Skin").Value & "', '" _
                            & rsCaptureUnit("Theatre").Value & "');"
                    modDB.ExecuteDBCommand(item, strSql)

                    strCapturedPilot = strGetPilotByAircraft(strPilotSeat)
                    strCaptureNotice = modUtilities.strAlignment(strOldNation)
                    strCaptureNotice &= " vehicle belonging to " & rsCaptureUnit("Unit_Name").Value _
                                      & " driven by " & strCapturedPilot & " was captured by the enemy."
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, _
                                        strLastMissionSequenceNumber, strOldNation, _
                                        strCaptureNotice)


                End If

                'Update ObjMissionData settings to destroy the old unit affiliation.
                strSql = "UPDATE ObjMissionData SET Quantity = 0 " _
                        & "WHERE Obj_Group_Name = '" & strOldGroup & "' AND " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                Call modDB.ExecuteDBCommand(item, strSql)
            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subCapturePlatoon: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    Private Function isNewPlatoonName(ByVal item As Integer, ByVal strClass As String, _
                                      ByVal strNation As String, ByRef strNewUnitName As String, _
                                      ByRef strNewGroupName As String, ByRef strNewParent As String) As Boolean
        Dim strSql, strAlignment, strDetails As String
        Dim rsNPN As New ADODB.Recordset

        Try
            'Pessimistic return value.
            isNewPlatoonName = False

            'Build alignment from strNation.
            strAlignment = modUtilities.strAlignment(strNation)

            'Load strings with lists of current platoon names.
            modMissionAnalyzer.strAlliedPlatoons = "|"
            modMissionAnalyzer.strAxisPlatoons = "|"
            strSql = "SELECT DISTINCT Obj_Group_Name, Obj_Nationality FROM " _
                    & "ObjMissionData, Object_Specifications WHERE " _
                    & "ObjMissionData.Mission_Name='" & basFiles.StrMissionName & "' AND " _
                    & "ObjMissionData.Obj_Type=Object_Specifications.Object_Type AND " _
                    & "(LEFT(Object_Specifications.Object_Class,1)='A' OR " _
                    & "LEFT(Object_Specifications.Object_Class,1)='V' OR " _
                    & "LEFT(Object_Specifications.Object_Class,1)='T');"
            modDB.openDBRecordset(item, rsNPN, strSql)

            While rsNPN.EOF = False
                If rsNPN("Obj_Nationality").Value = "r" Then
                    modMissionAnalyzer.strAlliedPlatoons &= rsNPN("Obj_Group_Name").Value & "|"
                Else
                    modMissionAnalyzer.strAxisPlatoons &= rsNPN("Obj_Group_Name").Value & "|"
                End If

                rsNPN.MoveNext()
            End While

            'close the recordset.
            rsNPN.Close()

            'Now we have all existing platoon names, get the next available platoon name.
            strNewGroupName = modUtilities.strPlatoonName(strClass, strNation)

            'If we don't get a good suggested name, bomb out!
            If strNewGroupName = "invalid" Then
                Exit Function
            End If

            'We have agood name, so build the literal equivalent.
            strDetails = modUtilities.UnitAffiliationDetails(item, strNewGroupName, strClass, strNation)
            strNewUnitName = Left(strDetails, InStr(strDetails, "|") - 1)

            'Construct the Parent name.
            strNewParent = Left(strNewGroupName, Len(strNewGroupName) - 3)

            'return in happiness.
            isNewPlatoonName = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isNewPlatoonName: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Function

    'This routine performs demolitions of airbases.
    Private Sub subDemolishAirbases(ByVal item As Integer)
        Dim strEngineerUnit As String
        Dim strEngineerAlignment As String
        Dim strAirbaseTarget As String
        Dim rsDemolitionOrders As New ADODB.Recordset
        Dim rsEngineerStrength As New ADODB.Recordset
        Dim rsEngineerLoss As New ADODB.Recordset
        Dim rsAirbaseTarget As New ADODB.Recordset
        Dim rsAircraftDestruction As New ADODB.Recordset
        Dim intEngineerStrength, intDice As Integer
        Dim AX, AY, E_X, E_Y, dblDist, dblX, dblY As Double
        Dim intFlightStrength, intInitialEngineerStrength, intDemolish As Integer
        Dim strFlight, strSql As String

        Try


            Randomize()

            'Find each engineer unit with a current demolition order for an airbase.
            strSql = "SELECT * FROM Demolition_Orders WHERE Map='" & strThisMap & "' AND " _
                    & "SEQUENCE='" & strLastMissionSequenceNumber & "' AND " _
                    & "Demolition_Target LIKE 'AB_%';"
            modDB.openDBRecordset(item, rsDemolitionOrders, strSql)

            While rsDemolitionOrders.EOF = False

                'Gather the demolition order details.
                strEngineerUnit = rsDemolitionOrders("Demolition_Unit").Value
                strEngineerAlignment = rsDemolitionOrders("Demolition_Unit_Alignment").Value
                strAirbaseTarget = Replace(rsDemolitionOrders("Demolition_Target").Value, "AB_", "")

                'Gather the current strength of the engineer unit.
                strSql = "SELECT * FROM ObjMissionData WHERE " _
                        & "Obj_Group_Name='" & strEngineerUnit & "' AND " _
                        & "Obj_Nationality='" & strEngineerAlignment & "' AND " _
                        & "Mission_Name='" & basFiles.StrMissionName & "';"
                modDB.openDBRecordset(item, rsEngineerStrength, strSql)
                intEngineerStrength = 0
                intInitialEngineerStrength = intEngineerStrength
                If rsEngineerStrength.EOF = False Then
                    intEngineerStrength = rsEngineerStrength("Quantity").Value
                    E_X = CDbl(rsEngineerStrength("Start_X_Axis").Value)
                    E_Y = CDbl(rsEngineerStrength("Start_Y_Axis").Value)
                End If
                'Close the engineer strength recordset.
                rsEngineerStrength.Close()

                strSql = "SELECT * FROM ActionData WHERE " _
                        & "Obj1Group='" & strEngineerUnit & "' AND " _
                        & "Mission='" & basFiles.StrMissionName & "';"
                modDB.openDBRecordset(item, rsEngineerLoss, strSql)
                While rsEngineerLoss.EOF = False And intEngineerStrength > 0
                    If rsEngineerLoss("Event").Value = "destroyed by" Or _
                       rsEngineerLoss("Event").Value = "crashed" Or _
                       rsEngineerLoss("Event").Value = "sank" Then
                        intEngineerStrength += -1
                    End If
                    'Go to the next record in the recordset.
                    rsEngineerLoss.MoveNext()
                End While
                'Close the engineer strength recordset.
                rsEngineerLoss.Close()

                'Right, we have calculated the loss-adjusted engineer unit strength. Now we determine
                'whether the target airbase is appropriately controlled and close to the engineers. 
                'If the engineers are still active, destroy ALL aircraft at the airbase and empty the fuel holding.
                intEngineerStrength = Math.Max(intEngineerStrength, 0)
                If intEngineerStrength > 0 Then

                    'Find the targeted airbase.
                    strSql = "SELECT * FROM Airbases WHERE Map='" & strThisMap & "' AND " _
                            & "Airbase_Name='" & modDB.strSQLquote(strAirbaseTarget) & "';"
                    modDB.openDBRecordset(item, rsAirbaseTarget, strSql)

                    'Check the proximity of the airbase and its current alignment.
                    If rsAirbaseTarget.EOF = False Then
                        AX = CDbl((rsAirbaseTarget("Takeoff_X").Value + rsAirbaseTarget("Landing_X").Value)) / 2.0
                        AY = CDbl((rsAirbaseTarget("Takeoff_Y").Value + rsAirbaseTarget("Landing_Y").Value)) / 2.0
                        dblDist = modUtilities.dblGetDistance(E_X, E_Y, AX, AY)
                        intDice = CInt(100.0 * Rnd())

                        If dblDist <= CS_Control_Radius And _
                           intDice <= basMain.intDemolitionSuccess And _
                           (rsAirbaseTarget("Control").Value = "n" Or _
                           rsAirbaseTarget("Control").Value = strEngineerAlignment) Then

                            'OK, we are all set to demolish the airbase!
                            'First, destroy the fuel holding.
                            If CS_Track_Fuel_Consumption = 1 Then
                                strSql = "UPDATE Airbases Set Fuel_Holding = 0 WHERE " _
                                        & "Airbase_Name='" & modDB.strSQLquote(strAirbaseTarget) & "' AND " _
                                        & "Map='" & strThisMap & "';"
                                modDB.ExecuteDBCommand(item, strSql)
                            End If

                            'Now create an action event for the airbase demolition.
                            'Create SQL Statement
                            strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, " _
                            & "Obj2, Obj2Group, Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                            & "INF_" & strAirbaseTarget & "', '" _
                            & "Infrastructure', 'demolished by', '" _
                            & modDB.strSQLquote(strEngineerUnit & "0") & "', '" _
                            & modDB.strSQLquote(strEngineerUnit) & "', '" _
                            & basFiles.StrMissionName & "', '" _
                            & CInt(AY) & "', '" & CInt(AX) & "', '" _
                            & strEndOfMission & "')"
                            Call modDB.ExecuteDBCommand(item, strSql)


                            'Now, destroy every aircraft at the base.
                            strSql = "SELECT * FROM ObjMissionData,Object_Specifications WHERE " _
                                    & "ObjMissionData.Obj_Type=Object_Specifications.Object_Type AND " _
                                    & "ObjMissionData.Mission_Name='" & basFiles.StrMissionName & "' AND " _
                                    & "LEFT(Object_Specifications.Object_Class,1)='P' AND " _
                                    & "ObjMissionData.Quantity > 0 AND " _
                                    & "(ObjMissionData.Start_X_Axis-" & AX & ")*(ObjMissionData.Start_X_Axis-" & AX & ") + " _
                                    & "(ObjMissionData.Start_Y_Axis-" & AY & ")*(ObjMissionData.Start_Y_Axis-" & AY & ") < " _
                                    & CDbl(CS_Control_Radius * CS_Control_Radius) & ";"
                            modDB.openDBRecordset(item, rsAircraftDestruction, strSql)

                            'For each flight located at the demolished airbase...
                            While rsAircraftDestruction.EOF = False

                                'Calculate the number of aircraft in the flight.
                                intFlightStrength = rsAircraftDestruction("Quantity").Value
                                strFlight = rsAircraftDestruction("Obj_Group_Name").Value

                                'Place a destruction event for each aircraft.
                                intDemolish = 0
                                While intDemolish < intFlightStrength

                                    'Calculate the demolition site for this aircraft.
                                    dblX = AX + 1000.0 * (Rnd() - 0.5)
                                    dblY = AY + 1000.0 * (Rnd() - 0.5)

                                    'Create SQL Statement
                                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, " _
                                    & "Obj2, Obj2Group, Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                                    & modDB.strSQLquote(strFlight & CStr(intDemolish)) & "', '" _
                                    & modDB.strSQLquote(strFlight) & "', 'demolished by', '" _
                                    & modDB.strSQLquote(strEngineerUnit & "0") & "', '" _
                                    & modDB.strSQLquote(strEngineerUnit) & "', '" _
                                    & basFiles.StrMissionName & "', '" _
                                    & CInt(dblY) & "', '" & CInt(dblX) & "', '" _
                                    & strEndOfMission & "')"
                                    Call modDB.ExecuteDBCommand(item, strSql)

                                    intDemolish += 1
                                End While

                                'Set the flight strength to zero.
                                strSql = "UPDATE ObjMissionData SET Quantity = 0, " _
                                        & "Fuel = 0, Fuel_Freight = 0 WHERE " _
                                        & "Obj_Group_Name = '" & rsAircraftDestruction("Obj_Group_Name").Value & "' AND " _
                                        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                                Call modDB.ExecuteDBCommand(item, strSql)

                                'Unload every unit carried as freight by the flight.
                                strSql = "UPDATE ObjMissionData SET Transporting_Group = 'Independent' WHERE " _
                                        & "Transporting_Group = '" & rsAircraftDestruction("Obj_Group_Name").Value & "' AND " _
                                        & "Mission_Name = '" & basFiles.StrMissionName & "';"
                                Call modDB.ExecuteDBCommand(item, strSql)

                                'Next flight at the airbase.
                                rsAircraftDestruction.MoveNext()

                            End While

                            'Close the aircraft destruction list.
                            rsAircraftDestruction.Close()

                        End If
                    End If

                    'Close the airbase target recordset.
                    rsAirbaseTarget.Close()

                Else
                    'The engineer unit is destroyed - no demolition required.
                End If

                'If the engineer unit has suffered losses, then update its strength.
                If intEngineerStrength <> intInitialEngineerStrength Then
                    strSql = "UPDATE ObjMissionData SET Quantity = " & intEngineerStrength & " WHERE " _
                            & "Obj_Group_Name = '" & strEngineerUnit & "' AND " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)
                End If

                'Go to the next record in the demolition recordset.
                rsDemolitionOrders.MoveNext()
            End While

            'Close the demolition recordset.
            rsDemolitionOrders.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subDemolishAirbases: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub





    'This routine gets the date of the mission to be analyzed, either by seeking user input
    'if this is the first mission of a new campaign, or by reading the date of the last mission
    'in an existing campaign from the database.
    Private Sub subGetMissionDate(ByRef frm As Form, ByVal item As Integer, ByRef dtMissionDate As Date)
        Dim strSql As String
        'Dim frmCalendar As New frmSECalendar
        Dim rsMissionData As New ADODB.Recordset

        Try

            If Not modCfg.User(item).IsCampaignInitialized Then

                If basMain.MISYEAR >= 1930 And basMain.MISYEAR <= 1960 And _
                   basMain.MISMONTH <= 12 And basMain.MISDAY <= 31 Then
                    'Campaign is not initialized and a date is supplied in mission file.
                    'Use this data to build the initial campaign date.
                    dtMissionDate = DateSerial(basMain.MISYEAR, basMain.MISMONTH, basMain.MISDAY)

                Else
                    'Campaign is not initialized and no date supplied in mission file.
                    'Ask the user to enter the initial campaign date, using a Calendar control.
                    frmSECalendar.ShowDialog()
                    If frmSECalendar.DialogResult = frmSECalendar.DialogResult.OK Then
                        dtMissionDate = frmSECalendar.contMonthCalendar.SelectionStart
                    End If
                End If


            Else

                'Campaign may not be initialized - check for previous mission in this theatre.
                strSql = "SELECT * FROM MissionData WHERE " _
                        & "Theatre = '" & modCfg.User(item).TxtTheatre & "' ORDER BY Mis_Nbr"
                Call modDB.openDBRecordset(item, rsMissionData, strSql)
                If rsMissionData.EOF = True Then
                    'Campaign is not initialized -  establish the starting date.
                    modCfg.User(item).IsCampaignInitialized = False
                    If basMain.MISYEAR >= 1930 And basMain.MISYEAR <= 1960 And _
                       basMain.MISMONTH <= 12 And basMain.MISDAY <= 31 Then
                        'Campaign is not initialized and a date is supplied in mission file.
                        'Use this data to build the initial campaign date.
                        dtMissionDate = DateSerial(basMain.MISYEAR, basMain.MISMONTH, basMain.MISDAY)

                    Else
                        'Campaign is not initialized and no date supplied in mission file.
                        'Ask the user to enter the initial campaign date, using a Calendar control.
                        frmSECalendar.ShowDialog()
                        If frmSECalendar.DialogResult = frmSECalendar.DialogResult.OK Then
                            dtMissionDate = frmSECalendar.contMonthCalendar.SelectionStart
                        End If
                    End If
                Else
                    'Campaign is already initialized - read out the date of the last mission.
                    rsMissionData.MoveLast()
                    dtMissionDate = CDate(rsMissionData("Mis_Date").Value)
                    dtMissionDate = DateAdd(DateInterval.Hour, CDbl(rsMissionData("Mis_Time").Value) + CDbl(CS_Mission_Length), dtMissionDate)
                    Call basMess.StatusLabelTxt(rsMissionData("Map").Value & " Campaign, date " _
                                        & Format(dtMissionDate, "dd MMM yyyy") & ", at " & Format(dtMissionDate, "HH:00") & " hours ", True)
                    strPreviousMissionFileName = rsMissionData("Mission_Name").Value
                    strPreviousMap = rsMissionData("Map").Value
                End If
                rsMissionData.Close()

            End If

            'Free up the resources in frmCalendar.
            frmSECalendar.Hide()
            Call basMess.FormRefresh(frm)

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGetMissionDate: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Sub


    'This routine looks at the [MAIN] section of the mission file and pulls out the mission time of day, the
    'campaign sector and weather conditions. It also asks the user to provide a campaign date.
    Private Function subLoadEnvironment(ByRef frm As Form, ByVal item As Integer, ByRef strLine As String) As Boolean
        Dim strLineArray() As String
        Dim strMapLoader As String
        Dim dtMissionDate As Date
        Dim strMissionDate As String
        Dim dblTime As Double
        Dim strSql As String

        Try
            'Initialize the success flag.
            subLoadEnvironment = True

            'Read header information (map, time, date, weather) from the mission file.
            If isLoadMissionHeaders(item) = False Then
                subLoadEnvironment = False
                Exit Function
            End If

            'Get the date of the mission to be analyzed.
            Call subGetMissionDate(frm, item, dtMissionDate)
            dtMissionStartDate = dtMissionDate

            'Determine the national opponents etc.
            Call subGatherCombatants(item)
            Call modUtilities.subGatherAirForces(item)
            Call modUtilities.subgatherArmyOrganizations(item)

            'Get all active metamaps.
            Call modUtilities.subGatherMetaMaps(item)

            'If we are adding forces, bypass most of the rest of this subroutine.
            If isAddForce = False Then

                'Gather the commencement time (of day) of the mission from the "TIME" line of the mission file.
                'Truncate it to an integer representation: SEOW runs missions on the hour.
                dblTime = basMain.MISHOUR

                'Convert the date and time into a sequence number for use in subUpdateCampaignStatus
                If Not modCfg.User(item).IsCampaignInitialized Then
                    strLastMissionSequenceNumber = DT2SN(DateAdd(DateInterval.Hour, dblTime, dtMissionDate))
                Else
                    strLastMissionSequenceNumber = DT2SN(dtMissionDate)
                End If


                'Write this mission into the MissionData table. If the campaign is being initialized, the 
                'following line will set the duration of the initial mission to zero. The call to subMissionDuration
                'in subLoadEventReports is only made for subsequent mission analysis.
                'strMissionDate = "#" & dtMissionDate & "#"
                strSql = "INSERT INTO MissionData (Mission_Name,Duration,Mis_Time," _
                            & "Host,Theatre,Map,Mis_Date,Mis_Commencement) VALUES " _
                            & "('" & basFiles.StrMissionName & "'," _
                            & "0," _
                            & dblTime & "," _
                            & "'" & modCfg.User(item).TxtHost & "', " _
                            & "'" & modCfg.User(item).TxtTheatre & "', " _
                            & "'" & strThisMap & "', '" _
                            & Format(SN2DT(strLastMissionSequenceNumber), strMissionDateFormat) & "', '" _
                            & "not hosted')"
                Call modDB.ExecuteDBCommand(item, strSql)


                'Construct a lists of registered recon, supply and assault flights.
                Call ProgressBarLabel("Gathering registered recon, supply drop and assault flights ...")
                Call ProgressBarPerformStep()
                Call subGatherReconFlights(item)
                Call subGatherSupplyDropFlights(item)
                Call subGatherParatrooperAssaultFlights(item)

                'Call the subWeather subroutine in the modWeather module to generate the upcoming weather conditions.
                Call ProgressBarLabel("Generating weather ...")
                Call ProgressBarPerformStep()
                Call modWeather.subWeather(item, strLine, strThisMap, dblTime, dtMissionDate)

            End If

            'Close the .mis file so it can be re-positioned at the [WING] section.
            srMissionMA.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadEnvironment: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Function


    'This routine looks at the [MAIN] section of the mission file and pulls out the mission time of day, the
    'campaign sector and weather conditions. It also asks the user to provide a campaign date.
    Private Function isLoadMissionHeaders(ByVal item As Integer) As Boolean
        Dim strLine, strLineArray() As String
        Dim strMapLoader As String
        Dim strMissionDate As String
        Dim strSql As String

        Try
            'Initialize the success flag.
            isLoadMissionHeaders = True


            'Step through the mission file until the [Wing] section is found.
            While InStr(UCase(strLine), "[WING]") < 1 And _
                  InStr(UCase(strLine), "[NSTATIONARY]") < 1 And _
                  InStr(UCase(strLine), "[BUILDINGS]") < 1

                If InStr(strLine, "MAP") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    strMapLoader = Trim(strLineArray(1))
                    strThisMap = strMapFromLoader(item, strMapLoader)
                    If strThisMap = "unsupported map" Then
                        isLoadMissionHeaders = False
                        Exit Function
                    End If

                ElseIf InStr(strLine, "TIME") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.MISHOUR = CDbl(CInt(strLineArray(1)))

                ElseIf InStr(strLine, "CloudType") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISCLOUDTYPE = CInt(strLineArray(1))

                ElseIf InStr(strLine, "CloudHeight") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISCLOUDHEIGHT = CInt(strLineArray(1))

                ElseIf InStr(strLine, "Year") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.MISYEAR = CInt(strLineArray(1))

                ElseIf InStr(strLine, "Month") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.MISMONTH = CInt(strLineArray(1))

                ElseIf InStr(strLine, "Day") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.MISDAY = CInt(strLineArray(1))

                ElseIf InStr(strLine, "WindDirection") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISWINDDIRECTION = CDbl(strLineArray(1))

                ElseIf InStr(strLine, "WindSpeed") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISWINDSPEED = CDbl(strLineArray(1))

                ElseIf InStr(strLine, "Gust") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISWINDGUST = CInt(strLineArray(1))

                ElseIf InStr(strLine, "Turbulence") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    modWeather.MISWINDTURBULENCE = CInt(strLineArray(1))

                ElseIf InStr(strLine, " Bigship ") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.RESPAWNBIGSHIP = CInt(strLineArray(1))

                ElseIf InStr(strLine, " Ship ") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.RESPAWNSHIP = CInt(strLineArray(1))

                ElseIf InStr(strLine, " Aeroanchored ") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.RESPAWNAEROANCHORED = CInt(strLineArray(1))

                ElseIf InStr(strLine, " Artillery ") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.RESPAWNARTILLERY = CInt(strLineArray(1))

                ElseIf InStr(strLine, " Searchlight ") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    basMain.RESPAWNSEARCHLIGHT = CInt(strLineArray(1))

                End If

                'Read the next line from the mission file.
                strLine = srMissionMA.ReadLine()

            End While


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isLoadMissionHeaders: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Function


    'This routine processes all the data in the [WING] section of the mission file.
    Private Sub subProcessWings(ByVal item As Integer, ByRef strLine As String, ByRef isValid As Boolean)
        Dim thisFlight As New SE_Unit
        Dim storedFlight As New SE_Unit
        Dim thisTransporter As New SE_Transporter
        Dim thisFlightName As String
        Dim strLineArray() As String
        Dim strSql As String
        Dim strObjType As String
        Dim rsGroup As New ADODB.Recordset
        Dim rsCarrier As New ADODB.Recordset
        Dim rsReinforcement, rsDriveable As New ADODB.Recordset
        Dim isReinforcement, isTopUp, isDriveable As Boolean
        Dim isFirst As Boolean
        Dim dblTakeoffX As Double
        Dim dblTakeoffY As Double
        Dim intFormCounter As Integer
        Dim thisAliasFlightName, thisTrueFlightName As String

        Try

            'This Boolean marks whether an invalid Flight has been detected.
            isValid = True

            'Open the .mis file, take it from the top...
            srMissionMA = File.OpenText(basFiles.StrAbsoluteMissionFileName)

            'Step through the mission file until the [WING] section is found.
            'This little loop ignores any occurrences of TIMECONSTANT or WEAPONSCONSTANT lines.
            Do Until InStr(UCase(strLine), "[WING]") > 0
                strLine = srMissionMA.ReadLine()
                If InStr(UCase(strLine), "[CHIEFS]") > 0 Or _
                   InStr(UCase(strLine), "[NSTATIONARY]") > 0 Then
                    Exit Sub
                End If
            Loop

            'We are now at the [WING] line in the mission file. This is followed by a list of names
            'for each of the active air flights in this mission. Then, after this list, details and 
            'waypoints are listed for each flight in turn. We will step through this structure, collecting
            'flight-related information as we go and storing that information in the SE_Units hashtable.
            'Then at the end of the WING section, we write this information to the database.

            'Read the next line of the mission file, should be either a flight name, a flight details section header,
            'a flight waypoints section header, or, if Wings are exhausted, the [Chiefs] or [NStationary] line etc.
            strLine = srMissionMA.ReadLine()

            intFormCounter = 0
            Do Until InStr(strLine, "[") = 1 Or _
                    InStr(UCase(strLine), "[CHIEFS]") > 0 Or _
                    InStr(UCase(strLine), "[NSTATIONARY]") > 0 Or _
                    InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                    InStr(UCase(strLine), "[BUILDINGS]") > 0

                'Set thisFlight structure properties to default values.
                Call modUtilities.subSetUnitDefault(thisFlight)

                'Get the flight name.
                thisFlightName = Trim(strLine)
                thisFlight.Object_Group_Name = thisFlightName

                'Is this an Add Force call?
                If isAddForce And InStr(strAFCurrentUnits, thisFlightName & "|") > 0 Then
                    isValid = False
                    Call basMess.MessageBox("Trying to add flight """ & thisFlightName & """ that already exists in the OOB - aborting Add Forces!", vbCritical)
                    Exit Sub
                End If

                'Check for Reinforcement flight - gather the name of the flight to be reinforced.
                If CInt(Mid(thisFlightName, Len(thisFlightName) - 1)) >= 30 And _
                   (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And _
                   isHostFlight(thisFlightName) = False Then

                    'The flight has a reserved name. Tell the user of the error and flush the DB.
                    frmReservedFlight.ReservedFlight(basFiles.StrMissionName, thisFlightName)
                    Call subFlushDB(item)


                    'isCampaignInitialized = False
                    modCfg.User(item).IsCampaignInitialized = False

                    isValid = False
                    Exit Sub

                ElseIf CInt(Mid(thisFlightName, Len(thisFlightName) - 1)) >= 30 And _
                    (isHostFlight(thisFlightName) = False Or CS_Create_Host_Seat = 0) Then
                    'The present flight could be a reinforcement flight!
                    strSql = "SELECT * FROM Unit_Name_Mapping, Resupply_Replacements WHERE " _
                            & "Unit_Name_Mapping.InGame_Name = '" & thisFlightName & "' " _
                            & "AND Unit_Name_Mapping.Unit_Name = Resupply_Replacements.Unit_Name " _
                            & "AND Resupply_Replacements.Used = 1 " _
                            & "AND Resupply_Replacements.Map = '" & strThisMap & "' " _
                            & "AND Unit_Name_Mapping.Mission = '" & basFiles.StrMissionName & "'"
                    Call modDB.openDBRecordset(item, rsReinforcement, strSql)
                    If rsReinforcement.EOF = False Then
                        'This is a top-up reinforcement flight. Store the name of the understrength unit in
                        'a temporary hash item with key equal to the reinforcement flight's name.
                        isTopUp = True
                        If modCfg.User(item).DatabaseProvider = "mysql" Then
                            thisFlight.Parent = rsReinforcement("Unit_Name").Value
                        Else
                            thisFlight.Parent = rsReinforcement("Resupply_Replacements.Unit_Name").Value
                        End If
                    End If
                    rsReinforcement.Close()

                    If isTopUp = False Then
                        'The present flight could be a flight alias for a driveable vehicle!
                        strSql = "SELECT * FROM Unit_Name_Mapping WHERE " _
                                & "Unit_Name_Mapping.InGame_Name = '" & thisFlightName & "' " _
                                & "AND Unit_Name_Mapping.InGame_Name <> Unit_Name_Mapping.Unit_Name " _
                                & "AND Unit_Name_Mapping.Mission = '" & basFiles.StrMissionName & "';"
                        Call modDB.openDBRecordset(item, rsDriveable, strSql)
                        If rsDriveable.EOF = False Then
                            'This is a flight alias for a driveable vehicle.
                            isDriveable = True
                            hashDriveable(thisFlightName) = rsDriveable("Unit_Name").Value
                            thisFlight.Object_Group_Name = rsDriveable("Unit_Name").Value
                        End If
                        rsDriveable.Close()
                    End If
                End If

                'Create a hashtable Key for this wing name and load the wing data structure into the Value property.
                If hashUnit.ContainsKey(thisFlightName) = False And isDriveable = False Then
                    'No unit of this name exists in the database. This might happen if the current
                    'mission is the first in a campaign, or if someone edited the last mission file
                    'and added a new flight. We could ignore the new flight, or accept it. Let's accept it
                    'as long as it it isn't the host flight or a reinforcement flight.
                    If isHostFlight(thisFlightName) = False Then
                        hashUnit(thisFlightName) = thisFlight
                    End If
                Else
                    'A unit of this name already exists in the database, probably from the last mission.
                    'No problems - just go ahead and capture its data as normal.
                End If

                'Read the next line in the mission file.
                isTopUp = False
                isDriveable = False
                strLine = srMissionMA.ReadLine()


                'Refresh main form periodically.
                intFormCounter += 1
                If 20 * Math.Floor(intFormCounter / 20.0) = intFormCounter Then
                    frmMain.Refresh()
                End If

            Loop



            'Now we should be at the beginning of the flights' details and waypoints lists. We will loop over
            'these flight header lines, processing all information until the next flight header line, etc.
            Do Until InStr(UCase(strLine), "[CHIEFS]") > 0 Or _
                    InStr(UCase(strLine), "[NSTATIONARY]") > 0 Or _
                    InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                    InStr(UCase(strLine), "[BUILDINGS]") > 0


                'The flight details subsection..............

                'Get the flight name: the line should look like "[I_JG2300]" or similar.
                thisFlightName = Trim(Replace(Replace(strLine, "[", ""), "]", ""))

                'Maybe this an alias flight name for a driveable unit.
                'Check to see if this flight is an alias. If so, substitute the true vehicle name.
                If hashDriveable.ContainsKey(thisFlightName) = True Then
                    thisAliasFlightName = thisFlightName                'The name of the alias flight in .mis file.
                    thisFlightName = hashDriveable(thisFlightName)      'The name of the actual vehicle unit.
                    thisTrueFlightName = thisFlightName                 'The name of the actual vehicle unit (backup).
                    isDriveable = True
                Else
                    isDriveable = False
                End If

                'Check that we have already initialized this hash key. 
                If hashUnit.ContainsKey(thisFlightName) = True Then
                    'Everything is OK, we have found the hash key of the flight as built in the earlier loop. Now
                    'just load the hash value structure into a local structure.
                    thisFlight = hashUnit(thisFlightName)
                End If
                strLine = srMissionMA.ReadLine()


                'Check for "Planes" quantity directive.
                If InStr(strLine, "Planes") <> 0 Then
                    strLineArray = Split(Trim(strLine))
                    thisFlight.Quantity = Trim(strLineArray(1))
                    strLine = srMissionMA.ReadLine()
                End If

                'Check for "OnlyAI" directive.
                If InStr(strLine, "OnlyAI") <> 0 Then
                    strLineArray = Split(Trim(strLine))
                    thisFlight.OnlyAI = Trim(strLineArray(1))
                    strLine = srMissionMA.ReadLine()
                End If

                'Check for "Parachute" availability directive. Ignore it if found.
                If InStr(strLine, "Parachute") <> 0 Then
                    strLine = srMissionMA.ReadLine()
                End If

                'Check for "Skill" directives.
                Do Until InStr(strLine, "Skill") = 0
                    strLineArray = Split(Trim(strLine))

                    If strLineArray(0) = "Skill" Then
                        thisFlight.Skill = Trim(strLineArray(1))
                        strLine = srMissionMA.ReadLine()
                    ElseIf strLineArray(0) = "Skill0" Then
                        thisFlight.Skill0 = Trim(strLineArray(1))
                        strLine = srMissionMA.ReadLine()
                    ElseIf strLineArray(0) = "Skill1" Then
                        thisFlight.Skill1 = Trim(strLineArray(1))
                        strLine = srMissionMA.ReadLine()
                    ElseIf strLineArray(0) = "Skill2" Then
                        thisFlight.Skill2 = Trim(strLineArray(1))
                        strLine = srMissionMA.ReadLine()
                    ElseIf strLineArray(0) = "Skill3" Then
                        thisFlight.Skill3 = Trim(strLineArray(1))
                        strLine = srMissionMA.ReadLine()
                    End If

                Loop

                'Ignore all explicit skin designation lines except the first one for each flight.
                Do Until InStr(strLine, "  skin") = 0
                    strLineArray = Split(Trim(strLine))

                    If strLineArray(0) = "skin0" And CS_Use_Skins = 1 Then
                        thisFlight.Skin = Trim(Right(Trim(strLine), Len(Trim(strLine)) - InStr(Trim(strLine), "skin0 ") - 5))
                        thisFlight.Skin = modUtilities.strEncodeQ(thisFlight.Skin)
                    End If

                    strLine = srMissionMA.ReadLine()
                Loop

                'Ignore all explicit pilot skin designation lines.
                Do Until InStr(strLine, "  pilot") = 0
                    strLine = srMissionMA.ReadLine()
                Loop

                'Ignore all explicit numbering lines.
                Do Until InStr(strLine, "numberOn") = 0
                    strLine = srMissionMA.ReadLine()
                Loop

                'We now should be at the aircraft "Class" line.
                If InStr(strLine, "Class") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    strObjType = Trim(Mid(strLineArray(1), InStrRev(strLineArray(1), ".") + 1))

                    'Check to see if this aircraft type is present in the database.
                    strSql = "SELECT * FROM Object_Specifications where Object_Type = '" & strObjType & "'"
                    Call modDB.openDBRecordset(item, rsGroup, strSql)
                    If rsGroup.EOF = True Then

                        If modCfg.User(item).IsCampaignInitialized = False Then
                            Call basMess.MessageBox("Unknown Aircraft type: " & strObjType & ". Check template compatibility with your HistorySFX Mod setting in the ""Campaign Modes"" tab." & vbCrLf & "The initialization will be aborted and the database will be flushed now.", vbCritical)
                            isValid = False
                            Call modUtilities.subFlushDB(item)
                            Exit Sub
                        Else
                            Call basMess.MessageBox("modMissionAnalyzer.subprocessWings: Object type " _
                                        & strObjType & " not found in Object_Specifications table", vbInformation)
                        End If


                    Else
                        'set various parameters from Object_Specifications table.
                        thisFlight.Object_Type = strObjType
                        thisFlight.Object_Class = rsGroup("Object_Class").Value
                        thisFlight.RefuelingTime = rsGroup("Refueling_Time").Value
                        thisFlight.Signals = rsGroup("Default_Signals").Value
                        thisFlight.ActivatedBy = ""
                        thisFlight.ActivationTime = 0

                        If modCfg.User(item).IsCampaignInitialized And Not isAddForce Then
                            'Copy the nationality from the previous mission.
                            thisFlight.Nationality = rsGroup("Nationality").Value
                        Else
                            'Assign nationality based on the flight name.
                            'The effect of this is to allow the use of captured units as specified
                            'in the template.
                            thisFlight.Nationality = strFlightAlignment(item, thisFlight.Object_Group_Name)
                        End If

                    End If
                    rsGroup.Close()
                    strLine = srMissionMA.ReadLine()
                End If

                'If we are initializing a new campaign, set the flight skill, recon, morale according to the 
                'Campaign Skill section in the Options menu.
                If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then
                    Call subSetSkill(thisFlight)
                    Call subSetRecon(thisFlight)
                    Call subSetMorale(thisFlight)
                End If

                'Aircraft "fuel" directive. This is the load of fuel the aircraft brought into this 
                'initialisation mission. We will capture this fuel capacity percentage, but we won't use 
                'it in the campaign initialization stage.
                If InStr(UCase(strLine), "FUEL") > 0 Then
                    strLineArray = Split(Trim(strLine))
                    thisFlight.Fuel = Trim(strLineArray(1))
                    If CS_Track_Fuel_Consumption = 1 Then
                        thisFlight.Fuel = 0
                    Else
                        If isUntriggeredScrambler(thisFlight.Object_Group_Name) = True Then
                            modSupply.intGetSupplyFromLocalResource(item, thisFlight.Object_Group_Name, SUPPLY_RESOURCE_AIRBASE, Math.Ceiling(0.05 * thisFlight.Fuel), thisFlight.Object_Class, thisFlight.Nationality, thisFlight.X, thisFlight.Y, False)
                        Else
                            modSupply.intGetSupplyFromLocalResource(item, thisFlight.Object_Group_Name, SUPPLY_RESOURCE_AIRBASE, thisFlight.Fuel, thisFlight.Object_Class, thisFlight.Nationality, thisFlight.X, thisFlight.Y, False)
                        End If
                    End If
                    strLine = srMissionMA.ReadLine()
                End If

                'Aircraft weapons directive. This is the weapons loadout the aircraft brought into this 
                'initialisation mission. We will ignore this value - munitions will not be tracked.
                If InStr(UCase(strLine), "WEAPONS") > 0 Then
                    strLineArray = strLine.Split(" ")
                    thisFlight.Loadout = Trim(strLineArray(3))
                    If modCfg.User(item).IsCampaignInitialized And CS_Plane_Refit_Delays = 1 And Not isAddForce Then
                        thisFlight.Refueling = thisFlight.RefuelingTime + modUtilities.intLoadoutRearmDelay(item, thisFlight.Object_Type, thisFlight.Loadout)
                    Else
                        thisFlight.Refueling = 0  'Ready to fly again now - the default value for Initial Campaign Load.
                    End If

                    strLine = srMissionMA.ReadLine()
                End If

                'Aircraft StartTime directive. We will ignore this value.
                If InStr(UCase(strLine), "STARTTIME") > 0 Then
                    strLine = srMissionMA.ReadLine()
                End If


                'The flight waypoints subsection.......................
                'We should now be at a line like "[I_JG2300_Way]". We need to capture the ending location
                'of the flight which should be an airbase landing coordinate. An earlier version of the DCS 
                'captured the entire waypoints list and stored it in the Waypoints table, but we will avoid this now.
                If isDriveable = True Then
                    thisFlightName = thisAliasFlightName
                End If
                If InStr(strLine, "[" & thisFlightName & "_Way]") > 0 Then
                    strLine = srMissionMA.ReadLine()

                    'Step through the waypoints coords and grab the TAKEOFF and LANDING coords in particular.
                    'With the inclusion of Off Map and Withdraw missions, some flights do not actually have
                    'LANDING waypoints, so we need to just capture the last waypoint coordinates and use them
                    'as landing coords.
                    isFirst = True
                    Do Until InStr(strLine, "[") > 0
                        strLineArray = Split(Trim(strLine))
                        If isFirst = True Then
                            dblTakeoffX = CDbl(strLineArray(1))
                            dblTakeoffY = CDbl(strLineArray(2))
                            isFirst = False
                        End If
                        'If strLineArray(0) = "LANDING" Then
                        '   thisFlight.X = CDbl(strLineArray(1))
                        '   thisFlight.Y = CDbl(strLineArray(2))
                        'End If

                        'Only track the planned waypoints for non-driveable flights.
                        If isDriveable = False And isUntriggeredScrambler(thisFlight.Object_Group_Name) = False Then
                            thisFlight.X = CDbl(strLineArray(1))
                            thisFlight.Y = CDbl(strLineArray(2))
                        End If

                        'Keep track of planned waypoints for scrambled flights
                        If InStr(strMissionDesignatedScramblers, "|" & thisFlightName & "|") > 0 Then
                            thisFlight.Scrambled_X = CDbl(strLineArray(1))
                            thisFlight.Scrambled_Y = CDbl(strLineArray(2))
                        End If


                        If strLineArray(0) = "LANDING" And strLineArray.GetUpperBound(0) >= 6 Then
                            'This flight is landing on an aircraft carrier or temporary strip.
                            'In this case we need to capture the raw name of the target object.
                            'The raw name will be converted to a proper name later on, if appropriate.

                            'Capture the transporting object for the current flight.
                            thisFlight.Transporting_Group = strLineArray(5)

                            'Carrier flights take longer than usual to refuel/rearm.
                            If modCfg.User(item).IsCampaignInitialized And Not isAddForce Then

                                'Is the transporter a carrier?
                                strSql = "SELECT * FROM Unit_Name_Mapping WHERE " _
                                        & "Mission = '" & basFiles.StrMissionName & "' AND " _
                                        & "InGame_Name = '" & thisFlight.Transporting_Group & "';"
                                Call modDB.openDBRecordset(item, rsCarrier, strSql)
                                If rsCarrier.EOF = False Then
                                    If modUtilities.isCarrier(item, rsCarrier("Unit_Name").Value, basFiles.StrMissionName) = True Then
                                        thisFlight.Refueling += 1
                                    End If
                                End If
                                rsCarrier.Close()
                            End If

                            'Add the transporting object to the Transporters hashtable.
                            If hashTransporter.ContainsKey(thisFlight.Transporting_Group) = False And _
                               thisFlight.Transporting_Group <> "Disembark" Then
                                'initialize a dummy Transporter structure and store this as the value.
                                modUtilities.subSetTransporterDefault(thisTransporter)
                                hashTransporter(thisFlight.Transporting_Group) = thisTransporter
                            End If

                        ElseIf strLineArray(0) = "LANDING" Then
                            'In this case we have a flight rebasing to a standard airbase, so it must 
                            'not be being transported!
                            'Bug reported by doubletap, 8 February 2008.
                            thisFlight.Transporting_Group = "Independent"
                        End If

                        strLine = srMissionMA.ReadLine()
                    Loop

                    If dblGetDistance(dblTakeoffX, dblTakeoffY, thisFlight.X, thisFlight.Y) > 4000.0 And _
                       isUntriggeredScrambler(thisFlight.Object_Group_Name) = False And _
                       CS_Dynamic_Reconnaissance = 1 Then
                        'The air unit has rebased, so set its Recon back to zero. There are no free lunches
                        'for enemy observers!
                        If modCfg.User(item).IsCampaignInitialized And Not isAddForce Then
                            thisFlight.Recon = 0
                        Else
                            Call subSetRecon(thisFlight)
                        End If
                    End If

                    If modCfg.User(item).IsCampaignInitialized And Not isAddForce And _
                        thisFlight.Object_Class = "PSE" And _
                        isUntriggeredScrambler(thisFlight.Object_Group_Name) = False And _
                        isSeaplaneTransit(item, strThisMap, thisFlight.X, thisFlight.Y) = True Then
                        'The air unit is a seaplane and it has landed at a mid-ocean transit point.
                        thisFlight.Refueling = 0
                    End If

                    'We can reduce the refueling time by 1 hour if there are AC_starter objects at the local airfield.
                    If isACStarterAvailable(thisFlight.Nationality, thisFlight.X, thisFlight.Y) = True Then
                        thisFlight.Refueling = Math.Max(1, thisFlight.Refueling - 1)
                    End If

                    'We also need to check for mismatches between landing coordinates and 
                    'the airbase coordinates listed in the DB.
                    'Especially useful for loading templates with temporary airbases.
                    If (Not modCfg.User(item).IsCampaignInitialized) And Not isAddForce And CS_Temporary_Airbases = 1 Then
                        Call modUtilities.strAirbaseLandingCoord(item, strThisMap, thisFlight.X, thisFlight.Y)
                    End If

                    'We need to apply some supply consumption to all driveable flights.
                    If isDriveable = True And CS_Track_Fuel_Consumption = 1 Then
                        storedFlight = hashUnit(thisTrueFlightName)
                        storedFlight.X = thisFlight.X
                        storedFlight.Y = thisFlight.Y
                        storedFlight.Fuel -= modSupply.intSupplyUseByTime(CS_Mission_Length, _
                                                                          storedFlight.Speed, _
                                                                          storedFlight.Range, _
                                                                          storedFlight.Fuel_Capacity, _
                                                                          storedFlight.Object_Class, _
                                                                          storedFlight.Nationality)
                        storedFlight.Fuel = Math.Max(storedFlight.Fuel, 0)
                        hashUnit(thisTrueFlightName) = storedFlight
                    End If


                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subprocessWings: Cannot locate waypoints for " & strObjType, vbInformation)
                End If

                'Finally, we are in a position to suggest a human-readable name for this flight, and also set
                'the parent (hierarchical) name equal to the corresponding Regimental/Gruppe name given simply
                'by stripping the two digits (squadron and flight) off the end of the Group Name. We should also be
                'careful to take note of temporary reinforcement flights. By convention, any flight with the
                'last two digits of its flight name >= 30 is a temporary reinforcement flight on a transfer
                'mission to re-strength a weakened existing flight. Such reinforcement flights have had the name
                'of the understrength unit stored in their temporary hashtable entries, so we don't want to
                'overwrite that information just yet.
                If isDriveable = False And modCfg.User(item).IsCampaignInitialized = False Or isAddForce Then
                    'suggest a full name for this flight.
                    thisFlight.Unit_Name = modUtilities.Affiliation(item, thisFlightName, thisFlight.Object_Class, thisFlight.Nationality)
                ElseIf isDriveable = False Then
                    'We should already have a full name, so do nothing.
                    'thisFlight.Unit_Name = modUtilities.Affiliation(item, thisFlightName, thisFlight.Object_Class, thisFlight.Nationality)
                Else
                    'Maintain the driveable unit name.
                    'thisFlight.Unit_Name = modUtilities.Affiliation(item, thisTrueFlightName, thisFlight.Object_Class, thisFlight.Nationality)
                End If
                If CInt(Mid(thisFlightName, Len(thisFlightName) - 1)) >= 30 And isDriveable = False Then
                    isReinforcement = True
                ElseIf isDriveable = False Then
                    thisFlight.Parent = Left(thisFlight.Object_Group_Name, Len(thisFlight.Object_Group_Name) - 2)
                    isReinforcement = False
                ElseIf isDriveable = True Then
                    isReinforcement = False
                    thisFlightName = thisTrueFlightName
                End If

                'Now we have collected all the information for the current flight, so stuff it back into
                'the hashtable, with the following provisos:
                '(1) We need to discard the host flight. The reason we test now is that
                '    we need to go through most of the above logic to make sure we step 
                '    through the mission file correctly.
                '(2) We need to correctly allocate reinforcements. According to the MP 
                '    reinforcements must always be directed to where the understrength
                '    flight is located. Thus all we need to do is increment the Quantity
                '    of the understrength unit by the Quantity of the reinforcement flight, and then
                '    update the refueling time, e.g. even if only 1 plane has come in as a reinforcement
                '    to a weakened flight, there is a full refueling (liaison/briefing) time.
                '    We can then discard the rest of the reinforcement mission data in thisFlight.
                '(3) If this is not the first mission in a campaign, then we only need to 
                '    update a few properties for each flight - everything else will
                '    stay the same until event reports are processed.
                '(4) If the first 3 are satisfied, just update the hashtable with the data in thisFlight.
                If CS_Create_Host_Seat = 1 And isHostFlight(thisFlightName) = True Then
                    'Ignore case (1) 
                ElseIf isReinforcement = True Then
                    'Case (2), no reinforcement flights in campaign initialization mission. Go ahead
                    'and modify the quantity and refueling time in the understrength unit. Also scrub
                    'the hash item for the temporary reinforcement flight. We don't want that flight
                    'entered into the database.
                    storedFlight = hashUnit(thisFlight.Parent)
                    storedFlight.Quantity += thisFlight.Quantity
                    storedFlight.Refueling = thisFlight.Refueling
                    hashUnit(thisFlight.Parent) = storedFlight
                    hashUnit.Remove(thisFlightName)
                    isReinforcement = False
                ElseIf modCfg.User(item).IsCampaignInitialized And Not isAddForce Then
                    'Case (3)
                    storedFlight = hashUnit(thisFlightName)
                    storedFlight.Unit_Name = thisFlight.Unit_Name
                    storedFlight.X = thisFlight.X
                    storedFlight.Y = thisFlight.Y
                    storedFlight.Scrambled_X = thisFlight.Scrambled_X
                    storedFlight.Scrambled_Y = thisFlight.Scrambled_Y
                    storedFlight.Refueling = thisFlight.Refueling
                    storedFlight.Transporting_Group = thisFlight.Transporting_Group
                    hashUnit(thisFlightName) = storedFlight
                Else
                    'Case (4)
                    hashUnit.Item(thisFlightName) = thisFlight
                End If

                'Refresh main form periodically.
                intFormCounter += 1
                If 20 * Math.Floor(intFormCounter / 20.0) = intFormCounter Then
                    frmMain.Refresh()
                End If

            Loop


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subProcessWings: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Sub


    'This subroutine sets the flight skill according to the value of the Campaign Skill slider in
    'the options form. This routine is only called at template load time.
    Public Sub subSetSkill(ByRef thisFlight As SE_Unit)
        Dim intSkill As Integer
        Dim dblRandom As Double

        Try
            'Establish a random seed.
            Randomize()

            If thisFlight.Nationality = "r" Then
                intSkill = CS_Allied_AI_Skill
            Else
                intSkill = CS_Axis_AI_Skill
            End If

            If intSkill = 6 Then
                'This is the default template option. Just maintain the flight's skill level.
                intSkill = thisFlight.Skill

            ElseIf intSkill = 4 Or intSkill = 5 Then
                'This is the random/balanced option. Simply calculate a random variate and assign skill
                'based on the following simple thresholding function.
                dblRandom = Rnd()
                If dblRandom < 0.2 Then
                    intSkill = 0                'Rookie
                ElseIf dblRandom < 0.6 Then
                    intSkill = 1                'Average
                ElseIf dblRandom < 0.9 Then
                    intSkill = 2                'Veteran
                Else
                    intSkill = 3                'Ace
                End If

            ElseIf intSkill = 5 Then
                'Capture ship skills.
                intSkill = thisFlight.Skill

            End If


            thisFlight.Skill = intSkill
            thisFlight.Skill0 = intSkill
            thisFlight.Skill1 = intSkill
            thisFlight.Skill2 = intSkill
            thisFlight.Skill3 = intSkill

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subSetSkill: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This subroutine sets the unit morale.
    'This routine is only called at template load time.
    Public Sub subSetMorale(ByRef thisUnit As SE_Unit)
        Dim intSkill As Integer
        Dim dblRandom As Double

        Try

            If CS_Track_Fuel_Consumption = 0 Then

                'No Supply Tracking - all units have MORALE_EXCELLENT
                thisUnit.Morale = modSupply.MORALE_EXCELLENT

            Else

                'Supply Tracking is on - units have morale as determined by settings.
                Select Case thisUnit.Nationality
                    Case "r"
                        thisUnit.Morale = CS_Initial_Allied_Morale
                    Case "g"
                        thisUnit.Morale = CS_Initial_Axis_Morale
                End Select

            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subSetMorale: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub



    'This subroutine sets the unit recon according to the value of the Dynamic Recon check box in
    'the options form. This routine is only called at template load time.
    Public Sub subSetRecon(ByRef thisUnit As SE_Unit)
        Dim intRecon As Integer
        Dim dblRandom As Double

        Try
            'Establish a random seed.
            Randomize()

            'Upon initializing a campaign, we assign random recons to percentages of units. This allows commanders
            'to get an idea of the opposing forces.
            If CS_Dynamic_Reconnaissance = 1 Then
                'Dynamic recon model - set random recons for the percentage of units set in Options form.
                If thisUnit.Nationality = "g" And Rnd() < CDbl(CS_Initial_Campaign_Intelligence_Allied) / 100.0 Then
                    intRecon = CInt(100 * Rnd())
                ElseIf thisUnit.Nationality = "r" And Rnd() < CDbl(CS_Initial_Campaign_Intelligence_Axis) / 100.0 Then
                    intRecon = CInt(100 * Rnd())
                Else
                    intRecon = 0
                End If
            Else
                'Static recon model - all units are visible.
                intRecon = 100
            End If

            'Set the recon level for this unit.
            thisUnit.Recon = intRecon

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subSetRecon: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub





    Private Sub subProcessChiefs(ByVal item As Integer, ByRef strLine As String, ByRef isValid As Boolean)
        Dim hashRawName As New Hashtable
        Dim thisUnit, thisUnitSpeed As New SE_Unit
        Dim thisTransporter As New SE_Transporter
        Dim thisCU As New SE_CombinedUnit
        Dim thisMU As New SE_MetaUnit
        Dim strRawUnitName As String
        Dim strProperUnitName As String
        Dim strLineArray() As String
        Dim strSQLINSERT() As String
        Dim strSqlCore As String
        Dim strSqlCoreSpd As String
        Dim strSql As String
        Dim strSqlSpd As String
        Dim strObjType As String
        Dim dblWPX As Double
        Dim dblWPY As Double
        Dim dblSpeed As Double
        Dim dblTerrain As Double
        Dim intWPDelay As Integer
        Dim intWPCounter As Integer
        Dim intVehicle As Integer
        Dim intShip As Integer
        Dim rsUnitNameMapping As New ADODB.Recordset
        Dim rsGroup As New ADODB.Recordset
        Dim strChiefsDone, strDummy As String
        Dim intFormCounter As Integer
        Dim intFuelCapacity As Integer
        Dim dblDX, dblDY As Double
        Dim thisShipSpeedProfile As SE_MaxSpeedProfile
        Dim strSignals As String
        Dim strMetaObjectType As String

        Try
            'Set the completion status.
            isValid = False

            'Intialize the SQL strings.
            strSqlCoreSpd = "INSERT INTO Waypoints (Mission_Name,Unit_Group,Way_Nbr," _
                     & "Way_Y_Axis,Way_X_Axis,Way_Speed,Terrain,Delay) VALUES "
            strSqlCore = "INSERT INTO Waypoints (Mission_Name,Unit_Group,Way_Nbr," _
                     & "Way_Y_Axis,Way_X_Axis,Terrain,Delay) VALUES "
            strSqlSpd = strSqlCoreSpd
            strSql = strSqlCore

            Do Until InStr(UCase(strLine), "[CHIEFS]") > 0 Or InStr(UCase(strLine), "[NSTATIONARY]") > 0
                strLine = srMissionMA.ReadLine()
            Loop

            'We are now at the [Chiefs] line in the mission file. This is followed by a list of names
            'for each of the active ground units in this mission. Then, after this list, the 
            'waypoints are listed for each unit in turn. We will step through this structure, collecting
            'unit-related information as we go and storing that information in the SE_Units hashtable.
            'Then at the end of the [Chiefs] section, we write this information to the database.

            'Read the next line of the mission file, should be either a Chief name, 
            'a unit waypoints section header, or, if Chiefs are exhausted, the [NStationary] line etc.
            If InStr(UCase(strLine), "[CHIEFS]") > 0 Then
                strLine = srMissionMA.ReadLine()
            End If

            intFormCounter = 0
            Do Until InStr(strLine, "[") = 1 Or _
                    InStr(UCase(strLine), "[NSTATIONARY]") > 0 Or _
                    InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                    InStr(UCase(strLine), "[BUILDINGS]") > 0

                'Set thisUnit structure properties to default values.
                modUtilities.subSetUnitDefault(thisUnit)

                'Split the current line by spaces.
                strLineArray = Split(Trim(strLine))

                'Get the unit nationality, "1" = "r" and "2" = "g".
                thisUnit.Nationality = strNationality(Trim(strLineArray(2)))

                'Get the raw object type, something like "2-PzIIIJ" or "BikeBMW".
                strObjType = Trim(Mid(strLineArray(1), InStrRev(strLineArray(1), ".") + 1))

                'Get the object class, e.g. "VMI" or "T"
                thisUnit.Object_Class = strObjectClass(item, strObjType)

                If thisUnit.Object_Class = "" And _
                modCfg.User(item).IsCampaignInitialized = False Then
                    'Defective template!
                    isValid = False
                    modUtilities.subFlushDB(item)
                    Exit Sub
                End If

                'For new campaigns, load up the specified ship skills from the template. 
                'These may be overwritten at the end of this routine using subSetSkill().
                If Left(thisUnit.Object_Class, 1) = "S" And _
                   (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then
                    thisUnit.Skill = Trim(strLineArray(4))
                    thisUnit.Skill0 = Trim(strLineArray(4))
                    thisUnit.Skill1 = Trim(strLineArray(4))
                    thisUnit.Skill2 = Trim(strLineArray(4))
                    thisUnit.Skill3 = Trim(strLineArray(4))
                End If

                If thisUnit.Nationality = "n" And _
                   (thisUnit.Object_Class <> "INF" And thisUnit.Object_Class <> Nothing) Then

                    'Tell the user that the Chief has an invalid alignment.
                    Call basMess.MessageBox("Campaign Template format error in file '" & basFiles.StrMissionName & "'. " _
                                & "Combat objects must be listed with Allied (1) or Axis (2) alignment codes in the template file. '" _
                                & Trim(strLineArray(0)) & "' has invalid alignment code '" & Trim(strLineArray(2)) & "'." & vbCrLf _
                                & "Please flush the database, correct your template and try again.", vbCritical)
                    Exit Sub

                End If

                'Get the raw (in-game) unit name and transform it to the proper unit name.
                strRawUnitName = Trim(strLineArray(0))
                strProperUnitName = modUtilities.strMakeProperName(item, strRawUnitName, _
                                    thisUnit.Object_Class, _
                                    thisUnit.Nationality)

                If strProperUnitName = "invalid" Then
                    'Tell the user that the Chief has an invalid name.
                    'Call basMess.MessageBox("Campaign army generation error: " & vbCrLf _
                    '            & "The Sector " & strThisMap & " will now be flushed. Please edit the Army_units table and try again.", vbCritical)
                    Exit Sub
                End If

                'Ignore the strINFSignallerDummy object and go to the next line of the mission file.
                If strProperUnitName = basMissionBuilder.strINFSignallerDummy Then
                    strDummy = strRawUnitName
                    strLine = srMissionMA.ReadLine()
                    Continue Do
                End If

                'If this unit is identified as a Transporter of another unit, capture its proper name.
                If hashTransporter.ContainsKey(strRawUnitName) = True Then
                    modUtilities.subSetTransporterDefault(thisTransporter)
                    thisTransporter.Name = strProperUnitName
                    hashTransporter(strRawUnitName) = thisTransporter
                End If

                'We need to keep track of how we have transformed strRawUnitName into strProperUnitName.
                hashRawName(strRawUnitName) = strProperUnitName
                thisUnit.Object_Group_Name = strProperUnitName
                thisUnit.Unit_Name = modUtilities.Affiliation(item, strProperUnitName, thisUnit.Object_Class, thisUnit.Nationality)
                If UCase(Left(thisUnit.Object_Class, 1)) = "S" Or _
                   UCase(Left(thisUnit.Object_Class, 1)) = "R" Then
                    thisUnit.Parent = Left(strProperUnitName, Len(strProperUnitName) - 2)
                Else
                    thisUnit.Parent = Left(strProperUnitName, Len(strProperUnitName) - 3)
                End If

                'We need to keep track of all Chiefs for possible speed reductions due to damage events.
                'For HSFX5 this is only important for shipping, but it may change later.
                'This is not important at initialize time, but is important in all other cases.
                If hashShipSpeedProfile.ContainsKey(strProperUnitName) = False Then
                    thisShipSpeedProfile.Name = strProperUnitName
                    thisShipSpeedProfile.TypeCode = thisUnit.Object_Class

                    'add initial time (in seconds) and initial max speed (in metres per second)
                    thisShipSpeedProfile.strElapsedTime = "0"

                    'get the accurate speed from the hashUnit table.
                    If hashUnit.ContainsKey(strProperUnitName) = True Then
                        thisUnitSpeed = hashUnit(strProperUnitName)
                        thisShipSpeedProfile.strMaxSpeed = Format(modUtilities.dblShipSpeedFactor(thisUnitSpeed.ShipDamage) * thisUnitSpeed.Speed / (3600.0), "###.#####")
                    Else
                        thisShipSpeedProfile.strMaxSpeed = "50.0"
                    End If

                    thisShipSpeedProfile.strX = "0"
                    thisShipSpeedProfile.strY = "0"
                    hashShipSpeedProfile(strProperUnitName) = thisShipSpeedProfile
                End If


                'Parse the raw object type into the true object type and associated quantity.
                Call subParseArmour(strObjType, thisUnit.Object_Type, thisUnit.Quantity)
                If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Load_At_Full_Strength = 1 Then
                    thisUnit.Quantity = intMaximumStrength(item, thisUnit.Object_Type)
                End If

                'Set default values for unit fuel load and reconnaissance.
                Call modUtilities.subGetFuelCapacity(item, strObjType, intFuelCapacity)
                thisUnit.Fuel_Capacity = intFuelCapacity

                'Set default values for signalling.
                Call modUtilities.subGetDefaultSignals(item, strObjType, strSignals)
                thisUnit.Signals = strSignals
                thisUnit.ActivatedBy = ""
                thisUnit.ActivationTime = 0

                If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Track_Fuel_Consumption = 1 Then

                    Select Case thisUnit.Nationality
                        Case "r"
                            thisUnit.Fuel = CInt(CDbl(CS_Initial_Allied_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                        Case "g"
                            thisUnit.Fuel = CInt(CDbl(CS_Initial_Axis_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                    End Select

                    'Set morale and recon.
                    Call subSetMorale(thisUnit)
                    Call subSetRecon(thisUnit)

                ElseIf (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Track_Fuel_Consumption = 0 Then

                    thisUnit.Fuel = intFuelCapacity                'Otherwise, fuel tank is always full.

                    'Set morale and recon.
                    Call subSetMorale(thisUnit)
                    Call subSetRecon(thisUnit)

                Else
                    'Campaign is already initialized.
                    'Carry existing fuel, recon and morale values forward. That is, do nothing in this clause.

                End If

                'Create a hashtable Key for this unit name, if required, e.g. for campaign initialization.
                'If the hash item already exists, do nothing yet as the unit's details will be updated
                'in the subResolveEvents routine in basMissionBuilder.
                If hashUnit.ContainsKey(strProperUnitName) = False Then
                    hashUnit.Item(strProperUnitName) = thisUnit
                End If

                'If this is a new combined unit, then create a hashCombinedUnit entry for it.
                If isCombinedUnit(thisUnit.Object_Group_Name) = True And _
                   hashCombinedUnit.ContainsKey(thisUnit.Object_Group_Name) = False Then

                    modUtilities.setCombinedUnitDefault(thisCU)
                    modUtilities.loadNewCombinedUnit(thisCU, thisUnit.Object_Type)
                    thisCU.Group = thisUnit.Object_Group_Name
                    thisCU.Nation = thisUnit.Nationality
                    thisCU.CurrentStrength = thisCU.MaxStrength
                    hashCombinedUnit(thisUnit.Object_Group_Name) = thisCU

                End If

                'If this is a new meta unit, then create hashMetaUnit entries for it.
                If isMetaUnit(thisUnit.Object_Group_Name, strMetaObjectType) = True And _
                   hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name) = False Then

                    Dim intMUcounter As Integer = 0
                    Dim strMUKey As String
                    While intMUcounter < thisUnit.Quantity
                        modUtilities.setMetaUnitDefault(thisMU)
                        modUtilities.loadNewMetaUnit(thisMU, thisUnit.Object_Type)
                        thisMU.Group = thisUnit.Object_Group_Name
                        thisMU.Member = intMUcounter
                        thisMU.Nation = thisUnit.Nationality
                        thisMU.CurrentStrength = thisMU.MaxStrength
                        'strMUKey = thisUnit.Object_Group_Name & "|m" & intMUcounter
                        strMUKey = thisUnit.Object_Group_Name & "|" & intMUcounter
                        If hashMetaUnit.ContainsKey(strMUKey) = False Then
                            hashMetaUnit.Add(strMUKey, thisMU)
                        End If

                        intMUcounter += 1
                    End While

                End If

                'Read the next line in the mission file.
                strLine = srMissionMA.ReadLine()

                'Refresh main form periodically.
                intFormCounter += 1
                If 20 * Math.Floor(intFormCounter / 20.0) = intFormCounter Then
                    frmMain.Refresh()
                End If

            Loop

            'Let the user know that we are still alive.
            Call ProgressBarLabel("Processing active ground/sea units ... mobile routes")
            Call ProgressBarPerformStep()

            'Now we should be at the beginning of the Chief units' waypoints lists. We will loop over
            'these unit header lines, processing all information until the next unit header line, etc.
            intShip = 0
            intVehicle = 0
            intFormCounter = 0
            Do Until InStr(UCase(strLine), "[NSTATIONARY]") > 0 Or _
                                InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                                InStr(UCase(strLine), "[BUILDINGS]") > 0

                'Get the unit name: the line should look like "[13_Chief_Road]" or similar.
                strRawUnitName = Trim(Replace(Replace(strLine, "[", ""), "_Road]", ""))

                'If we are reading the data for the strINFSignallerDummy then just jump over it.
                If strRawUnitName = strDummy Then
                    strLine = srMissionMA.ReadLine()
                    strLine = srMissionMA.ReadLine()
                    strLine = srMissionMA.ReadLine()
                    strRawUnitName = Trim(Replace(Replace(strLine, "[", ""), "_Road]", ""))
                End If

                'Check that we have already initialized this hash key. If not, we have a problem!
                If hashUnit.ContainsKey(hashRawName(strRawUnitName)) = False Then
                    Call basMess.MessageBox("modMissionAnalyzer.subProcessChiefs: cannot reference hashkey for " & strRawUnitName, vbInformation)
                    Exit Sub
                Else
                    'Everything is OK, we have found the hash key of the unit as built in the earlier loop. Now
                    'just load the hash value structure into a local structure.
                    thisUnit = hashUnit.Item(hashRawName(strRawUnitName))
                    strLine = srMissionMA.ReadLine()
                End If


                'Step through the waypoints coords and store them in the Waypoints table. We do this 
                'using Mako's idea of building an extended INSERT query. The only drawback with this 
                'is that there can be server-side limits to the number of rows in an INSERT. 
                intWPCounter = 0
                Do Until InStr(strLine, "[") > 0
                    strLineArray = Nothing
                    strLineArray = Split(Trim(strLine))

                    'Load the initial waypoint as this unit's location.
                    'Do not load the Z value - just maintain whatever Z value is currently 
                    'in the hashtable... This is so that we can put the desired ending orientation into
                    'the hashtable when commencing a movement request, safe in the knowledge that the
                    'orientation will persist in the unit hashtable from mission to mission until
                    'it is required.
                    If intWPCounter = 0 And InStr(strRollingStopTrains, thisUnit.Object_Group_Name) < 1 Then
                        thisUnit.X = CDbl(strLineArray(0))
                        thisUnit.Y = CDbl(strLineArray(1))
                        'thisUnit.Z = CDbl(strLineArray(2))
                    End If

                    If intWPCounter = 1 And _
                       Left(thisUnit.Object_Class, 1) = "R" And _
                       (modCfg.User(item).IsCampaignInitialized = False Or isAddForce) Then
                        'Here we have a chief train being loaded in the campaign initialization stage.
                        'We need to set its orientation.
                        dblDX = CDbl(strLineArray(0)) - thisUnit.X
                        dblDY = CDbl(strLineArray(1)) - thisUnit.Y
                        thisUnit.Z = dblConvertAtan2Angle(Math.Atan2(dblDY, dblDX))
                    End If

                    'Grab the waypoint coordinates.
                    dblWPX = CDbl(strLineArray(0))
                    dblWPY = CDbl(strLineArray(1))
                    dblTerrain = CDbl(strLineArray(2))
                    If Left(thisUnit.Object_Class, 1) = "S" And strLineArray.Length > 3 Then
                        dblSpeed = CDbl(strLineArray(strLineArray.Length - 1))
                    End If

                    If strLineArray.Length > 3 Then
                        'Human-entered waypoint, so read the delay timeout as well.
                        intWPDelay = CInt(strLineArray(3))
                    Else
                        'Machine-entered waypoint, no delay set.
                        intWPDelay = 0
                    End If

                    'Format waypoint INSERT statements.
                    If Left(thisUnit.Object_Class, 1) = "S" And _
                       InStr(strChiefsDone, hashRawName(strRawUnitName)) <= 0 Then
                        'If the current Chief is a ship and it has not already had its waypoints
                        'processed, then add this waypoint as a SQL INSERT.
                        strSqlSpd = strSqlSpd _
                                & "('" & basFiles.StrMissionName & "'," _
                                & "'" & thisUnit.Object_Group_Name & "'," _
                                & intWPCounter & "," _
                                & dblWPY & "," _
                                & dblWPX & "," _
                                & dblSpeed & "," _
                                & dblTerrain & "," _
                                & intWPDelay & "),"
                        intShip += 1

                    ElseIf InStr(strChiefsDone, hashRawName(strRawUnitName)) <= 0 Then
                        'If the current Chief vehicle has not already had its waypoints
                        'processed, then add this waypoint as a SQL INSERT.
                        strSql = strSql _
                                & "('" & basFiles.StrMissionName & "'," _
                                & "'" & thisUnit.Object_Group_Name & "'," _
                                & intWPCounter & "," _
                                & dblWPY & "," _
                                & dblWPX & "," _
                                & dblTerrain & "," _
                                & intWPDelay & "),"
                        intVehicle += 1
                    End If



                    'Increment the waypoint counter.
                    intWPCounter += 1
                    Application.DoEvents()

                    'Step to next waypoint in mission file.
                    strLine = srMissionMA.ReadLine()
                Loop

                'Another thing to do is to set skill for ships - this is new in PF. Only do
                'this if we are initializing the campaign.
                If Left(thisUnit.Object_Class, 1) = "S" And _
                   (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then
                    subSetSkill(thisUnit)
                End If

                'Now we have collected all the information for the current unit, so stuff it back into
                'the hashtable.
                hashUnit.Item(hashRawName(strRawUnitName)) = thisUnit
                Application.DoEvents()

                'Add this Chief to a list of Chief that have had their waypoints registered already.
                If InStr(strChiefsDone, hashRawName(strRawUnitName)) <= 0 Then
                    strChiefsDone &= "|" & hashRawName(strRawUnitName)
                End If

                'Refresh main form periodically.
                intFormCounter += 1
                If 20 * Math.Floor(intFormCounter / 20.0) = intFormCounter Then
                    frmMain.Refresh()
                End If


            Loop

            'Only write the waypoints to the Waypoints table if the unit is not a rolling stop train.
            If InStr(strRollingStopTrains, thisUnit.Object_Group_Name) < 1 And isUntriggeredScrambler(thisUnit.Object_Group_Name) = False Then
                'Strip the last comma and IGNORE if already exist - this will ensure that 
                'no duplicate entries are written. IGNORE works only for MySQL.
                strSql = Left(strSql, Len(strSql) - 1)
                strSqlSpd = Left(strSqlSpd, Len(strSqlSpd) - 1)

                'If MySQL, then we can use the extended INSERT format. Just add the IGNORE 
                'directive in case the record already exists. 
                If modCfg.User(item).DatabaseProvider = "mysql" And _
                   modCfg.User(item).chkUseExtendedInserts = True Then
                    'Write the waypoint to the database. For ships, use the speed-dependent SQL.
                    If intShip > 0 Then
                        Call modDB.ExecuteDBCommand(item, Replace(strSqlSpd, "INSERT INTO", "INSERT IGNORE INTO"), False)
                    End If
                    If intVehicle > 0 Then
                        Call modDB.ExecuteDBCommand(item, Replace(strSql, "INSERT INTO", "INSERT IGNORE INTO"), False)
                    End If
                Else
                    'For MS Access we need to split the SQL into individual INSERT queries 
                    'and do them one by one. This is also used for the "No Extended Inserts" option.
                    strSql = Replace(strSql, "),(", ");|" & strSqlCore & "(")
                    strSqlSpd = Replace(strSqlSpd, "),(", ");|" & strSqlCoreSpd & "(")
                    If intShip > 0 Then
                        strSQLINSERT = Split(strSqlSpd, "|")
                        For i As Integer = strSQLINSERT.GetLowerBound(0) To strSQLINSERT.GetUpperBound(0)
                            Call modDB.ExecuteDBCommand(item, strSQLINSERT(i), False)
                        Next
                    End If
                    If intVehicle > 0 Then
                        strSQLINSERT = Split(strSql, "|")
                        For i As Integer = strSQLINSERT.GetLowerBound(0) To strSQLINSERT.GetUpperBound(0)
                            Call modDB.ExecuteDBCommand(item, strSQLINSERT(i), False)
                        Next
                    End If
                End If
            End If



            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subProcessChiefs: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Sub


    'This routine processes any stationary units in the mission file.
    Private Sub subProcessStationaries(ByVal item As Integer, ByRef strLine As String, ByRef isValid As Boolean)
        Dim strLineArray() As String
        Dim strRawUnitName As String
        Dim strProperUnitName As String
        Dim strSql As String
        Dim thisUnit As New SE_Unit
        Dim thisTransporter As New SE_Transporter
        Dim thisCU As New SE_CombinedUnit
        Dim thisMU As New SE_MetaUnit
        Dim rsUnitNameMapping As New ADODB.Recordset
        Dim intFormCounter As Integer
        Dim intFuelCapacity As Integer
        Dim strSignals As String
        Dim strMetaObjectType As String


        Try

            'Set the completion status.
            isValid = False

            'Step forward in the mission file until the [NStationary] directive is found.
            Do Until InStr(UCase(strLine), "[NSTATIONARY]") > 0 Or _
                        InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                        InStr(UCase(strLine), "[BUILDINGS]") > 0 Or _
                        InStr(UCase(strLine), "[TARGET]") > 0
                strLine = srMissionMA.ReadLine()
            Loop

            'The format of this section is very simple, just a list of lines that look like:
            '   ID                 mode+type              Nation  X        Y       Z Timeout
            '  82_Static vehicles.artillery.Artillery$PzIIIJ 2 101804.91 31396.55 465 0.0
            'All we have to do is step through the list, parsing as we go.
            If InStr(UCase(strLine), "[NSTATIONARY]") > 0 Then
                strLine = srMissionMA.ReadLine()
            End If

            intFormCounter = 0
            Do Until InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                    InStr(UCase(strLine), "[BUILDINGS]") > 0 Or _
                    InStr(UCase(strLine), "[TARGET]") > 0

                'Set thisUnit structure properties to default values.
                modUtilities.subSetUnitDefault(thisUnit)

                'Split up the current line.
                strLineArray = Split(Trim(strLine))

                'Get the raw (in-game) unit name.
                strRawUnitName = Trim(strLineArray(0))

                If (Not modCfg.User(item).IsCampaignInitialized) Or isAddForce Then
                    'Get the unit type, something like "PzIIIJ".
                    modUtilities.getStationaryType(item, Trim(strLineArray(1)), _
                                                thisUnit.Object_Type, thisUnit.Object_Class)
                    If thisUnit.Object_Type = "unknown" Then
                        isValid = False
                        Call modUtilities.subFlushDB(item)
                        Exit Sub
                    End If

                    'Grab the skill defined in the template for this shipping or artillery units.
                    If Left(thisUnit.Object_Class, 1) = "S" Or InStr(strLineArray(1), "Artillery") > 0 Then
                        If strLineArray.Length >= 9 Then  'TD4.12 skill format
                            thisUnit.Skill = Trim(strLineArray(8))
                            thisUnit.Skill0 = Trim(strLineArray(8))
                            thisUnit.Skill1 = Trim(strLineArray(8))
                            thisUnit.Skill2 = Trim(strLineArray(8))
                            thisUnit.Skill3 = Trim(strLineArray(8))
                        Else
                            thisUnit.Skill = 0      'No skill directives - use a workaround
                            subSetSkill(thisUnit)
                        End If
                    End If

                    'Set default values for signalling.
                    Call modUtilities.subGetDefaultSignals(item, thisUnit.Object_Type, strSignals)
                    thisUnit.Signals = strSignals
                    thisUnit.ActivatedBy = ""
                    thisUnit.ActivationTime = 0


                Else

                    'There is a Unit_Name_Mapping, so all we need to know is whether the Stationary
                    'is an aircraft or not (to help decide whether to trim the Group_Name).
                    If InStr(Trim(strLineArray(1)), "vehicles.planes.Plane") > 0 Then
                        thisUnit.Object_Class = "P"
                    End If

                End If

                'Get the unit nationality, "1" = "r" and "2" = "g".
                thisUnit.Nationality = strNationality(Trim(strLineArray(2)))

                If thisUnit.Nationality = "n" And _
                   (thisUnit.Object_Class <> "INF" And thisUnit.Object_Class <> Nothing) Then

                    'We need to decrement the intNext_SE_Unit_Number counter because it was incremented
                    'in modUtilities.strMakeProperName and we are going to discard this unit.
                    Call basMess.MessageBox("Campaign Template format error in file '" & basFiles.StrMissionName & "'. " _
                                & "Static combat objects must be listed with Allied (1) or Axis (2) alignment codes in the template file. '" _
                                & Trim(strLineArray(0)) & "' has invalid alignment code '" & Trim(strLineArray(2)) & "'." & vbCrLf _
                                & "Please flush the database, correct your template and try again.", vbInformation)
                    Exit Sub

                End If


                'Transform the raw name to the proper unit group name.
                strProperUnitName = modUtilities.strMakeProperName(item, strRawUnitName, _
                                    thisUnit.Object_Class, _
                                    thisUnit.Nationality)

                If strProperUnitName = "invalid" Then
                    'Tell the user that the Chief has an invalid name.
                    'Call basMess.MessageBox("Campaign army generation error: " & vbCrLf _
                    '            & "The Sector " & strThisMap & " will now be flushed. Please edit the Army_units table and try again.", vbCritical)
                    Exit Sub
                End If

                'If this unit is identified as a Transporter of another unit, capture its proper name.
                If hashTransporter.ContainsKey(strRawUnitName) = True Then

                    If InStr(Trim(strLineArray(1)), "$RwySteelLow") <= 0 And _
                       InStr(Trim(strLineArray(1)), "$RwyTransp") <= 0 Then
                        'Unit is not a temporary runway, so add its proper name to the transporter hash.
                        modUtilities.subSetTransporterDefault(thisTransporter)
                        thisTransporter.Name = strProperUnitName
                        hashTransporter(strRawUnitName) = thisTransporter
                    Else
                        'Unit is a temporary runway, so remove it from the transporter hash.
                        hashTransporter.Remove(strRawUnitName)
                    End If

                End If

                'If the unit is a Smoke, Campfire or Siren and is not represented as an automatically
                'generated infrastructure object, then write it to the Scenery_Stationary table.
                'Note that if this is not a template initialization, any such Smoke, Campfire or Siren objects
                'should be prefixed "SCENERY_" in the Unit_Name_Mapping table.
                'Added railway wagons to the scenery for template loads.
                If (InStr(strLineArray(1), "Smoke") > 0 Or _
                    InStr(strLineArray(1), "SirenCity") > 0 Or _
                    InStr(strLineArray(1), "Campfire") > 0 Or _
                    ((Not modCfg.User(item).IsCampaignInitialized) And thisUnit.Object_Class = "RWA")) And _
                   InStr(strProperUnitName, "Infrastructure") <= 0 Then

                    'Write this object to the Scenery_Stationary table.
                    Call modScenery.subAddSceneryStationary(item, strLine)

                    'We need to decrement the intNext_SE_Unit_Number counter because it was incremented
                    'in modUtilities.strMakeProperName and we are going to discard that proper name.
                    intNext_SE_Unit_Number -= 1

                Else

                    'If the unit does not represent a factory, fuel tank or infrastructure target, then process
                    'the unit as a taskable military unit. If it is simply a target object, ignore it.
                    If InStr(strProperUnitName, "Infrastructure") <= 0 And _
                       InStr(strProperUnitName, "INF_") <= 0 Then

                        'Set the internal unit name, something like "5_Static".
                        thisUnit.Object_Group_Name = strProperUnitName

                        'Get the unit coordinates, ignoring the last value (the delay timeout)
                        thisUnit.X = CDbl(strLineArray(3))
                        thisUnit.Y = CDbl(strLineArray(4))
                        thisUnit.Z = CDbl(strLineArray(5))

                        'If the campaign is not initialized, i.e. we are loading a template, and
                        'we encounter an aircraft here in the Stationaries section, then we need to
                        'ignore it.
                        If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And _
                           Microsoft.VisualBasic.Left(thisUnit.Object_Class, 1) = "P" Then

                            'We need to decrement the intNext_SE_Unit_Number counter because it was incremented
                            'in modUtilities.strMakeProperName and we are going to discard this unit.
                            intNext_SE_Unit_Number -= 1
                            Call basMess.MessageBox("Campaign Template format error in file " & basFiles.StrMissionName & vbCrLf _
                                        & "Aircraft must not be listed as Statics in the template file." & vbCrLf & vbCrLf _
                                        & Trim(strLineArray(0)) & " has invalid type '" & thisUnit.Object_Type & "'." & vbCrLf _
                                        & "This unit will be ignored.", vbExclamation)

                        Else

                            'Another thing to do is to set skill for ships - this is new in PF. Only do
                            'this if we are initializing the campaign. If this is a sequential mission,
                            'the hashtable will already have the unit skill in it.
                            If InStr(thisUnit.Object_Class, "S") = 1 And _
                                (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then
                                subSetSkill(thisUnit)
                            End If


                            'We must load each stationary as a full strength unit if
                            'CS_Load_At_Full_Strength = 1 and we are initializing the campaign.
                            If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Load_At_Full_Strength = 1 Then
                                thisUnit.Quantity = intMaximumStrength(item, thisUnit.Object_Type)
                            Else
                                thisUnit.Quantity = 1       'Otherwise, quantity is always 1 for Stationaries.
                            End If


                            'We must load each stationary at full fuel supply unit if
                            'CS_Track_Fuel_Consumption = 1 and we are initializing the campaign or adding forces.
                            If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then

                                modUtilities.subGetFuelCapacity(item, thisUnit.Object_Type, intFuelCapacity)
                                thisUnit.Fuel_Capacity = intFuelCapacity

                                If CS_Track_Fuel_Consumption = 1 Then
                                    'Apply national fuel loads.
                                    Select Case thisUnit.Nationality
                                        Case "r"
                                            thisUnit.Fuel = CInt(CDbl(CS_Initial_Allied_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                                        Case "g"
                                            thisUnit.Fuel = CInt(CDbl(CS_Initial_Axis_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                                    End Select

                                ElseIf CS_Track_Fuel_Consumption = 0 Then
                                    'Otherwise, fuel load is always capacity.
                                    thisUnit.Fuel = intFuelCapacity
                                End If

                                'Set morale and recon.
                                Call subSetMorale(thisUnit)
                                Call subSetRecon(thisUnit)

                            End If

                            'Set the remaining unit properties with initial defaults.
                            thisUnit.Unit_Name = modUtilities.Affiliation(item, strProperUnitName, thisUnit.Object_Class, thisUnit.Nationality)
                            thisUnit.Parent = Left(strProperUnitName, Len(strProperUnitName) - 3)

                            'Create a hashtable Key for this unit name and load the unit data structure into the Value property.
                            If hashUnit.ContainsKey(strProperUnitName) = False Then
                                hashUnit(strProperUnitName) = thisUnit
                            Else
                                'The unit is already specified in a previous mission, so do nothing.
                            End If

                        End If

                        'If this is a new combined unit, then create a hashCombinedUnit entry for it.
                        If isCombinedUnit(thisUnit.Object_Type) = True And _
                           hashCombinedUnit.ContainsKey(thisUnit.Object_Group_Name) = False Then

                            modUtilities.setCombinedUnitDefault(thisCU)
                            modUtilities.loadNewCombinedUnit(thisCU, thisUnit.Object_Type)
                            thisCU.Group = thisUnit.Object_Group_Name
                            thisCU.Nation = thisUnit.Nationality
                            hashCombinedUnit(thisUnit.Object_Group_Name) = thisCU

                        End If

                        'If this is a new meta unit, then create a hashMetaUnit entry for it.
                        strMetaObjectType = ""
                        If isMetaUnit(thisUnit.Object_Group_Name, strMetaObjectType) = True And _
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|0") = False And _
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|1") = False And _
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|2") = False And _
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|3") = False Then

                            'Dim strMUkey As String
                            'thisUnit.Object_Type = strMetaObjectType
                            'modUtilities.setMetaUnitDefault(thisMU)
                            'modUtilities.loadNewMetaUnit(thisMU, thisUnit.Object_Type)
                            'thisMU.Group = thisUnit.Object_Group_Name
                            'thisMU.Member = 0
                            'thisMU.Nation = thisUnit.Nationality
                            'strMUKey = thisUnit.Object_Group_Name & "|0"
                            'hashMetaUnit.Add(strMUkey, thisMU)

                            Dim intMUcounter As Integer = 0
                            Dim strMUKey As String
                            While intMUcounter < thisUnit.Quantity
                                modUtilities.setMetaUnitDefault(thisMU)
                                modUtilities.loadNewMetaUnit(thisMU, strMetaObjectType)
                                thisMU.Group = thisUnit.Object_Group_Name
                                thisMU.Member = intMUcounter
                                thisMU.Nation = thisUnit.Nationality
                                thisMU.CurrentStrength = thisMU.MaxStrength
                                'strMUKey = thisUnit.Object_Group_Name & "|m" & intMUcounter
                                strMUKey = thisUnit.Object_Group_Name & "|" & intMUcounter
                                If hashMetaUnit.ContainsKey(strMUKey) = False Then
                                    hashMetaUnit.Add(strMUKey, thisMU)
                                End If

                                intMUcounter += 1
                            End While


                        End If


                    End If
                End If


                'Read the next line in the mission file.
                strLine = srMissionMA.ReadLine()

                'Refresh main form periodically.
                intFormCounter += 1
                If 20 * Math.Floor(intFormCounter / 20.0) = intFormCounter Then
                    frmMain.Refresh()
                End If


            Loop

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subProcessStationaries: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This routine processes any rocketry units in the mission file.
    Private Sub subProcessRockets(ByVal item As Integer, ByRef strLine As String, ByRef isValid As Boolean)
        Dim strLineArray() As String
        Dim strRawUnitName As String
        Dim strProperUnitName As String
        Dim strSql As String
        Dim thisUnit As New SE_Unit
        Dim thisRocketData As SE_Rocket
        Dim thisTransporter As New SE_Transporter
        Dim rsUnitNameMapping As New ADODB.Recordset
        Dim intFuelCapacity As Integer
        Dim thisMU As SE_MetaUnit
        Dim strMetaObjectType As String


        Try

            'Set the completion status.
            isValid = False

            'Step forward in the mission file until the [NStationary] directive is found.
            Do Until InStr(UCase(strLine), "[ROCKET]") > 0 Or _
                     InStr(UCase(strLine), "[BUILDINGS]") > 0 Or _
                     InStr(UCase(strLine), "[TARGET]") > 0 Or _
                     InStr(UCase(strLine), "[BRIDGE]") > 0
                strLine = srMissionMA.ReadLine()
            Loop

            'The format of this section is very simple, just a list of lines that look like:
            '   ID      type        Nation  X        Y       Z    Timeout  Count  Cycle  TargetX Target Y
            '  0_Rocket Fi103_V1_ramp 2 176775.60 175506.50 360.00 0.0 1 20.0 243939.71 169047.33

            'All we have to do is step through the list, parsing as we go.
            If InStr(UCase(strLine), "[ROCKET]") > 0 Then
                strLine = srMissionMA.ReadLine()
            End If

            Do Until InStr(UCase(strLine), "[BUILDINGS]") > 0 Or _
                     InStr(UCase(strLine), "[TARGET]") > 0 Or _
                     InStr(UCase(strLine), "[BRIDGE]") > 0

                'Set thisUnit structure properties to default values.
                modUtilities.subSetUnitDefault(thisUnit)

                'Split up the current line.
                strLineArray = Split(Trim(strLine))

                'Get the raw (in-game) unit name.
                strRawUnitName = Trim(strLineArray(0))

                If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) Then
                    'Get the unit type, something like "PzIIIJ".
                    modUtilities.getStationaryType(item, modUtilities.strCompatHSFX(Trim(strLineArray(1))), _
                                                thisUnit.Object_Type, thisUnit.Object_Class)
                End If

                'Get the unit nationality, "1" = "r" and "2" = "g".
                thisUnit.Nationality = strNationality(Trim(strLineArray(2)))

                If thisUnit.Nationality = "n" And _
                   (thisUnit.Object_Class <> "INF" And thisUnit.Object_Class <> Nothing) Then

                    'We need to decrement the intNext_SE_Unit_Number counter because it was incremented
                    'in modUtilities.strMakeProperName and we are going to discard this unit.
                    Call basMess.MessageBox("Campaign Template format error in file '" & basFiles.StrMissionName & "'. " _
                                & "Static combat objects must be listed with Allied (1) or Axis (2) alignment codes in the template file. '" _
                                & Trim(strLineArray(0)) & "' has invalid alignment code '" & Trim(strLineArray(2)) & "'." & vbCrLf _
                                & "Please flush the database, correct your template and try again.", vbInformation)
                    Exit Sub

                End If


                'Transform the raw name to the proper unit group name.
                strProperUnitName = modUtilities.strMakeProperName(item, strRawUnitName, _
                                    thisUnit.Object_Class, _
                                    thisUnit.Nationality)

                'If this unit is identified as a Transporter of another unit, capture its proper name.
                If hashTransporter.ContainsKey(strRawUnitName) = True Then

                    If InStr(modUtilities.strCompatHSFX(Trim(strLineArray(1))), "$RwySteelLow") <= 0 And _
                       InStr(modUtilities.strCompatHSFX(Trim(strLineArray(1))), "$RwyTransp") <= 0 Then
                        'Unit is not a temporary runway, so add its proper name to the transporter hash.
                        modUtilities.subSetTransporterDefault(thisTransporter)
                        thisTransporter.Name = strProperUnitName
                        hashTransporter(strRawUnitName) = thisTransporter
                    Else
                        'Unit is a temporary runway, so remove it from the transporter hash.
                        hashTransporter.Remove(strRawUnitName)
                    End If

                End If

                'Set the internal unit name, something like "5_Static".
                thisUnit.Object_Group_Name = strProperUnitName

                'The format of this section is very simple, just a list of lines that look like:
                '   ID      type        Nation  X        Y       Z    Timeout  Count  Cycle  TargetX Target Y
                '  0_Rocket Fi103_V1_ramp 2 176775.60 175506.50 360.00 0.0 1 20.0 243939.71 169047.33
                'Get the unit coordinates and other barrage parameters
                thisUnit.Object_Type = modUtilities.strCompatHSFX(Trim(strLineArray(1)))
                thisUnit.X = CDbl(strLineArray(3))
                thisUnit.Y = CDbl(strLineArray(4))
                thisUnit.Z = CDbl(strLineArray(5))
                thisUnit.BarrageDelay = CDbl(strLineArray(6))
                thisUnit.BarrageSalvoes = CDbl(strLineArray(7))
                thisUnit.BarragePeriod = CDbl(strLineArray(8))
                If hashRocketPar.ContainsKey(thisUnit.Object_Type) Then
                    thisRocketData = hashRocketPar.Item(thisUnit.Object_Type)
                    thisUnit.BarrageDispersion = 0
                    thisUnit.BarrageHitsMorale = 0
                    thisUnit.BarrageDispersion = thisRocketData.Dispersion
                    thisUnit.BarrageHitsMorale = thisRocketData.HitsMorale
                End If
                If strLineArray.Length > 9 Then
                    thisUnit.TargetX = CDbl(strLineArray(9))
                    thisUnit.TargetY = CDbl(strLineArray(10))
                Else
                    thisUnit.TargetX = 0.0
                    thisUnit.TargetY = 0.0
                End If

                'If the campaign is not initialized, i.e. we are loading a template, and
                'we encounter a non-rocket type here in the Stationaries section, then we need to
                'ignore it.
                If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And _
                   (thisUnit.Object_Class <> "ARK" And thisUnit.Object_Class <> "AIF") Then

                    'We need to decrement the intNext_SE_Unit_Number counter because it was incremented
                    'in modUtilities.strMakeProperName and we are going to discard this unit.
                    intNext_SE_Unit_Number -= 1
                    Call basMess.MessageBox("Campaign Template format error in file " & basFiles.StrMissionName & vbCrLf _
                                & "Invalid unit type listed in the Rockets section of the template file." & vbCrLf & vbCrLf _
                                & Trim(strLineArray(0)) & " has invalid type '" & thisUnit.Object_Type & "'." & vbCrLf _
                                & "This unit will be ignored.", vbExclamation)

                Else

                    'We must load each rocket at full fuel supply if
                    'CS_Track_Fuel_Consumption = 1 and we are initializing the campaign.
                    thisUnit.Object_Type = modUtilities.strCompatHSFX(Trim(strLineArray(1)))
                    modUtilities.subGetFuelCapacity(item, thisUnit.Object_Type, intFuelCapacity)
                    thisUnit.Fuel_Capacity = intFuelCapacity
                    If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Track_Fuel_Consumption = 1 Then
                        Select Case thisUnit.Nationality
                            Case "r"
                                thisUnit.Fuel = CInt(CDbl(CS_Initial_Allied_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                            Case "g"
                                thisUnit.Fuel = CInt(CDbl(CS_Initial_Axis_Unit_Fuel_Load) * CDbl(intFuelCapacity) / 100.0)
                        End Select
                        Call subSetRecon(thisUnit)
                        Call subSetMorale(thisUnit)
                        Call subSetSkill(thisUnit)

                    ElseIf (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Track_Fuel_Consumption = 0 Then
                        thisUnit.Fuel = intFuelCapacity       'Otherwise, fuel load is always capacity.
                        Call subSetRecon(thisUnit)
                        Call subSetMorale(thisUnit)
                        Call subSetSkill(thisUnit)
                    End If


                    'We must load each stationary as a full strength unit if
                    'CS_Load_At_Full_Strength = 1 and we are initializing the campaign.
                    If (Not modCfg.User(item).IsCampaignInitialized Or isAddForce) And CS_Load_At_Full_Strength = 1 Then
                        thisUnit.Quantity = intMaximumStrength(item, thisUnit.Object_Type)
                    Else
                        thisUnit.Quantity = 1       'Otherwise, quantity is always 1 for Stationaries.
                    End If

                    'Set the remaining unit properties with initial defaults.
                    thisUnit.Unit_Name = modUtilities.Affiliation(item, strProperUnitName, thisUnit.Object_Class, thisUnit.Nationality)
                    thisUnit.Parent = Left(strProperUnitName, Len(strProperUnitName) - 3)

                    'Create a hashtable Key for this unit name and load the unit data structure into the Value property.
                    If hashUnit.ContainsKey(strProperUnitName) = False Then
                        hashUnit(strProperUnitName) = thisUnit
                    Else
                        'The unit is already specified in a previous mission, so do nothing.
                    End If

                End If

                'Add this barraging unit to the active barrages hashtable
                If hashBarrageUnit.ContainsKey(strProperUnitName) = False Then
                    hashBarrageUnit.Add(strProperUnitName, thisUnit)
                End If


                'If this is a new meta unit, then create a hashMetaUnit entry for it.
                strMetaObjectType = ""
                If isMetaUnit(thisUnit.Object_Group_Name, strMetaObjectType) = True And
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|0") = False And
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|1") = False And
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|2") = False And
                           hashMetaUnit.ContainsKey(thisUnit.Object_Group_Name & "|3") = False Then

                    Dim intMUcounter As Integer = 0
                    Dim strMUKey As String
                    While intMUcounter < thisUnit.Quantity
                        modUtilities.setMetaUnitDefault(thisMU)
                        modUtilities.loadNewMetaUnit(thisMU, strMetaObjectType)
                        thisMU.Group = thisUnit.Object_Group_Name
                        thisMU.Member = intMUcounter
                        thisMU.Nation = thisUnit.Nationality
                        thisMU.CurrentStrength = thisMU.MaxStrength
                        strMUKey = thisUnit.Object_Group_Name & "|" & intMUcounter
                        If hashMetaUnit.ContainsKey(strMUKey) = False Then
                            hashMetaUnit.Add(strMUKey, thisMU)
                        End If

                        intMUcounter += 1
                    End While

                End If


                'Read the next line in the mission file.
                strLine = srMissionMA.ReadLine()

            Loop

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subProcessRockets: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This routine adds any parked off-map units to the relevant hashtable keys.
    Private Sub subProcessOffMapParked(ByVal item As Integer, ByRef isValid As Boolean)
        Dim rsOffMap As New ADODB.Recordset
        Dim strSql As String
        Dim thisUnit As New SE_Unit

        Try
            'Bail out if adding forces.
            If isAddForce Then
                Exit Sub
            End If

            'Set the completion status.
            isValid = False

            'Select all entries in the OffMap_Parking table.
            strSql = "SELECT * FROM OffMap_Parking WHERE Mission_Name = '" _
                    & basFiles.StrMissionName & "'"

            'Open the recordset.
            Call modDB.openDBRecordset(item, rsOffMap, strSql)

            While rsOffMap.EOF = False

                'Set thisUnit structure properties to default values.
                modUtilities.subSetUnitDefault(thisUnit)

                'Populate the unit structure with data from the current record.
                thisUnit.Unit_Name = rsOffMap("Unit_Name").Value
                thisUnit.Object_Group_Name = rsOffMap("Obj_Group_Name").Value
                thisUnit.Object_Type = rsOffMap("Obj_Type").Value
                thisUnit.X = rsOffMap("X").Value
                thisUnit.Y = rsOffMap("Y").Value
                thisUnit.Quantity = rsOffMap("Quantity").Value
                thisUnit.Refueling = rsOffMap("Refueling").Value

                If hashUnit.ContainsKey(thisUnit.Object_Group_Name) = False Then
                    'This unit is not present in the hashtable, so build a full key for it. This occurs
                    'if an off-map flight is sitting out the mission.
                    hashUnit(thisUnit.Object_Group_Name) = thisUnit
                Else
                    'This unit is already present in the hashtable at its full strength prior to the
                    'start of the mission, so do nothing. This occurs if a partial off-map flight has 
                    'been sent on a mission, leaving part of the flight behind.
                End If

                'Step forward to the next record.
                rsOffMap.MoveNext()

            End While

            'Close the recordset.
            rsOffMap.Close()

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subProcessOffMapParked: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub



    'The subroutine subLoadActionReports is the one that is called by the parser form.  This is where the log 
    'parsing begins.
    Public Sub subLoadEventReports(ByVal item As Integer)
        Dim strLine As String = ""

        Try

            'Bug out if using IF metamode - just need to calculate mission duration.
            If CS_MetaMode = basMain.META_IF44_ONLY Then
                Call subMissionDuration(item, strLine)
                Exit Sub
            End If

            'Set strDestroyedBridges and strDiscoKills to empty strings.
            strDestroyedBridges = ""
            strDiscoKills = ""
            strDestroyedUnlandedAircraft = ""

            'Here is the loop that does the actual parsing.  Essentially this loop pulls each line from the log
            'file and then based on the contents of the log it sends it to the correct subroutine for that line.
            'Remember the log file has already been opened on stream srLogMA and is positioned at the correct mission.
            Do While srLogMA.Peek <> -1
                strLine = srLogMA.ReadLine()

                If InStr(strLine, "] Mission:") > 0 Then
                    'This clause will only be active if a new Mission section is encountered. If this happens
                    'exit the Do loop immediately - we don't want to parse 2 logs at once!
                    Exit Do

                ElseIf InStr(strLine, "(255)") > 0 Or InStr(strLine, "(-1)") > 0 Then
                    Call subParatrooperEvent(item, strLine)

                ElseIf InStr(strLine, "seat occupied by") > 0 Or InStr(strLine, "is trying to occupy") > 0 Then
                    'Record which flight each human pilot served in.
                    Call subSeatOccupied(strLine)

                ElseIf InStr(strLine, "_Chief crashed at") > 0 Or _
                        InStr(strLine, "_Static crashed at") > 0 Then
                    'The line contains a single-sided event for the ActionData table.
                    Call subActionEvent(item, strLine)

                ElseIf InStr(strLine, "destroyed by") > 0 Or _
                        InStr(strLine, "shot down by") > 0 Then
                    'The line contains a double-sided event for the ActionData table.
                    Call subActionEvent(item, strLine)

                ElseIf InStr(strLine, "activated by") > 0 Then
                    'The line contains a trigger activation event for registration in the Mission_Triggers table.
                    Call subTriggerEvent(item, strLine)

                ElseIf InStr(strLine, basMain.SHIPDAMAGE_BALLISTIC) > 0 Or _
                       InStr(strLine, basMain.SHIPDAMAGE_EXPLOSIVE) > 0 Then
                    'The line contains a double-sided ship damage event for the ActionData table.
                    'Note that we must perform this check BEFORE the "bailed out" check, since the
                    'provisional ship damage logging code uses the onParaLanded() function in Eventlog.
                    'onParaLanded() normally handles bailing out logging and writes "bailed out successfully"
                    'to the eventlog record whenever it is called.
                    Call subActionEventShip(item, strLine)

                ElseIf InStr(strLine, basMain.SHIPDAMAGE_REDUCTION) > 0 Then
                    'The line contains a report of ship damage leading to ship speed reduction.
                    'This is new in HSFX5.
                    Call subReductionEventShip(item, strLine)

                ElseIf InStr(strLine, "was killed") > 0 Or _
                        InStr(strLine, "was heavily wounded") > 0 Or _
                        InStr(strLine, "was wounded") > 0 Or _
                        InStr(strLine, "was captured") > 0 Or _
                        InStr(strLine, "bailed out") > 0 Or _
                        InStr(strLine, "crashed at") > 0 Or _
                        InStr(strLine, "in flight at") > 0 Or _
                        InStr(strLine, "emergency landed at") > 0 Or _
                        InStr(strLine, "landed at") > 0 Then
                    'The line contains a single-sided event for the ObjMissionDisposition table.
                    Call subDispositionEvent(item, strLine)

                ElseIf InStr(strLine, "Mission BEGIN") <> 0 Or InStr(strLine, "Mission END") <> 0 Then
                    'Capture the start at end times and calculate the total mission duration.
                    Call subMissionDuration(item, strLine)

                ElseIf InStr(strLine, "has disconnected") > 0 And _
                       CS_Enforce_Landings = 1 Then
                    'Kill this pilot if he/she hasn't completed the flight properly.
                    Call subAddDisconnectionEvent(strLine)

                ElseIf InStr(strLine, "loaded weapons") > 0 And _
                       CS_Track_Fuel_Consumption = 1 And _
                       CS_Fixed_Loadouts = 0 Then
                    'This routine can be used to adjust the fuel decrements in the case that
                    'a pilot varies from the requested loadouts. Presently the routine has not
                    'been written and is disabled. This is because I think that it would be 
                    'unlikely that peope would run a campaign with supply tracking ON and 
                    'fixed loadouts OFF.
                    'Call subAdjustFuelUsage(strLine)

                ElseIf InStr(strLine, "turned wingtip smokes on") > 0 And _
                       CS_Human_Recon_Supply_Flights = 1 Then
                    'Register a human recon photo or a human supply drop.
                    Call subAddHumanIntervention(item, strLine)

                ElseIf InStr(strLine, "damaged by") > 0 Or InStr(strLine, "damaged on") > 0 Then
                    'Register a damage event, especially for aircraft.
                    Call subDelayDamagedAirframe(item, strLine)

                ElseIf strLine = "" Or InStr(strLine, "-------") > 0 Or _
                        InStr(strLine, " Target ") > 0 Or _
                        InStr(strLine, "has connected") > 0 Or _
                        InStr(strLine, "turned landing lights") > 0 Or _
                        InStr(strLine, "turned wingtip smokes off") > 0 Or _
                        InStr(strLine, "loaded weapons") > 0 Or _
                        InStr(strLine, "damaged") > 0 Or _
                        InStr(strLine, "has disconnected") > 0 Or _
                        InStr(strLine, "removed at") > 0 Or _
                        (CS_Create_Host_Seat = 1 And InStr(strLine, frmMain.FlightFromAlignment(CS_Host_Alignment)) <> 0) Then
                    'Ignore all these events/actions. Anyone who feels like adding a routine/module to handle
                    'any of these events should feel free to do so.

                ElseIf InStr(strLine, "Name:") > 0 Then
                    'We will get to why we use subscore() as the Else section when we get to that subroutine.
                    subGatherScores(strLine)
                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subLoadEventReports: extra lines found in log file '" & strLine & "'", vbExclamation)
                End If

                'and loop...
            Loop


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadEventReports: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub

    'This routine adds a disconnection event to the Actions list and Pilot Record.
    Private Sub subAddDisconnectionEvent(ByVal strLine As String)
        Dim strLineArray() As String
        Dim dtTime As Date
        Dim strPilot As String
        Dim strOtherPilot As String
        Dim thisPilot As Pilot_Record
        Dim otherPilot As Pilot_Record
        Dim thisEnumerator As IDictionaryEnumerator
        Dim isStillCrewed As Boolean

        Try
            'Parse the current line, removing tabs and double spaces.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the time the incident took place.
            dtTime = Trim(Replace(strLineArray(0), "\t", ""))

            'Extract Pilot name.
            strPilot = Right(strLine, Len(strLine) - 9)
            strPilot = Left(strPilot, InStr(strPilot, " has disconnected") - 1)
            strPilot = Replace(Replace(strPilot, "\t", ""), "'", "")
            strPilot = Trim(strPack(strPilot))

            'Get the corresponding Pilot Record.
            If hashPilotRecord.ContainsKey(strPilot) = True Then
                thisPilot = hashPilotRecord(strPilot)

                'Update the Pilot Record to show KIA for the disconnection if the pilot was
                'in control of the airframe.
                If thisPilot.inControl = True And _
                   thisPilot.Pilot_Status <> "Landed" And _
                   thisPilot.Pilot_Status <> "Crashed" And _
                   thisPilot.Pilot_Status <> "KIA" And _
                   thisPilot.Pilot_Status <> "Bailed" And _
                   thisPilot.Pilot_Status <> "Captured" Then

                    'Apply the disconnect penalty.
                    thisPilot.Pilot_Status = "KIA"
                    thisPilot.EventTime = strLineArray(0)
                    hashPilotRecord(strPilot) = thisPilot

                    'Are there any other pilots in the same aircraft? 
                    'Establish the enumerator of the PilotRecord hashtable.
                    'thisEnumerator = hashPilotRecord.GetEnumerator()

                    'Loop through all the pilot hashes looking for crew members.
                    'isStillCrewed = False
                    'While thisEnumerator.MoveNext()

                    'Potential crew member.
                    'otherPilot = thisEnumerator.Value

                    'If otherPilot.Pilot_Aircraft = thisPilot.Pilot_Aircraft And _
                    '   otherPilot.Pilot_Name <> thisPilot.Pilot_Name And _
                    '  (otherPilot.Pilot_Status = "In Cockpit" Or _
                    '   otherPilot.Pilot_Status = "In Flight" Or _
                    '  otherPilot.Pilot_Status = "Wounded" Or _
                    '  otherPilot.Pilot_Status = "Heavily Wounded") Then

                    'Yes, this aircraft is still crewed.
                    'isStillCrewed = True

                    'End If


                    'End While


                    'If the disconnected player is the pilot, tag the airframe and destroy it later.
                    'This is a change in DCS 3.3.5, the aircraft destruction is now independent of any
                    'remaining crew in the airframe. The airframe will disappear on pilot disconnection
                    'while the other human crew will remain suspended in the air as observers. They should
                    'not be punished.
                    If thisPilot.inControl = True And _
                       InStr(strDiscoKills, "^" & thisPilot.Pilot_Aircraft & "|") <= 0 Then
                        strDiscoKills &= "^" & thisPilot.Pilot_Aircraft & "|" & Format(dtTime, "HH:mm:ss")
                    End If



                End If


            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAddDisconnectionEvent: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This function creates action reports for lines that say one object destroyed/shot down another.
    Private Sub subActionEvent(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim intPosStart As Integer
        Dim intPosEnd As Integer
        Dim isSeaUnitSingle As Boolean = False
        Dim strObj1 As String
        Dim strObj1Group As String
        Dim strObj2 As String
        Dim strObj2Group As String
        Dim strAction As String
        Dim strTemp As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim strSql As String
        Dim dtMissionStart, dtTime, dtThisEventDay, dtThisEventTime As Date
        Dim rsRecon As New ADODB.Recordset
        Dim rsTransportedUnits As New ADODB.Recordset
        Dim intMember As Integer
        Dim intObj1Quantity As Integer
        Dim thisActionUnit As SE_Unit
        Dim isAirUnit As Boolean
        Dim intThisEventTime, intThisEventSeconds, intThisEventHour, intStartHour As Integer
        Dim thisInstallation As SE_Installation

        On Error GoTo ErrorHandler

        'Parse the current line, removing tabs and double spaces.
        strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

        'Extract the time the incident took place.
        dtTime = Trim(Replace(strLineArray(0), "\t", ""))

        'Extract raw Object 1 name.
        strObj1 = Trim(Replace(strLineArray(1), "\t", ""))
        If InStr(strObj1, "(") > 0 Then
            strObj1 = Left(strObj1, InStr(strObj1, "(") - 1)
        End If

        'Check to see if this Bridge has already been logged as destroyed in this mission.
        If InStr(strDestroyedBridges, strObj1 & "|") > 0 Then
            Exit Sub
        End If

        'Convert the raw name into the proper name.
        Call subGetProperName(item, strObj1, isAirUnit)

        'If the protagonist is the host seat runway, then ignore this event.
        If strObj1 = basMain.HOSTRUNWAY Or strObj1 = basMissionBuilder.strINFSignallerDummy Then
            Exit Sub
        End If

        'Get the group for Object 1.
        strObj1Group = modUtilities.getGroupName(strObj1, isAirUnit)

        'If the protagonist is the host seat runway, then ignore this event.
        If isHostFlight(strObj1Group) = True Then
            Exit Sub
        End If

        'Extract the location in grid coordinate that the action took place. 
        dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
        dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

        'Ignore this event if it is inside a metamap.
        If CS_MetaMode = basMain.META_IL2_IF44 And modUtilities.isInMetaMap(CStr(dblX_Axis), CStr(dblY_Axis)) Then
            Exit Sub
        End If


        'The action will always be 'destroyed by' or 'crashed at'.
        If InStr(strLine, "chute destroyed by") > 0 Or InStr(strLine, "killed in his chute by") > 0 Then
            strAction = "parachute destroyed by"
        ElseIf InStr(strLine, "destroyed by") > 0 Then
            strAction = "destroyed by"
            strAirframesLost &= strObj1 & "|"
            If Left(getObjectClass(strObj1Group), 1) = "S" Then
                Call subRegisterShipDestruction(strObj1Group)
            End If

            'Is the protagonist an installation?
            If hashInstallation.ContainsKey(Replace(strObj1, "INF_", "")) Then
                'Get the current status structure and set the destruction flag.
                thisInstallation = hashInstallation(Replace(strObj1, "INF_", ""))
                thisInstallation.isDestroyed = True
                hashInstallation(Replace(strObj1, "INF_", "")) = thisInstallation
            End If

        ElseIf InStr(strLine, "shot down by") > 0 Then
            strAction = "shot down by"
            strAirframesLost &= strObj1 & "|"
        ElseIf InStr(strLine, "crashed at") > 0 Then
            If Left(getObjectClass(strObj1Group), 1) = "S" Then
                strAction = "sank"
                Call subRegisterShipDestruction(strObj1Group)
            Else
                strAction = "crashed"
            End If
            isSeaUnitSingle = True
            strAirframesLost &= strObj1 & "|"
        Else
            Call basMess.MessageBox("modMissionAnalyzer.subActionEvent: Unknown action event '" & strLine & "'", vbInformation)
        End If

        If isSeaUnitSingle = True Then
            'This is a single-sided sea unit action event, e.g. a collision.
            'Create SQL Statement for a single-sided action.
            strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
            & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strObj1) _
            & "', '" & modDB.strSQLquote(strObj1Group) _
            & "', '" & strAction & "', '', " _
            & "'', '" & basFiles.StrMissionName _
            & "', '" & CInt(dblY_Axis) _
            & "', '" & CInt(dblX_Axis) _
            & "', '" & Format(dtTime, "HH:mm:ss") & "')"

        Else
            'This is a double-sided action event.
            'Extract Object 2 Name
            intPosStart = InStr(strLine, " by ") + 4
            strObj2 = Trim(Mid(strLine, intPosStart))
            intPosEnd = InStr(strObj2, " ")
            strObj2 = Replace(Left(strObj2, intPosEnd - 1), "\t", "")
            If InStr(strObj2, "(") > 0 Then
                strObj2 = Trim(Left(strObj2, InStr(strObj2, "(") - 1))
            End If

            'Convert the raw name into the proper name.
            Call subGetProperName(item, strObj2, isAirUnit)

            'And Group...
            strObj2Group = modUtilities.getGroupName(strObj2, isAirUnit)

            'Check for barrages! This is an HSFX6 feature.
            If strObj2Group = "Landscape" And hashBarrageUnit.Count > 0 Then
                Call subAllocateDestructionToBarrage(strObj2, strObj2Group, dblX_Axis, dblY_Axis, dtTime)
            End If

            'Create SQL Statement
            strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
            & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strObj1) & "', '" & modDB.strSQLquote(strObj1Group) & "', '" & strAction & "', '" _
            & modDB.strSQLquote(strObj2) & "', '" & modDB.strSQLquote(strObj2Group) & "', '" & basFiles.StrMissionName _
            & "', '" & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" & Format(dtTime, "HH:mm:ss") & "')"

            'If this is a Bridge destruction, add the bridge name to the strDestroyedBridges public string.
            'This will help prevent the multiple report lines.
            If InStr(strObj1, "Bridge") > 0 Then
                strDestroyedBridges = strDestroyedBridges & strObj1 & "|"
            End If
        End If



        On Error Resume Next
        'Execute SQL Statement.  Notice that we changed the the error handling to 'Resume Next'.  We can have 
        'situations where multiple crewmembers have the same 'action' occur.  We don't deal with multiple 
        'crewmembers on the same plane (except in scoring - if both are human players) so we ignore any of 
        'these errors and just don't write that line.
        'This has a side-effect of ignoring ALL SQL errors, so if you think that the ActionEvents are not
        'being written to the database correctly, comment out the above statement and check it out.
        Call modDB.ExecuteDBCommand(item, strSql, False)

        'If the destroyed object was running a Recon flight, set the Recon_Success field
        'to false for all its recon points.
        If strAction = "shot down by" Or strAction = "crashed" Then
            strSql = "UPDATE Mission_Recon SET Recon_Success = 0 " _
                    & "WHERE Recon_Unit = '" & strObj1Group & "' AND " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "';"
            Call modDB.ExecuteDBCommand(item, strSql)
        End If

        'Construct the elapsed time (in seconds) of this event, taking into account possible day changes at midnight.
        intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)

        'If the destroyed object was running a SUPPLY or PRPGNDA flight, set the Drop_Success field
        'to false for all its SUPPLY points.
        If strAction = "shot down by" Or strAction = "crashed" Then
            'dtMissionStart = Format(modUtilities.SN2DT(strLastMissionSequenceNumber), "d-MMMM-yyyy HH:mm:ss")
            'intThisEventTime = DateDiff(DateInterval.Second, dtMissionStart, dtTime)
            strSql = "UPDATE Supply_Drop_Orders SET Drop_Success = 0 " _
                   & "WHERE Transport_Flight = '" & strObj1Group & "' AND " _
                   & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                   & "Drop_ETA >= " & intThisEventTime & ";"
            Call modDB.ExecuteDBCommand(item, strSql)
        End If

        'If the destroyed object was running an ASSAULT flight, set the Drop_Success field
        'to false for all its ASSAULT points.
        If strAction = "shot down by" Or strAction = "crashed" Then
            'dtMissionStart = Format(modUtilities.SN2DT(strLastMissionSequenceNumber), "HH:mm:ss")
            'intThisEventTime = DateDiff(DateInterval.Second, dtMissionStart, dtTime)
            strSql = "UPDATE Paratrooper_Assault_Orders SET Drop_Success = 0 " _
                   & "WHERE Transport_Flight = '" & strObj1Group & "' AND " _
                   & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                   & "Drop_ETA >= " & intThisEventTime & ";"
            Call modDB.ExecuteDBCommand(item, strSql)
        End If

        'If the destroyed object is part of a combined unit, e.g. a vehicle column or train,
        'then log the action event to the Combined_Unit_Status table.
        If isCombinedUnit(strObj1Group) = True Then
            Call registerCULoss(item, strLine)
        End If

        'If the destroyed object is a Transporter of other units, then list destruction events for all those units too.
        'To do this, first construct a recordset of all units transported by the current destroyed unit.
        'We exclude aircraft carriers from this because their aircraft can be airborne - this case is
        'handled in subAllocateAirLosses().
        strSql = "SELECT * FROM ObjMissionData, Object_Specifications WHERE " _
                & "ObjMissionData.Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                & "ObjMissionData.Obj_Type = Object_Specifications.Object_Type AND " _
                & "ObjMissionData.Transporting_Group = '" & modUtilities.strEncodeQ(strObj1Group) & "' AND " _
                & "ObjMissionData.Quantity > 0 AND " _
                & "Object_Specifications.Object_Class <> 'SAC' AND " _
                & "Object_Specifications.Object_Class <> 'SACE';"
        Call modDB.openDBRecordset(item, rsTransportedUnits, strSql)
        While rsTransportedUnits.EOF = False

            'Victim object details
            strObj1Group = rsTransportedUnits("Obj_Group_Name").Value
            intObj1Quantity = rsTransportedUnits("Quantity").Value

            intMember = 0
            While intMember < intObj1Quantity

                'The individual object to be destroyed.
                strObj1 = strObj1Group & CStr(intMember)

                If isSeaUnitSingle = True Then
                    'This is a single-sided sea unit action event, e.g. a collision.
                    'Create SQL Statement for a single-sided action.
                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
                    & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strObj1) & "', '" & modDB.strSQLquote(strObj1Group) & "', '" & strAction & "', '', " _
                    & "'', " & basFiles.StrMissionName _
                    & "', '" & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" & Format(dtTime, "HH:mm:ss") & "')"

                Else

                    'Construct a separate destruction event for each object in each unit in the recordset.
                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
                    & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strObj1) & "', '" & modDB.strSQLquote(strObj1Group) & "', '" & strAction & "', '" _
                    & modDB.strSQLquote(strObj2) & "', '" & modDB.strSQLquote(strObj2Group) & "', '" & basFiles.StrMissionName _
                    & "', '" & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" & Format(dtTime, "HH:mm:ss") & "')"

                End If

                'Add this object to the AirframesLost string for later capture analysis.
                strAirframesLost &= strObj1 & "|"


                'Write this action event to the database.
                Call modDB.ExecuteDBCommand(item, strSql)

                'Go to the next object, if any.
                intMember += 1

            End While


            'Move to the next transported unit.
            rsTransportedUnits.MoveNext()

        End While

        'Close the transported unit recordset.
        rsTransportedUnits.Close()

        Exit Sub

ErrorHandler:
        Call basMess.MessageBox("modMissionAnalyzer.subActionEvent: " _
            & Err.Number & " " & Err.Description, vbCritical)

    End Sub


    'This function finds the barraging unit that has target coordinates closest to the supplied destruction event coordinates.
    Private Sub subAllocateDestructionToBarrage(ByRef strLuckyBarrager As String, ByRef strLuckyBarragerGroup As String, _
                                                ByVal dblX_Axis As Double, ByVal dblY_Axis As Double, ByVal dtTime As Date)

        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisBarrageUnit As SE_Unit
        Dim strBarrageUnit, strSuccessfulObj As String
        Dim dblMinDisplacement, dblDist As Double
        Dim intThisEventTime As Integer

        Try
            'Set a wide search radius in metres.
            dblMinDisplacement = 10000.0

            'Set an initialization value for the successful barrager identity.
            strSuccessfulObj = "not set"

            'Construct the elapsed time (in seconds) of this event, taking into account possible day changes at midnight.
            intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)

            'Enumerate the hash of barragers.
            thisEnumerator = hashBarrageUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strBarrageUnit = thisEnumerator.Key
                thisBarrageUnit = thisEnumerator.Value

                'Calculate the distance between the destruction coordinates and the barrage target coordinates.
                dblDist = modUtilities.dblGetDistance(CStr(dblX_Axis), CStr(dblY_Axis), CStr(thisBarrageUnit.TargetX), CStr(thisBarrageUnit.TargetY))

                'If the barrager is on target, then gather its obj name and its group name.
                If dblDist < dblMinDisplacement And _
                   dblDist < thisBarrageUnit.BarrageDispersion + 50 And _
                   thisBarrageUnit.ActivationTime < intThisEventTime Then
                    strSuccessfulObj = thisBarrageUnit.Object_Group_Name
                    dblMinDisplacement = dblDist
                End If

            End While

            'If we have identified a lucky barrager, then return its group name.
            If strSuccessfulObj <> "not set" Then
                strLuckyBarrager = strSuccessfulObj
                strLuckyBarragerGroup = strSuccessfulObj
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAllocateDestructionToBarrage: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This function delays any damaged airframes.
    Private Sub subDelayDamagedAirframe(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim strObj1 As String
        Dim strObj1Group As String
        Dim strAction As String
        Dim strTemp As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim strSql As String
        Dim dtMissionStart, dtTime As Date
        Dim intObj1Quantity, intAirframe As Integer
        Dim thisUnit As SE_Unit
        Dim isAirUnit As Boolean
        Dim intThisEventTime As Integer
        Dim strPilotSeat, strDriveableSeat, strAirframe, strPilot, strLog As String

        Try
            'Parse the current line, removing tabs and double spaces.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the time the incident took place.
            dtTime = Trim(Replace(strLineArray(0), "\t", ""))

            'Extract raw Object 1 name.
            strObj1 = Trim(Replace(strLineArray(1), "\t", ""))
            strPilotSeat = strObj1
            strDriveableSeat = strObj1
            If InStr(strObj1, "(") > 0 Then
                strAirframe = Trim(Left(strObj1, InStr(strObj1, "(") - 1))
                strObj1 = Left(strObj1, InStr(strObj1, "(") - 1)
            End If

            'Convert the raw name into the proper name.
            Call subGetProperName(item, strObj1, isAirUnit)

            'If the protagonist is the host seat runway, then ignore this event.
            If strObj1 = basMain.HOSTRUNWAY Or strObj1 = basMissionBuilder.strINFSignallerDummy Then
                Exit Sub
            End If

            'Get the group for Object 1.
            strObj1Group = modUtilities.getGroupName(strObj1, isAirUnit)

            'Extract the location in grid coordinate that the action took place. 
            dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
            dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

            'Interpret the action.
            If InStr(strLine, "damaged by landscape") > 0 Then
                strAction = "landing accident"
            Else
                strAction = "combat action"
            End If

            'Construct the elapsed time (in seconds) of this event, taking into account possible day changes at midnight.
            intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)

            'New in DCS v3.3.0
            'Add an airframe penalty because of the damage.
            If CS_Landing_Penalties = 1 And hashUnit.ContainsKey(strObj1Group) = True And isAirUnit = True Then

                'Find the present flight destination coords in the hashUnit structure.
                thisUnit = hashUnit(strObj1Group)

                'Find the airframe number.
                intAirframe = Right(strPilotSeat, 1)

                'If the landing is not within CS_Control_Radius, then the aircraft landed too far away.
                'We should penalize the pilot for his poor airmanship!
                strPilot = strGetPilotByAircraft(strPilotSeat)
                If InStr(strPilot, "AI") > 0 Then
                    strPilot = "AI (" & modUtilities.strSkillText(thisUnit.Skill) & ")"
                End If

                strLog = modUtilities.strAlignment(thisUnit.Nationality) & " aircraft " _
                        & (intAirframe + 1) & " of " _
                        & thisUnit.Unit_Name & " (" & thisUnit.Verbose_Type _
                        & ") piloted by " & strPilot _
                        & " was damaged in a " & LCase(strAction) _
                        & " and will be delayed one additional mission " _
                        & "before returning to duty."
                modUtilities.subAddPenaltyToAirframe(thisUnit, intAirframe, strLog)
                hashUnit(strObj1Group) = thisUnit

            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subDelayDamagedAirframe: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub



    'This routine handles ship damage events only. All events logged will be stored in
    'the ActionData table, even if the ship exceeds its maximum damage level after previous
    'damage is carried forward.
    Private Sub subActionEventShip(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim strProtagonist, strProtagonistNation As String
        Dim strProtagonistGroup As String
        Dim strProtagonistChunk As String
        Dim strAntagonist, strAntagonistNation As String
        Dim strAntagonistGroup As String
        Dim strAction As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim dblPreviousDamage As Double
        Dim strSql As String
        Dim dtMissionStart, dtTime As Date
        Dim intMember As Integer
        Dim intProtagonistQuantity As Integer
        Dim thisActionUnit As SE_Unit
        Dim thisShipStatus As SE_Ship_Damage_Events
        Dim thisInstallation As SE_Installation
        Dim isAirUnit, isInstallation As Boolean
        Dim intThisEventTime As Integer
        Dim strChunkEvents, strChunkEventDetails As String

        'Parse the current line, removing tabs and double spaces.
        strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

        'Extract the time the incident took place.
        dtTime = Trim(Replace(strLineArray(0), "\t", ""))

        'Extract raw protagonist name.
        strProtagonist = Trim(Replace(strLineArray(2), "\t", ""))
        If InStr(strProtagonist, "(") > 0 Then
            strProtagonist = Left(strProtagonist, InStr(strProtagonist, "(") - 1)
        End If

        'Extract the chunk name sustaining damage.
        strProtagonistChunk = Trim(Replace(strLineArray(3), "\t", ""))
        If InStr(strProtagonistChunk, "(") > 0 Then
            strProtagonistChunk = Left(strProtagonistChunk, InStr(strProtagonistChunk, "(") - 1)
        End If

        'Convert the raw protagonist name into the proper name.
        Call subGetProperName(item, strProtagonist, isAirUnit, strProtagonistNation)

        'If the protagonist is the host seat runway, then ignore this event.
        If strProtagonist = basMain.HOSTRUNWAY Or strProtagonist = basMissionBuilder.strINFSignallerDummy Then
            Exit Sub
        End If

        'Get the group for the protagonist.
        strProtagonistGroup = modUtilities.getGroupName(strProtagonist, isAirUnit)

        'Locate the current damage level for this protagonist ship.
        isInstallation = False
        If hashUnit.ContainsKey(strProtagonist) Then
            thisActionUnit = hashUnit(strProtagonist)
            If SHIPEVENTFORMAT = SHIPEVENTFORMAT_DETAILED Then
                'DETAILED format: The action will always be 'CHUNK damaged by (ddd.dd%)', where CHUNK is the descriptor for
                'the part of the ship that was hit.
                strAction = LCase(strProtagonistChunk) & " damaged by (" & Format((100.0 * CDbl(Trim(strLineArray(4)))), "##0.00") & "%)"
            Else
                'SPARSE format: The action will always be 'CHUNK damaged by', where CHUNK is the descriptor for
                'the part of the ship that was hit.
                strAction = LCase(strProtagonistChunk) & " damaged by"
            End If
        ElseIf strProtagonistGroup = "Infrastructure" Then
            isInstallation = True
            'The action will always be 'damaged by (ddd.dd%)'.
            strAction = "damaged by (" & Format((100.0 * CDbl(Trim(strLineArray(4)))), "##0.00") & "%)"
        Else
            Call basMess.MessageBox("modMissionAnalyzer.subActionEventShip: " _
                & "trying to gather damage status for unknown ship.", vbCritical)
        End If

        'Extract the location in grid coordinate that the action took place. 
        dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
        dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

        'Extract raw antagonist name.
        strAntagonist = Trim(Replace(strLineArray(7), "\t", ""))
        If InStr(strAntagonist, "(") > 0 Then
            strAntagonist = Left(strAntagonist, InStr(strAntagonist, "(") - 1)
        End If

        'Convert the raw antagonist name into the proper name.
        Call subGetProperName(item, strAntagonist, isAirUnit, strAntagonistNation)

        'Get the group for the antagonist.
        strAntagonistGroup = modUtilities.getGroupName(strAntagonist, isAirUnit)

        'Register this hit in the hashShipEvents hash by noting the chunk cumulative damage state.
        'Once the eventlog is all parsed, the ship hashes will be scanned to detect "sunk" events.
        If hashShipEvents.ContainsKey(strProtagonist) Then

            'Get the current status structure.
            thisShipStatus = hashShipEvents(strProtagonist)
            strChunkEvents = thisShipStatus.ChunkEvents

            'Construct the ChunkEvent value for this chunk event.
            strChunkEventDetails = Trim(strLineArray(0)) & "|"      'time of event
            strChunkEventDetails &= strProtagonist & "|"            'victim ship
            strChunkEventDetails &= strProtagonistGroup & "|"       'victim ship group
            strChunkEventDetails &= Trim(strLineArray(3)) & "|"     'hit chunk
            strChunkEventDetails &= Trim(strLineArray(4)) & "|"     'chunk cumulative or incremental damage
            strChunkEventDetails &= strAntagonist & "|"             'aggressor
            strChunkEventDetails &= strAntagonistGroup & "|"        'aggressor unit group
            strChunkEventDetails &= CStr(dblX_Axis) & "|"           'X location of event
            strChunkEventDetails &= CStr(dblY_Axis)                 'Y location of event

            'Add this chunk hit to the list of hits for this ship unit.
            strChunkEvents &= strChunkEventDetails & "^"

            'If the ship is still floating, log the identity of the antagonist as the last to score a hit.
            'Any hits after the ship is sunk will not be counted for kill purposes.
            If modUtilities.isSunk(thisActionUnit, strChunkEvents) = False Then
                thisShipStatus.LastAntagonist = strAntagonist
                thisShipStatus.LastAntagonistGroup = strAntagonistGroup
                thisShipStatus.LastHitX = Math.Round(dblX_Axis)
                thisShipStatus.LastHitY = Math.Round(dblY_Axis)
            End If

            If strProtagonistNation <> strAntagonistNation And strAntagonistNation <> "n" Then
                thisShipStatus.isSpotted = True
            End If

            'Repack the updated status.
            thisShipStatus.ChunkEvents = strChunkEvents
            hashShipEvents(strProtagonist) = thisShipStatus

        ElseIf hashInstallation.ContainsKey(Replace(strProtagonist, "INF_", "")) Then

            'Get the current status structure, assuming reported damage events are cumulative.
            thisInstallation = hashInstallation(Replace(strProtagonist, "INF_", ""))
            If thisInstallation.isDestroyed <> True Then
                If basMain.SHIPDAMAGEMODEL = basMain.SHIPDAMAGEMODEL_CUMULATIVE Then
                    thisInstallation.Damage_New = 100 * CDbl(Trim(strLineArray(4)))
                Else
                    thisInstallation.Damage_New += 100 * CDbl(Trim(strLineArray(4)))
                End If
                thisInstallation.Antagonist = strAntagonist
                thisInstallation.AntagonistGroup = strAntagonistGroup
                thisInstallation.X = Math.Round(dblX_Axis)
                thisInstallation.Y = Math.Round(dblY_Axis)
                thisInstallation.Clock = Trim(strLineArray(0))
                thisInstallation.isDestroyed = False
                hashInstallation(Replace(strProtagonist, "INF_", "")) = thisInstallation
            End If

        Else
            'This branch becomes active with HSFX5 logging damage to temporary runways.
            'Therefore in DCS v3.3.8 we will inactivate the following alert.
            'Call basMess.MessageBox("modMissionAnalyzer.subActionEventShip: " _
            '    & "trying to register chunk damage for unknown ship.", vbExclamation)
        End If

        'Create SQL Statement
        strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, " _
                & "Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                & modDB.strSQLquote(strProtagonist) & "', '" _
                & modDB.strSQLquote(strProtagonistGroup) & "', '" _
                & Trim(strAction) & "', '" _
                & modDB.strSQLquote(strAntagonist) & "', '" _
                & modDB.strSQLquote(strAntagonistGroup) & "', '" _
                & basFiles.StrMissionName & "', '" _
                & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" _
                & Format(dtTime, "HH:mm:ss") & "')"


        On Error Resume Next
        'Execute SQL Statement.  Notice that we changed the the error handling to 'Resume Next'. We can have 
        'situations where multiple hits may be reported simultaneously for the same chunk. 
        'We don't deal with multiple synchronous hits from the same antagonist so we ignore any of 
        'these errors and just don't write that line.
        'This has a side-effect of ignoring ALL SQL errors, so if you think that the ActionEvents are not
        'being written to the database correctly, comment out the above statement and check it out.

        'Execute the insertion of this ship damage event.
        modDB.ExecuteDBCommand(item, strSql, False)

    End Sub



    'This routine handles trigger activation events only. All events logged will be registered in
    'the Mission_Triggers table for later inclusion in Campaign Notices display.
    Private Sub subTriggerEvent(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim strSignaller As String
        Dim strScrambler As String
        Dim strScramblerGroup As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim strSql As String
        Dim dtMissionStart, dtTime As Date
        Dim intThisEventTime As Integer
        Dim thisUnit As SE_Unit
        Dim isMessageOnly As Boolean

        Try

            'Parse the current line, removing tabs and double spaces.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the time the incident took place.
            dtTime = Trim(Replace(strLineArray(0), "\t", ""))

            'Extract raw scrambler name.
            If InStr(strLine, "A message was activated") > 0 Then
                strScrambler = ""
                isMessageOnly = True
            Else
                strScrambler = Trim(Replace(strLineArray(1), "\t", ""))
                If InStr(strScrambler, "(") > 0 Then
                    strScrambler = Left(strScrambler, InStr(strScrambler, "(") - 1)
                    strScramblerGroup = modUtilities.getUnitGroupFromUNM(item, strScrambler)
                End If
                isMessageOnly = False
            End If

            'Extract raw signaller name (n_Trigger).
            If isMessageOnly = True Then
                strSignaller = Trim(Replace(strLineArray(6), "\t", ""))
            Else
                strSignaller = Trim(Replace(strLineArray(5), "\t", ""))
            End If
            If InStr(strSignaller, "(") > 0 Then
                strSignaller = Left(strSignaller, InStr(strSignaller, "(") - 1)
            End If

            'Extract the signaller location in grid coordinates when the trigger was activated. 
            dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
            dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

            'Create SQL Statement
            strSql = "UPDATE Mission_Triggers SET " _
                    & "Triggered=1, " _
                    & "Trigger_X=" & CInt(dblX_Axis) & "," _
                    & "Trigger_Y=" & CInt(dblY_Axis) & "," _
                    & "Trigger_Grid='" & modUtilities.MapGridCoordsKP(CInt(dblX_Axis), CInt(dblY_Axis)) & "'," _
                    & "Trigger_Time='" & Format(dtTime, "HH:mm:ss") & "' " _
                    & "WHERE " _
                    & "Mission = '" & basFiles.StrMissionName & "' AND " _
                    & "Trigger_Name = '" & strSignaller & "';"

            'Execute the trigger event update.
            modDB.ExecuteDBCommand(item, strSql, False)

            'Store the name of any associated scrambler for later use in unit advance calculations.
            If isMessageOnly = False Then
                strMissionActuallyScrambled &= strScramblerGroup & "|"
            Else
                strScramblerGroup = "noscrambler"
            End If

            'Update the scramble time for the scrambler unit, and ETAs of all associated interventions.
            If isMessageOnly = False And hashUnit.ContainsKey(strScramblerGroup) = True Then
                thisUnit = hashUnit(strScramblerGroup)
                thisUnit.Scrambled_T = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)
                hashUnit(strScramblerGroup) = thisUnit

                'Recon photos.
                strSql = "UPDATE Mission_Recon SET Recon_ETA = Recon_ETA+" & thisUnit.Scrambled_T & " " _
                       & "WHERE Recon_Unit = '" & thisUnit.Object_Group_Name & "' AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "'"
                Call modDB.ExecuteDBCommand(item, strSql)

                'Supply drops.
                strSql = "UPDATE Supply_Drop_Orders SET Drop_ETA = Drop_ETA+" & thisUnit.Scrambled_T & " " _
                       & "WHERE Transport_Flight = '" & thisUnit.Object_Group_Name & "' AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "'"
                Call modDB.ExecuteDBCommand(item, strSql)

                'Paratrooper assaults.
                strSql = "UPDATE Paratrooper_Assault_Orders SET Drop_ETA = Drop_ETA+" & thisUnit.Scrambled_T & " " _
                       & "WHERE Transport_Flight = '" & thisUnit.Object_Group_Name & "' AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "'"
                Call modDB.ExecuteDBCommand(item, strSql)

            End If

            'Scan through all active barragers and set the activation time of the scrambler, if present.
            Dim thisEnumerator As IDictionaryEnumerator
            Dim thisBarrageUnit As SE_Unit
            Dim strBarrageUnit As String

            'Enumerate the hash of barragers.
            thisEnumerator = hashBarrageUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the scrambler.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strBarrageUnit = thisEnumerator.Key
                thisBarrageUnit = thisEnumerator.Value

                'If the barrager is also the scrambler from this trigger event, then store the trigger time.
                If strBarrageUnit = strScramblerGroup Then
                    intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)
                    thisUnit.ActivationTime = Math.Min(thisUnit.ActivationTime, intThisEventTime)
                    hashBarrageUnit(strBarrageUnit) = thisUnit
                    Exit While
                End If

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subTriggerEvent: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub




    'This routine handles ship speed reduction events only. 
    Private Sub subReductionEventShip(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim strProtagonist As String
        Dim strProtagonistGroup As String
        Dim strProtagonistChunk As String
        Dim strThisEventTime, strNextSequence, strSql, strMessage, strWaypointUnit As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim dblNewMaxSpeed As Double
        Dim dtLastMissionSequence, dtNextMissionSequence As Date
        Dim intThisEventTime As Integer
        Dim isAirUnit As Boolean
        Dim thisShipSpeedProfile As New SE_MaxSpeedProfile
        Dim rsTF, rsTFL As New ADODB.Recordset
        Dim thisUnit As SE_Unit
        Try
            'Parse the current line, removing tabs and double spaces.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the elapsed time (seconds) that the reduction event took place.
            strThisEventTime = Trim(Replace(strLineArray(0), "\t", ""))
            intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, CDate(strThisEventTime))

            'Extract raw protagonist name.
            strProtagonist = Trim(Replace(strLineArray(2), "\t", ""))
            If InStr(strProtagonist, "(") > 0 Then
                strProtagonist = Left(strProtagonist, InStr(strProtagonist, "(") - 1)
            End If

            'Convert the raw protagonist name into the proper name.
            Call subGetProperName(item, strProtagonist, isAirUnit)

            'If the protagonist is the host seat runway, then ignore this event.
            If strProtagonist = basMain.HOSTRUNWAY Or strProtagonist = basMissionBuilder.strINFSignallerDummy Then
                Exit Sub
            End If

            'Get the group for the protagonist.
            strProtagonistGroup = modUtilities.getGroupName(strProtagonist, isAirUnit)

            'Extract the new maximum speed (in metres per second) for the damaged protagonist.
            dblNewMaxSpeed = CDbl(Trim(strLineArray(5)))

            'Extract the location in grid coordinate that the action took place. 
            dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
            dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

            'Register this max speed reduction event in the hashShipSpeedProfile hash.
            'This information will be used later in basMissionBuilder.subAdvanceUnit()
            If hashShipSpeedProfile.ContainsKey(strProtagonist) Then

                'Get the current speed profile structure.
                thisShipSpeedProfile = hashShipSpeedProfile(strProtagonist)

                'Add this event to the maximum speed profile data.
                thisShipSpeedProfile.strElapsedTime &= "|" & CStr(intThisEventTime)
                thisShipSpeedProfile.strMaxSpeed &= "|" & Format(dblNewMaxSpeed, "###.#####")
                thisShipSpeedProfile.strX &= "|" & Format(dblX_Axis, "###.#####")
                thisShipSpeedProfile.strY &= "|" & Format(dblY_Axis, "###.#####")

                'Store back in the hash for later use.
                hashShipSpeedProfile(strProtagonist) = thisShipSpeedProfile

                'Build the next sequence number.
                dtLastMissionSequence = modUtilities.SN2DT(strLastMissionSequenceNumber)
                dtNextMissionSequence = DateAdd(DateInterval.Hour, CDbl(CS_Mission_Length), dtLastMissionSequence)
                strNextSequence = DT2SN(dtNextMissionSequence)

                'Look for a task force containing this protagonist.
                strSql = "SELECT * FROM Task_Forces WHERE " _
                        & "Mission_Sequence = '" & strNextSequence & "' AND " _
                        & "Unit_Group_Name = '" & strProtagonistGroup & "' AND " _
                        & "Map = '" & strThisMap & "';"
                Call modDB.openDBRecordset(item, rsTF, strSql)

                If rsTF.EOF = False Then

                    'Find the associated Task Force Leader unit, and remove any corresponding entries from the Waypoints table.
                    If rsTF("Task_Force_Rank").Value = "Leader" Then
                        strWaypointUnit = strProtagonistGroup
                    Else
                        strSql = "SELECT * FROM Task_Forces WHERE " _
                                & "Mission_Sequence = '" & strNextSequence & "' AND " _
                                & "Task_Force_Name = '" & rsTF("Task_Force_Name").Value & "' AND " _
                                & "Task_Force_Rank = 'Leader' AND " _
                                & "Map = '" & strThisMap & "';"
                        Call modDB.openDBRecordset(item, rsTFL, strSql)
                        If rsTFL.EOF = False Then
                            strWaypointUnit = rsTFL("Unit_Group_Name").Value
                        Else
                            strWaypointUnit = strProtagonistGroup
                        End If
                        rsTFL.Close()
                    End If

                    'Remove any waypoints carried forward - the Task Force is removed and all members are stationary.
                    'strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Waypoints WHERE " _
                    '        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    '        & "Unit_Group = '" & strWaypointUnit & "';"
                    'Call modDB.ExecuteDBCommand(item, strSql)

                    'Dissolve any Task Force that contains this protagonist.
                    strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Task_Forces WHERE " _
                            & "Task_Force_Name = '" & rsTF("Task_Force_Name").Value & "' AND " _
                            & "Mission_Sequence = '" & strNextSequence & "' AND " _
                            & "Nationality = '" & rsTF("Nationality").Value & "' AND " _
                            & "Map = '" & strThisMap & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    'Locate the protagonist details.
                    If hashUnit.ContainsKey(strProtagonistGroup) = True Then
                        thisUnit = hashUnit(strProtagonistGroup)

                        'Issue a campaign notice for this instance.
                        strMessage = modUtilities.strAlignment(rsTF("Nationality").Value) & " ship " _
                                    & modUtilities.strEncodeQ(thisUnit.Unit_Name) _
                                    & " (" & modUtilities.strEncodeQ(thisUnit.Verbose_Type) & ") " _
                                    & "has suffered combat damage, causing " & modUtilities.strAlignment(rsTF("Nationality").Value) & " Task Force " _
                                    & rsTF("Task_Force_Name").Value & " to be dissolved and formation disrupted."
                        Call modUtilities.subTrackCampaignNotice(item, strThisMap, strLastMissionSequenceNumber, thisUnit.Nationality, strMessage)
                    Else
                        'Wow, we found a case where a ShipReduction event was logged for a non-existent unit!
                    End If

                End If
                rsTF.Close()


            Else
                Call basMess.MessageBox("modMissionAnalyzer.subReductionEventShip: " _
                    & "trying to register speed reduction for unknown ship.", vbCritical)
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subReductionEventShip: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This routine logs a destruction event of a combined unit.
    Private Sub registerCULoss(ByVal item As Integer, ByVal strLine As String)
        Dim strSql As String
        Dim strLineArray() As String
        Dim strVictim, strVictimGroup, strVictimMember, strVictimNation, strVictimType As String
        Dim strMember As String
        Dim isAirUnit As Boolean
        Dim thisCU As SE_CombinedUnit
        Dim intMember As Integer

        Try
            'Parse the logfile record.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract raw protagonist name.
            strVictim = Trim(Replace(strLineArray(1), "\t", ""))

            'Find the CU member that corresponds to this protagonist name.
            Call modUtilities.getCUByUNM(strVictim, thisCU, intMember)

            If intMember >= 0 Then
                strVictimGroup = thisCU.Group
                strVictimType = thisCU.Type
                strVictimNation = thisCU.Nation
                strMember = CStr(intMember)

            Else
                'Get the group of the victim.
                strVictimGroup = modUtilities.getGroupName(strVictim, isAirUnit)

                'Parse any designated member number.
                strVictimMember = Replace(strVictim, strVictimGroup, "")

                'Convert the raw name into the proper name.
                Call subGetProperName(item, strVictimGroup, isAirUnit)

                'Convert the raw name into the proper name.
                strVictimType = getObjectType(strVictimGroup)

                'Get the nationality of the victim.
                strVictimNation = strUnitNation(strVictimGroup)

                If Len(strVictimMember) > 0 Then

                    'We have a designated member of an intact combined unit.
                    strMember = strVictimMember

                Else

                    'We have an unidentified member of a partially destroyed combined unit.
                    strMember = basMain.COMBINED_MEMBER_DESTROYED

                End If

            End If

            'Insert a loss record into the Combined_Unit_Losses table.
            strSql = "INSERT INTO Combined_Unit_Losses (Map,Mission,Unit_Group,Object_Type,Member,Nation) VALUES " _
                    & "('" & strThisMap & "', " _
                    & "'" & basFiles.StrMissionName & "', " _
                    & "'" & modUtilities.strEncodeQ(strVictimGroup) & "', " _
                    & "'" & strVictimType & "', " _
                    & "'" & strMember & "', " _
                    & "'" & strVictimNation & "');"
            modDB.ExecuteDBCommand(item, strSql)


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.registerCULoss: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    'This routine gets the object class of the supplied protagonist group name.
    'The hashUnit hashtable is used.
    Private Function getObjectClass(ByVal strProtagonist As String) As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Try

            'Enumerate the hash.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strUnitName = thisEnumerator.Key

                If strUnitName = strProtagonist Then
                    thisUnit = thisEnumerator.Value
                    getObjectClass = thisUnit.Object_Class
                    Exit Function
                End If

            End While

            'No matching unit found, so return default value.
            getObjectClass = "VMI"

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.getObjectClass: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function



    'This routine gets the object class of the supplied protagonist group name.
    'The hashUnit hashtable is used.
    Private Function getObjectType(ByVal strProtagonist As String) As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Try

            'Enumerate the hash.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strUnitName = thisEnumerator.Key

                If strUnitName = strProtagonist Then
                    thisUnit = thisEnumerator.Value
                    getObjectType = thisUnit.Object_Type
                    Exit Function
                End If

            End While

            'No matching unit found, so return default value.
            getObjectType = "VMI"

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.getObjectType: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function


    'This routine tests the CU status of the supplied protagonist group name.
    'The hashUnit hashtable is used.
    Private Function isCombinedUnit(ByVal strProtagonist As String) As Boolean
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Try
            'Bail out for zero length protagonist units.
            If Len(strProtagonist) = 0 Or strProtagonist = Nothing Then
                isCombinedUnit = False
                Exit Function
            End If

            'Enumerate the hash.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strUnitName = thisEnumerator.Key
                thisUnit = thisEnumerator.Value

                If UCase(strUnitName) <> "SCENERY" Then

                    If strUnitName = strProtagonist And _
                      (InStr(thisUnit.Object_Type, COMBINED_VEHICLE) > 0 Or _
                       InStr(thisUnit.Object_Type, COMBINED_TRAIN) > 0 Or _
                       hashCombinedUnitDefinition.ContainsKey(thisUnit.Object_Type) = True) Then
                        isCombinedUnit = True
                        Exit Function
                    End If

                End If


            End While

            'No matching unit found, so return default value.
            isCombinedUnit = False

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isCombinedUnit: " & strUnitName & " :: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function



    'This routine tests the meta status of the supplied protagonist group name.
    'The hashUnit hashtable is used.
    Private Function isMetaUnit(ByVal strProtagonist As String, ByRef strProtagonistType As String) As Boolean
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Try
            'Bail out for zero length protagonist units.
            If Len(strProtagonist) = 0 Or strProtagonist = Nothing Then
                isMetaUnit = False
                Exit Function
            End If

            'Enumerate the hash.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strUnitName = thisEnumerator.Key
                thisUnit = thisEnumerator.Value

                If UCase(strUnitName) <> "SCENERY" Then

                    If strUnitName = strProtagonist And hashMetaUnitDefinition.ContainsKey(thisUnit.Object_Type) = True Then
                        isMetaUnit = True
                        strProtagonistType = thisUnit.Object_Type
                        Exit Function
                    End If

                End If


            End While

            'No matching unit found, so return default value.
            isMetaUnit = False
            strProtagonistType = ""

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isMetaUnit: " & strUnitName & " :: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function


    'This routine gets the object class of the supplied protagonist group name.
    'The hashUnit hashtable is used.
    Private Function strUnitNation(ByVal strGroup As String) As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Try

            'Enumerate the hash.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, looking for the protagonist.
            While thisEnumerator.MoveNext()

                'Here is the name of the current unit in the hash.
                strUnitName = thisEnumerator.Key
                thisUnit = thisEnumerator.Value

                If strUnitName = strGroup Then
                    strUnitNation = thisUnit.Nationality
                    Exit Function
                End If

            End While

            'No matching unit found, so return neutral value.
            strUnitNation = "n"

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.strUnitNation: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function





    'This routine uses the name mapping in Unit_Name_Mapping table to convert a raw (in-game) name
    'into a proper strategic unit name.
    Private Sub subGetProperName(ByVal item As Integer, ByRef strObjName As String, ByRef isAircraft As Boolean, Optional ByRef strNation As String = "n")
        Dim rsUNM As New ADODB.Recordset
        Dim strSql As String
        Dim strTestParked As String
        Dim SE_TestUnit As SE_Unit
        Try
            isAircraft = False

            'We have to take account of the possibility that the raw name can look like "p_Chiefq"
            'or "p_Staticq", where p and q are integers, and q can potentially be absent or, if not absent,
            'q will identify the member number of a combined unit, e.g. vehicle 6 of a truck convoy, or
            'tank3 of a tank platoon. We need to remove q from the raw name to get a match with the 
            'Unit_Name_Mapping table.
            If InStr(strObjName, "_Chief") > 0 Then
                'Object is a Chief.
                strObjName = Left(strObjName, InStr(strObjName, "_Chief") + 5)
            ElseIf InStr(strObjName, "_Static") > 0 Then
                'Object is a Static.
                strObjName = Left(strObjName, InStr(strObjName, "_Static") + 6)
            ElseIf InStr(strObjName, "_Rocket") > 0 Then
                'Object is a Rocket.
                strObjName = Left(strObjName, InStr(strObjName, "_Rocket") + 6)
            ElseIf InStr(strObjName, "Bridge") > 0 Then
                'Object is a bridge - use the same name and return isAircraft = False.
                Exit Sub
            ElseIf InStr(strObjName, "landscape") > 0 Then
                'Object is the landscape - use the same name and return isAircraft = False.
                Exit Sub
            ElseIf InStr(strObjName, "NONAME") > 0 Then
                'Object is the landscape - use the same name and return isAircraft = False.
                Exit Sub
            ElseIf InStr(strObjName, "_SE") > 0 Then
                'Object is any "_SE", should never happen.
                strObjName = Left(strObjName, InStr(strObjName, "_SE") + 2)
            Else
                'Object is an active aircraft. In this case the aircraft in-game name
                'is perfectly representative of the aircraft at the strategic SE layer, so
                'we normally don't need to perform any name mapping. The exception is top-up
                'flights where the incoming aircraft fly under a transfer squadron. To check 
                'for this eventuality we perform a name mapping for all aircraft.
                isAircraft = True
            End If

            'If we make it this far, we do need to perform the name mapping.
            'Make a recordset of the Unit_Name_Mapping table.
            strSql = "SELECT * FROM Unit_Name_Mapping WHERE InGame_Name = '" _
                    & strObjName & "' AND Theatre = '" & modCfg.User(item).TxtTheatre & "' AND " _
                    & "Unit_Name_Mapping.Mission = '" & basFiles.StrMissionName & "'"
            Call modDB.openDBRecordset(item, rsUNM, strSql)

            'If the recordset isn't empty, set strObjName to the proper name for the unit. 
            If rsUNM.EOF = False Then
                strObjName = rsUNM("Unit_Name").Value
                strNation = rsUNM("Nationality").Value

                'the following code is obsolescent and removed on 10 May 2015 to prevent
                'problems with unit names containing parentheses
                'If InStr(strObjName, "(") > 0 Then
                '   strObjName = Left(strObjName, InStr(strObjName, "(") - 1)
                'End If
            End If

            'Close the recordset.
            rsUNM.Close()

            'We now have the proper name as entered in the Unit_Name_Mapping table. There is another
            'case we need to check - parked aircraft. In this case, the proper name will also contain
            'an extra digit signifying the rank number of the parked plane. So we will check the 
            'hashUnits table for the present proper name.
            strTestParked = Left(strObjName, Len(strObjName) - 1)
            If hashUnit.ContainsKey(strTestParked) = True Then
                SE_TestUnit = hashUnit(strTestParked)
                If UCase(Left(SE_TestUnit.Object_Class, 1)) = "P" Then
                    'Yes, the current object is a parked aircraft.
                    isAircraft = True
                End If
            ElseIf hashDriveable.ContainsKey(strTestParked) = True Then
                'This is an alias flight for a driveable ground unit.
                'Return the ground unit name.
                strObjName = hashDriveable(strTestParked)
                isAircraft = False
            Else
                'No, there is no record of a unit with name = strTestParked
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGetProperName: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub



    'This subroutine handles reports for disposition events to be stored in ObjMissionDisposition.
    Private Sub subDispositionEvent(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String
        Dim intPosStart As Integer
        Dim intPosEnd As Integer
        Dim strObj As String
        Dim strPilotSeat, strAirframe As String
        Dim strObjGroup As String
        Dim strAction As String
        Dim thisUnit As SE_Unit
        Dim strTemp As String
        Dim strSql As String
        Dim dblY_Axis As Double
        Dim dblX_Axis As Double
        Dim dtTime As Date
        Dim isAirUnit, isDriveable As Boolean
        Dim isReconSuccessful As Boolean
        Dim isSupplyDropSuccessful As Boolean
        Dim isParatrooperAssaultSuccessful As Boolean
        Dim dtMissionStart, dtThisEventDay, dtThisEventTime As Date
        Dim intThisEventTime, intThisEventHour, intStartHour, intThisEventSeconds As Integer
        Dim thisCaptureEvent As SE_AirframeCapture
        Dim thisAICrew As AICrewCapture
        Dim dblLandingPrecision As Double
        Dim strPilot, strLog As String
        Dim intAirframe As Integer
        Dim strDriveableSeat As String


        Try
            'Set up success flags.
            isReconSuccessful = True
            isSupplyDropSuccessful = True
            isParatrooperAssaultSuccessful = True
            isDriveable = False

            'Parse the current line.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the Time.
            dtTime = Trim(strLineArray(0))

            'Extract the Object Name that is the active in this event.
            strObj = Trim(strLineArray(1))
            strPilotSeat = strObj
            strDriveableSeat = strObj
            If InStr(strObj, "(") > 0 Then
                strAirframe = Trim(Left(strObj, InStr(strObj, "(") - 1))
                strObj = Trim(Left(strObj, InStr(strObj, "(") - 1))
            End If

            'Convert the raw name into the proper name.
            Call subGetProperName(item, strObj, isAirUnit)
            If isAirUnit = False And _
              (InStr(strLine, "in his chute") > 0 Or _
               InStr(strLine, "was killed") > 0 Or _
               InStr(strLine, "wounded") > 0 Or _
               InStr(strLine, "captured") > 0 Or _
               InStr(strLine, "bailed") > 0 Or _
               InStr(strLine, "crashed") > 0 Or _
               InStr(strLine, "emergency landed") > 0 Or _
               InStr(strLine, "landed") > 0) Then
                isDriveable = True
            End If

            'Extract the Group Name.
            strObjGroup = modUtilities.getGroupName(strObj, isAirUnit)

            'Determine if this unit is a glider aircraft. If so, and if this was a capture event, then exit subroutine immediately!
            If hashUnit.ContainsKey(strObjGroup) Then
                thisUnit = hashUnit(strObjGroup)
                If thisUnit.Object_Class = "PGL" And InStr(strLine, "captured") > 0 Then
                    Exit Sub
                End If
            End If

            If CS_Create_Host_Seat = 0 Or isHostFlight(strObjGroup) = False Then

                'The action.
                If InStr(strLine, "in his chute") > 0 Then
                    strAction = "KIA"
                    isReconSuccessful = False
                    isSupplyDropSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashSupplyDropTime)
                    isParatrooperAssaultSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashParatroooperAssaultTime)
                ElseIf InStr(strLine, "was killed") > 0 Then
                    strAction = "KIA"
                    isReconSuccessful = False
                    isSupplyDropSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashSupplyDropTime)
                    isParatrooperAssaultSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashParatroooperAssaultTime)
                ElseIf InStr(strLine, "heavily wounded") > 0 Then
                    strAction = "Heavily Wounded"
                ElseIf InStr(strLine, "wounded") > 0 Then
                    strAction = "Wounded"
                ElseIf InStr(strLine, "was captured") > 0 Then
                    strAction = "Captured"
                    isReconSuccessful = False
                    isSupplyDropSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashSupplyDropTime)
                    isParatrooperAssaultSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashParatroooperAssaultTime)

                    'Need to test for corresponding destruction event later on.
                    thisCaptureEvent.Airframe_Name = strObj
                    thisCaptureEvent.X = strLineArray(5)
                    thisCaptureEvent.Y = strLineArray(6)
                    thisCaptureEvent.Time = strLineArray(0)
                    thisCaptureEvent.isDriveable = isDriveable
                    thisCaptureEvent.PilotSeat = strPilotSeat
                    If hashCaptureEvents.ContainsKey(strObj) = False Then
                        hashCaptureEvents(strObj) = thisCaptureEvent
                    End If

                    'Gather AI crew member details for subsequent interrogation. We use strObjGroup to test whether this 
                    'is a human-flown aircraft.
                    thisAICrew.CrewMember = Trim(strLineArray(1))
                    thisAICrew.Flight = strObjGroup
                    thisAICrew.EventTime = Trim(strLineArray(0))
                    thisAICrew.X = Trim(strLineArray(5))
                    thisAICrew.Y = Trim(strLineArray(6))
                    If hashCapturedAICrew.ContainsKey(thisAICrew.CrewMember) = False And _
                       strGetPilotByAircraft(thisAICrew.CrewMember) = "AI" Then
                        hashCapturedAICrew(thisAICrew.CrewMember) = thisAICrew
                    End If

                ElseIf InStr(strLine, "bailed out") > 0 Then
                    strAction = "Bailed"
                    isReconSuccessful = False
                    isSupplyDropSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashSupplyDropTime)
                    isParatrooperAssaultSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashParatroooperAssaultTime)
                ElseIf InStr(strLine, "crashed at") > 0 Then
                    strAction = "Crashed"
                    isReconSuccessful = False
                    isSupplyDropSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashSupplyDropTime)
                    isParatrooperAssaultSuccessful = modUtilities.hasDropMissionOccurred(strLine, hashParatroooperAssaultTime)
                    strAirframesLost &= strObj & "|"
                ElseIf InStr(strLine, "landed") > 0 Then
                    strAction = "Landed"
                ElseIf InStr(strLine, "emergency") > 0 Then
                    strAction = "Emergency Landed"
                ElseIf InStr(strLine, "in flight") > 0 Then
                    strAction = "In Flight"
                ElseIf InStr(strLine, "removed at") > 0 Then
                    strAction = "Landed"
                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subDispositionEvent: Unknown action event '" & strLine & "'", vbInformation)
                End If

                If InStr(strLineArray(1), "(") <= 0 Or isDriveable = True Then
                    'This is a disposition event for the aircraft.
                    'Extract the location in grid coordinate that the action took place. 
                    dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
                    dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

                    'Ignore this event if it is inside a metamap.
                    If CS_MetaMode = basMain.META_IL2_IF44 And modUtilities.isInMetaMap(CStr(dblX_Axis), CStr(dblY_Axis)) Then
                        Exit Sub
                    End If

                    'Create SQL Statement
                    strSql = "INSERT INTO ObjMissionDisposition (Obj, ObjGroup, Mission, Disposition, Y_Axis, " _
                            & "X_Axis, EventTime) VALUES ('" & strObj & "', '" & strObjGroup & "', '" _
                            & basFiles.StrMissionName & "', '" & strAction & "', '" & dblY_Axis & "', '" _
                            & dblX_Axis & "', '" & Format(dtTime, "HH:mm:ss") & "')"

                    'If the current unit is a glider and strAction is "Landed", set the unit location
                    'to the landing coordinates.
                    If hashUnit.ContainsKey(strObjGroup) = True Then
                        thisUnit = hashUnit(strObjGroup)
                        If thisUnit.Object_Class = "PGL" Or Left(thisUnit.Object_Class, 2) = "VP" Then
                            thisUnit.X = dblX_Axis
                            thisUnit.Y = dblY_Axis
                            hashUnit(strObjGroup) = thisUnit
                        End If
                    End If

                    'Make sure we update the pilot status as well as the plane status.
                    If strAction = "Landed" Or _
                       strAction = "Emergency Landed" Or _
                       strAction = "Crashed" Or _
                       strAction = "Captured" Or _
                       strAction = "KIA" Or _
                       strAction = "Bailed" Or _
                       strAction = "In Flight" Then

                        If isDriveable = True Then
                            subUpdatePilotStatus(item, strObjGroup, strAction, strLine, strDriveableSeat)
                        Else
                            subUpdatePilotStatus(item, strPilotSeat, strAction, strLine, strPilotSeat)
                        End If

                    End If

                Else
                    'This is a disposition event for the crew.
                    'Update any corresponding pilot status in the hashPilotDisposition hashtable.
                    If isDriveable = True Then
                        subUpdatePilotStatus(item, strObjGroup, strAction, strLine, strDriveableSeat)
                    Else
                        subUpdatePilotStatus(item, strPilotSeat, strAction, strLine, strPilotSeat)
                    End If

                End If


                'New in DCS v3.2.15
                'Check the landing location: if it does not match the planned destination then add a penalty!
                If CS_Landing_Penalties = 1 And hashUnit.ContainsKey(strObjGroup) = True And _
                   (strAction = "Landed" Or strAction = "Emergency Landed" Or strAction = "Crashed") Then

                    'Find the present flight destination coords in the hashUnit structure.
                    thisUnit = hashUnit(strObjGroup)

                    'Find the airframe number.
                    intAirframe = Right(strPilotSeat, 1)

                    'How far is the landing location away from the planned landing coordinates?
                    dblLandingPrecision = modUtilities.dblGetDistance(thisUnit.X, thisUnit.Y, dblX_Axis, dblY_Axis)

                    'If the landing is not within CS_Control_Radius, then the aircraft landed too far away.
                    'We should penalize the pilot for his poor airmanship!
                    If dblLandingPrecision > CS_Control_Radius Then
                        strPilot = strGetPilotByAircraft(strPilotSeat)
                        strLog = modUtilities.strAlignment(thisUnit.Nationality) & " pilot " _
                                & strPilot & " in control of aircraft " & (intAirframe + 1) & " of " _
                                & thisUnit.Unit_Name & " (" & thisUnit.Verbose_Type _
                                & ") has " & LCase(strAction) & " " & Format(dblLandingPrecision / 1000.0, "0.00") _
                                & " km from the planned destination, incurring an additional one mission " _
                                & "penalty for that aircraft."
                        modUtilities.subAddPenaltyToAirframe(thisUnit, intAirframe, strLog)
                        hashUnit(strObjGroup) = thisUnit
                    Else
                        modUtilities.subCancelAirframePenalties(thisUnit, intAirframe)
                        hashUnit(strObjGroup) = thisUnit
                    End If

                End If

                'If the driver of a vehicle is captured, then his vehicle is captured too.
                If isDriveable = True And strAction = "captured" Then

                End If

            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subDispositionEvent: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

        'Execute SQL Statement - again multiple crewmembers are possible so we ignore any errors from writing 
        'to the database.
        Try

            If CS_Create_Host_Seat = 0 Or isHostFlight(strObjGroup) = False Then
                Call modDB.ExecuteDBCommand(item, strSql, False)
            End If

        Catch ex As Exception
        End Try

        Try

            'If the destroyed object was running a Recon flight, set the Recon_Success field
            'to false for all its recon points. Ignore any errors, i.e. if this flight was not
            'a Recon flight.
            If hashUnit.ContainsKey(strObjGroup) = True Then
                thisUnit = hashUnit(strObjGroup)
            End If
            If isReconSuccessful = False And thisUnit.Object_Class <> "PSE" Then
                strSql = "UPDATE Mission_Recon SET Recon_Success = 0 " _
                        & "WHERE Recon_Unit = '" & strObjGroup & "' AND " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "'"
                Call modDB.ExecuteDBCommand(item, strSql)
            End If

            'Construct the elapsed time (in seconds) of this event, taking into account possible day changes at midnight.
            intThisEventTime = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, dtTime)

            'If the destroyed object was running a SupplyDrop flight, set the Drop_Success field
            'to false for all its SupplyDrop points. Ignore any errors, i.e. if this flight was not
            'a SupplyDrop flight.
            If isSupplyDropSuccessful = False Then
                strSql = "UPDATE Supply_Drop_Orders SET Drop_Success = 0 " _
                       & "WHERE Transport_Flight = '" & strObjGroup & "' AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                       & "Drop_ETA >= " & intThisEventTime & ";"
                Call modDB.ExecuteDBCommand(item, strSql)
            End If

            'If the destroyed object was running a Paratrooper Assault flight and at least one
            'parachute landed successfully, set the Drop_Success field
            'to true. Ignore any errors, i.e. if this flight was not a Paratrooper Assault flight.
            If isParatrooperAssaultSuccessful = True Then
                dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
                dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))
                strSql = "UPDATE Paratrooper_Assault_Orders SET " _
                       & "Drop_Success = 1, " _
                       & "Drop_X = " & CStr(dblX_Axis) & ", " _
                       & "Drop_Y = " & CStr(dblY_Axis) & ", " _
                       & "Drop_ETA = " & intThisEventTime & " " _
                       & "WHERE Transport_Flight = '" & strObjGroup & "' AND " _
                       & "Drop_Success = 0 AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "';"
                Call modDB.ExecuteDBCommand(item, strSql)
            End If


        Catch ex As Exception
        End Try

    End Sub

    'This subroutine adds an action event for every paratrooper KIA event.
    Private Sub subParatrooperEvent(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray(), strPU() As String
        Dim strSql As String
        Dim dtMissionStart, dtTime As Date
        Dim strObj, strObjGroup, strObj2, strObj2Group, strParatrooperData As String
        Dim isAirUnit As Boolean
        Dim intPosStart, intPosEnd, intPUcount, intThisPU, intThisEventTime As Integer
        Dim dblX_Axis, dblY_Axis As Double

        Try

            'Initialize the random seed.
            Randomize()

            'Ignore spurious parachute collision events all of the time.
            If (InStr(strLine, "has chute destroyed by _para_") > 0 Or _
               InStr(strLine, "has chute destroyed by NONAME") > 0 Or _
               InStr(strLine, "has chute destroyed by  at") > 0 Or _
               InStr(strLine, "was killed at ") > 0) Then
                Exit Sub
            End If

            'Parse the current line.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the Time.
            dtTime = Trim(strLineArray(0))

            'Extract the location.
            dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
            dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))

            'Extract the Object Name that is the active in this event.
            strObj = Trim(strLineArray(1))
            If InStr(strObj, "(") > 0 Then
                strObj = Trim(Left(strObj, InStr(strObj, "(") - 1))
            End If

            'Convert the air transport raw name into the proper name.
            Call subGetProperName(item, strObj, isAirUnit)

            'Extract the air transport Group Name.
            strObjGroup = modUtilities.getGroupName(strObj, isAirUnit)

            'Get the paratrooper unit names for this air transport.
            strParatrooperData = getParatroopers(item, basFiles.StrMissionName, strObjGroup)
            strPU = Split(strParatrooperData, "|")
            intPUcount = strPU.Length - 2

            'Allocate the event to a PU at random.
            intThisPU = CInt(Int((intPUcount + 1) * Rnd()))

            If InStr(strLine, "(255) was killed in his chute by") > 0 Or _
               InStr(strLine, "(-1) was killed in his chute by") > 0 Then

                'Roll the dice.
                If Rnd() < 0.2 Then
                    'Deus ex machina - no paratrooper kill.
                    Exit Sub
                End If

                'Extract Object 2 Name
                intPosStart = InStr(strLine, " by ") + 4
                strObj2 = Trim(Mid(strLine, intPosStart))
                intPosEnd = InStr(strObj2, " ")
                strObj2 = Replace(Left(strObj2, intPosEnd - 1), "\t", "")
                If InStr(strObj2, "(") > 0 Then
                    strObj2 = Trim(Left(strObj2, InStr(strObj2, "(") - 1))
                End If

                'Convert the raw name into the proper name.
                Call subGetProperName(item, strObj2, isAirUnit)

                'And Group...
                strObj2Group = modUtilities.getGroupName(strObj2, isAirUnit)

                'Make an ActionData event for this paratrooper KIA event.
                strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
                & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strPU(intThisPU)) & "', '" _
                & modDB.strSQLquote(strPU(intThisPU)) & "', 'destroyed by', '" _
                & modDB.strSQLquote(strObj2) & "', '" & modDB.strSQLquote(strObj2Group) & "', '" _
                & basFiles.StrMissionName & "', '" & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" _
                & Format(dtTime, "HH:mm:ss") & "')"
                modDB.ExecuteDBCommand(item, strSql)


            ElseIf InStr(strLine, "(255) has chute destroyed by ") > 0 Or _
                   InStr(strLine, "(-1) has chute destroyed by ") > 0 Then
                'Two-sided action event.

                'Roll the dice.
                If Rnd() < 0.5 Then
                    'Deemed to be just an equipment chute - no paratrooper kill.
                    Exit Sub
                End If

                'Extract Object 2 Name
                intPosStart = InStr(strLine, " by ") + 4
                strObj2 = Trim(Mid(strLine, intPosStart))
                intPosEnd = InStr(strObj2, " ")
                strObj2 = Replace(Left(strObj2, intPosEnd - 1), "\t", "")
                If InStr(strObj2, "(") > 0 Then
                    strObj2 = Trim(Left(strObj2, InStr(strObj2, "(") - 1))
                End If

                'Convert the raw name into the proper name.
                Call subGetProperName(item, strObj2, isAirUnit)

                'And Group...
                strObj2Group = modUtilities.getGroupName(strObj2, isAirUnit)

                'Make an ActionData event for this paratrooper KIA event.
                strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
                & "X_Axis, EventTime) VALUES ('parachute', '" & modDB.strSQLquote(strPU(intThisPU)) & "', 'strafed by', '" _
                & modDB.strSQLquote(strObj2) & "', '" & modDB.strSQLquote(strObj2Group) & "', '" _
                & basFiles.StrMissionName & "', '" & CInt(dblY_Axis) & "', '" & CInt(dblX_Axis) & "', '" _
                & Format(dtTime, "HH:mm:ss") & "')"
                modDB.ExecuteDBCommand(item, strSql)



            ElseIf InStr(strLine, "(255) was killed at ") > 0 Or _
                   InStr(strLine, "(-1) was killed at ") > 0 Then
                'Single-sided action event.

                'Roll the dice.
                If Rnd() < 0.5 Then
                    'Deemed to be just an equipment chute - no paratrooper kill.
                    Exit Sub
                End If

                'Make an ActionData event for this paratrooper KIA event.
                strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, Mission, Y_Axis, " _
                & "X_Axis, EventTime) VALUES ('" & modDB.strSQLquote(strPU(intThisPU)) _
                & "', '" & modDB.strSQLquote(strPU(intThisPU)) _
                & "', 'killed', '', " _
                & "'', '" & basFiles.StrMissionName _
                & "', '" & CInt(dblY_Axis) _
                & "', '" & CInt(dblX_Axis) _
                & "', '" & Format(dtTime, "HH:mm:ss") & "')"
                modDB.ExecuteDBCommand(item, strSql)


            ElseIf InStr(strLine, "(255) successfully bailed out at ") > 0 Or _
                   InStr(strLine, "(-1) successfully bailed out at ") > 0 Then
                'If the destroyed object was running a Paratrooper Assault flight and at least one
                'parachute landed successfully, set the Drop_Success field to true. 
                'Ignore any errors, i.e. if this flight was not a Paratrooper Assault flight.
                dtMissionStart = Format(modUtilities.SN2DT(strLastMissionSequenceNumber), "HH:mm:ss")
                intThisEventTime = DateDiff(DateInterval.Second, dtMissionStart, dtTime)
                dblX_Axis = CDbl(strLineArray(strLineArray.Length - 2))
                dblY_Axis = CDbl(strLineArray(strLineArray.Length - 1))
                strSql = "UPDATE Paratrooper_Assault_Orders SET " _
                       & "Drop_Success = 1, " _
                       & "Drop_X = " & CStr(dblX_Axis) & ", " _
                       & "Drop_Y = " & CStr(dblY_Axis) & ", " _
                       & "Drop_ETA = " & intThisEventTime & " " _
                       & "WHERE Transport_Flight = '" & strObjGroup & "' AND " _
                       & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                       & "Drop_ETA > " & intThisEventTime & ";"
                Call modDB.ExecuteDBCommand(item, strSql)

            Else
                'No paratrooper event recognized.

            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subParatrooperEvent: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub

    Private Function getParatroopers(ByVal item As Integer, ByVal strMissionName As String, ByVal strFreighter As String) As String
        Dim strSql As String
        Dim rsPD As New ADODB.Recordset

        Try

            'Default value.
            getParatroopers = ""

            'Get any paratroopers carried by this freight flight.
            strSql = "SELECT * FROM Paratrooper_Assault_Orders WHERE " _
                    & "Paratrooper_Assault_Orders.Mission_Name='" & strMissionName & "' AND " _
                    & "Paratrooper_Assault_Orders.Transport_Flight='" & strFreighter & "';"
            modDB.openDBRecordset(item, rsPD, strSql)

            'Loop through all records.
            While rsPD.EOF = False

                'Append each paratroop unit to the return string.
                getParatroopers &= rsPD("Paratrooper_Unit").Value & "|"

                'Next record.
                rsPD.MoveNext()
            End While

            'Close recordset.
            rsPD.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.getParatroopers: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function


    'This subroutine updates any disposition changes to the hashPilotDisposition table.
    Private Sub subUpdatePilotStatus(ByVal item As Integer, _
                                     ByVal strPilotSeat As String, ByVal strAction As String, _
                                     ByVal strLine As String, ByVal strDriveableSeat As String)
        Dim thisPilot As Pilot_Record
        Dim thisAICrew As AICrewDisposition
        Dim strPilot, strLineArray(), strEventTime, strX, strY, strProperFlight, strSkill As String
        Dim strRegiment, strAirframeIndex, strAirframeClass, strAirframeType, strAirframeAlignment As String
        Dim intCrewIndex As Integer
        Dim isAircraft As Boolean
        Dim strLineTemp As String

        Try

            'If this action refers to paratroopers, then ignore it here.
            If InStr(strPilotSeat, "(255)") > 0 Or InStr(strPilotSeat, "(-1)") > 0 Then
                Exit Sub
            End If

            'Parse the eventline into useful parameters.
            strLineTemp = Replace(strLine, "  ", " ")
            strLineArray = Split(strLineTemp, " ")
            strEventTime = strLineArray(0)
            strX = strLineArray(UBound(strLineArray) - 1)
            strY = strLineArray(UBound(strLineArray))

            If InStr(strLineArray(1), "(") > 0 Then
                strRegiment = Left(strLineArray(1), InStr(strLineArray(1), "(") - 4)
                strAirframeIndex = Mid(strLineArray(1), InStr(strLineArray(1), "(") - 3, 3)
                intCrewIndex = CInt(Replace(Mid(strLineArray(1), InStr(strLineArray(1), "(") + 1, 3), ")", ""))
            Else
                strRegiment = Left(strLineArray(1), Len(strLineArray(1)) - 3)
                strAirframeIndex = Mid(strLineArray(1), Len(strLineArray(1)) - 2, 3)
                intCrewIndex = -99
            End If

            strProperFlight = strRegiment & Left(strAirframeIndex, 2)
            Call subGetProperName(item, strProperFlight, isAircraft)
            strAirframeClass = getUnitDetails(strProperFlight, "Class")
            strAirframeType = getUnitDetails(strProperFlight, "Type")
            strAirframeAlignment = getUnitDetails(strProperFlight, "Alignment")
            strSkill = getUnitDetails(strProperFlight, "Skill")

            'Capture the hashPilotRecord keys into arrays.
            Dim PilotKeys As ArrayList = New ArrayList(hashPilotRecord.Keys)

            'Step through the keys in the hashPilotRecord hashtable.
            For Each Pkey As String In PilotKeys
                strPilot = Trim(Pkey)
                thisPilot = hashPilotRecord(strPilot)

                If strPilot <> strHostPilot Then

                    'If the supplied seat matches the hashtable seat, then the disposition of the
                    'corresponding pilot has changed, so update the hashtable value structure.
                    If thisPilot.Pilot_Seat = strPilotSeat Or _
                      (thisPilot.Pilot_Aircraft = strPilotSeat And _
                       InStr(strPilotSeat, "(") <= 0) Then
                        Select Case strAction
                            Case "KIA", "Crashed", "Captured", "Bailed"
                                If thisPilot.Pilot_Status <> "KIA" And _
                                   thisPilot.Pilot_Status <> "Captured" Then

                                    'Inner test for driveable vehicle status. Because driveables are 
                                    'related to ground platoons, the vehicle position in the group is
                                    'not tracked in strPilotSeat. So we need to test whether this event
                                    'actually pertains to the pilot by comparing strPilotSeat with 
                                    'strDriveableSeat. If they are different, we have a driveable group.
                                    'In that case we only log the event if the strDriveableSeat is equal to
                                    'the Raw_Seat stored for the pilot.
                                    If strPilotSeat = strDriveableSeat Or thisPilot.Raw_Seat = strDriveableSeat Then
                                        thisPilot.Pilot_Status = strAction
                                        thisPilot.EventTime = strEventTime
                                    End If

                                End If
                            Case Else
                                If thisPilot.Pilot_Status <> "KIA" And _
                                   thisPilot.Pilot_Status <> "Crashed" And _
                                   thisPilot.Pilot_Status <> "Captured" And _
                                   thisPilot.Pilot_Status <> "Bailed" Then

                                    'Inner test for driveable vehicle status. Because driveables are 
                                    'related to ground platoons, the vehicle position in the group is
                                    'not tracked in strPilotSeat. So we need to test whether this event
                                    'actually pertains to the pilot by comparing strPilotSeat with 
                                    'strDriveableSeat. If they are different, we have a driveable group.
                                    'In that case we only log the event if the strDriveableSeat is equal to
                                    'the Raw_Seat stored for the pilot.
                                    If strPilotSeat = strDriveableSeat Or _
                                       thisPilot.Raw_Seat = strDriveableSeat Or _
                                       thisPilot.Raw_Seat = strDriveableSeat & "(0)" Then
                                        thisPilot.Pilot_Status = strAction
                                        thisPilot.EventTime = strEventTime
                                    End If

                                End If
                        End Select
                        hashPilotRecord(strPilot) = thisPilot

                    Else
                        'This is a non-human crew member, so as of DCS v3.3.6 we will capture the details
                        'in a new hashtable and write these to the AI_Crew_Disposition table for later use.
                        If hashAICrewDisposition.ContainsKey(strPilotSeat) = False Then
                            Call setAICrewDefault(thisAICrew)
                            thisAICrew.Disposition = strAction
                            thisAICrew.EventTime = strEventTime
                            thisAICrew.Air_Regiment = strRegiment
                            thisAICrew.Airframe_Class = strAirframeClass
                            thisAICrew.Airframe_Index = strAirframeIndex
                            thisAICrew.Airframe_Type = strAirframeType
                            thisAICrew.Skill = CInt(strSkill)
                            thisAICrew.Alignment = strAirframeAlignment
                            thisAICrew.Crew_Index = intCrewIndex
                            thisAICrew.X = CInt(strX)
                            thisAICrew.Y = CInt(strY)
                            hashAICrewDisposition.Add(strPilotSeat, thisAICrew)

                        Else
                            thisAICrew = hashAICrewDisposition(strPilotSeat)
                            thisAICrew.Disposition = strAction
                            thisAICrew.EventTime = strEventTime
                            thisAICrew.X = CInt(strX)
                            thisAICrew.Y = CInt(strY)
                            hashAICrewDisposition(strPilotSeat) = thisAICrew
                        End If

                    End If

                End If

            Next

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subUpdatePilotStatus: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This routine finds the pilot name for a given pilot seat. The pilot seat must end in (0).
    Public Function strGetPilotByAircraft(ByVal strPilotSeat As String) As String
        Dim strPilot As String
        Dim thisPilot As Pilot_Record

        Try

            'The default function value is "AI"
            strGetPilotByAircraft = "AI"

            'If this pilot seat does not refer to the actual pilot "(0)", then ignore it here.
            'If InStr(strPilotSeat, "(0)") <= 0 Then
            'Exit Function
            'End If

            'Capture the hashPilotRecord keys into arrays.
            Dim PilotKeys As ArrayList = New ArrayList(hashPilotRecord.Keys)

            'Step through the keys in the hashPilotRecord hashtable.
            For Each Pkey As String In PilotKeys
                strPilot = Trim(Pkey)
                thisPilot = hashPilotRecord(strPilot)

                If strPilot <> strHostPilot Then

                    'If the supplied seat matches the hashtable seat, then we have found the pilot.
                    If thisPilot.Pilot_Seat = strPilotSeat Or _
                       (thisPilot.Pilot_Aircraft = strPilotSeat And _
                       InStr(strPilotSeat, "(") <= 0 And _
                       thisPilot.inControl = True) Then
                        strGetPilotByAircraft = strPilot
                        Exit Function
                    End If

                End If

            Next

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.strGetPilotByAircraft: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Function

    'All scoring data for all pilots is processed by this subroutine.  
    'This includes shots fired, hits, kills by type, 
    'and 'points' as assigned by the game.
    Private Sub subGatherScores(ByRef strLine As String)
        Dim strLineArray() As String

        Dim strPilotName As String = ""
        Dim strPilotState As String = ""
        Dim thisPilot As Pilot_Record
        Dim intPilotScore As Integer = 0
        Dim intEnemyAircraft As Integer = 0
        Dim intEnemyStaticAircraft As Integer = 0
        Dim intEnemyTank As Integer = 0
        Dim intEnemyCar As Integer = 0
        Dim intEnemyArtillery As Integer = 0
        Dim intEnemyAAA As Integer = 0
        Dim intEnemyWagon As Integer = 0
        Dim intEnemyShip As Integer = 0
        Dim intFriendAircraft As Integer = 0
        Dim intFriendStaticAircraft As Integer = 0
        Dim intFriendTank As Integer = 0
        Dim intFriendCar As Integer = 0
        Dim intFriendArtillery As Integer = 0
        Dim intFriendAAA As Integer = 0
        Dim intFriendWagon As Integer = 0
        Dim intFriendShip As Integer = 0
        Dim intFireBullets As Integer = 0
        Dim intHitBullets As Integer = 0
        Dim intHitAirBullets As Integer = 0
        Dim intFireRockets As Integer = 0
        Dim intHitRockets As Integer = 0
        Dim intFireBombs As Integer = 0
        Dim intHitBombs As Integer = 0

        Dim intPilotNameStart As Integer
        Dim strScoreType As String
        Dim strSql As String
        Dim isRegisteredPilot As Boolean

        Try
            'We should be at a "Name:" line in the mission log. We will run through the rest of this Pilot section
            'to compile score statistics and store them in the ScoreData table.

            'Parse the current line.
            strLineArray = Nothing
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))

            'Extract the pilot name.
            intPilotNameStart = InStr(strLine, "Name:") + 5
            strPilotName = Mid(strLine, intPilotNameStart)
            strPilotName = Replace(Replace(strPilotName, "\t", ""), "'", "")
            strPilotName = Trim(strPack(strPilotName))

            'Grab the Pilot_Record for this pilot.
            If strPilotName <> strHostPilot Then
                If hashPilotRecord.ContainsKey(strPilotName) Then
                    thisPilot = hashPilotRecord(strPilotName)
                    isRegisteredPilot = True
                Else
                    Call basMess.MessageBox("Pilot " & strPilotName & " was not properly registered in this mission!" & vbCrLf & vbCrLf & "This pilot will not appear in post-mission statistics.", vbInformation)
                    thisPilot = New Pilot_Record
                    thisPilot.inControl = False
                    thisPilot.Pilot_Aircraft = "unknown"
                    thisPilot.Pilot_Name = strPilotName
                    thisPilot.Pilot_Seat = "unknown"
                    thisPilot.Pilot_Status = "unknown"
                    thisPilot.Raw_Seat = "unknown"
                    thisPilot.EventTime = "unknown"
                    isRegisteredPilot = False
                    'Exit Sub
                End If
            End If

            strLine = srLogMA.ReadLine()
            Do Until InStr(strLine, "---------------") > 0

                'Parse the current line.
                strLineArray = Nothing
                strLineArray = Split(Trim(strLine), ":")

                'Set up the score type field.
                strScoreType = Trim(strLineArray(0))

                'Grab the data and load the appropriate variable.
                If InStr(strScoreType, "Score") Then
                    thisPilot.FBScore = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "State") > 0 Then
                    'Ignore the FB State data. This data will already have been collected
                    'from single-sided disposition events.

                ElseIf InStr(strScoreType, "Enemy Aircraft") Then
                    thisPilot.E_Air = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Static Aircraft") Then
                    thisPilot.E_StaticAir = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Tank") Then
                    thisPilot.E_Tank = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Car") Then
                    thisPilot.E_Car = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Artillery") Then
                    thisPilot.E_Artillery = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy AAA") Then
                    thisPilot.E_AAA = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Wagon") Then
                    thisPilot.E_Wagon = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Enemy Ship") Then
                    thisPilot.E_Ship = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Aircraft") Then
                    thisPilot.F_Air = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Static Aircraft") Then
                    thisPilot.F_StaticAir = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Tank") Then
                    thisPilot.F_Tank = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Car") Then
                    thisPilot.F_Car = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Artillery") Then
                    thisPilot.F_Artillery = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend AAA") Then
                    thisPilot.F_AAA = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Wagon") Then
                    thisPilot.F_Wagon = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Friend Ship") Then
                    thisPilot.F_Ship = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Fire Bullets") Then
                    thisPilot.Bullets = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Hit Bullets") Then
                    thisPilot.BulletsHit = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Hit Air Bullets") Then
                    thisPilot.BulletsHitAir = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Fire Roskets") Then
                    thisPilot.Rockets = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Hit Roskets") Then
                    thisPilot.RocketsHit = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Fire Bombs") Then
                    thisPilot.Bombs = CInt(Trim(strPack(strLineArray(1))))

                ElseIf InStr(strScoreType, "Hit Bombs") Then
                    thisPilot.BombsHit = CInt(Trim(strPack(strLineArray(1))))

                End If


                'Update the Pilot Record.
                If isRegisteredPilot = True Then
                    hashPilotRecord(strPilotName) = thisPilot
                ElseIf strPilotName <> strHostPilot Then
                    hashPilotRecord.Add(strPilotName, thisPilot)
                    isRegisteredPilot = True
                End If


                'Step forward to the next line of the log file.
                strLine = srLogMA.ReadLine()

            Loop


            'Now we have gathered all the statistics for the pilot, write the data to Pilot_Scores
            'as long as the pilot was not in the host seat.
            'No longer used - replaced by subStorePilotRecords()
            'If strPilotName <> strHostPilot Then
            'strSql = "INSERT INTO Pilot_Scores (Mission, Pilot_Name, Pilot_State, Pilot_Score, " _
            '        & "Enemy_Aircraft, Enemy_Static_Aircraft, Enemy_Tank, Enemy_Car, " _
            '        & "Enemy_Artillery, Enemy_AAA, Enemy_Wagon, Enemy_Ship, " _
            '        & "Friend_Aircraft, Friend_Static_Aircraft, Friend_Tank, Friend_Car, " _
            '        & "Friend_Artillery, Friend_AAA, Friend_Wagon, Friend_Ship, " _
            '        & "Fire_Bullets, Hit_Bullets, Hit_Air_Bullets, " _
            '        & "Fire_Rockets, Hit_Rockets, Fire_Bombs, Hit_Bombs) VALUES ('" _
            '        & basFiles.StrMissionName & "', '" & strPilotName & "', '" & strPilotState & "'," & intPilotScore & ", " _
            '        & intEnemyAircraft & "," & intEnemyStaticAircraft & "," & intEnemyTank & "," & intEnemyCar & "," _
            '        & intEnemyArtillery & "," & intEnemyAAA & "," & intEnemyWagon & "," & intEnemyShip & "," _
            '        & intFriendAircraft & "," & intFriendStaticAircraft & "," & intFriendTank & "," & intFriendCar & "," _
            '        & intFriendArtillery & "," & intFriendAAA & "," & intFriendWagon & "," & intFriendShip & "," _
            '        & intFireBullets & "," & intHitBullets & "," & intHitAirBullets & "," _
            '        & intFireRockets & "," & intHitRockets & "," & intFireBombs & "," & intHitBombs & ")"
            'modDB.executeDBCommand(item,strSql)
            'End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGatherScores: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub



    'This function creates a hashtable entry for each pilot that enters the game and takes a plane, and then 
    'enters that into the database.
    Private Sub subSeatOccupied(ByVal strLine As String)
        Dim strLineArray() As String
        Dim intPosStart As Integer
        Dim intPosEnd As Integer
        Dim strObj As String
        Dim strPilotPosition As String
        Dim thisPilot As Pilot_Record
        Dim strGroup As String
        Dim intPilotNameStart As Integer
        Dim intPilotNameEnd As Integer
        Dim strSql As String
        Dim strPilot As String
        Dim rsPilots As New ADODB.Recordset

        Try
            'Parse the current line.
            strLineArray = Split(Trim(Replace(Replace(strLine, "  ", " "), "\t", "")))


            If InStr(strLine, "trying to occupy") > 0 Then

                'Extract the aircraft object name from the current line.
                strObj = strLineArray(strLineArray.GetUpperBound(0))
                strPilotPosition = strObj
                strObj = Left(strObj, InStr(strObj, "(") - 1)

                'Extract the aircraft hierarchical group name.
                strGroup = modUtilities.getGroupName(strObj, True)

                'Check for driveable flight alias.
                If hashDriveable.ContainsKey(Left(strObj, Len(strObj) - 1)) = True Then
                    strObj = hashDriveable(Left(strObj, Len(strObj) - 1))
                    strPilotPosition = strObj
                    strGroup = modUtilities.getGroupName(strObj, False)
                End If

                'Extract Pilot Name.
                intPilotNameStart = 10
                intPilotNameEnd = InStrRev(strLine, " is trying to occupy seat ") - 1
                strPilot = Mid(strLine, intPilotNameStart, intPilotNameEnd - intPilotNameStart + 1)
                strPilot = Trim(Replace(strPilot, "'", ""))

                If isHostFlight(strGroup) = False Then
                    'Exit this subroutine if this is not the host pilot.
                    Exit Sub
                End If

            Else 'Branch for "seat occupied by"

                'Extract the aircraft object name from the current line.
                strObj = Trim(strLineArray(1))
                strPilotPosition = strObj
                strObj = Left(strObj, InStr(strObj, "(") - 1)

                'Extract the aircraft hierarchical group name.
                strGroup = modUtilities.getGroupName(strObj, True)

                'Check for driveable flight alias.
                If hashDriveable.ContainsKey(Left(strObj, Len(strObj) - 1)) = True Then
                    strObj = hashDriveable(Left(strObj, Len(strObj) - 1))
                    strPilotPosition = strObj
                    strGroup = modUtilities.getGroupName(strObj, False)
                End If

                'Extract Pilot Name.
                'strPilot = Replace(Trim(strLineArray(5)), "'", "")
                intPilotNameStart = InStr(strLine, "seat occupied by") + 17
                intPilotNameEnd = InStrRev(strLine, " at ") - 1
                strPilot = Mid(strLine, intPilotNameStart, intPilotNameEnd - intPilotNameStart + 1)
                strPilot = Trim(Replace(strPilot, "'", ""))

                'Trap TAB characters at the end of pilot names.
                If Right(strPilot, 1) = vbTab Then
                    strPilot = Replace(strPilot, vbTab, "")
                    Call basMess.MessageBox("Bad Pilot Name: " & strPilot & " has a trailing TAB character!", vbInformation)
                End If

            End If




            'If this pilot is already initialized into the hashPilotRecord table, then
            'simply do nothing. Otherwise, set the pilot status to "In Cockpit" and
            'add to the hash.
            'DCS v3.2.18: need to correct this for the case that a pilot selects the host seat
            'and then moves to a different slot.
            If hashPilotRecord.ContainsKey(strPilot) = False Then
                'Place this pilot and seat combination into the module-scope hashtable.
                'We do this so we can keep track of what happens to pilots.
                subInitializePilotRecord(thisPilot)
                thisPilot.Pilot_Status = "In Cockpit"
                thisPilot.EventTime = strLineArray(0)
                thisPilot.Pilot_Name = strPilot
                thisPilot.Pilot_Aircraft = strObj
                thisPilot.Pilot_Seat = strPilotPosition
                thisPilot.Raw_Seat = strLineArray(1)
                If InStr(strLineArray(1), "(0)") > 0 Then
                    thisPilot.inControl = True
                End If
                hashPilotRecord(strPilot) = thisPilot


                If CS_Create_Host_Seat = 1 And isHostFlight(Left(strObj, Len(strObj) - 1)) = True Then
                    'Store the host pilot name in a module-scope variable for exclusion in
                    'subsequent routines.
                    strHostPilot = strPilot
                End If

            Else

                'Check for driveable flight alias.
                If hashDriveable.ContainsKey(Left(strObj, Len(strObj) - 1)) = True Then
                    strObj = hashDriveable(Left(strObj, Len(strObj) - 1))
                    strPilotPosition = strObj
                    strGroup = modUtilities.getGroupName(strObj, False)
                End If

                thisPilot = hashPilotRecord(strPilot)
                If thisPilot.Pilot_Aircraft <> strObj Then
                    'Pilot has swapped aircraft, so we need to be careful about setting up his details.
                    subInitializePilotRecord(thisPilot)
                    thisPilot.Pilot_Status = "In Cockpit"
                    thisPilot.EventTime = strLineArray(0)
                    thisPilot.Pilot_Name = strPilot
                    thisPilot.Pilot_Aircraft = strObj
                    thisPilot.Pilot_Seat = strPilotPosition
                    thisPilot.Raw_Seat = strLineArray(1)
                    If InStr(strLineArray(1), "(0)") > 0 Then
                        thisPilot.inControl = True
                    End If
                Else
                    'Pilot has swapped a seat in the aircraft.
                    If thisPilot.Pilot_Status <> "Landed" Then
                        thisPilot.Pilot_Status = "In Cockpit"
                    End If
                    thisPilot.EventTime = strLineArray(0)
                    thisPilot.Pilot_Name = strPilot
                    thisPilot.Pilot_Aircraft = strObj
                    thisPilot.Pilot_Seat = strPilotPosition
                    thisPilot.Raw_Seat = strLineArray(1)
                End If
                hashPilotRecord(strPilot) = thisPilot

                'If the pilot was formerly in the host seat, but has since moved, then reset the host pilot name.
                If strPilot = strHostPilot And isHostFlight(Left(strObj, Len(strObj) - 1)) = False Then
                    strHostPilot = ""
                End If

            End If

            'Check to see if this flight is a registered recon flight. If so, add this flight
            'to the list of confirmed human recon flights.
            If thisPilot.inControl = True And _
               InStr(strAllReconFlights, strGroup) > 0 And _
               InStr(strConfirmedHumanReconFlights, strGroup) = 0 And _
               CS_Human_Recon_Supply_Flights = 1 Then
                strConfirmedHumanReconFlights &= strGroup & "|"
            End If

            'Check to see if this flight is a registered supply drop flight. If so, add this flight
            'to the list of confirmed human supply drop flights.
            If thisPilot.inControl = True And _
               InStr(strAllSupplyDropFlights, strGroup) > 0 And _
               InStr(strConfirmedHumanSupplyDropFlights, strGroup) = 0 And _
               CS_Human_Recon_Supply_Flights = 1 Then
                strConfirmedHumanSupplyDropFlights &= strGroup & "|"
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subSeatOccupied: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub




    'This routine loads all current unit data into a hashtable of SE_Unit structures.
    Private Sub subLoadUnitHashtable(ByVal item As Integer, ByRef isValid As Boolean)
        Dim rsUnits As New ADODB.Recordset
        Dim rsCU As New ADODB.Recordset
        Dim rsShips As New ADODB.Recordset
        Dim thisUnit As SE_Unit
        Dim thisTransporter As SE_Transporter
        Dim strUnitName As String
        Dim strSQL As String
        Dim dtLastMissionSequence, dtPreviousMissionSequence As Date
        Dim strPreviousSequence As String

        Try

            'Bail out if we are adding forces.
            If isAddForce Then
                basMain.strAddForceMission = strPreviousMissionFileName
                isValid = True
                Exit Sub
            End If

            'Set the completion code.
            isValid = False

            'Determine the mission date/time parameters.
            dtLastMissionSequence = modUtilities.SN2DT(strLastMissionSequenceNumber)
            dtPreviousMissionSequence = DateAdd(DateInterval.Hour, -CDbl(CS_Mission_Length), dtLastMissionSequence)
            strPreviousSequence = DT2SN(dtPreviousMissionSequence)

            'Open a recordset for all non-infrastructure units, ignoring any Dissolved units.
            strSQL = "SELECT * FROM ObjMissionData, Object_Specifications, MissionData WHERE " _
                    & "ObjMissionData.Mission_Name = MissionData.Mission_Name AND " _
                    & "ObjMissionData.Mission_Name = '" & strPreviousMissionFileName & "' AND " _
                    & "ObjMissionData.Obj_Type = Object_Specifications.Object_Type AND " _
                    & "ObjMissionData.Dissolved = 0 AND " _
                    & "MissionData.Map = '" & strPreviousMap & "' AND " _
                    & "ObjMissionData.Theatre = MissionData.Theatre AND " _
                    & "Object_Specifications.Object_Class <> 'INF' ORDER BY ObjMissionData.Obj_Group_Name"
            Call modDB.openDBRecordset(item, rsUnits, strSQL)


            'Step through the entries in the hashtable, writing out the data.
            Do Until rsUnits.EOF = True

                strUnitName = rsUnits("Obj_Group_Name").Value

                modUtilities.subSetUnitDefault(thisUnit)
                thisUnit.Unit_Name = rsUnits("Unit_Name").Value
                thisUnit.Object_Group_Name = rsUnits("Obj_Group_Name").Value
                thisUnit.Object_Type = rsUnits("Obj_Type").Value
                thisUnit.Nationality = rsUnits("Obj_Nationality").Value

                If thisUnit.Nationality = "r" Then
                    thisUnit.Verbose_Type = rsUnits("Verbose_Names_Allies").Value
                Else
                    thisUnit.Verbose_Type = rsUnits("Verbose_Names_Axis").Value
                End If

                thisUnit.Object_Class = rsUnits("Object_Class").Value
                thisUnit.Parent = rsUnits("Obj_Parent").Value
                thisUnit.Transporting_Group = rsUnits("Transporting_Group").Value
                thisUnit.X = rsUnits("Start_X_Axis").Value
                thisUnit.Y = rsUnits("Start_Y_Axis").Value
                thisUnit.Z = rsUnits("Start_Orientation").Value
                thisUnit.Fuel = Math.Max(0, rsUnits("Fuel").Value)
                thisUnit.Fuel_Capacity = rsUnits("Fuel_Capacity").Value
                thisUnit.Fuel_Freight = Math.Max(0, rsUnits("Fuel_Freight").Value)
                thisUnit.Skill = rsUnits("Skill").Value
                thisUnit.Skill0 = rsUnits("Skill0").Value
                thisUnit.Skill1 = rsUnits("Skill1").Value
                thisUnit.Skill2 = rsUnits("Skill2").Value
                thisUnit.Skill3 = rsUnits("Skill3").Value
                thisUnit.OnlyAI = rsUnits("OnlyAI").Value
                thisUnit.Quantity = Math.Min(4, Math.Max(0, rsUnits("Quantity").Value))
                thisUnit.Morale = rsUnits("Morale").Value
                thisUnit.Refueling = Math.Max(0, rsUnits("Refueling").Value)
                thisUnit.RefuelingTime = Math.Max(0, rsUnits("Refueling_Time").Value)
                thisUnit.Penalty = Math.Max(0, rsUnits("Penalty").Value)
                thisUnit.Recon = Math.Min(100, Math.Max(0, rsUnits("Recon_Percent").Value))
                thisUnit.Skin = rsUnits("Skin").Value
                thisUnit.TargetX = rsUnits("GAttack_X_Axis").Value
                thisUnit.TargetY = rsUnits("GAttack_Y_Axis").Value
                thisUnit.Speed = CDbl(rsUnits("Speed").Value)
                thisUnit.Range = rsUnits("Range").Value
                thisUnit.CC = rsUnits("Command_Control").Value
                thisUnit.Formation = rsUnits("Formation").Value
                thisUnit.Crew = rsUnits("Crew").Value
                If rsUnits("Signaller").Value Is System.DBNull.Value Then
                    thisUnit.Signals = ""
                Else
                    thisUnit.Signals = rsUnits("Signaller").Value
                End If
                If rsUnits("Activated_By").Value Is System.DBNull.Value Then
                    thisUnit.ActivatedBy = ""
                Else
                    thisUnit.ActivatedBy = rsUnits("Activated_By").Value
                End If
                If thisUnit.ActivatedBy Is System.DBNull.Value Then
                    thisUnit.ActivationTime = 0
                ElseIf thisUnit.ActivatedBy = "" Then
                    thisUnit.ActivationTime = 0
                Else
                    thisUnit.ActivationTime = 86400
                End If

                If hashUnit.ContainsKey(strUnitName) = False Then
                    hashUnit(strUnitName) = thisUnit
                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subLoadUnitHashtable: duplicate hashkey for " & strUnitName, vbInformation)
                    Exit Sub
                End If

                'Look for Transporter references and add to hashTransporter hashtable.
                If thisUnit.Transporting_Group <> "Independent" And
                   thisUnit.Transporting_Group <> "Disembark" And
                   hashTransporter.ContainsKey(thisUnit.Transporting_Group) = False Then
                    modUtilities.subSetTransporterDefault(thisTransporter)
                    thisTransporter.Name = thisUnit.Transporting_Group
                    hashTransporter(thisUnit.Transporting_Group) = thisTransporter
                End If

                'If this is an air unit, gather any flight status data from GroupMissionData table and manage recon photos.
                'New in DCS 7.2.8
                Call subManageFlightStatus(item, thisUnit, strLastMissionSequenceNumber, strPreviousMissionFileName)

                'Next unit...
                rsUnits.MoveNext()
            Loop

            'Close the recordset.
            rsUnits.Close()

            'Load combined unit statuses (get the current UNM mapping as well).
            Call modUtilities.subLoadCombinedUnits(item, strPreviousMissionFileName, True)

            'Load meta unit statuses.
            Call modUtilities.subLoadMetaUnits(item, strPreviousSequence)

            'Load ship and factory installation damage status values.
            Call subLoadShipDamageValues(item, strPreviousMissionFileName)
            Call subLoadInstallationDamageValues(item, strPreviousMissionFileName)

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadUnitHashtable: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'The following routine was suggested by Dore and Tabarnak after AI recon flights that were cancelled due to fuel shortages
    'were later incorrectly shown as having flown and taken photos. New in DCS 7.2.8
    Private Sub subManageFlightStatus(ByVal item As Integer, ByRef thisFlight As SE_Unit, ByVal strSequenceNumber As String, ByVal strMissionName As String)
        Dim rsFlightStatus As New ADODB.Recordset
        Dim strFlightStatus, strSQL As String

        Try
            'If this is an air unit, gather any flight status data from GroupMissionData table.
            If Left(thisFlight.Object_Class, 1) = "P" Then

                'Default flight status.
                thisFlight.FlightStatus = basMissionBuilder.FLIGHT_OK

                'Look for existing flight plans for this unit.
                strSQL = "SELECT * FROM GroupMissionData WHERE Obj_Group='" & thisFlight.Object_Group_Name & "' AND " _
                            & "Mission_Nbr='" & strSequenceNumber & "';"
                modDB.openDBRecordset(item, rsFlightStatus, strSQL)

                'Extract any flight status information encoded in Requested_Signaller field.
                If rsFlightStatus.EOF = False Then
                    strFlightStatus = rsFlightStatus("Requested_Signaller").Value

                    'Check for null values.
                    If strFlightStatus Is System.DBNull.Value Then
                        strFlightStatus = ""
                    End If

                    'Set the Flight Status property accordingly - ignore valid Signaller values.
                    Select Case strFlightStatus
                        Case basMissionBuilder.FLIGHT_ABORTED, basMissionBuilder.FLIGHT_CANCELLED, basMissionBuilder.FLIGHT_OVERRUN
                            thisFlight.FlightStatus = strFlightStatus
                        Case Else
                            thisFlight.FlightStatus = basMissionBuilder.FLIGHT_OK
                    End Select
                End If

                'Set Mission_Recon photos to unsuccessful for cancelled or overrun flights because they didn't actually fly.
                If thisFlight.FlightStatus = basMissionBuilder.FLIGHT_CANCELLED Or thisFlight.FlightStatus = basMissionBuilder.FLIGHT_OVERRUN Then
                    strSQL = "UPDATE Mission_Recon SET Recon_Success=0 WHERE Recon_Unit='" & thisFlight.Object_Group_Name & "' AND " _
                                & "Mission_Name='" & strMissionName & "';"
                    modDB.ExecuteDBCommand(item, strSQL)
                End If

                'Close recordset.
                rsFlightStatus.Close()

            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subManageFlightStatus: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    Private Sub subLoadInstallationDamageValues(ByVal item As Integer, ByVal strMissionName As String)
        Dim strSQL, strInstallationName, strInstallationType As String
        Dim rsInstallations As New ADODB.Recordset
        Dim thisInstallation As SE_Installation
        Dim intInstallationDamage As Integer

        Try

            'Load installation damage status values.
            strSQL = "SELECT * FROM Industrial_Installations WHERE " _
                    & "Map = '" & strThisMap & "' " _
                    & "ORDER BY Location;"
            Call modDB.openDBRecordset(item, rsInstallations, strSQL)
            'Step through the entries in the hashtable, writing out the data.
            Do Until rsInstallations.EOF = True
                strInstallationName = rsInstallations("Location").Value
                strInstallationType = rsInstallations("Installation_Type").Value
                intInstallationDamage = rsInstallations("Damage_Level").Value
                If hashInstallation.ContainsKey(strInstallationName) Then
                    thisInstallation = hashInstallation(strInstallationName)
                    thisInstallation.Name = strInstallationName
                    thisInstallation.Type = strInstallationType
                    thisInstallation.Damage_Old = intInstallationDamage
                    thisInstallation.Damage_New = 0
                    thisInstallation.Holding = rsInstallations("Holding").Value
                    thisInstallation.Production_Points = rsInstallations("Production_Points").Value
                    hashInstallation(strInstallationName) = thisInstallation
                Else
                    thisInstallation.Name = strInstallationName
                    thisInstallation.Type = strInstallationType
                    thisInstallation.Damage_Old = intInstallationDamage
                    thisInstallation.Damage_New = 0
                    thisInstallation.Holding = rsInstallations("Holding").Value
                    thisInstallation.Production_Points = rsInstallations("Production_Points").Value
                    hashInstallation(strInstallationName) = thisInstallation
                End If

                rsInstallations.MoveNext()
            Loop

            rsInstallations.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadInstallationDamageValues: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    Private Sub subLoadShipDamageValues(ByVal item As Integer, ByVal strMissionName As String)
        Dim strSQL, strShipName As String
        Dim rsShips As New ADODB.Recordset
        Dim thisShip As SE_Unit
        Dim thisShipStatus As SE_Ship_Damage_Events
        Dim dblShipDamageValue, dblGunDamageValue As Double
        Dim dblHull1DamageValue, dblHull2DamageValue, dblHull3DamageValue As Double

        Try

            'Load ship damage status values.
            strSQL = "SELECT * FROM Ship_Damage_Status WHERE " _
                    & "Mission_Name = '" & strMissionName & "' " _
                    & "ORDER BY Ship_Unit;"
            Call modDB.openDBRecordset(item, rsShips, strSQL)
            'Step through the entries in the hashtable, writing out the data.
            Do Until rsShips.EOF = True
                strShipName = rsShips("Ship_Unit").Value
                dblShipDamageValue = CDbl(rsShips("Total_Damage").Value)
                dblHull1DamageValue = CDbl(rsShips("Hull1").Value)
                dblHull2DamageValue = CDbl(rsShips("Hull2").Value)
                dblHull3DamageValue = CDbl(rsShips("Hull3").Value)
                dblGunDamageValue = CDbl(rsShips("Guns").Value)
                If hashUnit.ContainsKey(strShipName) Then
                    thisShip = hashUnit(strShipName)
                    thisShip.ShipDamage = dblShipDamageValue
                    thisShip.Hull1Damage = dblHull1DamageValue
                    thisShip.Hull2Damage = dblHull2DamageValue
                    thisShip.Hull3Damage = dblHull3DamageValue
                    thisShip.GunDamage = dblGunDamageValue
                    hashUnit(strShipName) = thisShip
                Else
                    'this ship is destroyed or dissolved - no action required.
                    'Call basMess.MessageBox("modMissionAnalyzer.subLoadShipDamageValues: " _
                    '& "trying to load ship damage for non-existent unit " & strShipName, vbInformation)
                End If

                If hashShipEvents.ContainsKey(strShipName) = False Then
                    thisShipStatus.Group_Name = strShipName
                    thisShipStatus.LastAntagonist = ""
                    thisShipStatus.Ship_Class = ""
                    thisShipStatus.Ship_Type = ""
                    thisShipStatus.ChunkEvents = ""
                    thisShipStatus.isSpotted = False
                    hashShipEvents(strShipName) = thisShipStatus
                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subLoadShipDamageValues: " _
                    & "trying to load ship status for duplicate unit " & strShipName, vbInformation)
                End If

                rsShips.MoveNext()
            Loop

            rsShips.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadShipDamageValues: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This routine steps through a hashtable of SE_Unit structures and writes the data into the 
    'relevant database table. 
    'As of 2.0beta(G), this routine also updates transported unit coordinates with Transporter coords.
    Private Sub subStoreUnitHashtable(ByVal item As Integer, ByRef isValid As Boolean)
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisUnit As SE_Unit
        Dim thisTransporter As SE_Transporter
        Dim strUnitName As String
        Dim strSQL As String
        Dim strSQLCore As String
        Dim strUHTMission As String

        Try
            'Set the completion code.
            isValid = False

            'If the hashUnit hashtable contains no units, then bug out.
            If hashUnit.Count = 0 Then
                'Successful completion.
                isValid = True
                Exit Sub
            End If

            'Run through all the Transporters, finding their location coords and co-locating
            'all the associated transported units.
            'Capture the hashUnit and hashTransporter keys into arrays.
            Dim TransporterKeys As ArrayList = New ArrayList(hashTransporter.Keys)
            Dim UnitKeys As ArrayList = New ArrayList(hashUnit.Keys)

            'Step through the keys in the hashTransporter hashtable, locating the positional coords
            'from the associated unit in hashUnit hashtable.
            For Each Tkey As String In TransporterKeys
                Tkey = Trim(Tkey)
                thisTransporter = hashTransporter(Tkey)

                If hashUnit.ContainsKey(thisTransporter.Name) Then
                    thisUnit = hashUnit(thisTransporter.Name)
                    thisTransporter.X = thisUnit.X
                    thisTransporter.Y = thisUnit.Y
                    thisTransporter.Z = thisUnit.Z
                    hashTransporter(Tkey) = thisTransporter
                Else
                    Call basMess.MessageBox("modMissionAnalyzer.subStoreUnitHashtable: unregistered Transporter " & thisTransporter.Name, vbInformation)
                End If

            Next

            'Step through the keys in the hashUnit hashtable, locating the positional coords
            'of each transported unit with the associated Transporter unit.
            For Each Ukey As String In UnitKeys
                thisUnit = hashUnit(Ukey)

                For Each Tkey As String In TransporterKeys
                    thisTransporter = hashTransporter(Tkey)

                    If thisTransporter.Name = thisUnit.Transporting_Group And _
                       thisUnit.Quantity > 0 Then
                        thisUnit.X = thisTransporter.X
                        thisUnit.Y = thisTransporter.Y
                        thisUnit.Z = thisTransporter.Z
                        hashUnit(Ukey) = thisUnit
                    End If

                Next

            Next



            'Now we can commence storing all the unit data. 
            'Set the core SQL syntax.
            strSQLCore = "INSERT INTO ObjMissionData " _
                            & "(Mission_Name, Unit_Name, Obj_Group_Name, Obj_Type, " _
                            & "Obj_Nationality, Transporting_Group, Dissolved, Obj_Parent, " _
                            & "Start_X_Axis, Start_Y_Axis, Start_Orientation, " _
                            & "GAttack_X_Axis, GAttack_Y_Axis, Activated_By, Signaller, Fuel, Fuel_Freight, " _
                            & "Skill, Skill0, Skill1, Skill2, Skill3, " _
                            & "OnlyAI, Quantity, Morale, Refueling, Penalty, Recon_Percent, " _
                            & "Formation, Skin, Theatre) VALUES "
            If modCfg.User(item).DatabaseProvider = "mysql" Then
                strSQL = strSQLCore
            End If

            'Establish the enumerator of the hashtable.
            thisEnumerator = hashUnit.GetEnumerator()

            'Step through the entries in the hashtable, writing out the data.
            While thisEnumerator.MoveNext()

                strUnitName = thisEnumerator.Key
                thisUnit = thisEnumerator.Value

                'If the unit is presently in the "Disembark" mode, then update it to "Independent",
                'since one hour has just elapsed.
                If thisUnit.Transporting_Group = "Disembark" Then
                    thisUnit.Transporting_Group = "Independent"
                End If

                'If the unit is a ship, then create a damage status record for the ship in the
                'Ship_Damage_Status table.
                If Left(thisUnit.Object_Class, 1) = "S" Then
                    Call subStoreShipDamageStatus(item, thisUnit, basFiles.StrMissionName)
                End If

                'If the unit is a destroyed airframe, remove all landing penalty information.
                If InStr(strAirframesLost, thisUnit.Object_Group_Name & "0|") > 0 Then
                    thisUnit.Penalty0 = 0
                    thisUnit.Penalty0Log = ""
                End If
                If InStr(strAirframesLost, thisUnit.Object_Group_Name & "1|") > 0 Then
                    thisUnit.Penalty1 = 0
                    thisUnit.Penalty1Log = ""
                End If
                If InStr(strAirframesLost, thisUnit.Object_Group_Name & "2|") > 0 Then
                    thisUnit.Penalty2 = 0
                    thisUnit.Penalty2Log = ""
                End If
                If InStr(strAirframesLost, thisUnit.Object_Group_Name & "3|") > 0 Then
                    thisUnit.Penalty3 = 0
                    thisUnit.Penalty3Log = ""
                End If

                'If the unit is an aircraft with a landing penalty, write the message log to the 
                'Campaign_Notices table.
                If CS_Landing_Penalties = 1 And _
                  (Len(thisUnit.Penalty0Log) > 0 Or Len(thisUnit.Penalty1Log) > 0 Or _
                   Len(thisUnit.Penalty2Log) > 0 Or Len(thisUnit.Penalty3Log) > 0) Then
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, strLastMissionSequenceNumber, thisUnit.Nationality, thisUnit.Penalty0Log)
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, strLastMissionSequenceNumber, thisUnit.Nationality, thisUnit.Penalty1Log)
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, strLastMissionSequenceNumber, thisUnit.Nationality, thisUnit.Penalty2Log)
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, strLastMissionSequenceNumber, thisUnit.Nationality, thisUnit.Penalty3Log)
                End If

                'If the unit is a scrambled flight, then alter the coordinates to the planned landing location.
                If CS_Triggers >= 1 And Left(thisUnit.Object_Class, 1) = "P" And InStr(strMissionDesignatedScramblers, "|" & thisUnit.Object_Group_Name & "|") > 0 Then
                    thisUnit.X = thisUnit.Scrambled_X
                    thisUnit.Y = thisUnit.Scrambled_Y
                End If

                'If the unit's location is not a Withdraw Point, then write the unit data to the
                'ObjMissionData table. Otherwise, load the unit strength into the 
                'nearest Supply Point as future reinforcement pool.
                If isAddForce Then
                    strUHTMission = basMain.strAddForceMission
                Else
                    strUHTMission = basFiles.StrMissionName
                End If

                If isWithdrawing(item, thisUnit) = False Or _
                   modCfg.User(item).IsCampaignInitialized = False Then

                    If isAddForce Then

                        If basMain.intAFCoordinateMode = basMain.AF_MODE_OFFSET Then

                            thisUnit.X = thisUnit.X + basMain.intAFOffsetX
                            thisUnit.Y = thisUnit.Y + basMain.intAFOffsetY
                            If Math.Abs(thisUnit.TargetX + 99.0) > 1.0 Or Math.Abs(thisUnit.TargetY + 99.0) > 1.0 Then
                                thisUnit.TargetX = thisUnit.TargetX + basMain.intAFOffsetX
                                thisUnit.TargetY = thisUnit.TargetY + basMain.intAFOffsetY
                            End If

                        Else

                            thisUnit.X = thisUnit.X + basMain.intAFOffsetX - basMain.intAFCentreX
                            thisUnit.Y = thisUnit.Y + basMain.intAFOffsetY - basMain.intAFCentreY
                            If Math.Abs(thisUnit.TargetX + 99.0) > 1.0 Or Math.Abs(thisUnit.TargetY + 99.0) > 1.0 Then
                                thisUnit.TargetX = thisUnit.TargetX + basMain.intAFOffsetX - basMain.intAFCentreX
                                thisUnit.TargetY = thisUnit.TargetY + basMain.intAFOffsetY - basMain.intAFCentreY
                            End If

                        End If
                    End If

                    'Construct the insertions to the ObjMissionData Table.
                    If modCfg.User(item).DatabaseProvider = "mysql" And _
                       modCfg.User(item).chkUseExtendedInserts = True Then
                        strSQL &= "('" _
                            & strUHTMission & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Unit_Name) & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Object_Group_Name) & "', '" _
                            & thisUnit.Object_Type & "', '" _
                            & thisUnit.Nationality & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Transporting_Group) & "', " _
                            & "0, '" _
                            & modUtilities.strEncodeQ(thisUnit.Parent) & "', " _
                            & thisUnit.X & ", " _
                            & thisUnit.Y & ", " _
                            & thisUnit.Z & ", " _
                            & thisUnit.TargetX & ", " _
                            & thisUnit.TargetY & ", '" _
                            & thisUnit.ActivatedBy & "', '" _
                            & thisUnit.Signals & "', " _
                            & Math.Max(0, thisUnit.Fuel) & ", " _
                            & Math.Max(0, thisUnit.Fuel_Freight) & ", " _
                            & thisUnit.Skill & ", " _
                            & thisUnit.Skill0 & ", " _
                            & thisUnit.Skill1 & ", " _
                            & thisUnit.Skill2 & ", " _
                            & thisUnit.Skill3 & ", " _
                            & thisUnit.OnlyAI & ", " _
                            & thisUnit.Quantity & ", " _
                            & thisUnit.Morale & ", " _
                            & Math.Max(0, thisUnit.Refueling) & ", " _
                            & Math.Max(0, thisUnit.Penalty + thisUnit.Penalty0 + thisUnit.Penalty1 + thisUnit.Penalty2 + thisUnit.Penalty3) & ", " _
                            & Math.Min(100, Math.Max(0, thisUnit.Recon)) & ", '" _
                            & thisUnit.Formation & "', '" _
                            & thisUnit.Skin & "', '" _
                            & modCfg.User(item).TxtTheatre & "'),"
                    Else
                        strSQL = strSQLCore & "('" _
                            & strUHTMission & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Unit_Name) & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Object_Group_Name) & "', '" _
                            & thisUnit.Object_Type & "', '" _
                            & thisUnit.Nationality & "', '" _
                            & modUtilities.strEncodeQ(thisUnit.Transporting_Group) & "', " _
                            & "0, '" _
                            & modUtilities.strEncodeQ(thisUnit.Parent) & "', " _
                            & thisUnit.X & ", " _
                            & thisUnit.Y & ", " _
                            & thisUnit.Z & ", " _
                            & thisUnit.TargetX & ", " _
                            & thisUnit.TargetY & ", " _
                            & thisUnit.ActivatedBy & "', '" _
                            & thisUnit.Signals & "', " _
                            & Math.Max(0, thisUnit.Fuel) & ", " _
                            & Math.Max(0, thisUnit.Fuel_Freight) & ", " _
                            & thisUnit.Skill & ", " _
                            & thisUnit.Skill0 & ", " _
                            & thisUnit.Skill1 & ", " _
                            & thisUnit.Skill2 & ", " _
                            & thisUnit.Skill3 & ", " _
                            & thisUnit.OnlyAI & ", " _
                            & thisUnit.Quantity & ", " _
                            & thisUnit.Morale & ", " _
                            & Math.Max(0, thisUnit.Refueling) & ", " _
                            & Math.Max(0, thisUnit.Penalty + thisUnit.Penalty0 + thisUnit.Penalty1 + thisUnit.Penalty2 + thisUnit.Penalty3) & ", " _
                            & Math.Min(100, Math.Max(0, thisUnit.Recon)) & ", '" _
                            & thisUnit.Formation & "', '" _
                            & thisUnit.Skin & "', '" _
                            & modCfg.User(item).TxtTheatre & "');"
                        Call modDB.ExecuteDBCommand(item, strSQL)
                    End If

                End If


            End While

            If modCfg.User(item).DatabaseProvider = "mysql" And _
               modCfg.User(item).chkUseExtendedInserts = True Then
                strSQL = Left(strSQL, Len(strSQL) - 1)
                Call modDB.ExecuteDBCommand(item, strSQL)
            End If

            'Clear the hashUnit hashtable.
            'hashUnit.Clear()

            'Reset all AddForces variables
            basMain.intAFOffsetX = 0
            basMain.intAFCentreX = 0
            basMain.intAFOffsetY = 0
            basMain.intAFCentreY = 0
            basMain.intAFCoordinateMode = basMain.AF_MODE_OFFSET

            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subStoreUnitHashtable: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    Private Sub subStoreShipDamageStatus(ByVal item As Integer, ByVal thisUnit As SE_Unit, ByVal strMissionName As String)
        Dim strSql, strSSDSMission As String

        Try
            If isAddForce Then
                strSSDSMission = basMain.strAddForceMission
            Else
                strSSDSMission = basFiles.StrMissionName
            End If

            'Format the SQL INSERT query.
            strSql = "INSERT INTO Ship_Damage_Status (Mission_Name,Ship_Unit,Ship_Type,Ship_Nation,Total_Damage,Hull1,Hull2,Hull3,Guns) VALUES " _
                    & "('" & strSSDSMission & "', " _
                    & "'" & thisUnit.Object_Group_Name & "', " _
                    & "'" & thisUnit.Object_Type & "', " _
                    & "'" & thisUnit.Nationality & "', " _
                    & thisUnit.ShipDamage & ", " _
                    & thisUnit.Hull1Damage & ", " _
                    & thisUnit.Hull2Damage & ", " _
                    & thisUnit.Hull3Damage & ", " _
                    & thisUnit.GunDamage & ");"
            modDB.ExecuteDBCommand(item, strSql)

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subStoreShipDamageStatus: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This function determines whether a supplied unit is near a Withdraw Point. If it isn't the function
    'returns False. If it is, the function puts the unit strength into the nearby Supply Point and 
    'returns True. The function assumes that for every "Air_Withdraw" point there is a corresponding
    '(nearby) "Air_Supply" point.
    Private Function isWithdrawing(ByVal item As Integer, ByVal thisUnit As SE_Unit)
        Dim rsSupplyPoint As New ADODB.Recordset
        Dim rsWithdrawPoint As New ADODB.Recordset
        Dim strSql As String
        Dim strSupplyPoint As String
        Dim intQuantity, intReliability As Integer
        Dim thisX, thisY As String
        Try

            'Nothing can withdraw at template load time. Any other time, only aircraft can withdraw.
            If modCfg.User(item).IsCampaignInitialized = False Or isAddForce Or _
               Left(thisUnit.Object_Class, 1) <> "P" Then
                isWithdrawing = False
                Exit Function
            End If

            'Select the nearest Withdraw Point to the supplied unit location.
            If modCfg.User(item).DatabaseProvider = "mysql" Then
                strSql = "SELECT Supply_Point, Point_X AS X, Point_Y AS Y, Reliability FROM Resupply_Points WHERE " _
                       & "Map = '" & strThisMap & "' AND " _
                       & "Supply_Control = '" & thisUnit.Nationality & "' AND " _
                       & "UPPER(Supply_Type) = 'AIR_WITHDRAW' AND " _
                       & "Point_X=" & thisUnit.X & " AND " _
                       & "Point_Y=" & thisUnit.Y & ";"
            Else
                strSql = "SELECT Supply_Point, Point_X AS X, Point_Y AS Y, Reliability FROM Resupply_Points WHERE " _
                       & "Map = '" & strThisMap & "' AND " _
                       & "Supply_Control = '" & thisUnit.Nationality & "' AND " _
                       & "UCase(Supply_Type) = 'AIR_WITHDRAW' AND " _
                       & "Point_X=" & thisUnit.X & " AND " _
                       & "Point_Y=" & thisUnit.Y & ";"
            End If
            Call modDB.openDBRecordset(item, rsWithdrawPoint, strSql)

            If rsWithdrawPoint.EOF = True Then

                'Close the recordset.
                rsWithdrawPoint.Close()

                'It might be the case that the flight is routed to a special "WITHDRAW" airbase, so check this.
                If modCfg.User(item).DatabaseProvider = "mysql" Then
                    strSql = "SELECT Landing_X AS X, Landing_Y AS Y FROM Airbases WHERE " _
                           & "Map = '" & strThisMap & "' AND " _
                           & "SQRT(POWER((Takeoff_X+Landing_X)/2-" & thisUnit.X & ",2) + " _
                           & "POWER((Takeoff_Y+Landing_Y)/2-" & thisUnit.Y & ",2))<" & CStr(CDbl(CS_Control_Radius)) & " AND " _
                           & "InStr(UPPER(Airbase_Name),'WITHDRAW')>=1;"
                Else
                    strSql = "SELECT Landing_X AS X, Landing_Y AS Y FROM Airbases WHERE " _
                           & "Map = '" & strThisMap & "' AND " _
                           & "SQR(((Takeoff_X+Landing_X)/2-" & thisUnit.X & ")^2 + " _
                           & "((Takeoff_Y+Landing_Y)/2-" & thisUnit.Y & ")^2)<" & CStr(CDbl(CS_Control_Radius)) & " AND " _
                           & "InStr(UCase(Airbase_Name),'WITHDRAW')>=1;"
                End If
                Call modDB.openDBRecordset(item, rsWithdrawPoint, strSql)

                If rsWithdrawPoint.EOF = True Then
                    'There is no withdrawal point or airbase at the unit location, so the unit stays in action.
                    isWithdrawing = False
                    rsWithdrawPoint.Close()
                    Exit Function

                Else
                    'First, find the name of the nearest Supply Point.
                    thisX = rsWithdrawPoint("X").Value
                    thisY = rsWithdrawPoint("Y").Value
                    If modCfg.User(item).DatabaseProvider = "mysql" Then
                        strSql = "SELECT * FROM Resupply_Points WHERE " _
                               & "Map = '" & strThisMap & "' AND " _
                               & "Supply_Control = '" & thisUnit.Nationality & "' AND " _
                               & "Supply_Type = 'Air_Supply' ORDER BY " _
                               & "SQRT(POWER(Point_X-" & thisX & ",2) + POWER(Point_Y-" & thisY & ",2));"
                    Else
                        strSql = "SELECT * FROM Resupply_Points WHERE " _
                               & "Map = '" & strThisMap & "' AND " _
                               & "Supply_Control = '" & thisUnit.Nationality & "' AND " _
                               & "Supply_Type = 'Air_Supply' ORDER BY " _
                               & "SQR((Point_X-" & thisX & ")^2 + (Point_Y-" & thisY & ")^2);"
                    End If
                    Call modDB.openDBRecordset(item, rsSupplyPoint, strSql)

                    If rsSupplyPoint.EOF = True Then
                        'No Air_Supply point can be found, so no withdraw proceeds.
                        isWithdrawing = False
                        rsWithdrawPoint.Close()
                        rsSupplyPoint.Close()
                        Exit Function

                    Else
                        'We have found an Air_Supply point.
                        strSupplyPoint = rsSupplyPoint("Supply_Point").Value
                        intReliability = rsSupplyPoint("Reliability").Value
                    End If


                End If

                'Close open recordsets.
                rsWithdrawPoint.Close()
                rsSupplyPoint.Close()

            Else

                'We have found an Air_Withdraw point. Assume a corresponding Air_Supply exists.
                strSupplyPoint = Replace(rsWithdrawPoint("Supply_Point").Value, "Withdraw", "Supply")
                intReliability = rsWithdrawPoint("Reliability").Value
                rsWithdrawPoint.Close()

            End If


            'There is a Air_Withdraw point, so the unit has made it out of the map. We have also
            'found the right Air_Supply point to accept the reinforcement. We have to move it into the
            'reinforcement pool at the Air_Supply point.

            'Find out if there is already a pool of this kind of unit at the Supply Point.
            'As of DCS v3.2.15, we need to take the Transit delay into account.
            strSql = "SELECT * FROM Resupply_Replacements WHERE " _
                    & "Map = '" & strThisMap & "' AND " _
                    & "Supply_Point = '" & modDB.strSQLquote(strSupplyPoint) & "' AND " _
                    & "ObjType = '" & thisUnit.Object_Type & "' AND " _
                    & "Used = 0 AND Committed = 0 AND Transit = " & thisUnit.RefuelingTime & ";"
            Call modDB.openDBRecordset(item, rsSupplyPoint, strSql)

            If rsSupplyPoint.EOF = True Then

                'Make a new record for this kind of unit at the Supply Point.
                strSql = "INSERT INTO Resupply_Replacements (Supply_Point, Map, Number_Objects, Committed, " _
                        & "ObjType, Used, Transit, Transit_Probability) VALUES ('" _
                        & modDB.strSQLquote(strSupplyPoint) & "', '" _
                        & strThisMap & "', " _
                        & thisUnit.Quantity & ", 0, '" _
                        & thisUnit.Object_Type & "', 0," & thisUnit.RefuelingTime _
                        & ", " & intReliability & ")"
                Call modDB.ExecuteDBCommand(item, strSql)

            Else
                'Increment the number of objects of the current type by the Quantity of the 
                'current unit.
                intQuantity = rsSupplyPoint("Number_Objects").Value + thisUnit.Quantity
                strSql = "UPDATE Resupply_Replacements SET Number_Objects = " & intQuantity & " WHERE " _
                    & "Map = '" & strThisMap & "' AND " _
                    & "Supply_Point = '" & modDB.strSQLquote(strSupplyPoint) & "' AND " _
                    & "ObjType = '" & thisUnit.Object_Type & "' AND " _
                    & "Used = 0 AND Committed = 0 AND Transit = " & thisUnit.RefuelingTime & ";"
                Call modDB.ExecuteDBCommand(item, strSql)

            End If

            'If Supply Tracking is on, add any supplies carried by these withdrawing units to
            'the OffMap reserves.
            If CS_Track_Fuel_Consumption = 1 Then
                strSql = "UPDATE OffMap_Supply_Reserves SET " _
                        & "Allied_OffMap_Reserves = Allied_OffMap_Reserves + " & (thisUnit.Fuel_Freight * intQuantity) & ";"
                If UCase(thisUnit.Nationality) = "R" Then
                    strSql = "UPDATE OffMap_Supply_Reserves SET " _
                            & "Allied_OffMap_Reserves = Allied_OffMap_Reserves + " & (thisUnit.Fuel_Freight * intQuantity) & ";"
                Else
                    strSql = "UPDATE OffMap_Supply_Reserves SET " _
                            & "Axis_OffMap_Reserves = Axis_OffMap_Reserves + " & (thisUnit.Fuel_Freight * intQuantity) & ";"
                End If
                Call modDB.ExecuteDBCommand(item, strSql)
            End If

            rsSupplyPoint.Close()
            isWithdrawing = True



        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isWithdrawing: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Function



    'This routine gets the long map name corresponding to the supplied map loader string from the mission file.
    Public Function strMapFromLoader(ByVal item As Integer, ByVal strMapLoader As String) As String
        Dim strSql As String
        Dim rsSectorSeasons As New ADODB.Recordset
        Dim rsCombatants As New ADODB.Recordset

        Try

            'Find the sector map name.
            strSql = "SELECT * FROM Sector_Seasons WHERE Summer_Map = """ & strMapLoader & """ OR " _
                    & "Winter_Map = """ & strMapLoader & """"
            Call modDB.openDBRecordset(item, rsSectorSeasons, strSql)
            If rsSectorSeasons.EOF = True Then
                Call basMess.MessageBox("Sector " & Left(strMapLoader, InStr(strMapLoader, "/") - 1) _
                            & " is not supported in this database. Template initialization ceasing.", vbCritical)
                strMapFromLoader = "unsupported map"
            Else
                strMapFromLoader = rsSectorSeasons("Campaign_Sector").Value
            End If
            rsSectorSeasons.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.strMapFromLoader: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try


    End Function


    'This subroutine works out exactly how long the mission was hosted for. This
    'is important when determining how far ground/sea units have travelled.
    Private Sub subMissionDuration(ByVal item As Integer, ByVal strLine As String)
        Dim strSql As String
        Dim dtEndOfMission As Date
        Try

            'If we are using IF metamode, then synthesize the mission duration.
            If CS_MetaMode = basMain.META_IF44_ONLY Then

                BeginTime = Format(CInt(strMissionCommencement), "0#")
                BeginTime = BeginTime & ":00:00"

                EndTime = CInt(strMissionCommencement) + CS_Mission_Length
                If EndTime > 23 Then
                    EndTime = EndTime - 24
                End If
                EndTime = Format(CInt(EndTime), "0#")
                EndTime = EndTime & ":00:00"

            Else

                If InStr(strLine, "BEGIN") <> 0 Then
                    BeginTime = Left(strLine, 8)
                ElseIf InStr(strLine, "END") <> 0 Then
                    EndTime = Left(strLine, 8)
                End If

            End If


            If Len(BeginTime) > 0 And Len(EndTime) > 0 Then
                intMissionDuration = DateDiff(DateInterval.Second, CDate(BeginTime), CDate(EndTime))

                'If the end time is less than the start time, it is likely that
                'the mission went past 23:59:59 into the next day, i.e. 00:00:00.
                'We need to add 86400 to intMissionDuration to account for this case.
                '86400 is the number of seconds in 24 hours (= 60*60*24).
                If intMissionDuration < 0 Then
                    intMissionDuration += 86400
                End If

                'add the mission duration to the MissionData table
                strSql = "UPDATE MissionData SET Duration = " & intMissionDuration _
                        & " WHERE (Mission_Name = '" & basFiles.StrMissionName _
                        & "' AND Host = '" & modCfg.User(item).TxtHost & "' AND Theatre = '" & modCfg.User(item).TxtTheatre & "')"
                Call modDB.ExecuteDBCommand(item, strSql)

                'Calculate the end of mission time and store it in a public string.
                dtEndOfMission = dtMissionStartDate
                dtEndOfMission = DateAdd(DateInterval.Second, intMissionDuration, dtEndOfMission)
                strEndOfMission = Format(dtEndOfMission, "HH:mm:ss")

            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subMissionDuration: " _
                & Err.Number & " " & Err.Description, vbCritical)

        End Try
    End Sub

    'This subroutine captures the player time mission commencement date and time and writes
    'it to the MissionData table, just for metadata purposes.
    Private Sub subMissionCommencement(ByVal item As Integer)
        Dim strSql As String
        Dim dtCommencement As Date
        Dim strTemp As String
        Try

            'Add the mission commencement to the MissionData table.
            'The global string strMissionCommencement was set by intOpenLogFile().

            'We can parse the commencement string into a "standard SE date format"...
            'dtCommencement = CDate(Trim(strMissionCommencement))
            'strTemp = Format(dtCommencement, "d MMM yyyy HH:mm:ss")

            'Or we can report it as is ...
            strTemp = strMissionCommencement

            'The "as is" method is probably best since the DCS runs in the neutral culture,
            'but FB+PF may well be localized and use different formats in the log file.
            strSql = "UPDATE MissionData SET Mis_Commencement = '" & strTemp _
                    & "' WHERE (Mission_Name = '" & basFiles.StrMissionName _
                    & "' AND Host = '" & modCfg.User(item).TxtHost & "' AND Theatre = '" & modCfg.User(item).TxtTheatre & "')"
            Call modDB.ExecuteDBCommand(item, strSql)

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subMissionCommencement: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Sub


    'This routine simply parses a standard Chief armour directive "n-ArmourType"
    'into the two values for the true object type "ArmourType" and the quantity "n".
    'If the supplied raw object type isn't for an armoured unit, then simpler values are returned.
    'Extended to cater for combinations up to 20 strong, based on Charlie's request. 29/9/2008
    Private Sub subParseArmour(ByVal strRawObjType As String, _
                                ByRef strObjType As String, _
                                ByRef intQuantity As Integer)

        If Left(strRawObjType, 2) = "1-" Or _
            Left(strRawObjType, 2) = "2-" Or _
            Left(strRawObjType, 2) = "3-" Or _
            Left(strRawObjType, 2) = "4-" Or _
            Left(strRawObjType, 2) = "5-" Or _
            Left(strRawObjType, 2) = "6-" Or _
            Left(strRawObjType, 2) = "7-" Or _
            Left(strRawObjType, 2) = "8-" Or _
            Left(strRawObjType, 2) = "9-" Or _
            Left(strRawObjType, 3) = "10-" Or _
            Left(strRawObjType, 3) = "11-" Or _
            Left(strRawObjType, 3) = "12-" Or _
            Left(strRawObjType, 3) = "13-" Or _
            Left(strRawObjType, 3) = "14-" Or _
            Left(strRawObjType, 3) = "15-" Or _
            Left(strRawObjType, 3) = "16-" Or _
            Left(strRawObjType, 3) = "17-" Or _
            Left(strRawObjType, 3) = "18-" Or _
            Left(strRawObjType, 3) = "19-" Or _
            Left(strRawObjType, 3) = "20-" Then
            strObjType = Mid(strRawObjType, InStr(strRawObjType, "-") + 1)
            intQuantity = CInt(Left(strRawObjType, InStr(strRawObjType, "-") - 1))
        Else
            strObjType = strRawObjType
            intQuantity = 1
        End If


    End Sub


    'This function determines the Normal_Group_Strength for the supplied object type.
    Private Function intMaximumStrength(ByVal item As Integer, ByVal strObjectType As String) As Integer
        Dim strSql As String
        Dim rsNGS As New ADODB.Recordset
        Try
            strSql = "SELECT Normal_Group_Strength FROM Object_Specifications WHERE " _
                    & "Object_Type = '" & strObjectType & "';"
            Call modDB.openDBRecordset(item, rsNGS, strSql)
            If rsNGS.EOF = True Then
                'Something is wrong!
                Call basMess.MessageBox("No maximum strength listed for units of type " & strObjectType _
                            & ". Setting to strength = 1", vbInformation)
                intMaximumStrength = 1
            Else
                intMaximumStrength = rsNGS("Normal_Group_Strength").Value
            End If
            rsNGS.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.intMaximumStrength: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Function

    'This routine loads campaign objetives from the template .properties file, if defined.
    Private Sub subLoadCampaignObjectives(ByVal item As Integer)
        Dim srObjectives As StreamReader
        Dim strBuffer As String
        Dim intStart As Integer
        Dim intEnd As Integer
        Dim strTemp As String
        Dim strBackground As String
        Dim strAllied As String
        Dim strAxis As String
        Dim strSql As String

        Try

            'If we have a defined .properties file, then we can get started.
            If basFiles.strPropertiesTemplateFileName <> basMain.TXT_UNDEFINED And _
               File.Exists(basFiles.strPropertiesTemplateFileName) = True Then

                'Open the .properties file as a text stream.
                srObjectives = File.OpenText(basFiles.strPropertiesTemplateFileName)

                'Convert the .properties file into a single long string.
                strBuffer = ""
                Do Until srObjectives.Peek = -1
                    strBuffer = strBuffer & srObjectives.ReadLine()
                Loop

                'Close the file.
                srObjectives.Close()

                'Capture the campaign background, as delimited by <ARMY NONE> tag.
                strBackground = ""
                If InStr(UCase(strBuffer), "<ARMY NONE>") > 0 Then
                    intStart = InStr(UCase(strBuffer), "<ARMY NONE>") + 11
                    intEnd = InStr(UCase(strBuffer), "</ARMY>")
                    strBackground = Mid(strBuffer, intStart, intEnd - intStart)
                    strBackground = modUtilities.strEncodeQ(strBackground)
                    strBackground = Replace(strBackground, "\t", " ")
                    strBackground = Replace(strBackground, "\n", "<br>")
                    strBackground = Replace(strBackground, "/n", "<br>")
                    strBackground = Replace(strBackground, vbCrLf, "<br>")
                End If

                'Capture the Allied objectives, as delimited by the <ARMY RED> tag.
                strAllied = ""
                If InStr(UCase(strBuffer), "<ARMY RED>") > 0 Then
                    intStart = InStr(UCase(strBuffer), "<ARMY RED>") + 10
                    strTemp = Mid(strBuffer, intStart)
                    intEnd = InStr(UCase(strTemp), "</ARMY>") - 1
                    strAllied = Left(strTemp, intEnd)
                    strAllied = modUtilities.strEncodeQ(strAllied)
                    strAllied = Replace(strAllied, "\t", " ")
                    strAllied = Replace(strAllied, "\n", "<br>")
                    strAllied = Replace(strAllied, "/n", "<br>")
                    strAllied = Replace(strAllied, vbCrLf, "<br>")
                End If

                'Capture the Axis objectives, as delimited by the <ARMY BLUE> tag.
                strAxis = ""
                If InStr(UCase(strBuffer), "<ARMY BLUE>") > 0 Then
                    intStart = InStr(UCase(strBuffer), "<ARMY BLUE>") + 11
                    strTemp = Mid(strBuffer, intStart)
                    intEnd = InStr(UCase(strTemp), "</ARMY>") - 1
                    strAxis = Left(strTemp, intEnd)
                    strAxis = modUtilities.strEncodeQ(strAxis)
                    strAxis = Replace(strAxis, "\t", " ")
                    strAxis = Replace(strAxis, "\n", "<br>")
                    strAxis = Replace(strAxis, "/n", "<br>")
                    strAxis = Replace(strAxis, vbCrLf, "<br>")
                End If


                'Write the campaign objectives to the database.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Sector_Objectives WHERE " _
                         & "Campaign_Sector = '" & strThisMap & "'"
                Call modDB.ExecuteDBCommand(item, strSql)
                strSql = "INSERT INTO Sector_Objectives (Campaign_Sector, Background, " _
                        & "Allied_Objectives, Axis_Objectives) VALUES ('" _
                        & strThisMap & "', '" _
                        & strBackground & "', '" _
                        & strAllied & "', '" _
                        & strAxis & "')"
                Call modDB.ExecuteDBCommand(item, strSql)

            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadCampaignObjectives: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    Private Function subGatherCombatants(ByVal item As Integer)
        Dim strSql As String
        Dim rsCombatants As New ADODB.Recordset
        Dim rsNavy As New ADODB.Recordset
        Dim rsArmy As New ADODB.Recordset
        Dim rsRailway As New ADODB.Recordset
        Dim newDivision As Army_Division
        Dim intDivisionsAllied As Integer = 0
        Dim intDivisionsAxis As Integer = 0
        Try

            'Find out the locally dominant combatants for this sector.
            strSql = "SELECT * FROM Sector_Dominance WHERE " _
                    & "Campaign_Sector = '" & strThisMap & "'"
            Call modDB.openDBRecordset(item, rsCombatants, strSql)
            strDominantAllied = rsCombatants("Allied").Value
            strDominantAxis = rsCombatants("Axis").Value
            rsCombatants.Close()

            'Determine the name of the Allied navy.
            strSql = "SELECT * FROM Navy_Units WHERE Country = '" & strDominantAllied & "';"
            Call modDB.openDBRecordset(item, rsNavy, strSql)
            strAlliedNavyName = rsNavy("Navy").Value
            rsNavy.Close()

            'Determine the name of the Axis navy.
            strSql = "SELECT * FROM Navy_Units WHERE Country = '" & strDominantAxis & "';"
            Call modDB.openDBRecordset(item, rsNavy, strSql)
            strAxisNavyName = rsNavy("Navy").Value
            rsNavy.Close()

            'Determine the name of the Allied Railway.
            strSql = "SELECT * FROM Railway_Units WHERE Country = '" & strDominantAllied & "';"
            Call modDB.openDBRecordset(item, rsRailway, strSql)
            strAlliedRailwayName = rsRailway("Railway").Value
            rsRailway.Close()

            'Determine the name of the Axis Railway.
            strSql = "SELECT * FROM Railway_Units WHERE Country = '" & strDominantAxis & "';"
            Call modDB.openDBRecordset(item, rsRailway, strSql)
            strAxisRailwayName = rsRailway("Railway").Value
            rsRailway.Close()

            'Determine the participating Allied and Axis divisions.
            strSql = "SELECT * FROM Army_Units WHERE " _
                    & "Campaign_Sector = '" & strThisMap & "' " _
                    & "ORDER BY Alignment,Country;"
            Call modDB.openDBRecordset(item, rsArmy, strSql)

            While rsArmy.EOF = False
                newDivision.Name = rsArmy("Division_Name").Value
                newDivision.Type = rsArmy("Division_Type").Value
                newDivision.Country = rsArmy("Country").Value
                newDivision.Alignment = rsArmy("Alignment").Value
                newDivision.CountryID = rsArmy("Country_ID").Value
                newDivision.Icon_Set = rsArmy("Icon_Set").Value
                newDivision.Max_Platoons = rsArmy("Max_Platoons").Value
                newDivision.Organizational_Level = rsArmy("Organization_Level").Value
                If newDivision.Alignment = "Allied" And intDivisionsAllied < intMaxDivisions Then
                    AlliedArmy.SetValue(newDivision, intDivisionsAllied)
                    intDivisionsAllied += 1
                ElseIf newDivision.Alignment = "Axis" And intDivisionsAxis < intMaxDivisions Then
                    AxisArmy.SetValue(newDivision, intDivisionsAxis)
                    intDivisionsAxis += 1
                End If
                rsArmy.MoveNext()
            End While
            rsArmy.Close()

        Catch ex As Exception
            If intDivisionsAllied > 0 Or intDivisionsAxis > 0 Then
                Call basMess.MessageBox("Too many " & newDivision.Alignment & " divisions found for Sector " & strThisMap & ".", vbInformation)
            Else
                Call basMess.MessageBox("modMissionAnalyzer.subGatherCombatants: possible missing combatant nations in Navy_Units or Railway_Units tables", vbCritical)
            End If
        End Try

    End Function


    'This subroutine initializes the supplied Pilot_Record structure.
    Private Sub subInitializePilotRecord(ByRef thisPilot As Pilot_Record)

        thisPilot.Pilot_Name = ""
        thisPilot.Pilot_Aircraft = ""
        thisPilot.inControl = False
        thisPilot.Pilot_Seat = ""
        thisPilot.Pilot_Status = ""
        thisPilot.EventTime = ""
        thisPilot.E_Air = 0
        thisPilot.E_StaticAir = 0
        thisPilot.E_Tank = 0
        thisPilot.E_Car = 0
        thisPilot.E_Artillery = 0
        thisPilot.E_AAA = 0
        thisPilot.E_Wagon = 0
        thisPilot.E_Ship = 0
        thisPilot.F_Air = 0
        thisPilot.F_StaticAir = 0
        thisPilot.F_Tank = 0
        thisPilot.F_Car = 0
        thisPilot.F_Artillery = 0
        thisPilot.F_AAA = 0
        thisPilot.F_Wagon = 0
        thisPilot.F_Ship = 0
        thisPilot.Bullets = 0
        thisPilot.BulletsHit = 0
        thisPilot.BulletsHitAir = 0
        thisPilot.Bombs = 0
        thisPilot.BombsHit = 0
        thisPilot.Rockets = 0
        thisPilot.RocketsHit = 0

    End Sub


    'This subroutine writes the contents of the hashPilotRecord hashtable to PilotMissionDisposition
    'and Pilot_Scores tables.
    Private Sub subStorePilotRecords(ByVal item As Integer, ByRef isValid As Boolean)
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisPilot As Pilot_Record
        Dim strPilotName As String
        Dim strSQLPS As String
        Dim strSQLCorePS As String
        Dim strSQLPMData As String
        Dim strSQLCorePMData As String
        Dim strSQLPMDisp As String
        Dim strSQLCorePMDisp As String
        Dim intPilots As Integer

        Try
            'Set the completion code.
            isValid = False

            'Set the core SQL syntax for Pilot_Scores table.
            strSQLCorePS = "INSERT INTO Pilot_Scores (Mission, Pilot_Name, Pilot_State, Pilot_Score, " _
                            & "Enemy_Aircraft, Enemy_Static_Aircraft, Enemy_Tank, Enemy_Car, " _
                            & "Enemy_Artillery, Enemy_AAA, Enemy_Wagon, Enemy_Ship, " _
                            & "Friend_Aircraft, Friend_Static_Aircraft, Friend_Tank, Friend_Car, " _
                            & "Friend_Artillery, Friend_AAA, Friend_Wagon, Friend_Ship, " _
                            & "Fire_Bullets, Hit_Bullets, Hit_Air_Bullets, " _
                            & "Fire_Rockets, Hit_Rockets, Fire_Bombs, Hit_Bombs) VALUES "
            strSQLPS = strSQLCorePS

            'Set the core SQL syntax for PilotMissionData table.
            strSQLCorePMData = "INSERT INTO PilotMissionData (Pilot, Mission, ObjectType, ObjGroup) VALUES "
            strSQLPMData = strSQLCorePMData

            'Set the core SQL syntax for PilotMissionDisposition table.
            strSQLCorePMDisp = "INSERT INTO PilotMissionDisposition (Pilot, Mission, Disposition, EventTime) VALUES "
            strSQLPMDisp = strSQLCorePMDisp

            'Establish the enumerator of the hashtable.
            thisEnumerator = hashPilotRecord.GetEnumerator()

            'Step through the entries in the hashtable, writing out the data.
            intPilots = 0
            While thisEnumerator.MoveNext()

                strPilotName = thisEnumerator.Key
                thisPilot = thisEnumerator.Value

                If strPilotName <> strHostPilot And thisPilot.Pilot_Aircraft <> "unknown" Then

                    'Count the number of pilot records.
                    intPilots += 1

                    'If CS_Enforce_Landings is selected, register all "In Control" 
                    'pilots as KIA and destroy their aircraft.
                    If CS_Enforce_Landings = 1 And _
                       thisPilot.inControl = True And _
                      (thisPilot.Pilot_Status <> "Landed" And _
                       thisPilot.Pilot_Status <> "Crashed" And _
                       thisPilot.Pilot_Status <> "Captured" And _
                       thisPilot.Pilot_Status <> "Bailed" And _
                       thisPilot.Pilot_Status <> "KIA") Then

                        'Kill this cowardly pilot.
                        thisPilot.Pilot_Status = "KIA"
                        thisPilot.EventTime = EndTime

                        'Destroy the cowardly pilot's aircraft.
                        Call subDestroyUnlandedAircraft(item, thisPilot.Pilot_Aircraft)

                    End If

                    'Otherwise, if the pilot is still "In Cockpit" or "In Flight", set the 
                    'timestamp to the actual mission end time to allow more accurate estimates 
                    'of flight times for pilot statistics.
                    If thisPilot.Pilot_Status = "In Cockpit" Or _
                       thisPilot.Pilot_Status = "In Flight" Then
                        thisPilot.EventTime = EndTime
                    End If

                    'For each non-host pilot, write the scores record to Pilot_Scores table.
                    strSQLPS &= "('" _
                            & basFiles.StrMissionName & "', '" _
                            & thisPilot.Pilot_Name & "', '" _
                            & thisPilot.Pilot_Status & "'," _
                            & thisPilot.FBScore & ", " _
                            & thisPilot.E_Air & "," _
                            & thisPilot.E_StaticAir & "," _
                            & thisPilot.E_Tank & "," _
                            & thisPilot.E_Car & "," _
                            & thisPilot.E_Artillery & "," _
                            & thisPilot.E_AAA & "," _
                            & thisPilot.E_Wagon & "," _
                            & thisPilot.E_Ship & "," _
                            & thisPilot.F_Air & "," _
                            & thisPilot.F_StaticAir & "," _
                            & thisPilot.F_Tank & "," _
                            & thisPilot.F_Car & "," _
                            & thisPilot.F_Artillery & "," _
                            & thisPilot.F_AAA & "," _
                            & thisPilot.F_Wagon & "," _
                            & thisPilot.F_Ship & "," _
                            & thisPilot.Bullets & "," _
                            & thisPilot.BulletsHit & "," _
                            & thisPilot.BulletsHitAir & "," _
                            & thisPilot.Rockets & "," _
                            & thisPilot.RocketsHit & "," _
                            & thisPilot.Bombs & "," _
                            & thisPilot.BombsHit & "),"

                    'And we also write the pilot disposition to PilotMissionData table.
                    If hashDriveable.ContainsValue(thisPilot.Pilot_Aircraft) = True Then
                        strSQLPMData &= "('" _
                                        & thisPilot.Pilot_Name & "', '" _
                                        & basFiles.StrMissionName & "', '" _
                                        & thisPilot.Pilot_Aircraft & "', '" _
                                        & thisPilot.Pilot_Aircraft & "'),"
                    Else
                        strSQLPMData &= "('" _
                                        & thisPilot.Pilot_Name & "', '" _
                                        & basFiles.StrMissionName & "', '" _
                                        & thisPilot.Pilot_Aircraft & "', '" _
                                        & Left(thisPilot.Pilot_Aircraft, Len(thisPilot.Pilot_Aircraft) - 1) & "'),"
                    End If

                    'And we also write the pilot disposition to PilotMissionDisposition table.
                    strSQLPMDisp &= "('" _
                            & thisPilot.Pilot_Name & "', '" _
                            & basFiles.StrMissionName & "', '" _
                            & thisPilot.Pilot_Status & "', '" _
                            & thisPilot.EventTime & "'),"

                    If modCfg.User(item).DatabaseProvider = "msaccess" Or _
                        modCfg.User(item).chkUseExtendedInserts = False Then
                        strSQLPS = Left(strSQLPS, Len(strSQLPS) - 1)
                        Call modDB.ExecuteDBCommand(item, strSQLPS)
                        strSQLPS = strSQLCorePS
                        strSQLPMData = Left(strSQLPMData, Len(strSQLPMData) - 1)
                        Call modDB.ExecuteDBCommand(item, strSQLPMData)
                        strSQLPMData = strSQLCorePMData
                        strSQLPMDisp = Left(strSQLPMDisp, Len(strSQLPMDisp) - 1)
                        Call modDB.ExecuteDBCommand(item, strSQLPMDisp)
                        strSQLPMDisp = strSQLCorePMDisp
                    End If

                End If

            End While

            If modCfg.User(item).DatabaseProvider = "mysql" And _
               modCfg.User(item).chkUseExtendedInserts = True And _
               intPilots > 0 Then
                strSQLPS = Left(strSQLPS, Len(strSQLPS) - 1)
                Call modDB.ExecuteDBCommand(item, strSQLPS)
                strSQLPMData = Left(strSQLPMData, Len(strSQLPMData) - 1)
                Call modDB.ExecuteDBCommand(item, strSQLPMData)
                strSQLPMDisp = Left(strSQLPMDisp, Len(strSQLPMDisp) - 1)
                Call modDB.ExecuteDBCommand(item, strSQLPMDisp)
            End If

            'Clear the hashPilotRecord hashtable.
            'This was de-activated for DCS v3.3.5 where the hashPilotRecord hashtable was used
            'in basMissionBuilder to identify controlling pilots for reporting of recon notices.
            'hashPilotRecord.Clear()

            'Successful completion.
            isValid = True


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subStorePilotRecords: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This routine goes through the hashAICrewDisposition hashtable and writes the contents to the
    'AI_Crew_Disposition table.
    'New for DCS v3.3.6
    Private Sub subStoreAICrewDisposition(ByVal item As Integer, ByRef isValid As Boolean)

        Dim strSql, strSQLCoreAICD As String
        Dim thisAICrew As AICrewDisposition
        Dim intAICrew As Integer
        Dim thisEnumerator As IDictionaryEnumerator

        Try
            'Pessimistic default.
            isValid = False

            'Set the core SQL syntax for AI_Crew_Disposition table.
            strSQLCoreAICD = "INSERT INTO AI_Crew_Disposition (Mission, Disposition, Air_Regiment, Alignment, " _
                            & "Command_Index, Crew_Index, Skill, Airframe_Type, Airframe_Class, " _
                            & "Event_Time, X, Y) VALUES "
            strSql = strSQLCoreAICD

            'Establish the enumerator of the hashtable.
            thisEnumerator = hashAICrewDisposition.GetEnumerator()

            'Step through the entries in the hashtable, writing out the data.
            intAICrew = 0
            While thisEnumerator.MoveNext()

                'Get this AI Crew record.
                thisAICrew = thisEnumerator.Value

                If thisAICrew.Crew_Index >= 0 Then

                    'Count the number of AI Crew records being stored.
                    intAICrew += 1

                    'Format the SQL insert.
                    'For each non-host pilot, write the scores record to Pilot_Scores table.
                    strSql &= "('" _
                            & basFiles.StrMissionName & "', '" _
                            & thisAICrew.Disposition & "', '" _
                            & thisAICrew.Air_Regiment & "', '" _
                            & thisAICrew.Alignment & "', '" _
                            & thisAICrew.Airframe_Index & "', " _
                            & thisAICrew.Crew_Index & ", " _
                            & thisAICrew.Skill & ", '" _
                            & thisAICrew.Airframe_Type & "', '" _
                            & thisAICrew.Airframe_Class & "', '" _
                            & thisAICrew.EventTime & "', " _
                            & thisAICrew.X & ", " _
                            & thisAICrew.Y & "),"

                    'We are not using extended inserts, so store this individual data set and get ready for the next.
                    If modCfg.User(item).DatabaseProvider = "msaccess" Or _
                       modCfg.User(item).chkUseExtendedInserts = False Then
                        strSql = Left(strSql, Len(strSql) - 1)
                        Call modDB.ExecuteDBCommand(item, strSql)
                        strSql = strSQLCoreAICD
                    End If

                End If


            End While

            'We have compiled the extended insert, so do it!
            If modCfg.User(item).DatabaseProvider = "mysql" And _
               modCfg.User(item).chkUseExtendedInserts = True And _
               intAICrew > 0 Then
                strSql = Left(strSql, Len(strSql) - 1)
                Call modDB.ExecuteDBCommand(item, strSql)
            End If


            'Successful completion.
            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subStoreAICrewDisposition: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This routine decrements the Quantity of the supplied aircraft's flight by one by directly
    'updating the ObjMissionData table. The hashUnit hashtable is already written to the DB.
    Private Sub subDestroyUnlandedAircraft(ByVal item As Integer, ByVal strAircraft As String)
        Dim strFlight As String
        Dim strFlightGroup As String
        Dim strX, strY As String
        Dim strSql As String
        Dim rsDestroy As New ADODB.Recordset
        Dim intQuantity As Integer
        Try

            If InStr(strDestroyedUnlandedAircraft, strAircraft & "|") <= 0 Then

                'We have not already destroyed this aircraft - needed for multi-crewed aircraft.
                strDestroyedUnlandedAircraft &= strAircraft & "|"

            Else

                'Already destroyed - no need to go further.
                Exit Sub
            End If


            'Form the parent flight name.
            strFlight = Left(strAircraft, Len(strAircraft) - 1)

            'Form the SQL to interrogate the strength of this flight.
            strSql = "SELECT ObjMissionData.Obj_Group_Name, ObjMissionData.Quantity, ObjMissionData.Start_X_Axis, ObjMissionData.Start_Y_Axis FROM " _
                    & "ObjMissionData, Unit_Name_Mapping WHERE " _
                    & "ObjMissionData.Mission_Name=Unit_Name_Mapping.Mission AND " _
                    & "ObjMissionData.Mission_Name='" & basFiles.StrMissionName & "' AND " _
                    & "Unit_Name_Mapping.InGame_Name = '" & strFlight & "' AND " _
                    & "ObjMissionData.Obj_Group_Name = Unit_Name_Mapping.Unit_Name;"
            Call modDB.openDBRecordset(item, rsDestroy, strSql)

            If rsDestroy.EOF = False Then

                'Get the actual flight name and location.
                strFlightGroup = rsDestroy("Obj_Group_Name").Value
                strX = rsDestroy("Start_X_Axis").Value
                strY = rsDestroy("Start_Y_Axis").Value

                'Calculate the reduced unit strength.
                intQuantity = Math.Max(0, rsDestroy("Quantity").Value - 1)

                'Close the recordset.
                rsDestroy.Close()

                'Form and execute the SQL to update the flight strength in ObjMissionData.
                'Ignore errors.
                strSql = "UPDATE ObjMissionData SET Quantity= " & intQuantity & "  WHERE " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "Obj_Group_Name = '" & strFlightGroup & "';"
                Call modDB.ExecuteDBCommand(item, strSql, False)

                'Form and execute the SQL to create a crash event in ObjMissionDisposition.
                strSql = "INSERT INTO ObjMissionDisposition (EventTime,Obj,ObjGroup,Mission,Disposition,X_Axis,Y_Axis) VALUES " _
                        & "('" & strEndOfMission & "', '" _
                        & strAircraft & "', '" _
                        & strFlightGroup & "', '" _
                        & basFiles.StrMissionName & "', 'Crashed', " _
                        & CDbl(strX) & ", " & CDbl(strY) & ");"
                Call modDB.ExecuteDBCommand(item, strSql)

            Else

                'Close the recordset.
                rsDestroy.Close()

            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subDestroyUnlandedAircraft: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub




    'This subroutine reads all Task Forces defined in the previous hour and copies to the next hour.
    Private Sub subMaintainTaskForces(ByVal item As Integer)
        Dim rsTaskForces As New ADODB.Recordset
        Dim strSql As String
        Dim dtLastMissionSequence As Date
        Dim dtNextMissionSequence As Date
        Dim strNextSequence As String
        Try
            'Bail out if we are adding forces.
            If isAddForce Then
                Exit Sub
            End If

            'Build the next sequence number.
            dtLastMissionSequence = modUtilities.SN2DT(strLastMissionSequenceNumber)
            dtNextMissionSequence = DateAdd(DateInterval.Hour, CDbl(CS_Mission_Length), dtLastMissionSequence)
            strNextSequence = DT2SN(dtNextMissionSequence)

            'Remove any spurious Task Forces already defined at the next sequence number.
            strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Task_Forces WHERE " _
                    & "Map = '" & strThisMap & "' AND " _
                    & "Mission_Sequence = '" & strNextSequence & "';"
            Call modDB.ExecuteDBCommand(item, strSql)

            'Create query to gather all Task Forces into one recordset.
            strSql = "SELECT * FROM Task_Forces WHERE Map = '" & strThisMap & "' AND " _
                    & "Mission_Sequence = '" & strLastMissionSequenceNumber & "' " _
                    & "ORDER BY Task_Force_Name, Task_Force_Rank, Unit_Group_Name;"
            Call modDB.openDBRecordset(item, rsTaskForces, strSql)

            'Loop through all Task Force members.
            Do Until rsTaskForces.EOF = True

                'For each Task Force member, create a new record in the table for the 
                'next campaign sequence number.
                strSql = "INSERT INTO Task_Forces (Map,Mission_Sequence,Task_Force_Name,Unit_Group_Name,Task_Force_Rank,Nationality) VALUES " _
                        & "('" & rsTaskForces("Map").Value & "', '" _
                        & strNextSequence & "', '" _
                        & rsTaskForces("Task_Force_Name").Value & "', '" _
                        & rsTaskForces("Unit_Group_Name").Value & "', '" _
                        & rsTaskForces("Task_Force_Rank").Value & "', '" _
                        & rsTaskForces("Nationality").Value & "')"
                Call modDB.ExecuteDBCommand(item, strSql)

                'Move to next Task Force member.
                rsTaskForces.MoveNext()
            Loop

            'Close the recordset.
            rsTaskForces.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subMaintainTaskForces: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub




    'This subroutine processes a human wingtip smoke event as a recon photo event.
    Private Sub subAddHumanReconPhoto(ByVal item As Integer, ByVal strLine As String)
        Dim rsReconLocations As New ADODB.Recordset
        Dim strLineArray() As String
        Dim strSql As String
        Dim strThisPhotoFlight As String
        Dim strThisPhotoTime As String
        Dim strThisPhotoX As String
        Dim strThisPhotoY As String
        Dim strThisPhotoZ As String
        Dim intMaxPhotos As Integer
        Dim intRegisteredPhotos As Integer
        Dim intETA As Integer
        Dim strThisFlightType As String
        Dim strThisFlightNation As String

        Try

            'Parse the current logfile record.
            strLineArray = Trim(strLine).Split(" ")
            strThisPhotoTime = strLineArray(0)
            strThisPhotoFlight = strLineArray(1)
            strThisPhotoX = strLineArray(7)
            strThisPhotoY = strLineArray(8)

            'Check for the altitude element in the logfile record, added in HSFX.
            If strLineArray.GetUpperBound(0) = 9 Then
                strThisPhotoZ = strLineArray(9)
                If strLineArray(9) < 0 Then   'Bad caste in Fireball's original mod.
                    strThisPhotoZ = Format(-50.0 * CDbl(strLineArray(9)), "######")
                End If
            Else
                strThisPhotoZ = "2500.0"
            End If

            'Calculate the true elapsed time of this event.
            'intETA = DateDiff(DateInterval.Second, CDate(BeginTime), CDate(strThisPhotoTime))
            intETA = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, CDate(strThisPhotoTime))


            'Check to see if this flight is a marked RECON flight.
            strSql = "SELECT Mission_Recon.Recon_Type, Mission_Recon.Nationality AS Nation, " _
                    & "Object_Specifications.Recon_Capacity, Mission_Recon.Human_Flight " _
                    & "FROM Mission_Recon, Object_Specifications WHERE " _
                    & "Mission_Recon.Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Mission_Recon.Recon_Unit = '" & Left(strThisPhotoFlight, Len(strThisPhotoFlight) - 1) & "' AND " _
                    & "Mission_Recon.Recon_Type = Object_Specifications.Object_Type;"
            modDB.openDBRecordset(item, rsReconLocations, strSql)

            If rsReconLocations.EOF = False Then

                'We have a scheduled recon flight listed. We need to delete the scheduled recon locations
                'and replace with this registered location.
                intMaxPhotos = rsReconLocations("Recon_Capacity").Value
                intRegisteredPhotos = 0
                strThisFlightType = rsReconLocations("Recon_Type").Value
                strThisFlightNation = rsReconLocations("Nation").Value

                'Loop through all recon locations found and count up the number of human photos
                'already taken.
                While rsReconLocations.EOF = False

                    If rsReconLocations("Human_Flight").Value = 1 Then
                        intRegisteredPhotos += 1
                    End If

                    rsReconLocations.MoveNext()

                End While

                'Close the record set.
                rsReconLocations.Close()

                'If we still have remaining recon photo capacity, then register the current location.
                If intRegisteredPhotos < intMaxPhotos Then

                    'Register this human photo event.
                    strSql = "INSERT INTO Mission_Recon (Mission_Name, Human_Flight, " _
                            & "Recon_Unit, Recon_Type, Nationality, " _
                            & "Recon_X, Recon_Y, Recon_Alt, Recon_ETA, Recon_Success) VALUES ('" _
                            & basFiles.StrMissionName & "' , " _
                            & "1 , '" _
                            & Left(strThisPhotoFlight, Len(strThisPhotoFlight) - 1) & "' , '" _
                            & strThisFlightType & "' , '" _
                            & strThisFlightNation & "', '" _
                            & CDbl(strThisPhotoX) & "', '" _
                            & CDbl(strThisPhotoY) & "', '" _
                            & CDbl(strThisPhotoZ) & "', '" _
                            & intETA & "',1)"
                    Call modDB.ExecuteDBCommand(item, strSql)

                Else
                    'This pilot is clicking his camera but the film has already run out!

                End If

                'Delete any non-human recon locations remaining for this flight.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Mission_Recon WHERE " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "Recon_Unit = '" & Left(strThisPhotoFlight, Len(strThisPhotoFlight) - 1) & "' AND " _
                        & "Nationality = '" & strThisFlightNation & "' AND " _
                        & "Human_Flight = 0;"
                Call modDB.ExecuteDBCommand(item, strSql)

            Else

                'There is no registered recon flight, so this wingtip smoke event is spurious.
                rsReconLocations.Close()
                Exit Sub

            End If



        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAddHumanReconPhoto: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub




    'This subroutine processes a human wingtip smoke event as a supply drop event.
    Private Sub subAddHumanSupplyDrop(ByVal item As Integer, ByVal strLine As String)
        Dim rsSDLocations As New ADODB.Recordset
        Dim strLineArray() As String
        Dim strSql As String
        Dim strThisSDFlight As String
        Dim strThisSDTime As String
        Dim strThisSDX As String
        Dim strThisSDY As String
        Dim intRegisteredSD As Integer
        Dim intETA, intPreviousETA As Integer
        Dim strThisFlightNation As String
        Dim strThisFlightType As String
        Dim intThisFlightHolding As Integer

        Try

            'Parse the current logfile record.
            strLineArray = strLine.Split(" ")
            strThisSDTime = strLineArray(0)
            strThisSDFlight = strLineArray(1)
            strThisSDX = strLineArray(7)
            strThisSDY = strLineArray(8)

            'Calculate the true elapsed time of this event.
            'intETA = DateDiff(DateInterval.Second, CDate(BeginTime), CDate(strThisSDTime))
            intETA = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, CDate(strThisSDTime))


            'Check to see if this flight is a marked SUPPLY flight.
            strSql = "SELECT * FROM Supply_Drop_Orders WHERE " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Transport_Flight = '" & Left(strThisSDFlight, Len(strThisSDFlight) - 1) & "';"
            modDB.openDBRecordset(item, rsSDLocations, strSql)

            If rsSDLocations.EOF = False Then

                'We have a scheduled supply drop flight listed. We need to delete the scheduled drop location
                'and replace with this registered location.
                intRegisteredSD = 0
                strThisFlightNation = rsSDLocations("Nationality").Value
                strThisFlightType = rsSDLocations("Transport_Type").Value
                intThisFlightHolding = rsSDLocations("Supply_Holding").Value
                intPreviousETA = rsSDLocations("Drop_ETA").Value

                'Loop through all supply drop locations found and count up the number of drops
                'already made.
                While rsSDLocations.EOF = False

                    If rsSDLocations("Human_Flight").Value = 1 Then
                        intRegisteredSD += 1
                    End If

                    rsSDLocations.MoveNext()

                End While

                'Close the record set.
                rsSDLocations.Close()

                'If we still have remaining supply drop capacity, then register the current location.
                If intRegisteredSD < 1 Then

                    'Register this human SUPPLY event.
                    strSql = "INSERT INTO Supply_Drop_Orders (Mission_Name, Human_Flight, " _
                            & "Transport_Flight, Transport_Type, Nationality, Supply_Holding, " _
                            & "Drop_X, Drop_Y, Drop_ETA, Drop_Success) VALUES ('" _
                            & basFiles.StrMissionName & "' , " _
                            & "1 , '" _
                            & Left(strThisSDFlight, Len(strThisSDFlight) - 1) & "' , '" _
                            & strThisFlightType & "' , '" _
                            & strThisFlightNation & "', " _
                            & intThisFlightHolding & ", '" _
                            & CDbl(strThisSDX) & "', '" _
                            & CDbl(strThisSDY) & "', " _
                            & intETA & ",1)"
                    Call modDB.ExecuteDBCommand(item, strSql)

                Else
                    'This pilot is hitting the release button, but no more supplies to drop!

                End If

                'Store the time of the SD for later comparison with plane action events.
                If hashSupplyDropTime.ContainsKey(Left(strThisSDFlight, Len(strThisSDFlight) - 1)) = True Then
                    hashSupplyDropTime(Left(strThisSDFlight, Len(strThisSDFlight) - 1)) = intETA
                Else
                    hashSupplyDropTime.Add(Left(strThisSDFlight, Len(strThisSDFlight) - 1), intETA)
                End If

            Else

                'There is no registered supply flight, so this wingtip smoke event is spurious.
                rsSDLocations.Close()
                Exit Sub

            End If



        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAddHumanSupplyDrop: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub




    'This subroutine processes a human wingtip smoke event as a supply drop event.
    Private Sub subAddHumanParatrooperAssault(ByVal item As Integer, ByVal strLine As String)
        Dim rsPALocations As New ADODB.Recordset
        Dim strLineArray() As String
        Dim strSql As String
        Dim strThisPAFlight As String
        Dim strThisPATime As String
        Dim strThisPAX As String
        Dim strThisPAY As String
        Dim strThisPAZ As String
        Dim intRegisteredPA As Integer
        Dim intETA, intPreviousETA As Integer
        Dim strThisFlightNation As String
        Dim strThisFlightType As String
        Dim strThisParatrooperUnit As String
        Dim strPANotice As String
        Dim strPilotName As String
        Dim strFlightName As String
        Dim thisUnit As SE_Unit

        Try

            'Parse the current logfile record.
            strLineArray = strLine.Split(" ")
            strThisPATime = strLineArray(0)
            strThisPAFlight = strLineArray(1)
            strThisPAX = strLineArray(7)
            strThisPAY = strLineArray(8)
            If strLineArray.GetUpperBound(0) = 9 Then
                strThisPAZ = strLineArray(9)
            Else
                strThisPAZ = CStr(Math.Round((basMain.MIN_JUMP_ALTITUDE + basMain.MAX_JUMP_ALTITUDE) / 2.0))
            End If

            'Calculate the true elapsed time of this event.
            'intETA = DateDiff(DateInterval.Second, CDate(BeginTime), CDate(strThisPATime))
            intETA = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, CDate(strThisPATime))


            'Check to see if this flight is a marked ASSAULT flight.
            strSql = "SELECT * FROM Paratrooper_Assault_Orders WHERE " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Transport_Flight = '" & Left(strThisPAFlight, Len(strThisPAFlight) - 1) & "';"
            modDB.openDBRecordset(item, rsPALocations, strSql)

            While rsPALocations.EOF = False

                'We have a scheduled paratrooper assault flight listed and yet to be released. 
                'We need to delete the scheduled assault location
                'and replace with this registered location.
                intRegisteredPA = rsPALocations("Human_Flight").Value
                strThisFlightNation = rsPALocations("Nationality").Value
                strThisFlightType = rsPALocations("Transport_Type").Value
                strThisParatrooperUnit = rsPALocations("Paratrooper_Unit").Value
                intPreviousETA = rsPALocations("Drop_ETA").Value

                If intRegisteredPA = 0 Then
                    'Register this human Paratrooper Assault event.
                    strSql = "UPDATE Paratrooper_Assault_Orders SET " _
                            & "Human_Flight=1, " _
                            & "Drop_X = " & strThisPAX & ", " _
                            & "Drop_Y = " & strThisPAY & ", " _
                            & "Drop_ETA = " & intETA & ", " _
                            & "Drop_Success = 1 WHERE " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                            & "Transport_Flight = '" & Left(strThisPAFlight, Len(strThisPAFlight) - 1) & "' AND " _
                            & "Paratrooper_Unit = '" & strThisParatrooperUnit & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                    'Store the time of the PA for later comparison with plane action events.
                    If hashParatroooperAssaultTime.ContainsKey(Left(strThisPAFlight, Len(strThisPAFlight) - 1)) = True Then
                        hashParatroooperAssaultTime(Left(strThisPAFlight, Len(strThisPAFlight) - 1)) = intETA
                    Else
                        hashParatroooperAssaultTime.Add(Left(strThisPAFlight, Len(strThisPAFlight) - 1), intETA)
                    End If

                    'If this assault jump took place below minimum altitude then destroy all paratroop units involved
                    'and issue an appropriate campaign notice.
                    If CInt(strThisPAZ) < basMain.MIN_JUMP_ALTITUDE Then
                        Call subDestroyFreight(item, Left(strThisPAFlight, Len(strThisPAFlight) - 1), strThisPAX, strThisPAY, _
                                               "destroyed by", "landscape", basFiles.StrMissionName, strThisPATime, 1.0, 1)
                        'get the flight details.
                        If hashUnit.ContainsKey(Left(strThisPAFlight, Len(strThisPAFlight) - 1)) Then
                            thisUnit = hashUnit(Left(strThisPAFlight, Len(strThisPAFlight) - 1))
                            strFlightName = thisUnit.Unit_Name
                        Else
                            strFlightName = strThisPAFlight
                        End If

                        'Get the pilot details.
                        strPilotName = strGetPilotByAircraft(strThisPAFlight)

                        If UCase(strThisFlightNation) = "R" Then
                            strPANotice = "Allied paratroop units aboard " & strFlightName & " (pilot " & strPilotName & ") were ordered at time " & strThisPATime & " to jump at low altitude (" & strThisPAZ & " m) and fell to their deaths - disciplinary action against pilot required."
                        Else
                            strPANotice = "Axis paratroop units aboard " & strFlightName & " (pilot " & strPilotName & ") were ordered at time " & strThisPATime & " to jump at low altitude (" & strThisPAZ & " m) and fell to their deaths - disciplinary action against pilot required."
                        End If
                        Call modUtilities.subCampaignNotice(item, strThisMissionMap, strLastMissionSequenceNumber, strThisFlightNation, strPANotice)
                    End If

                    'If this assault jump took place above maximum altitude then destroy all paratroop units involved
                    'and issue an appropriate campaign notice.
                    If CInt(strThisPAZ) > basMain.MAX_JUMP_ALTITUDE Then
                        Call subDestroyFreight(item, Left(strThisPAFlight, Len(strThisPAFlight) - 1), strThisPAX, strThisPAY, _
                                               "destroyed by", "landscape", basFiles.StrMissionName, strThisPATime, 1.0, 1)
                        'get the flight details.
                        If hashUnit.ContainsKey(Left(strThisPAFlight, Len(strThisPAFlight) - 1)) Then
                            thisUnit = hashUnit(Left(strThisPAFlight, Len(strThisPAFlight) - 1))
                            strFlightName = thisUnit.Unit_Name
                        Else
                            strFlightName = strThisPAFlight
                        End If

                        'Get the pilot details.
                        strPilotName = strGetPilotByAircraft(strThisPAFlight)

                        If UCase(strThisFlightNation) = "R" Then
                            strPANotice = "Allied paratroop units aboard " & strFlightName & " (pilot " & strPilotName & ") were ordered at time " & strThisPATime & " to jump at high altitude (" & strThisPAZ & " m) and were lost - disciplinary action against pilot required."
                        Else
                            strPANotice = "Axis paratroop units aboard " & strFlightName & " (pilot " & strPilotName & ") were ordered at time " & strThisPATime & " to jump at high altitude (" & strThisPAZ & " m) and were lost - disciplinary action against pilot required."
                        End If
                        Call modUtilities.subCampaignNotice(item, strThisMissionMap, strLastMissionSequenceNumber, strThisFlightNation, strPANotice)
                    End If

                End If

                'Move to the next record.
                rsPALocations.MoveNext()

            End While

            'There is no registered assault flight, so this wingtip smoke event is spurious.
            rsPALocations.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAddHumanParatrooperAssault: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub





    'This subroutine clears the MissionRecon table of non-Human recon locations for all flights
    'that have been confirmed to have been flown by humans.
    Private Sub subConfirmReconPhotos(ByVal item As Integer)
        Dim strSql As String
        Dim strFlights() As String
        Dim intFlight As Integer

        Try
            'If no human recon flights indentified, exit sub.
            If Len(strConfirmedHumanReconFlights) = 0 Or CS_Human_Recon_Supply_Flights = 0 Then
                Exit Sub
            End If

            'Parse the list of confirmed human recon flights into an array.
            strFlights = strConfirmedHumanReconFlights.Split("|")

            'Loop through the list of confirmed flights.
            intFlight = 0
            While intFlight < strFlights.Length - 1

                'Remove any existing non-human points for this flight.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Mission_Recon WHERE " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "Recon_Unit = '" & strFlights(intFlight) & "' AND " _
                        & "Human_Flight = 0;"
                modDB.ExecuteDBCommand(item, strSql)

                'Next confirmed recon flight.
                intFlight += 1

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subConfirmReconPhotos: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This subroutine clears the MissionRecon table of non-Human supply drop locations for all flights
    'that have been confirmed to have been flown by humans.
    Private Sub subConfirmSupplyDrops(ByVal item As Integer)
        Dim strSql As String
        Dim strFlights() As String
        Dim intFlight As Integer

        Try
            'If no human recon flights indentified, exit sub.
            If Len(strConfirmedHumanSupplyDropFlights) = 0 Or CS_Human_Recon_Supply_Flights = 0 Then
                Exit Sub
            End If

            'Parse the list of confirmed human recon flights into an array.
            strFlights = strConfirmedHumanSupplyDropFlights.Split("|")

            'Loop through the list of confirmed flights.
            intFlight = 0
            While intFlight < strFlights.Length - 1

                'Remove any existing non-human points for this flight.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Supply_Drop_Orders WHERE " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "Transport_Flight = '" & strFlights(intFlight) & "' AND " _
                        & "Human_Flight = 0;"
                modDB.ExecuteDBCommand(item, strSql)

                'Next confirmed recon flight.
                intFlight += 1

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subConfirmSupplyDrops: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This subroutine clears the Paratrooper_Assault_Orders table of non-Human assault locations for all flights
    'that have been confirmed to have been flown by humans.
    Private Sub subConfirmParatrooperAssaults(ByVal item As Integer)
        Dim strSql As String
        Dim strFlights() As String
        Dim intFlight As Integer

        Try
            'If no human assault flights indentified, exit sub.
            If Len(strConfirmedHumanParatrooperAssaultFlights) = 0 Or CS_Human_Recon_Supply_Flights = 0 Then
                Exit Sub
            End If

            'Parse the list of confirmed human assault flights into an array.
            strFlights = strConfirmedHumanParatrooperAssaultFlights.Split("|")

            'Loop through the list of confirmed flights.
            intFlight = 0
            While intFlight < strFlights.Length - 1

                'Remove any existing non-human points for this flight.
                strSql = "DELETE " & modDB.strSQLglob(item) & " FROM Paratrooper_Assault_Orders WHERE " _
                        & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                        & "Transport_Flight = '" & strFlights(intFlight) & "' AND " _
                        & "Human_Flight = 0;"
                modDB.ExecuteDBCommand(item, strSql)

                'Next confirmed assault flight.
                intFlight += 1

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subConfirmParatrooperAssaults: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub




    Private Sub subGatherReconFlights(ByVal item As Integer)
        Dim strSql As String
        Dim rsRRF As New ADODB.Recordset

        Try
            'Build a query to extract the names of the registered recon flights for this mission.
            strSql = "SELECT DISTINCT Recon_Unit FROM Mission_Recon WHERE " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Human_Flight = 0;"
            modDB.openDBRecordset(item, rsRRF, strSql)

            While rsRRF.EOF = False

                'Add this flight name to the public string.
                strAllReconFlights &= rsRRF("Recon_Unit").Value & "|"

                'Go to next flight.
                rsRRF.MoveNext()

            End While

            'Close the Recordset.
            rsRRF.Close()

            'Trim the last "|" character off the end of the public string.
            If Len(strAllReconFlights) > 0 Then
                strAllReconFlights = Left(strAllReconFlights, Len(strAllReconFlights) - 1)
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGatherReconFlights: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    Private Sub subGatherSupplyDropFlights(ByVal item As Integer)
        Dim strSql As String
        Dim rsRSDF As New ADODB.Recordset

        Try
            'Build a query to extract the names of the registered supply drop flights for this mission.
            strSql = "SELECT DISTINCT Transport_Flight FROM Supply_Drop_Orders WHERE " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Human_Flight = 0;"
            modDB.openDBRecordset(item, rsRSDF, strSql)

            While rsRSDF.EOF = False

                'Add this flight name to the public string.
                strAllSupplyDropFlights &= rsRSDF("Transport_Flight").Value & "|"

                'Go to next flight.
                rsRSDF.MoveNext()

            End While

            'Close the Recordset.
            rsRSDF.Close()

            'Trim the last "|" character off the end of the public string.
            If Len(strAllSupplyDropFlights) > 0 Then
                strAllSupplyDropFlights = Left(strAllSupplyDropFlights, Len(strAllSupplyDropFlights) - 1)
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGatherSupplyDropFlights: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This routine takes the ActionData events for paratrooper assaults and converts them into proportional 
    'damage. In doing this, some ActionData events may be deleted.
    Private Sub subAdjustAssaultEvents(ByVal item As Integer)
        Dim strSql As String
        Dim rsPUDamage, rsPUQuantity, rsPUExcess As New ADODB.Recordset
        Dim strPU As String
        Dim intPUStrength, intPUDamage As Integer
        Dim thisCU As SE_CombinedUnit

        Try
            'Build a query to extract the names of the registered paratrooper assault units for this mission.
            'Note that "strafed by" events are not classed as kills, so they are not included in the query.
            strSql = "SELECT Paratrooper_Assault_Orders.Paratrooper_Unit, Count(ActionData.Event) AS PUDamage " _
                    & "FROM Paratrooper_Assault_Orders, ActionData WHERE " _
                    & "Paratrooper_Assault_Orders.Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Paratrooper_Assault_Orders.Mission_Name = ActionData.Mission AND " _
                    & "Paratrooper_Assault_Orders.Paratrooper_Unit = ActionData.Obj1Group AND " _
                    & "Paratrooper_Assault_Orders.Drop_Success = 1 AND " _
                    & "(ActionData.Event = 'destroyed by' OR ActionData.Event = 'killed') " _
                    & "GROUP BY Paratrooper_Assault_Orders.Paratrooper_Unit " _
                    & "ORDER BY Paratrooper_Assault_Orders.Paratrooper_Unit;"
            modDB.openDBRecordset(item, rsPUDamage, strSql)

            While rsPUDamage.EOF = False

                'Here is the name and number of damage events for the current paratrooper assault unit.
                strPU = rsPUDamage("Paratrooper_Unit").Value
                intPUDamage = rsPUDamage("PUDamage").Value

                'We need to find out the strength of the PU. That depends on whether it is a combined unit.
                If hashCombinedUnit.ContainsKey(strPU) Then

                    'It is a CU, so just load the CU data directly.
                    thisCU = hashCombinedUnit(strPU)
                    intPUStrength = thisCU.CurrentStrength

                Else

                    'It is not a CU, so grab the PU quantity from ObjMissionData.
                    strSql = "SELECT Quantity FROM ObjMissionData WHERE " _
                            & "Obj_Group_Name = '" & strPU & "' AND " _
                            & "Mission_Name = '" & basFiles.StrMissionName & "';"
                    modDB.openDBRecordset(item, rsPUQuantity, strSql)

                    If rsPUQuantity.EOF = False Then
                        intPUStrength = rsPUQuantity("Quantity").Value
                    Else
                        'Strange, we can't find the PU. Give it zero strength.
                        intPUStrength = 0
                    End If

                    'Close the quantity recordset.
                    rsPUQuantity.Close()

                End If


                'OK, we now know the PU name and its strength, plus how many kill hits it has taken.
                'If the PU has sustained more hits than its strength, we need to remove the excess
                'hit events from ActionData to retain consistent statistics.
                If intPUDamage > intPUStrength Then

                    If modCfg.User(item).DatabaseProvider = "mysql" Then

                        'It is easy to do this in one step in MySQL.
                        strSql = "DELETE FROM ActionData WHERE Obj1Group='" & strPU & "' AND " _
                                & "Mission = '" & basFiles.StrMissionName & "' AND " _
                                & "(Event='killed' OR Event = 'destroyed by') " _
                                & "ORDER BY EventTime DESC " _
                                & "LIMIT " & (intPUDamage - intPUStrength) & ";"
                        modDB.ExecuteDBCommand(item, strSql)

                    Else

                        'Harder in MSAccess.
                        strSql = "SELECT TOP " & (intPUDamage - intPUStrength) & " * FROM ActionData " _
                                & "WHERE Mission = '" & basFiles.StrMissionName & "' AND " _
                                & "Obj1Group='" & strPU & "' AND " _
                                & "(Event='killed' OR Event = 'destroyed by') " _
                                & "ORDER BY EventTime DESC;"
                        modDB.openDBRecordset(item, rsPUExcess, strSql)

                        'Loop through all excess events in ActionData.
                        While rsPUExcess.EOF = False

                            'Delete the current event from ActionData.
                            strSql = "DELETE * FROM ActionData " _
                                & "WHERE Mission = '" & basFiles.StrMissionName & "' AND " _
                                & "Obj1Group='" & strPU & "' AND " _
                                & "Event = '" & rsPUExcess("Event").Value & "' AND " _
                                & "EventTime = '" & rsPUExcess("EventTime").Value & "';"
                            modDB.ExecuteDBCommand(item, strSql)

                            'Go to the next event.
                            rsPUExcess.MoveNext()

                        End While

                        'Close the recordset.
                        rsPUExcess.Close()

                    End If
                End If


                'Move to the next record.
                rsPUDamage.MoveNext()

            End While

            'Close the damaged paratrooper assault unit recordset.
            rsPUDamage.Close()

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAdjustAssaultEvents: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    Private Sub subGatherParatrooperAssaultFlights(ByVal item As Integer)
        Dim strSql As String
        Dim rsRSDF As New ADODB.Recordset

        Try
            'Build a query to extract the names of the registered supply drop flights for this mission.
            strSql = "SELECT DISTINCT Transport_Flight FROM Paratrooper_Assault_Orders WHERE " _
                    & "Mission_Name = '" & basFiles.StrMissionName & "' AND " _
                    & "Human_Flight = 0;"
            modDB.openDBRecordset(item, rsRSDF, strSql)

            While rsRSDF.EOF = False

                'Add this flight name to the public string.
                strAllParatrooperAssaultFlights &= rsRSDF("Transport_Flight").Value & "|"

                'Go to next flight.
                rsRSDF.MoveNext()

            End While

            'Close the Recordset.
            rsRSDF.Close()

            'Trim the last "|" character off the end of the public string.
            If Len(strAllParatrooperAssaultFlights) > 0 Then
                strAllParatrooperAssaultFlights = Left(strAllParatrooperAssaultFlights, Len(strAllParatrooperAssaultFlights) - 1)
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGatherParatrooperAssaultFlights: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    Private Sub subAddHumanIntervention(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray(), strCoordType, strDummy As String
        Dim strFlight, thisDriveable, strLOSPilot, strLOSNotice As String
        Dim thisUnit As SE_Unit
        Dim isRallyPoint As Boolean = False

        Try

            'Parse this logfile record.
            strDummy = Replace(strLine, "  ", " ")
            strLineArray = strDummy.Split(" ")
            strFlight = Left(strLineArray(1), Len(strLineArray(1)) - 1)

            If InStr(strAllReconFlights, strFlight) > 0 Then
                'This wingtip smoke event came from a Recon flight.
                Call subAddHumanReconPhoto(item, strDummy)

            ElseIf InStr(strAllSupplyDropFlights, strFlight) > 0 Then
                'This wingtip smoke event came from a Supply Drop flight.
                Call subAddHumanSupplyDrop(item, strDummy)

            ElseIf InStr(strAllParatrooperAssaultFlights, strFlight) > 0 Then
                'This wingtip smoke event came from a Paratrooper Assault flight.
                Call subAddHumanParatrooperAssault(item, strDummy)

            ElseIf hashDriveable.ContainsKey(strFlight) = True Then
                'This wingtip smoke event came from a pilot driving a vehicle.
                'Update the unit location so we can track where this driveable is.
                thisDriveable = hashDriveable(strFlight)
                If hashUnit.ContainsKey(thisDriveable) = True Then
                    thisUnit = hashUnit(thisDriveable)
                    thisUnit.X = strLineArray(7)
                    thisUnit.Y = strLineArray(8)
                    hashUnit(thisDriveable) = thisUnit

                    'If supply tracking is on, this may be a rally point marker!
                    If CS_Track_Fuel_Consumption = 1 Then
                        isRallyPoint = isSuccessfulRallyPoint(thisUnit, strDummy)
                    End If

                    If isRallyPoint = True Then
                        strCoordType = "a rally point at"
                    Else
                        strCoordType = "map location"
                    End If

                    'And log a campaign notice for exact coordinate LOS or rally point tracking.
                    strLOSPilot = strGetPilotByAircraft(thisUnit.Object_Group_Name)
                    strLOSNotice = "Time " & strLineArray(0) & " :: " & modUtilities.strAlignment(thisUnit.Nationality)
                    strLOSNotice &= " vehicle belonging to " & thisUnit.Unit_Name _
                                  & ", driven by " & strLOSPilot & ", has marked " & strCoordType & " (" & thisUnit.X & ", " & thisUnit.Y & ")."
                    Call modUtilities.subCampaignNotice(item, strPreviousMap, _
                                        strLastMissionSequenceNumber, thisUnit.Nationality, _
                                        strLOSNotice)

                End If

                'Also update the pilot status to "Landed" so that the vehicle is 
                'not lost if no landing notice is issued for an upturned vehicle.
                Call subUpdatePilotStatus(item, thisDriveable, "Landed", strLine, thisDriveable)


            Else
                'Spurious wingtip smoke event - no intervention required.
            End If


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subAddHumanIntervention: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    'This function tests whether a smoke event can be successfully registered as a rally point.
    'The function assumes that the SE_Unit structure passed to it corresponds to a driveable vehicle piloted by a human.
    'Only one rally point can be marked in each hour of the mission.
    Private Function isSuccessfulRallyPoint(ByRef thisUnit As SE_Unit, ByVal strLine As String) As Boolean

        Dim strLineArray(), strTime, strX, strY As String
        Dim intSmokeElapsed, intThisHour, intNumberRallyPoints As Integer
        Try
            'Parse the smoke event line.
            strLineArray = Split(strLine, " ")
            strTime = Trim(strLineArray(0))
            strX = CStr(Math.Floor(CDbl(strLineArray(7))))
            strY = CStr(Math.Floor(CDbl(strLineArray(8))))

            'Find the elapsed time (in seconds) of this smoke event.
            intSmokeElapsed = modUtilities.intMissionIntervalSeconds(strLastMissionSequenceNumber, CDate(strTime))

            'Find the hour of the mission, e.g. first, second, third etc.
            intThisHour = Math.Floor(CDbl(intSmokeElapsed) / 3600.0) + 1

            'Find the current number of rally points defined for this units (from previous smoke events).
            intNumberRallyPoints = thisUnit.RallyX.Count

            'If we have less marked rally points than the number of the current hour, then we add this point as a new rally point.
            If intNumberRallyPoints < intThisHour Then
                thisUnit.RallyX.Add(strX)
                thisUnit.RallyY.Add(strY)
                thisUnit.RallyT.Add(intSmokeElapsed)
                isSuccessfulRallyPoint = True
            Else
                isSuccessfulRallyPoint = False
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isSuccessfulRallyPoint: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function

    'This routine checks the isDestroyed flag for the relevant unit.
    Private Sub subRegisterShipDestruction(ByVal strShipGroup As String)
        Dim thisShip As SE_Unit
        Try
            If hashUnit.ContainsKey(strShipGroup) Then
                thisShip = hashUnit(strShipGroup)
                thisShip.isDestroyed = True
                hashUnit(strShipGroup) = thisShip
            Else
                Call basMess.MessageBox("modMissionAnalyzer.subRegisterShipDestruction: " _
                    & "cannot register destruction for ship unit " & strShipGroup, vbCritical)
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subRegisterShipDestruction: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This routine formats a public string to contain the names of all destroyed aircraft
    'in ActionData and ObjMissionDisposition.
    Private Sub subGetVanquished(ByVal item)
        Dim strSql As String
        Dim rsVanquished As New ADODB.Recordset
        Try
            'Initialize the strVanquishedUnits string.
            strVanquishedUnits = ""

            'Get a recordset of all vanquished units in ActionData.
            strSql = "SELECT Obj1 FROM ActionData WHERE Mission='" & basFiles.StrMissionName & "' AND " _
                    & "(Event='shot down by' OR Event='destroyed by');"
            modDB.openDBRecordset(item, rsVanquished, strSql)

            'Loop through, appending to strVanquishedUnits
            While rsVanquished.EOF = False
                strVanquishedUnits &= rsVanquished("Obj1").Value & "|"
                rsVanquished.MoveNext()
            End While

            'Close the recordset.
            rsVanquished.Close()


            'Get a recordset of all vanquished units in ObjMissionDisposition.
            strSql = "SELECT Obj FROM ObjMissionDisposition WHERE Mission='" & basFiles.StrMissionName & "' AND " _
                    & "Disposition='Crashed';"
            modDB.openDBRecordset(item, rsVanquished, strSql)

            'Loop through, appending to strVanquishedUnits
            While rsVanquished.EOF = False
                strVanquishedUnits &= rsVanquished("Obj").Value & "|"
                rsVanquished.MoveNext()
            End While

            'Close the recordset.
            rsVanquished.Close()

            'All done.

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGetVanquished: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    Private Sub subLoadCatsyTriggers(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String ' create an array to hold the split of the lines in [MODSLOT] section of mission
        Dim strSql As String 'this is the string of the SQL statement to perform against the recordset

        Try

            'Here we are looping until we reach the end of the mission file. We assume we are starting at the 
            '[Trigger] directive line.
            Do Until InStr(UCase(strLine), "[MODSLOT]") > 0 Or srMissionMA.EndOfStream = True

                'Read the next line of the mission file.
                strLine = srMissionMA.ReadLine()

            Loop

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadCatsyTriggers: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    Private Sub subLoadCoopModSlot(ByVal item As Integer, ByVal strLine As String)
        Dim strLineArray() As String ' create an array to hold the split of the lines in [MODSLOT] section of mission
        Dim strSql As String 'this is the string of the SQL statement to perform against the recordset

        Try
            'Bail out if we are adding forces.
            If isAddForce Then
                Exit Sub
            End If

            'Here we are looping until we reach the end of the mission file. We assume we are starting at the 
            '[MODSLOT] directive line.
            Do Until srMissionMA.EndOfStream = True

                'Read the next line of the mission file.
                strLine = srMissionMA.ReadLine()

                'If we find a national directive, we will write it to the Coop_Mod_Slot table.
                If InStr(UCase(strLine), "R ") = 1 Then

                    strSql = "UPDATE Coop_Mod_Slot SET Allied_Directive= '" & Trim(strLine) & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                ElseIf InStr(UCase(strLine), "B ") = 1 Then

                    strSql = "UPDATE Coop_Mod_Slot SET Axis_Directive= '" & Trim(strLine) & "';"
                    Call modDB.ExecuteDBCommand(item, strSql)

                End If

            Loop

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subLoadCoopModSlot: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub



    'This routine scans all industrial assets to find assets that have suffered sufficient damage to be 
    'classed as destroyed. If any such assets are found, they are given an ActionData event.
    Private Sub subScanPartialIndustrialDamage(ByVal item As Integer, ByRef isValid As Boolean)
        Dim rsScanPartial As New ADODB.Recordset
        Dim strSql, strLocation As String
        Dim thisEnumerator As IDictionaryEnumerator
        Dim thisInstallation As SE_Installation


        Try
            'Establish the enumerator of the hashtable.
            thisEnumerator = hashInstallation.GetEnumerator()

            'Step through the entries in the hashtable, looking cumulative destruction instances.
            While thisEnumerator.MoveNext()

                'Get the current installation key-value pair.
                strLocation = thisEnumerator.Key
                thisInstallation = thisEnumerator.Value

                'If the cumulative damage has destroyed the installation, add a corresponding action event.
                If thisInstallation.Damage_Old + thisInstallation.Damage_New >= 100.0 And _
                   Len(thisInstallation.Antagonist) > 1 And _
                   thisInstallation.isDestroyed <> True Then

                    strSql = "INSERT INTO ActionData (Obj1, Obj1Group, Event, Obj2, Obj2Group, " _
                            & "Mission, Y_Axis, X_Axis, EventTime) VALUES ('" _
                            & modDB.strSQLquote("INF_" & strLocation) & "', '" _
                            & modDB.strSQLquote("Infrastructure") & "', 'destroyed by', '" _
                            & modDB.strSQLquote(thisInstallation.Antagonist) & "', '" _
                            & modDB.strSQLquote(thisInstallation.AntagonistGroup) & "', '" _
                            & basFiles.StrMissionName & "', '" _
                            & CInt(thisInstallation.Y) & "', '" _
                            & CInt(thisInstallation.X) & "', '" _
                            & thisInstallation.Clock & "')"
                    modDB.ExecuteDBCommand(item, strSql)

                End If

            End While

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subScanPartialIndustrialDamage: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub

    Private Function isHostFlight(ByVal strGroup As String) As Boolean
        Dim strHostFlight As String
        Dim strRadarFlight As String
        Try
            'pessimistic default
            isHostFlight = False

            'current defined host flight
            strHostFlight = frmMain.FlightFromAlignment(CS_Host_Alignment)

            'current defined auxiliary radar flight, if any
            If CS_Radar = 1 And InStr(CS_Host_Aircraft, "RadarControlChainHome") <= 0 Then
                strRadarFlight = Left(strHostFlight, strHostFlight.Length - 1) & "2"
            Else
                strRadarFlight = ""
            End If

            If strGroup = strHostFlight Or strGroup = strRadarFlight Then
                isHostFlight = True
            End If

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isHostFlight: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try

    End Function


    Private Function strLoadDesignatedScramblers(ByVal item As Integer) As String
        Dim strSql As String
        Dim rsDesignatedScramblers As New ADODB.Recordset
        Try
            strLoadDesignatedScramblers = "|"

            'Load all trigger directives.
            strSql = "SELECT * FROM Mission_Triggers WHERE Mission='" & basFiles.StrMissionName & "' ORDER BY Trigger_Name;"
            modDB.openDBRecordset(item, rsDesignatedScramblers, strSql)

            'Loop through directives assembling the list of designated scrambler units.
            While rsDesignatedScramblers.EOF = False
                strLoadDesignatedScramblers &= rsDesignatedScramblers("Scrambler").Value & "|"
                rsDesignatedScramblers.MoveNext()
            End While

            'Close the recordset.
            rsDesignatedScramblers.Close()


        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.strLoadDesignatedScramblers: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function

    Public Function isUntriggeredScrambler(ByVal strScrambler As String) As Boolean

        If InStr(strMissionDesignatedScramblers, "|" & strScrambler & "|") > 0 Then

            'This unit was a designated scrambler.
            If InStr(strMissionActuallyScrambled, strScrambler) > 0 Then
                'This unit was actually scrambled.
                isUntriggeredScrambler = False
            Else
                'this unit was not actually scrambled.
                isUntriggeredScrambler = True
            End If

        Else

            'This unit was not a designated scrambler.
            isUntriggeredScrambler = False

        End If

    End Function


    Private Sub subGatherACStarters(ByRef isValid As Boolean)
        Dim thisACStarter As SE_ACStarter
        Dim thisUnit As SE_Unit
        Dim strUnitName As String
        Dim thisEnumerator As IDictionaryEnumerator
        Try
            isValid = False

            'Step through all units defined in the hashUnit table and extract details for each AC_Starter.
            thisEnumerator = hashACStarter.GetEnumerator()

            While thisEnumerator.MoveNext()

                'Get the unit details.
                thisUnit = thisEnumerator.Value
                strUnitName = thisUnit.Object_Group_Name

                If thisUnit.Quantity > 0 And thisUnit.Transporting_Group = "Independent" And Left(thisUnit.Object_Type, 10) = "AC_Starter" Then

                    thisACStarter.Alignment = thisUnit.Nationality
                    thisACStarter.Name = thisUnit.Object_Group_Name
                    thisACStarter.X = thisUnit.X
                    thisACStarter.Y = thisUnit.Y
                    thisACStarter.Fuel = thisUnit.Fuel
                    thisACStarter.Type = thisUnit.Object_Type

                    If hashACStarter.ContainsKey(strUnitName) = False Then
                        hashUnit(strUnitName) = thisACStarter
                    Else
                        Call basMess.MessageBox("modMissionAnalyzer.subGatherACStarters: duplicate hashkey for " & strUnitName, vbInformation)
                        Exit Sub
                    End If
                End If

            End While

            isValid = True

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.subGatherACStarters: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Sub


    'This function determines whether there is a capable friendly AC_Starter unit within the control radius of the supplied location.
    'Because of the order of assembling the hashACStarter table (just after subLoadUnitHashtable) it is possible that an AC_Starter
    'was present at the beginning of the mission and was destroyed by mission end, meaning no effect should accrue. The following code will ignore that eventuality.
    Private Function isACStarterAvailable(ByVal strNationality As String, ByVal intX As Integer, ByVal intY As Integer) As Boolean
        Dim thisACStarter As SE_ACStarter
        Dim thisEnumerator As IDictionaryEnumerator
        Dim dblDistance As Double

        Try
            'Pessimistic default.
            isACStarterAvailable = False

            'Step through all units defined in the hashUnit table and extract details for each AC_Starter.
            thisEnumerator = hashACStarter.GetEnumerator()

            While thisEnumerator.MoveNext()

                thisACStarter = thisEnumerator.Value
                dblDistance = modUtilities.dblGetDistance(intX, intY, thisACStarter.X, thisACStarter.Y)

                'If the distance is less than the supplied control radius and the nationality matches then we are in business.
                If dblDistance <= CDbl(CS_Control_Radius) And thisACStarter.Alignment = strNationality Then
                    isACStarterAvailable = True
                    Exit Function
                End If

            End While

            'No match!

        Catch ex As Exception
            Call basMess.MessageBox("modMissionAnalyzer.isACStarterAvailable: " _
                & Err.Number & " " & Err.Description, vbCritical)
        End Try
    End Function
End Module


