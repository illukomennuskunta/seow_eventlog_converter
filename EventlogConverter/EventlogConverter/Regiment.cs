﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventlogConverter
{
    class Regiment
    {
        public string regimentCode { get; set; }
        public string planeType { get; set; }
        public string xCoordinate { get; set; }
        public string yCoordinate { get; set; }
        public int strength { get; set; }
        public int planesUsed { get; set; }

        public Regiment(string regimentCode, int strength, string planeType, string xCoordinate, string yCoordinate)
        {
            this.regimentCode = regimentCode;
            this.strength = strength;
            this.planeType = planeType;
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
        }
    }
}
