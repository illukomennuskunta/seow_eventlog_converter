﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventlogConverter
{
    class Converter
    {
        private String eventlogPath;
        private String processedEventlogPath;
        private String regimentsPath;
        private String aircraftNamesPath;
        private Dictionary<String, Regiment> regiments;
        private Dictionary<String, String> aircraft;
        private Dictionary<String, Pilot> pilots;
        private Dictionary<String, Pilot> pilotsByNameAndType;
        private int lineNumber;

        //static int Main(string[] args)
        //{
        //    if (args.Length < 4)
        //    {
        //        Console.WriteLine("INVALID NUMBER OF PARAMETERS");
        //        return 1;
        //    }

        //    //remove last log
        //    try
        //    {
        //        File.Delete("log.txt");
        //    }
        //    catch (Exception e) { }

        //    Converter converter = new Converter(args);
        //    return converter.convert();
        //}

        public Converter(string[] args)
        {
            Console.WriteLine(this.GetType().Name);

            eventlogPath = args[0];
            processedEventlogPath = args[1];
            regimentsPath = args[2];
            aircraftNamesPath = args[3];

            regiments = readRegiments();
            aircraft = readAircraft();
            pilots = new Dictionary<String, Pilot>();
            pilotsByNameAndType = new Dictionary<String, Pilot>();
        }

        private int convert()
        {
            if (eventlogPath == null || eventlogPath.Equals(""))
            {
                writeToLog("eventlogPath missing!", true);
                return 1;
            }
            if (processedEventlogPath == null || processedEventlogPath.Equals(""))
            {
                writeToLog("processedEventlogPath missing!", true);
                return 1;
            }
            if (regiments == null)
            {
                writeToLog("human_regiments invalid or missing!", true);
                return 1;
            }
            if (aircraft == null)
            {
                writeToLog("dogf_aircraftnames invalid or missing!", true);
                return 1;
            }

            try
            {
                List<String> newLines = new List<String>();
                List<String> lines = new List<String>(File.ReadAllLines(eventlogPath));

                for (int i = 0; i < lines.Count; i++)
                {
                    lineNumber = i;
                    try
                    {
                        String line = lines[i];
                        if (line.Contains("selected army") || line.Contains("removed at") || line.Contains("entered refly menu"))
                        {
                            //filter out
                            continue;
                        }
                        else if (line.Contains("in flight at"))
                        {
                            processFlight(lines, newLines, line);
                        }
                        else if (line.Contains("seat occupied by") || line.Contains("loaded weapons"))
                        {
                            newLines.Add(line);
                        }
                        else if (line.Contains("_0"))
                        {
                            explosiveOrMunitionLine(line, newLines);
                        }
                        else
                        {
                            replaceNameAndType(newLines, line);
                        }
                    }
                    catch (Exception ee)
                    {
                        writeToLog("error: " + ee.Message + "\nstacktrace:  " + ee.StackTrace, true);
                        return 1;
                    }
                }

                //filter out remaining "seat occupied by" and "loaded weapons" with "name:type"
                List<String> evenNewerLines = new List<String>();
                foreach (String line in newLines)
                {
                    int count = line.Split(':').Length - 1;
                    if (count >= 3 && (line.Contains("seat occupied by") || line.Contains("loaded weapons")))
                    {
                        continue;
                    }
                    else
                    {
                        evenNewerLines.Add(line);
                    }
                }


                //write newlines to a file
                File.WriteAllLines(processedEventlogPath, evenNewerLines);

                writeToLog("CONVERSION COMPLETE", true);
            }
            catch (Exception e)
            {
                writeToLog("convert error: " + e.Message + "\nstacktrace:  " + e.StackTrace, true);
                return 1;
            }

            return 0;
        }

        private void explosiveOrMunitionLine(String line, List<String> newLines)
        {
            foreach (String name in pilots.Keys)
            {
                String toReplace = name + "_0";
                if (line.Contains(toReplace))
                {
                    Pilot pilot = pilots[name];
                    String newLine = line.Replace(toReplace, pilot.regimentCode + pilot.regimentSubCode);
                    newLines.Add(newLine);
                    return;
                }
            }

            //not found
            newLines.Add(line);
        }

        private void replaceNameAndType(List<String> newLines, String line)
        {
            int replaced = 0;
            int count = line.Split(':').Length - 1;
            if (count >= 3)
            {
                List<String> keysToReplace = new List<String>();
                foreach (String key in pilotsByNameAndType.Keys)
                {
                    if (line.Contains(key))
                    {
                        keysToReplace.Add(key);
                        if (keysToReplace.Count >= 2)
                        {
                            break;
                        }
                    }
                }
                if (keysToReplace.Count > 0)
                {
                    String newLine = line;
                    foreach (String key in keysToReplace)
                    {
                        Pilot pilot = pilotsByNameAndType[key];
                        String replaceWith = pilot.regimentCode + pilot.regimentSubCode;
                        newLine = newLine.Replace(key, replaceWith);
                        replaced++;
                    }
                    newLines.Add(newLine);
                }
            }

            if (replaced == 0)
            {
                newLines.Add(line);
            }
        }

        private void processFlight(List<String> lines, List<String> newLines, String line)
        {
            String timestamp = line.Substring(0, line.IndexOf(" "));
            String afterSpace = line.Substring(line.IndexOf(" ") + 1);
            String nameAndType = afterSpace.Substring(0, afterSpace.IndexOf(" ", afterSpace.IndexOf(":")));

            String[] nameAndTypeParts = nameAndType.Split(':');
            String name = nameAndTypeParts[0];
            String type = nameAndTypeParts[1];

            String atString = " in flight at ";
            int start = afterSpace.LastIndexOf(atString) + atString.Length;
            String coordinates = afterSpace.Substring(start);
            String[] parts = coordinates.Split(' ');
            String xCoordinate = parts[0];
            String yCoordinate = parts[1];

            String xCoordinateWithoutDecimals = xCoordinate.Substring(0, xCoordinate.IndexOf("."));
            String yCoordinateWithoutDecimals = yCoordinate.Substring(0, yCoordinate.IndexOf("."));

            Regiment regiment = getRegimentByTypeAndCoordinates(type, xCoordinateWithoutDecimals, yCoordinateWithoutDecimals);
            if (regiment == null)
            {
                writeToLog("Regiment not found for type: " + type + " and coordinates, X: " + xCoordinate + " Y: " + yCoordinate, true);
                newLines.Add(line);
                return;
            }

            Pilot pilot = null;

            if (pilots.ContainsKey(name))
            {
                pilot = pilots[name];
            }

            if (pilot == null)
            {
                //first sortie
                pilot = new Pilot();
                pilot.name = name;
                pilot.regimentCode = regiment.regimentCode;
                pilot.sortie = 1;
                pilots.Add(name, pilot);
            }
            else
            {
                //increment sortie number
                pilot.regimentCode = regiment.regimentCode;
                pilot.sortie++;
            }
            if (!pilotsByNameAndType.ContainsKey(nameAndType))
            {
                pilotsByNameAndType.Add(nameAndType, pilot);
            }
            pilot.regimentSubCode = regiment.planesUsed;

            //replace "in flight at" line
            String inFlight = timestamp + " " + regiment.regimentCode + regiment.planesUsed + " in flight at " + xCoordinate + " " + yCoordinate;
            newLines.Add(inFlight);

            //search name and type above, until "seat occupied by" reached
            for (int i = newLines.Count - 1; i >= 0; i--)
            {
                String searchLine = newLines[i];
                if (searchLine.Contains("seat occupied by") && searchLine.Contains(nameAndType))
                {
                    timestamp = searchLine.Substring(0, searchLine.IndexOf(" "));
                    String seatOccupied = timestamp + " " + regiment.regimentCode + regiment.planesUsed + "(0) seat occupied by " + name + "_Sortie" + pilot.sortie + " at " + regiment.xCoordinate + " " + regiment.yCoordinate;
                    newLines[i] = seatOccupied;
                    break;
                }
                else if (searchLine.Contains(nameAndType))
                {
                    String replaceWith = regiment.regimentCode + regiment.planesUsed;
                    String newLine = searchLine.Replace(nameAndType, replaceWith);
                    newLines[i] = newLine;
                }
            }

            //increment planes used by regiment
            regiment.planesUsed++;
        }

        private Regiment getRegimentByTypeAndCoordinates(string type, string xCoordinate, string yCoordinate)
        {
            Regiment regiment = null;
            int closest = int.MaxValue;
            foreach (Regiment reg in regiments.Values)
            {
                if (reg.planesUsed >= reg.strength)
                {
                    writeToLog("regiment out of planes: " + reg.regimentCode + " strength: " + reg.strength + " used: " + reg.planesUsed);
                    continue;
                }
                if (reg.planeType.Equals(aircraft[type]))
                {
                    writeToLog("regiment found: " + reg.regimentCode + " strength: " + reg.strength + " used: " + reg.planesUsed);
                    int distance = getDistanceBetweenCoordinates(reg.xCoordinate, reg.yCoordinate, xCoordinate, yCoordinate);
                    if (distance < closest)
                    {
                        regiment = reg;
                        closest = distance;
                    }
                }
            }

            return regiment;
        }

        private int getDistanceBetweenCoordinates(String firstXCoord, String firstYCoord, String secondXCoord, String secondYCoord)
        {
            int firstX = int.Parse(firstXCoord);
            int firstY = int.Parse(firstYCoord);
            int secondX = int.Parse(secondXCoord);
            int secondY = int.Parse(secondYCoord);

            int xAbs = Math.Abs(firstX - secondX);
            int yAbs = Math.Abs(secondX - secondY);

            return (int)Math.Sqrt(xAbs * xAbs + yAbs * yAbs);
        }

        private Dictionary<String, Regiment> readRegiments()
        {
            try
            {
                Dictionary<String, Regiment> regiments = new Dictionary<String, Regiment>();
                List<string> lines = new List<string>(File.ReadAllLines(regimentsPath));

                foreach (String line in lines)
                {
                    String[] parts = line.Split(',');
                    String regimentCode = parts[0];
                    int strength = int.Parse(parts[1]);
                    String planeType = parts[2];
                    String xCoordinate = parts[3];
                    String yCoordinate = parts[4];

                    if (regiments.ContainsKey(regimentCode))
                    {
                        continue;
                    }

                    if (xCoordinate.Contains("."))
                    {
                        xCoordinate = xCoordinate.Substring(0, xCoordinate.IndexOf("."));
                    }
                    if (yCoordinate.Contains("."))
                    {
                        yCoordinate = yCoordinate.Substring(0, yCoordinate.IndexOf("."));
                    }

                    writeToLog("Regiment created: " + regimentCode + " strength: " + strength + " planeType: " + planeType + " xCoordinate: " + xCoordinate + " yCoordinate: " + yCoordinate);
                    Regiment regiment = new Regiment(regimentCode, strength, planeType, xCoordinate, yCoordinate);
                    regiments.Add(regimentCode, regiment);
                }

                return regiments;
            }
            catch (Exception e)
            {
                writeToLog("readRegiments error: " + e.Message + "\nstacktrace:  " + e.StackTrace, true);
                return null;
            }
        }

        private Dictionary<String, String> readAircraft()
        {
            try
            {
                Dictionary<String, String> aircraftNames = new Dictionary<String, String>();
                List<string> lines = new List<string>(File.ReadAllLines(aircraftNamesPath));

                foreach (String line in lines)
                {
                    String[] parts = line.Split(',');
                    String dogfightName = parts[0];
                    String dbName = parts[1];

                    aircraftNames.Add(dogfightName, dbName);
                }

                return aircraftNames;
            }
            catch (Exception e)
            {
                writeToLog("readAircraft error: " + e.Message + "\nstacktrace:  " + e.StackTrace, true);
                return null;
            }
        }

        private void writeToLog(String text)
        {
            writeToLog(text, false);
        }

        private void writeToLog(String text, bool printOnConsole)
        {
            try
            {
                if (printOnConsole)
                {
                    Console.WriteLine(text);
                }
                //String timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                StreamWriter sw = File.AppendText("log.txt");
                sw.WriteLine(lineNumber + ": " + text);
                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
            }
        }
    }
}
