﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventlogConverter
{
    class Pilot
    {
        public string name { get; set; }
        public string regimentCode { get; set; }
        public int regimentSubCode { get; set; }
        public int sortie { get; set; }
        public bool waitingForFlightStart { get; set; }
    }
}
