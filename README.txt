Usage of EventlogConverter.exe

parameters:
1. dogfight eventlog path
2. resulting eventlog path
3. objmissiondata table dump path (file in CSV format, format example below)
4. dogf_aircraftnames table dump path (file in CSV format, format example below)
5. (optional) if you give paramater "no-occupied", converter will not add "occupied by" lines 

File paths are absolute paths.

Example execution:
EventlogConverter.exe C:\folder\eventlog.lst C:\folder\processed_eventlog.lst C:\folder\objmissiondata.csv C:\folder\dogf_aircraftnames.csv


Db dumps must be in pure CSV (comma separated values)

objmissiondata table columns: [regiment code, plane type, X-coordinate, Y-coordinate], example line:
1701sq00,F6F5,334518.84,163144.86

dogf_aircraftnames table columns: [dogfight name, db name], example line:
F6F-5,F6F5